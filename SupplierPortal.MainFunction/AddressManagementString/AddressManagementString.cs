﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.MainFunction.SupportModel;

namespace SupplierPortal.MainFunction.AddressManagementString
{
    public static class AddressManagementString
    {
        public static string GetGenAddress(AddressModel oldAddress, AddressModel newAddress, out string oldStringAddress)
        {
            oldStringAddress = "";
            string newStringAddress = "";
            Type modelType = (typeof(AddressModel));
            var modelProperties = modelType.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (var item in modelProperties)
            {
                string dataMember = item.Name;
                string strOld = oldAddress.GetType().GetProperty(dataMember).GetValue(oldAddress).ToString();
                string strNew = newAddress.GetType().GetProperty(dataMember).GetValue(newAddress).ToString();

                if (strOld.Trim() != "")
                {
                    // oldStringAddress เวลาเอาไปโชว์ไม่ต้อง split
                    oldStringAddress += strOld + " ";
                }
                if (!string.IsNullOrEmpty(strNew) || !string.IsNullOrEmpty(strOld))
                {
                    if (strOld.Trim() == strNew.Trim())
                    {
                        //ถ้าค่าเท่ากันแสดงว่าไม่มีการเปลี่ยนแปลง
                        newStringAddress += strNew + '|';
                    }
                    else
                    {
                        //แสดงว่ามีการเปลี่ยนแปลงค่า
                        if (!string.IsNullOrEmpty(strOld.Trim()) && string.IsNullOrEmpty(strNew.Trim()))
                        {
                            //กรณีที่ค่าเก่ามีค่า แล้วค่าใหม่ไม่มีค่า จะแสดงเป็นค่าเก่าแล้วมีขีดกลางคั่น(ใช้สัญลักษ์ & นำหน้า)
                            newStringAddress += '&' + strOld + '|';
                        }
                        else
                        {
                            //กรณีที่มีการเปลี่ยนแปลงค่า จะแสดงเป็น ตัวหนังสือสีแดง(ใช้สัญลักษ์ # นำหน้า)
                            newStringAddress += '#' + strNew + '|';
                        }
                    }
                }
                
              
            }

            return newStringAddress;

        }

        public static AddressModel GetPrefix(AddressModel prefix)
        {
            //  prefix.road 
            // bool chk = false;
            if (prefix.State.Replace(".", "").Replace(".", "").Trim().StartsWith("กรุงเทพ") || prefix.State.Replace(".", "").Replace(".", "").Trim().StartsWith("กทม"))
            {
                //  chk = true;
                prefix.SubDistrict = string.IsNullOrEmpty(prefix.SubDistrict) ? "" : "แขวง" + prefix.SubDistrict;
                prefix.City = string.IsNullOrEmpty(prefix.City) ? "" : "เขต" + prefix.City;
            }
            else
            {
                prefix.SubDistrict = string.IsNullOrEmpty(prefix.SubDistrict) ? "" : "ตำบล" + prefix.SubDistrict;
                prefix.City = string.IsNullOrEmpty(prefix.City) ? "" : "อำเภอ" + prefix.City;

            }
            prefix.Road = !string.IsNullOrEmpty(prefix.Road) ? "ถนน" + prefix.Road : "";
            prefix.Lane = !string.IsNullOrEmpty(prefix.Lane) ? "ซอย" + prefix.Lane : "";
            prefix.VillageNo = string.IsNullOrEmpty(prefix.VillageNo) ? "" : "หมู่ " + prefix.VillageNo;

            return prefix;
        }

    }

    //public class AddressModel
    //{
    //    public string HouseNo { get; set; }
    //    public string VillageNo { get; set; }
    //    public string Lane { get; set; }
    //    public string Road { get; set; }
    //    public string SubDistrict { get; set; }
    //    public string City { get; set; }
    //    public string State { get; set; }
    //    public string PostalCode { get; set; }
    //    public string CountryName { get; set; }
    //}

    public class PrefixModel
    {
        public string road { get; set; }
        public string lane { get; set; }
        public string subdistrict { get; set; }
        public string city { get; set; }
        public string state { get; set; }


    }
}

﻿using SupplierPortal.MainFunction.SupportModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.MainFunction.Address
{
    public static class CompoundStringManage
    {
        public static string CompoundAddressTracking(IList<CompoundAddressModel> trackingField)
        {

            string oldAddress = "";
            string newAddress = "";

            #region Compound Address Field

            #region Address Local

            #region HouseNo_Local
            var houseNo_Local = trackingField.Where(m => m.TrackingKey == "HouseNo_Local").FirstOrDefault();
            if (houseNo_Local != null)
            {
                oldAddress += houseNo_Local.OldKeyValue + " ";
                if (houseNo_Local.OldKeyValue == houseNo_Local.NewKeyValue)
                {
                    //ถ้าค่าเท่ากันแสดงว่าไม่มีการเปลี่ยนแปลง
                    newAddress += houseNo_Local.NewKeyValue + "|";
                }
                else
                {
                    //แสดงว่ามีการเปลี่ยนแปลงค่า
                    if (!string.IsNullOrEmpty(houseNo_Local.OldKeyValue.Trim()) && string.IsNullOrEmpty(houseNo_Local.NewKeyValue.Trim()))
                    {
                        //กรณีที่ค่าเก่ามีค่า แล้วค่าใหม่ไม่มีค่า จะแสดงเป็น ขีดกลางคั่น(ใช้สัญลักษ์ & นำหน้า)
                        newAddress += "&" + houseNo_Local.OldKeyValue + "|";
                    }
                    else
                    {
                        //กรณีที่มีการเปลี่ยนแปลงค่า จะแสดงเป็น ตัวหนังสือสีแดง(ใช้สัญลักษ์ # นำหน้า)
                        newAddress += "#" + houseNo_Local.NewKeyValue + "|";
                    }

                }
            }
            #endregion

            #region VillageNo_Local
            var villageNo_Local = trackingField.Where(m => m.TrackingKey == "VillageNo_Local").FirstOrDefault();
            if (villageNo_Local != null)
            {
                oldAddress += villageNo_Local.OldKeyValue + " ";
                if (villageNo_Local.OldKeyValue == villageNo_Local.NewKeyValue)
                {
                    //ถ้าค่าเท่ากันแสดงว่าไม่มีการเปลี่ยนแปลง
                    newAddress += villageNo_Local.NewKeyValue + "|";
                }
                else
                {
                    //แสดงว่ามีการเปลี่ยนแปลงค่า
                    if (!string.IsNullOrEmpty(villageNo_Local.OldKeyValue.Trim()) && string.IsNullOrEmpty(villageNo_Local.NewKeyValue.Trim()))
                    {
                        //กรณีที่ค่าเก่ามีค่า แล้วค่าใหม่ไม่มีค่า จะแสดงเป็น ขีดกลางคั่น(ใช้สัญลักษ์ & นำหน้า)
                        newAddress += "&" + villageNo_Local.OldKeyValue + "|";
                    }
                    else
                    {
                        //กรณีที่มีการเปลี่ยนแปลงค่า จะแสดงเป็น ตัวหนังสือสีแดง(ใช้สัญลักษ์ # นำหน้า)
                        newAddress += "#" + villageNo_Local.NewKeyValue + "|";
                    }

                }
            }
            #endregion

            #region Lane_Local
            var lane_Local = trackingField.Where(m => m.TrackingKey == "Lane_Local").FirstOrDefault();
            if (lane_Local != null)
            {
                oldAddress += lane_Local.OldKeyValue + " ";
                if (lane_Local.OldKeyValue == lane_Local.NewKeyValue)
                {
                    //ถ้าค่าเท่ากันแสดงว่าไม่มีการเปลี่ยนแปลง
                    newAddress += lane_Local.NewKeyValue + "|";
                }
                else
                {
                    //แสดงว่ามีการเปลี่ยนแปลงค่า
                    if (!string.IsNullOrEmpty(lane_Local.OldKeyValue.Trim()) && string.IsNullOrEmpty(lane_Local.NewKeyValue.Trim()))
                    {
                        //กรณีที่ค่าเก่ามีค่า แล้วค่าใหม่ไม่มีค่า จะแสดงเป็น ขีดกลางคั่น(ใช้สัญลักษ์ & นำหน้า)
                        newAddress += "&" + lane_Local.OldKeyValue + "|";
                    }
                    else
                    {
                        //กรณีที่มีการเปลี่ยนแปลงค่า จะแสดงเป็น ตัวหนังสือสีแดง(ใช้สัญลักษ์ # นำหน้า)
                        newAddress += "#" + lane_Local.NewKeyValue + "|";
                    }

                }
            }
            #endregion

            #region Road_Local
            var road_Local = trackingField.Where(m => m.TrackingKey == "Road_Local").FirstOrDefault();
            if (road_Local != null)
            {
                oldAddress += road_Local.OldKeyValue + " ";
                if (road_Local.OldKeyValue == road_Local.NewKeyValue)
                {
                    //ถ้าค่าเท่ากันแสดงว่าไม่มีการเปลี่ยนแปลง
                    newAddress += road_Local.NewKeyValue + "|";
                }
                else
                {
                    //แสดงว่ามีการเปลี่ยนแปลงค่า
                    if (!string.IsNullOrEmpty(road_Local.OldKeyValue.Trim()) && string.IsNullOrEmpty(road_Local.NewKeyValue.Trim()))
                    {
                        //กรณีที่ค่าเก่ามีค่า แล้วค่าใหม่ไม่มีค่า จะแสดงเป็น ขีดกลางคั่น(ใช้สัญลักษ์ & นำหน้า)
                        newAddress += "&" + road_Local.OldKeyValue + "|";
                    }
                    else
                    {
                        //กรณีที่มีการเปลี่ยนแปลงค่า จะแสดงเป็น ตัวหนังสือสีแดง(ใช้สัญลักษ์ # นำหน้า)
                        newAddress += "#" + road_Local.NewKeyValue + "|";
                    }

                }
            }
            #endregion

            #region SubDistrict_Local
            var subDistrict_Local = trackingField.Where(m => m.TrackingKey == "SubDistrict_Local").FirstOrDefault();
            if (subDistrict_Local != null)
            {
                oldAddress += subDistrict_Local.OldKeyValue + " ";
                if (subDistrict_Local.OldKeyValue == subDistrict_Local.NewKeyValue)
                {
                    //ถ้าค่าเท่ากันแสดงว่าไม่มีการเปลี่ยนแปลง
                    newAddress += subDistrict_Local.NewKeyValue + "|";
                }
                else
                {
                    //แสดงว่ามีการเปลี่ยนแปลงค่า
                    if (!string.IsNullOrEmpty(subDistrict_Local.OldKeyValue.Trim()) && string.IsNullOrEmpty(subDistrict_Local.NewKeyValue.Trim()))
                    {
                        //กรณีที่ค่าเก่ามีค่า แล้วค่าใหม่ไม่มีค่า จะแสดงเป็น ขีดกลางคั่น(ใช้สัญลักษ์ & นำหน้า)
                        newAddress += "&" + subDistrict_Local.OldKeyValue + "|";
                    }
                    else
                    {
                        //กรณีที่มีการเปลี่ยนแปลงค่า จะแสดงเป็น ตัวหนังสือสีแดง(ใช้สัญลักษ์ # นำหน้า)
                        newAddress += "#" + subDistrict_Local.NewKeyValue + "|";
                    }

                }
            }
            #endregion

            #region City_Local
            var city_Local = trackingField.Where(m => m.TrackingKey == "City_Local").FirstOrDefault();
            if (city_Local != null)
            {
                oldAddress += city_Local.OldKeyValue + " ";
                if (city_Local.OldKeyValue == city_Local.NewKeyValue)
                {
                    //ถ้าค่าเท่ากันแสดงว่าไม่มีการเปลี่ยนแปลง
                    newAddress += city_Local.NewKeyValue + "|";
                }
                else
                {
                    //แสดงว่ามีการเปลี่ยนแปลงค่า
                    if (!string.IsNullOrEmpty(city_Local.OldKeyValue.Trim()) && string.IsNullOrEmpty(city_Local.NewKeyValue.Trim()))
                    {
                        //กรณีที่ค่าเก่ามีค่า แล้วค่าใหม่ไม่มีค่า จะแสดงเป็น ขีดกลางคั่น(ใช้สัญลักษ์ & นำหน้า)
                        newAddress += "&" + city_Local.OldKeyValue + "|";
                    }
                    else
                    {
                        //กรณีที่มีการเปลี่ยนแปลงค่า จะแสดงเป็น ตัวหนังสือสีแดง(ใช้สัญลักษ์ # นำหน้า)
                        newAddress += "#" + city_Local.NewKeyValue + "|";
                    }

                }
            }
            #endregion

            #region State_Local
            var state_Local = trackingField.Where(m => m.TrackingKey == "State_Local").FirstOrDefault();
            if (state_Local != null)
            {
                oldAddress += state_Local.OldKeyValue + " ";
                if (state_Local.OldKeyValue == state_Local.NewKeyValue)
                {
                    //ถ้าค่าเท่ากันแสดงว่าไม่มีการเปลี่ยนแปลง
                    newAddress += state_Local.NewKeyValue + "|";
                }
                else
                {
                    //แสดงว่ามีการเปลี่ยนแปลงค่า
                    if (!string.IsNullOrEmpty(state_Local.OldKeyValue.Trim()) && string.IsNullOrEmpty(state_Local.NewKeyValue.Trim()))
                    {
                        //กรณีที่ค่าเก่ามีค่า แล้วค่าใหม่ไม่มีค่า จะแสดงเป็น ขีดกลางคั่น(ใช้สัญลักษ์ & นำหน้า)
                        newAddress += "&" + state_Local.OldKeyValue + "|";
                    }
                    else
                    {
                        //กรณีที่มีการเปลี่ยนแปลงค่า จะแสดงเป็น ตัวหนังสือสีแดง(ใช้สัญลักษ์ # นำหน้า)
                        newAddress += "#" + state_Local.NewKeyValue + "|";
                    }

                }
            }
            #endregion

            #region PostalCode
            var postalCode = trackingField.Where(m => m.TrackingKey == "PostalCode").FirstOrDefault();
            if (postalCode != null)
            {
                oldAddress += postalCode.OldKeyValue + " ";
                if (postalCode.OldKeyValue == postalCode.NewKeyValue)
                {
                    //ถ้าค่าเท่ากันแสดงว่าไม่มีการเปลี่ยนแปลง
                    newAddress += postalCode.NewKeyValue + "|";
                }
                else
                {
                    //แสดงว่ามีการเปลี่ยนแปลงค่า
                    if (!string.IsNullOrEmpty(postalCode.OldKeyValue.Trim()) && string.IsNullOrEmpty(postalCode.NewKeyValue.Trim()))
                    {
                        //กรณีที่ค่าเก่ามีค่า แล้วค่าใหม่ไม่มีค่า จะแสดงเป็น ขีดกลางคั่น(ใช้สัญลักษ์ & นำหน้า)
                        newAddress += "&" + postalCode.OldKeyValue + "|";
                    }
                    else
                    {
                        //กรณีที่มีการเปลี่ยนแปลงค่า จะแสดงเป็น ตัวหนังสือสีแดง(ใช้สัญลักษ์ # นำหน้า)
                        newAddress += "#" + postalCode.NewKeyValue + "|";
                    }

                }
            }
            #endregion

            #region CountryCode
            var countryCode = trackingField.Where(m => m.TrackingKey == "CountryCode").FirstOrDefault();
            if (countryCode != null)
            {

                oldAddress += countryCode.OldKeyValue;
                if (countryCode.OldKeyValue == countryCode.NewKeyValue)
                {
                    //ถ้าค่าเท่ากันแสดงว่าไม่มีการเปลี่ยนแปลง
                    newAddress += countryCode.NewKeyValue + "|";
                }
                else
                {
                    //แสดงว่ามีการเปลี่ยนแปลงค่า
                    if (!string.IsNullOrEmpty(countryCode.OldKeyValue.Trim()) && string.IsNullOrEmpty(countryCode.NewKeyValue.Trim()))
                    {
                        //กรณีที่ค่าเก่ามีค่า แล้วค่าใหม่ไม่มีค่า จะแสดงเป็น ขีดกลางคั่น(ใช้สัญลักษ์ & นำหน้า)
                        newAddress += "&" + countryCode.OldKeyValue + "|";
                    }
                    else
                    {
                        //กรณีที่มีการเปลี่ยนแปลงค่า จะแสดงเป็น ตัวหนังสือสีแดง(ใช้สัญลักษ์ # นำหน้า)
                        newAddress += "#" + countryCode.NewKeyValue + "|";
                    }

                }
            }
            #endregion

            #endregion

            #region Address Inter

            #region HouseNo_Inter
            var houseNo_Inter = trackingField.Where(m => m.TrackingKey == "HouseNo_Inter").FirstOrDefault();
            if (houseNo_Inter != null)
            {
                oldAddress += houseNo_Inter.OldKeyValue + " ";
                if (houseNo_Inter.OldKeyValue == houseNo_Inter.NewKeyValue)
                {
                    //ถ้าค่าเท่ากันแสดงว่าไม่มีการเปลี่ยนแปลง
                    newAddress += houseNo_Inter.NewKeyValue + "|";
                }
                else
                {
                    //แสดงว่ามีการเปลี่ยนแปลงค่า
                    if (!string.IsNullOrEmpty(houseNo_Inter.OldKeyValue.Trim()) && string.IsNullOrEmpty(houseNo_Inter.NewKeyValue.Trim()))
                    {
                        //กรณีที่ค่าเก่ามีค่า แล้วค่าใหม่ไม่มีค่า จะแสดงเป็น ขีดกลางคั่น(ใช้สัญลักษ์ & นำหน้า)
                        newAddress += "&" + houseNo_Inter.OldKeyValue + "|";
                    }
                    else
                    {
                        //กรณีที่มีการเปลี่ยนแปลงค่า จะแสดงเป็น ตัวหนังสือสีแดง(ใช้สัญลักษ์ # นำหน้า)
                        newAddress += "#" + houseNo_Inter.NewKeyValue + "|";
                    }

                }
            }
            #endregion

            #region VillageNo_Inter
            var villageNo_Inter = trackingField.Where(m => m.TrackingKey == "VillageNo_Inter").FirstOrDefault();
            if (villageNo_Inter != null)
            {
                oldAddress += villageNo_Inter.OldKeyValue + " ";
                if (villageNo_Inter.OldKeyValue == villageNo_Inter.NewKeyValue)
                {
                    //ถ้าค่าเท่ากันแสดงว่าไม่มีการเปลี่ยนแปลง
                    newAddress += villageNo_Inter.NewKeyValue + "|";
                }
                else
                {
                    //แสดงว่ามีการเปลี่ยนแปลงค่า
                    if (!string.IsNullOrEmpty(villageNo_Inter.OldKeyValue.Trim()) && string.IsNullOrEmpty(villageNo_Inter.NewKeyValue.Trim()))
                    {
                        //กรณีที่ค่าเก่ามีค่า แล้วค่าใหม่ไม่มีค่า จะแสดงเป็น ขีดกลางคั่น(ใช้สัญลักษ์ & นำหน้า)
                        newAddress += "&" + villageNo_Inter.OldKeyValue + "|";
                    }
                    else
                    {
                        //กรณีที่มีการเปลี่ยนแปลงค่า จะแสดงเป็น ตัวหนังสือสีแดง(ใช้สัญลักษ์ # นำหน้า)
                        newAddress += "#" + villageNo_Inter.NewKeyValue + "|";
                    }

                }
            }
            #endregion

            #region Lane_Inter
            var lane_Inter = trackingField.Where(m => m.TrackingKey == "Lane_Inter").FirstOrDefault();
            if (lane_Inter != null)
            {
                oldAddress += lane_Inter.OldKeyValue + " ";
                if (lane_Inter.OldKeyValue == lane_Inter.NewKeyValue)
                {
                    //ถ้าค่าเท่ากันแสดงว่าไม่มีการเปลี่ยนแปลง
                    newAddress += lane_Inter.NewKeyValue + "|";
                }
                else
                {
                    //แสดงว่ามีการเปลี่ยนแปลงค่า
                    if (!string.IsNullOrEmpty(lane_Inter.OldKeyValue.Trim()) && string.IsNullOrEmpty(lane_Inter.NewKeyValue.Trim()))
                    {
                        //กรณีที่ค่าเก่ามีค่า แล้วค่าใหม่ไม่มีค่า จะแสดงเป็น ขีดกลางคั่น(ใช้สัญลักษ์ & นำหน้า)
                        newAddress += "&" + lane_Inter.OldKeyValue + "|";
                    }
                    else
                    {
                        //กรณีที่มีการเปลี่ยนแปลงค่า จะแสดงเป็น ตัวหนังสือสีแดง(ใช้สัญลักษ์ # นำหน้า)
                        newAddress += "#" + lane_Inter.NewKeyValue + "|";
                    }

                }
            }
            #endregion

            #region Road_Inter
            var road_Inter = trackingField.Where(m => m.TrackingKey == "Road_Inter").FirstOrDefault();
            if (road_Inter != null)
            {
                oldAddress += road_Inter.OldKeyValue + " ";
                if (road_Inter.OldKeyValue == road_Inter.NewKeyValue)
                {
                    //ถ้าค่าเท่ากันแสดงว่าไม่มีการเปลี่ยนแปลง
                    newAddress += road_Inter.NewKeyValue + "|";
                }
                else
                {
                    //แสดงว่ามีการเปลี่ยนแปลงค่า
                    if (!string.IsNullOrEmpty(road_Inter.OldKeyValue.Trim()) && string.IsNullOrEmpty(road_Inter.NewKeyValue.Trim()))
                    {
                        //กรณีที่ค่าเก่ามีค่า แล้วค่าใหม่ไม่มีค่า จะแสดงเป็น ขีดกลางคั่น(ใช้สัญลักษ์ & นำหน้า)
                        newAddress += "&" + road_Inter.OldKeyValue + "|";
                    }
                    else
                    {
                        //กรณีที่มีการเปลี่ยนแปลงค่า จะแสดงเป็น ตัวหนังสือสีแดง(ใช้สัญลักษ์ # นำหน้า)
                        newAddress += "#" + road_Inter.NewKeyValue + "|";
                    }

                }
            }
            #endregion

            #region SubDistrict_Inter
            var subDistrict_Inter = trackingField.Where(m => m.TrackingKey == "SubDistrict_Inter").FirstOrDefault();
            if (subDistrict_Inter != null)
            {
                oldAddress += subDistrict_Inter.OldKeyValue + " ";
                if (subDistrict_Inter.OldKeyValue == subDistrict_Inter.NewKeyValue)
                {
                    //ถ้าค่าเท่ากันแสดงว่าไม่มีการเปลี่ยนแปลง
                    newAddress += subDistrict_Inter.NewKeyValue + "|";
                }
                else
                {
                    //แสดงว่ามีการเปลี่ยนแปลงค่า
                    if (!string.IsNullOrEmpty(subDistrict_Inter.OldKeyValue.Trim()) && string.IsNullOrEmpty(subDistrict_Inter.NewKeyValue.Trim()))
                    {
                        //กรณีที่ค่าเก่ามีค่า แล้วค่าใหม่ไม่มีค่า จะแสดงเป็น ขีดกลางคั่น(ใช้สัญลักษ์ & นำหน้า)
                        newAddress += "&" + subDistrict_Inter.OldKeyValue + "|";
                    }
                    else
                    {
                        //กรณีที่มีการเปลี่ยนแปลงค่า จะแสดงเป็น ตัวหนังสือสีแดง(ใช้สัญลักษ์ # นำหน้า)
                        newAddress += "#" + subDistrict_Inter.NewKeyValue + "|";
                    }

                }
            }
            #endregion

            #region City_Inter
            var city_Inter = trackingField.Where(m => m.TrackingKey == "City_Inter").FirstOrDefault();
            if (city_Inter != null)
            {
                oldAddress += city_Inter.OldKeyValue + " ";
                if (city_Inter.OldKeyValue == city_Inter.NewKeyValue)
                {
                    //ถ้าค่าเท่ากันแสดงว่าไม่มีการเปลี่ยนแปลง
                    newAddress += city_Inter.NewKeyValue + "|";
                }
                else
                {
                    //แสดงว่ามีการเปลี่ยนแปลงค่า
                    if (!string.IsNullOrEmpty(city_Inter.OldKeyValue.Trim()) && string.IsNullOrEmpty(city_Inter.NewKeyValue.Trim()))
                    {
                        //กรณีที่ค่าเก่ามีค่า แล้วค่าใหม่ไม่มีค่า จะแสดงเป็น ขีดกลางคั่น(ใช้สัญลักษ์ & นำหน้า)
                        newAddress += "&" + city_Inter.OldKeyValue + "|";
                    }
                    else
                    {
                        //กรณีที่มีการเปลี่ยนแปลงค่า จะแสดงเป็น ตัวหนังสือสีแดง(ใช้สัญลักษ์ # นำหน้า)
                        newAddress += "#" + city_Inter.NewKeyValue + "|";
                    }

                }
            }
            #endregion

            #region State_Inter
            var state_Inter = trackingField.Where(m => m.TrackingKey == "State_Inter").FirstOrDefault();
            if (state_Inter != null)
            {
                oldAddress += state_Inter.OldKeyValue + " ";
                if (state_Inter.OldKeyValue == state_Inter.NewKeyValue)
                {
                    //ถ้าค่าเท่ากันแสดงว่าไม่มีการเปลี่ยนแปลง
                    newAddress += state_Inter.NewKeyValue + "|";
                }
                else
                {
                    //แสดงว่ามีการเปลี่ยนแปลงค่า
                    if (!string.IsNullOrEmpty(state_Inter.OldKeyValue.Trim()) && string.IsNullOrEmpty(state_Inter.NewKeyValue.Trim()))
                    {
                        //กรณีที่ค่าเก่ามีค่า แล้วค่าใหม่ไม่มีค่า จะแสดงเป็น ขีดกลางคั่น(ใช้สัญลักษ์ & นำหน้า)
                        newAddress += "&" + state_Inter.OldKeyValue + "|";
                    }
                    else
                    {
                        //กรณีที่มีการเปลี่ยนแปลงค่า จะแสดงเป็น ตัวหนังสือสีแดง(ใช้สัญลักษ์ # นำหน้า)
                        newAddress += "#" + state_Inter.NewKeyValue + "|";
                    }

                }
            }
            #endregion

            #region PostalCode
            //var postalCode = trackingField.Where(m => m.TrackingKey == "PostalCode").FirstOrDefault();
            if (postalCode != null)
            {
                oldAddress += postalCode.OldKeyValue + " ";
                if (postalCode.OldKeyValue == postalCode.NewKeyValue)
                {
                    //ถ้าค่าเท่ากันแสดงว่าไม่มีการเปลี่ยนแปลง
                    newAddress += postalCode.NewKeyValue + "|";
                }
                else
                {
                    //แสดงว่ามีการเปลี่ยนแปลงค่า
                    if (!string.IsNullOrEmpty(postalCode.OldKeyValue.Trim()) && string.IsNullOrEmpty(postalCode.NewKeyValue.Trim()))
                    {
                        //กรณีที่ค่าเก่ามีค่า แล้วค่าใหม่ไม่มีค่า จะแสดงเป็น ขีดกลางคั่น(ใช้สัญลักษ์ & นำหน้า)
                        newAddress += "&" + postalCode.OldKeyValue + "|";
                    }
                    else
                    {
                        //กรณีที่มีการเปลี่ยนแปลงค่า จะแสดงเป็น ตัวหนังสือสีแดง(ใช้สัญลักษ์ # นำหน้า)
                        newAddress += "#" + postalCode.NewKeyValue + "|";
                    }

                }
            }
            #endregion

            #region CountryCode
            //var countryCode = trackingField.Where(m => m.TrackingKey == "CountryCode").FirstOrDefault();
            if (countryCode != null)
            {

                oldAddress += countryCode.OldKeyValue;
                if (countryCode.OldKeyValue == countryCode.NewKeyValue)
                {
                    //ถ้าค่าเท่ากันแสดงว่าไม่มีการเปลี่ยนแปลง
                    newAddress += countryCode.NewKeyValue + "|";
                }
                else
                {
                    //แสดงว่ามีการเปลี่ยนแปลงค่า
                    if (!string.IsNullOrEmpty(countryCode.OldKeyValue.Trim()) && string.IsNullOrEmpty(countryCode.NewKeyValue.Trim()))
                    {
                        //กรณีที่ค่าเก่ามีค่า แล้วค่าใหม่ไม่มีค่า จะแสดงเป็น ขีดกลางคั่น(ใช้สัญลักษ์ & นำหน้า)
                        newAddress += "&" + countryCode.OldKeyValue + "|";
                    }
                    else
                    {
                        //กรณีที่มีการเปลี่ยนแปลงค่า จะแสดงเป็น ตัวหนังสือสีแดง(ใช้สัญลักษ์ # นำหน้า)
                        newAddress += "#" + countryCode.NewKeyValue + "|";
                    }

                }
            }
            #endregion

            #endregion

            #endregion

            return newAddress;
        }

        public static string CompoundAddressTracking(AddressModel oldAddressModel, AddressModel newAddressModel, out string oldAddress)
        {
            
            oldAddress = "";
            string newAddress = "";

            if (oldAddressModel != null && newAddressModel != null)
            {
                #region Compound Address

                #region HouseNo
                oldAddress += oldAddressModel.HouseNo + " ";
                if(oldAddressModel.HouseNo == newAddressModel.HouseNo)
                {
                    //ถ้าค่าเท่ากันแสดงว่าไม่มีการเปลี่ยนแปลง
                    newAddress += newAddressModel.HouseNo + "|";
                }else
                {
                    //แสดงว่ามีการเปลี่ยนแปลงค่า
                    if (!string.IsNullOrEmpty(oldAddressModel.HouseNo.Trim()) && string.IsNullOrEmpty(newAddressModel.HouseNo.Trim()))
                    {
                        //กรณีที่ค่าเก่ามีค่า แล้วค่าใหม่ไม่มีค่า จะแสดงเป็น ขีดกลางคั่น(ใช้สัญลักษ์ & นำหน้า)
                        newAddress += "&" + oldAddressModel.HouseNo + "|";
                    }
                    else
                    {
                        //กรณีที่มีการเปลี่ยนแปลงค่า จะแสดงเป็น ตัวหนังสือสีแดง(ใช้สัญลักษ์ # นำหน้า)
                        newAddress += "#" + newAddressModel.HouseNo + "|";
                    }
                }
                #endregion

                #region VillageNo
                oldAddress += oldAddressModel.VillageNo + " ";
                if (oldAddressModel.VillageNo == newAddressModel.VillageNo)
                {
                    //ถ้าค่าเท่ากันแสดงว่าไม่มีการเปลี่ยนแปลง
                    newAddress += newAddressModel.VillageNo + "|";
                }
                else
                {
                    //แสดงว่ามีการเปลี่ยนแปลงค่า
                    if (!string.IsNullOrEmpty(oldAddressModel.VillageNo.Trim()) && string.IsNullOrEmpty(newAddressModel.VillageNo.Trim()))
                    {
                        //กรณีที่ค่าเก่ามีค่า แล้วค่าใหม่ไม่มีค่า จะแสดงเป็น ขีดกลางคั่น(ใช้สัญลักษ์ & นำหน้า)
                        newAddress += "&" + oldAddressModel.VillageNo + "|";
                    }
                    else
                    {
                        //กรณีที่มีการเปลี่ยนแปลงค่า จะแสดงเป็น ตัวหนังสือสีแดง(ใช้สัญลักษ์ # นำหน้า)
                        newAddress += "#" + newAddressModel.VillageNo + "|";
                    }
                }
                #endregion

                #region Lane
                oldAddress += oldAddressModel.Lane + " ";
                if (oldAddressModel.Lane == newAddressModel.Lane)
                {
                    //ถ้าค่าเท่ากันแสดงว่าไม่มีการเปลี่ยนแปลง
                    newAddress += newAddressModel.Lane + "|";
                }
                else
                {
                    //แสดงว่ามีการเปลี่ยนแปลงค่า
                    if (!string.IsNullOrEmpty(oldAddressModel.Lane.Trim()) && string.IsNullOrEmpty(newAddressModel.Lane.Trim()))
                    {
                        //กรณีที่ค่าเก่ามีค่า แล้วค่าใหม่ไม่มีค่า จะแสดงเป็น ขีดกลางคั่น(ใช้สัญลักษ์ & นำหน้า)
                        newAddress += "&" + oldAddressModel.Lane + "|";
                    }
                    else
                    {
                        //กรณีที่มีการเปลี่ยนแปลงค่า จะแสดงเป็น ตัวหนังสือสีแดง(ใช้สัญลักษ์ # นำหน้า)
                        newAddress += "#" + newAddressModel.Lane + "|";
                    }
                }
                #endregion

                #region Road
                oldAddress += oldAddressModel.Road + " ";
                if (oldAddressModel.Road == newAddressModel.Road)
                {
                    //ถ้าค่าเท่ากันแสดงว่าไม่มีการเปลี่ยนแปลง
                    newAddress += newAddressModel.Road + "|";
                }
                else
                {
                    //แสดงว่ามีการเปลี่ยนแปลงค่า
                    if (!string.IsNullOrEmpty(oldAddressModel.Road.Trim()) && string.IsNullOrEmpty(newAddressModel.Road.Trim()))
                    {
                        //กรณีที่ค่าเก่ามีค่า แล้วค่าใหม่ไม่มีค่า จะแสดงเป็น ขีดกลางคั่น(ใช้สัญลักษ์ & นำหน้า)
                        newAddress += "&" + oldAddressModel.Road + "|";
                    }
                    else
                    {
                        //กรณีที่มีการเปลี่ยนแปลงค่า จะแสดงเป็น ตัวหนังสือสีแดง(ใช้สัญลักษ์ # นำหน้า)
                        newAddress += "#" + newAddressModel.Road + "|";
                    }
                }
                #endregion

                #region SubDistrict
                oldAddress += oldAddressModel.SubDistrict + " ";
                if (oldAddressModel.SubDistrict == newAddressModel.SubDistrict)
                {
                    //ถ้าค่าเท่ากันแสดงว่าไม่มีการเปลี่ยนแปลง
                    newAddress += newAddressModel.SubDistrict + "|";
                }
                else
                {
                    //แสดงว่ามีการเปลี่ยนแปลงค่า
                    if (!string.IsNullOrEmpty(oldAddressModel.SubDistrict.Trim()) && string.IsNullOrEmpty(newAddressModel.SubDistrict.Trim()))
                    {
                        //กรณีที่ค่าเก่ามีค่า แล้วค่าใหม่ไม่มีค่า จะแสดงเป็น ขีดกลางคั่น(ใช้สัญลักษ์ & นำหน้า)
                        newAddress += "&" + oldAddressModel.SubDistrict + "|";
                    }
                    else
                    {
                        //กรณีที่มีการเปลี่ยนแปลงค่า จะแสดงเป็น ตัวหนังสือสีแดง(ใช้สัญลักษ์ # นำหน้า)
                        newAddress += "#" + newAddressModel.SubDistrict + "|";
                    }
                }
                #endregion

                #region City
                oldAddress += oldAddressModel.City + " ";
                if (oldAddressModel.City == newAddressModel.City)
                {
                    //ถ้าค่าเท่ากันแสดงว่าไม่มีการเปลี่ยนแปลง
                    newAddress += newAddressModel.City + "|";
                }
                else
                {
                    //แสดงว่ามีการเปลี่ยนแปลงค่า
                    if (!string.IsNullOrEmpty(oldAddressModel.City.Trim()) && string.IsNullOrEmpty(newAddressModel.City.Trim()))
                    {
                        //กรณีที่ค่าเก่ามีค่า แล้วค่าใหม่ไม่มีค่า จะแสดงเป็น ขีดกลางคั่น(ใช้สัญลักษ์ & นำหน้า)
                        newAddress += "&" + oldAddressModel.City + "|";
                    }
                    else
                    {
                        //กรณีที่มีการเปลี่ยนแปลงค่า จะแสดงเป็น ตัวหนังสือสีแดง(ใช้สัญลักษ์ # นำหน้า)
                        newAddress += "#" + newAddressModel.City + "|";
                    }
                }
                #endregion

                #region State
                oldAddress += oldAddressModel.State + " ";
                if (oldAddressModel.State == newAddressModel.State)
                {
                    //ถ้าค่าเท่ากันแสดงว่าไม่มีการเปลี่ยนแปลง
                    newAddress += newAddressModel.State + "|";
                }
                else
                {
                    //แสดงว่ามีการเปลี่ยนแปลงค่า
                    if (!string.IsNullOrEmpty(oldAddressModel.State.Trim()) && string.IsNullOrEmpty(newAddressModel.State.Trim()))
                    {
                        //กรณีที่ค่าเก่ามีค่า แล้วค่าใหม่ไม่มีค่า จะแสดงเป็น ขีดกลางคั่น(ใช้สัญลักษ์ & นำหน้า)
                        newAddress += "&" + oldAddressModel.State + "|";
                    }
                    else
                    {
                        //กรณีที่มีการเปลี่ยนแปลงค่า จะแสดงเป็น ตัวหนังสือสีแดง(ใช้สัญลักษ์ # นำหน้า)
                        newAddress += "#" + newAddressModel.State + "|";
                    }
                }
                #endregion

                #region PostalCode
                oldAddress += oldAddressModel.PostalCode + " ";
                if (oldAddressModel.PostalCode == newAddressModel.PostalCode)
                {
                    //ถ้าค่าเท่ากันแสดงว่าไม่มีการเปลี่ยนแปลง
                    newAddress += newAddressModel.PostalCode + "|";
                }
                else
                {
                    //แสดงว่ามีการเปลี่ยนแปลงค่า
                    if (!string.IsNullOrEmpty(oldAddressModel.PostalCode.Trim()) && string.IsNullOrEmpty(newAddressModel.PostalCode.Trim()))
                    {
                        //กรณีที่ค่าเก่ามีค่า แล้วค่าใหม่ไม่มีค่า จะแสดงเป็น ขีดกลางคั่น(ใช้สัญลักษ์ & นำหน้า)
                        newAddress += "&" + oldAddressModel.PostalCode + "|";
                    }
                    else
                    {
                        //กรณีที่มีการเปลี่ยนแปลงค่า จะแสดงเป็น ตัวหนังสือสีแดง(ใช้สัญลักษ์ # นำหน้า)
                        newAddress += "#" + newAddressModel.PostalCode + "|";
                    }
                }
                #endregion

                #region CountryName
                oldAddress += oldAddressModel.CountryName + " ";
                if (oldAddressModel.CountryName == newAddressModel.CountryName)
                {
                    //ถ้าค่าเท่ากันแสดงว่าไม่มีการเปลี่ยนแปลง
                    newAddress += newAddressModel.CountryName + "|";
                }
                else
                {
                    //แสดงว่ามีการเปลี่ยนแปลงค่า
                    if (!string.IsNullOrEmpty(oldAddressModel.CountryName.Trim()) && string.IsNullOrEmpty(newAddressModel.CountryName.Trim()))
                    {
                        //กรณีที่ค่าเก่ามีค่า แล้วค่าใหม่ไม่มีค่า จะแสดงเป็น ขีดกลางคั่น(ใช้สัญลักษ์ & นำหน้า)
                        newAddress += "&" + oldAddressModel.CountryName + "|";
                    }
                    else
                    {
                        //กรณีที่มีการเปลี่ยนแปลงค่า จะแสดงเป็น ตัวหนังสือสีแดง(ใช้สัญลักษ์ # นำหน้า)
                        newAddress += "#" + newAddressModel.CountryName + "|";
                    }
                }
                #endregion

                #endregion
            }


            return newAddress;
        }
    }
}

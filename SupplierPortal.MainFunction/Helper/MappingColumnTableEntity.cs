﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.MainFunction.Helper
{
    public static class MappingColumnTableEntity
    {
        public static string MappingColumnAndTable(string columnName)
        {
            string result = string.Empty;

            switch (columnName)
            {
                case "OrgID":
                    return result = "org.OrgID";

                case "SupplierID":
                    return result = "org.SupplierID";

                case "CompanyName_Inter":
                    return result = "org.CompanyName_Inter";

                case "CompanyName_Local":
                    return result = "org.CompanyName_Local";

                case "ContactID":
                    return result = "Tbl_ContactPerson.ContactID";

                case "FirstName_Local":
                    return result = "Tbl_ContactPerson.FirstName_Local";

                case "LastName_Local":
                    return result = "Tbl_ContactPerson.LastName_Local";

                case "FirstName_Inter":
                    return result = "Tbl_ContactPerson.FirstName_Inter";

                case "LastName_Inter":
                    return result = "Tbl_ContactPerson.LastName_Inter";

                case "PhoneNo":
                    return result = "Tbl_ContactPerson.PhoneNo";

                case "PhoneExt":
                    return result = "Tbl_ContactPerson.PhoneExt";

                case "PhoneCountryCode":
                    return result = "Tbl_ContactPerson.PhoneCountryCode";

                case "MobileNo":
                    return result = "Tbl_ContactPerson.MobileNo";

                case "MobileCountryCode":
                    return result = "Tbl_ContactPerson.MobileCountryCode";

                case "Email":
                    return result = "Tbl_ContactPerson.Email";

                case "UserID":
                    return result = "Tbl_User.UserID";

                case "Username":
                    return result = "Tbl_User.Username";

                default: return result;
            }
        }
    }
}

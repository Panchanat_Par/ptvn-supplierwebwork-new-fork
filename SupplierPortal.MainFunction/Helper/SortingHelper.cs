﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.MainFunction.Helper
{
    public static class SortingHelper
    {
        public static string CreateSorting(bool sortAscending, string sortColumn)
        {
            string result = string.Empty;

            if (!string.IsNullOrWhiteSpace(sortColumn))
            {
                result = MappingColumnTableEntity.MappingColumnAndTable(sortColumn) + " " + (sortAscending ? "ASC" : "DESC");
            }

            return result;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SupplierPortal.Framework.Localization;

namespace SupplierPortal.Controllers
{
    public class BaseController : Controller
    {
        protected override IAsyncResult BeginExecuteCore(AsyncCallback callback, object state)
        {
            string cultureId = null;
            //string cultureCode = null;
            //string cultureCurrenc = System.Threading.Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName;
            

            var languagesBrowser = Request.UserLanguages[0];
            string[] words = languagesBrowser.Split('-');
            string cultureCurrenc = words[0];
            string currencCultureId = CultureHelper.GetCultureIdByCurrencCultureCode(cultureCurrenc);
            // Attempt to read the culture cookie from Request
            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (cultureCookie != null)
            {

                cultureId = cultureCookie.Value;
            }
            else
            {
                cultureId = null;
                cultureId = CultureHelper.GetImplementedCulture(currencCultureId); // This is safe
                cultureCookie = new HttpCookie("_culture");
                cultureCookie.Value = cultureId;
                cultureCookie.Expires = DateTime.Now.AddDays(30);

            }
            Response.Cookies.Add(cultureCookie);
            //cultureName = Request.UserLanguages != null && Request.UserLanguages.Length > 0 ? Request.UserLanguages[0] : null; // obtain it from HTTP header AcceptLanguages

            // Validate culture name
            //cultureName = CultureHelper.GetImplementedCulture(cultureName); // This is safe

            //+++++++++++++++++Report Set Cookie Default++++++++++++++++++++++++++++//

            string year = null;
            string buyer = null;


            HttpCookie cookieYear = Request.Cookies["_year"];
            HttpCookie cookieBuyerGrp = Request.Cookies["_buyer"];

            if ((cookieYear != null) && (cookieYear.Value != ""))
            {

                year = cookieYear.Value;
                buyer = cookieBuyerGrp.Value;
            }
            else
            {

                year = null;
                buyer = null;
                DateTime dt = DateTime.Now;
                year = dt.Year.ToString();
                cookieYear = new HttpCookie("_year");
                cookieYear.Value = year;
                cookieYear.Expires = DateTime.Now.AddDays(1);
                cookieBuyerGrp = new HttpCookie("_buyer");
                cookieBuyerGrp.Value = "All";
                cookieBuyerGrp.Expires = DateTime.Now.AddDays(1);

            }
            Response.Cookies.Add(cookieYear);
            Response.Cookies.Add(cookieBuyerGrp);

            return base.BeginExecuteCore(callback, state);
        }
    }
}
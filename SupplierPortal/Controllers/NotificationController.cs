﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SupplierPortal.Data.Models.Repository.User;
using SupplierPortal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SupplierPortal.Data.Models.SupportModel;
using SupplierPortal.Framework.MethodHelper;
using SupplierPortal.Data.Models.Repository.Language;
using SupplierPortal.Data.Models.Repository.NotificationTopic;
using SupplierPortal.Data.Models.Repository.LocaleStringResource;
using System.Web.Script.Serialization;
using SupplierPortal.Data.Models.Repository.Notification;
using SupplierPortal.Data.Models.Repository.Organization;
using SupplierPortal.Data.Models.Repository.ContactPerson;
using SupplierPortal.Data.Models.Repository.NameTitle;
using SupplierPortal.Data.Models.Repository.Address;
using SupplierPortal.ReportSupport;
using SupplierPortal.Services.SupplyManagement;
using System.Data;
using SupplierPortal.Data.Models.Repository.MenuPortal;
using SupplierPortal.Services.MenuPortalManage;
using SupplierPortal.Data.Models.Repository.TimeZone;
using SupplierPortal.Data.Models.Repository.UserReportMapping;
using SupplierPortal.Data.Models.Repository.UserServiceMapping;
using System.IO;
using System.Drawing;
using System.Threading;
using System.Globalization;
using SupplierPortal.Framework.Localization;
using SupplierPortal.Data.Models.Repository.Locale;
using SupplierPortal.Data.Models.Repository.Topic;
//using SupplierPortal.Data.Models.SupportModel.MenuPortal;

namespace SupplierPortal.Controllers
{
    public class NotificationController : Controller
    {
        public Notification _repository { get; set; }
        IUserRepository _userRepository;
        ILanguageRepository _languageRepository;
        INotificationTopicRepository _notificationTopicRepository;
        ILocaleStringRepository _localeStringRepository;
        INotificationsRepository _notificationsRepository;
        IOrganizationRepository _organizationRepository;
        IContactPersonRepository _contactPersonRepository;
        INameTitleRepository _nameTitleRepository;
        IAddressRepository _addressRepository;
        IReportService _reportServiceRepository;
        IMenuPortalRepository _menuPortalRepository;
        IMenuPortalService _menuPortalService;
        ITimeZoneRepository _timeZoneRepository;
        IUserReportMappingRepository _userReportMappingRepository;
        IUserServiceMappingRepository _userServiceMappingRepository;
        ILocaleRepository _localeRepository;
        ITopicRepository _topicRepository;

        public NotificationController()
        {
            _repository = new Notification();
            _userRepository = new UserRepository();
            _languageRepository = new LanguageRepository();
            _notificationTopicRepository = new NotificationTopicRepository();
            _localeStringRepository = new LocaleStringRepository();
            _notificationsRepository = new NotificationsRepository();
            _organizationRepository = new OrganizationRepository();
            _contactPersonRepository = new ContactPersonRepository();
            _nameTitleRepository = new NameTitleRepository();
            _addressRepository = new AddressRepository();
            _reportServiceRepository = new ReportService();
            _menuPortalRepository = new MenuPortalRepository();
            _menuPortalService = new MenuPortalService();
            _timeZoneRepository = new TimeZoneRepository();
            _userReportMappingRepository = new UserReportMappingRepository();
            _userServiceMappingRepository = new UserServiceMappingRepository();
            _localeRepository = new LocaleRepository();
            _topicRepository = new TopicRepository();
        }

        public NotificationController(
            Notification repositoty,
            UserRepository userRepository,
            LanguageRepository languageRepository,
            NotificationTopicRepository notificationTopicRepository,
            LocaleStringRepository localeStringRepository,
            NotificationsRepository notificationsRepository,
            OrganizationRepository organizationRepository,
            ContactPersonRepository contactPersonRepository,
            NameTitleRepository nameTitleRepository,
            AddressRepository addressRepository,
            ReportService reportServiceRepository,
            MenuPortalRepository menuPortalRepository,
            MenuPortalService menuPortalService,
            TimeZoneRepository timeZoneRepository,
            UserReportMappingRepository userReportMappingRepository,
            UserServiceMappingRepository userServiceMappingRepository,
            LocaleRepository localeRepository,
            TopicRepository topicRepository
            )
        {
            _repository = repositoty;
            _userRepository = userRepository;
            _languageRepository = languageRepository;
            _notificationTopicRepository = notificationTopicRepository;
            _localeStringRepository = localeStringRepository;
            _notificationsRepository = notificationsRepository;
            _organizationRepository = organizationRepository;
            _contactPersonRepository = contactPersonRepository;
            _nameTitleRepository = nameTitleRepository;
            _addressRepository = addressRepository;
            _reportServiceRepository = reportServiceRepository;
            _menuPortalRepository = menuPortalRepository;
            _menuPortalService = menuPortalService;
            _timeZoneRepository = timeZoneRepository;
            _userReportMappingRepository = userReportMappingRepository;
            _userServiceMappingRepository = userServiceMappingRepository;
            _localeRepository = localeRepository;
            _topicRepository = topicRepository;
        }

        // GET: Notification
        public PartialViewResult _Index()
        {
            string languageID = GetCurrentLanguageHelper.GetStringLanguageIDCurrent();
            int userID = 0;
            string username = ""; //Metro-Admin
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }
            var user = _userRepository.FindByUsername(username);
            if (user != null)
            {
                userID = user.UserID;
            }
            int supplierID = _organizationRepository.GetSupplierIDByUsername(username);

            var dataOrganization = _organizationRepository.GetDataBySupplierIDAndlanguageID(supplierID, int.Parse(languageID));
            var userContact = _userRepository.FindAllByUsername(User.Identity.Name);
            var contactData = _contactPersonRepository.GetContactPersonByContectID(userContact.ContactID ?? 0);

            ViewBag.contactData = contactData;
            ViewBag.dataOrgan = dataOrganization;
            ViewBag.TitleName = _nameTitleRepository.GetTitleNameByIdAndLanguageID(contactData.TitleID ?? 0, int.Parse(languageID));
            ViewBag.Address = _addressRepository.GetAddressByUsername(username);

            ViewBag.MainBanner = _topicRepository.GetResourceBody("Home.MainBanner", languageID);

            try
            {
                #region Get Dashboard POValue

                var year = DateTime.UtcNow.Year;
                var buyerGroupName = "ALL";
                var suppShortName = userContact.OrgID;

                //3D Pie Chart
                DataTable dtPOVolume_Chart;
                string chart_POVolume;

                DateTime dt = DateTime.Now;
                if (year == -1)
                {
                    year = dt.Year;
                }
                ChartsModels_POVolume model = new ChartsModels_POVolume();
                //Get ServerName
                var permission = _userReportMappingRepository.GerUserReportMappingByUserIDAndReportID(userID, 1);

                if (permission != null)
                {
                    ClsSupportConnection connRpt = new ClsSupportConnection();
                    connRpt.GetServerValueBySupplier(suppShortName);

                    ClsReportChart oRptChart = new ClsReportChart();

                    dtPOVolume_Chart = oRptChart.getDtPOVolume_Chart(suppShortName, buyerGroupName, year);

                    chart_POVolume = dtPOVolume_Chart.Rows[0]["POVolume_Chart"].ToString();

                    DataTable dtPOVolume;
                    dtPOVolume = oRptChart.getDtPOVolume(suppShortName, buyerGroupName, year);
                    int POVolumeRound = dtPOVolume.Rows.Count;

                    model = new ChartsModels_POVolume
                    {
                        data_POVolume = Enumerable.Range(1, POVolumeRound).Select(x => new POVolume
                        {
                            buyer = Convert.ToString(dtPOVolume.Rows[x - 1]["Buyer"]),
                            volume = String.Format("{0:#,##0.00}", dtPOVolume.Rows[x - 1]["POVolume"]),
                            percentVolume = Convert.ToDecimal(dtPOVolume.Rows[x - 1]["PercentVolume"])
                        })
                    };
                    model.json_POVolume = chart_POVolume;
                }


                var data_MappingChart = _userReportMappingRepository.CheckdataForShowChart(userID);

                if (data_MappingChart == 0)
                {
                    ViewBag.CheckDataChart = 0;
                }
                else
                {
                    ViewBag.CheckDataChart = 1;
                }

                var iconProfileNoti = _userServiceMappingRepository.GetIconNotificationByUserID(userID);

                //MemoryStream memoryStream = new MemoryStream(iconProfileNoti);
                //Image img = Image.FromStream(memoryStream);

                ViewBag.iconProfileNoti = iconProfileNoti;
                #endregion
                return PartialView(@"~/Views/Notification/_Index.cshtml", model);
            }
            catch (Exception ex)
            {
                ViewBag.CheckDataChart = 0;
                ChartsModels_POVolume model = new ChartsModels_POVolume();
                return PartialView(@"~/Views/Notification/_Index.cshtml", model);
                throw;
            }

        }

        public PartialViewResult NotiMianPatial(int TopicType = 0)
        {
            string languageID = GetCurrentLanguageHelper.GetStringLanguageIDCurrent();
            string LanguageCode = _languageRepository.GetLanguageCodeById(int.Parse(languageID));

            int userID = 0;
            string username = ""; //Metro-Admin
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }
            var user = _userRepository.FindByUsername(username);
            if (user != null)
            {
                userID = user.UserID;
            }
            string userGuid = "";
            if (Session["GUID"] != null)
            {
                userGuid = Session["GUID"].ToString();
            }
            var timeZone = _timeZoneRepository.GetTimeZoneByTimeZoneIDString(user.TimeZone);
            var ListStrJoinNotiTopic = _notificationTopicRepository.GetListStrJoinNotiTopic(int.Parse(languageID));

            var dateTime = DateTime.UtcNow;

            string NewFeed = _repository.GetCountNewFeed(userID, dateTime, TopicType);
            string AllFeed = _repository.GetCountAllFeed(userID, dateTime, TopicType);
            ViewBag.count_new_feed = NewFeed;
            ViewBag.count_all_feed = AllFeed;
            //return PartialView("NotipicationPartial", new List<NotificationModel>());
            return PartialView(@"~/Views/Notification/NotipicationPartial.cshtml", new List<NotificationModel>());
        }

        public PartialViewResult GetMessages(int TopicType = 0, int skip = 0)
       {
            string languageID = GetCurrentLanguageHelper.GetStringLanguageIDCurrent();
            string LanguageCode = _languageRepository.GetLanguageCodeById(int.Parse(languageID));

            int userID = 0;
            string username = ""; //Metro-Admin
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }
            var user = _userRepository.FindByUsername(username);
            if (user != null)
            {
                userID = user.UserID;
            }
            string userGuid = "";
            if (Session["GUID"] != null)
            {
                userGuid = Session["GUID"].ToString();
            }
            var timeZone = _timeZoneRepository.GetTimeZoneByTimeZoneIDString(user.TimeZone);
            var ListStrJoinNotiTopic = _notificationTopicRepository.GetListStrJoinNotiTopic(int.Parse(languageID));

            string message = "";
            var dateTime = DateTime.UtcNow;
            var dataNotification = _repository.GetAllMessages(userID, dateTime, TopicType, skip);
            ReplaceHtml replace = new ReplaceHtml();
            var dataLocale = _localeRepository.GetLocaleByLocaleName(user.Locale);
            var ListMessage = new List<NotificationModel>();
            foreach (var item in dataNotification)
            {
                var obj = JsonConvert.DeserializeObject<NotificationModel>(item.RawData);
                // var nortification = obj.NotificationData[LanguageCode.ToUpper()] ?? obj.NotificationData.Standard;
                var nortification = obj.NotificationData[LanguageCode.ToUpper()] != null ? obj.NotificationData[LanguageCode.ToUpper()] : obj.NotificationData.Standard;
                DateTime? Timer = null;
                DateTime? TCD = null;
                DateTime DTNow = DateTime.UtcNow.AddMinutes(Convert.ToDouble(timeZone.MinuteOffset));
                Dictionary<string, string> notiDT = JsonConvert.DeserializeObject<Dictionary<string, string>>(nortification.ToString());

                #region DTC DT
                var dtcField = notiDT.Keys.FirstOrDefault(w => w.StartsWith("DTC_"));
                if (dtcField != null)
                {
                    Timer = Convert.ToDateTime(notiDT[dtcField]);
                    Timer = Timer.Value.AddMinutes(Convert.ToDouble(timeZone.MinuteOffset));
                    TCD = Timer;
                    notiDT[dtcField] = Timer.ToString();
                    var js = new JavaScriptSerializer();
                    var json = js.Serialize(notiDT);

                    nortification = json;
                }

                var dtFields = notiDT.Keys.Where(w => w.StartsWith("DT_")).ToList();
                if (dtFields != null)
                {
                    foreach (var dtField in dtFields)
                    {
                        //double dOffSet = 0;
                        var tryDate = DateTime.MinValue;
                        if (DateTime.TryParse(notiDT[dtField], out tryDate))
                        {

                            string timeLanguage = "";
                            //switch (languageID)
                            //{
                            //    case "2":
                            //        Thread.CurrentThread.CurrentCulture = new CultureInfo("th-TH");
                            //        timeLanguage = GetLocaleStringResource.getStringResourceByResourceName("Portal_DateFormat_TH");
                            //        break;

                            //    case "1":
                            //    default:
                            //        Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                            //        timeLanguage = GetLocaleStringResource.getStringResourceByResourceName("Portal_DateFormat_EN");
                            //        break;

                            //}

                            if (dataLocale != null)
                            {
                                timeLanguage = dataLocale.MediumDateMask;
                            }

                            notiDT[dtField] = tryDate.AddMinutes(Convert.ToDouble(timeZone.MinuteOffset)).ToString(timeLanguage);

                            var js = new JavaScriptSerializer();
                            var json = js.Serialize(notiDT);
                            nortification = json;
                        }
                    }

                }
                #endregion
                //--dataTopicbyTopicId
                //var dataTopic = _notificationTopicRepository.GetDataByTopicID(item.TopicID ?? 0);

                var dataTopic = ListStrJoinNotiTopic.Where(s => s.TopicID == item.TopicID).FirstOrDefault();
                var htmlStr = "";
                if (dataTopic != null)
                {
                    //--getStringResource
                    //htmlStr = _localeStringRepository.GetResource(dataTopic.ResourceValue, languageID);
                    htmlStr = dataTopic.ResourceValue;
                    var FullUrl = "";
                    var stopTime = "";

                    //หา UserGuid,PageID เพื่อต่อ url
                    #region URL

                    var exceptKeys = new Dictionary<string, string>();
                    if (obj.URLParameters != null)
                        exceptKeys = JsonConvert.DeserializeObject<Dictionary<string, string>>(obj.URLParameters.ToString());

                    //  Dictionary<string, string> exceptKeys = obj.URLParameters ?? new Dictionary<string, string>();
                    var paramValues = new Dictionary<string, object>()
                    {
                        {"UserGuid", userGuid},
                        {"PageID", dataTopic.PageID}
                    };
                    var urlParameter = dataTopic.TopicURLParameters.Split(',')
                        .Where(w => !string.IsNullOrEmpty(w))
                        .Select(key => string.Format("{0}={1}", key, paramValues.ContainsKey(key) ? paramValues[key] : ""))
                        .Union(exceptKeys.Select(s => string.Format("{0}={1}", s.Key, s.Value)))
                        .Distinct();

                    FullUrl = dataTopic.TopicURL + string.Join("&", urlParameter);

                    #endregion

                    stopTime = item.StopTime == null ? "" : item.StopTime.Value.AddMinutes(Convert.ToDouble(timeZone.MinuteOffset)).ToString("d/MMMM/yyyy HH:mm:ss");


                    #region StartTimer
                    var startTimer = "";
                    if (item.StartTime != null)
                    {
                        item.StartTime = item.StartTime.Value.AddMinutes(Convert.ToDouble(timeZone.MinuteOffset));

                        if (item.StartTime.Value.Year == dateTime.Year && item.StartTime.Value.Month == dateTime.Month && item.StartTime.Value.Date == dateTime.Date)
                        {
                            var shortTime = "HH:mm";
                            switch (languageID)
                            {
                                case "2":
                                    Thread.CurrentThread.CurrentCulture = new CultureInfo("th-TH");
                                    shortTime = GetLocaleStringResource.getStringResourceByResourceName("Noti_CurrentDateFormat_TH");
                                    break;

                                case "1":
                                default:
                                    Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                                    shortTime = GetLocaleStringResource.getStringResourceByResourceName("Noti_CurrentDateFormat_NonTH");
                                    break;

                            }
                            startTimer = item.StartTime.Value.ToString(shortTime);
                        }
                        else if (item.StartTime.Value.Year == dateTime.Year && item.StartTime.Value.Month == dateTime.Month && item.StartTime.Value.Date.AddDays(1) == dateTime.Date)
                        {
                            if (languageID == "1")
                            {
                                startTimer = "Yesterday";
                            }
                            else if (languageID == "2")
                            {
                                startTimer = "เมื่อวานนี้";
                            }
                        }
                        else if (item.StartTime.Value.Year == dateTime.Year && item.StartTime.Value.Month == dateTime.Month && item.StartTime.Value.Date.AddDays(2) == dateTime.Date)
                        {
                            if (languageID == "1")
                            {
                                startTimer = "2 days ago";
                            }
                            else if (languageID == "2")
                            {
                                startTimer = "2 วันที่ผ่านมา";
                            }
                        }
                        else if (item.StartTime.Value.Year == dateTime.Year && item.StartTime.Value.Month == dateTime.Month && item.StartTime.Value.Date.AddDays(3) == dateTime.Date)
                        {
                            if (languageID == "1")
                            {
                                startTimer = "3 days ago";
                            }
                            else if (languageID == "2")
                            {
                                startTimer = "3 วันที่ผ่านมา";
                            }
                        }
                        else if (item.StartTime.Value.Year == dateTime.Year && item.StartTime.Value.Month == dateTime.Month && item.StartTime.Value.Date.AddDays(4) == dateTime.Date)
                        {
                            if (languageID == "1")
                            {
                                startTimer = "4 days ago";
                            }
                            else if (languageID == "2")
                            {
                                startTimer = "4 วันที่ผ่านมา";
                            }
                        }
                        else if (item.StartTime.Value.Year == dateTime.Year && item.StartTime.Value.Month == dateTime.Month && item.StartTime.Value.Date.AddDays(5) == dateTime.Date)
                        {
                            if (languageID == "1")
                            {
                                startTimer = "5 days ago";
                            }
                            else if (languageID == "2")
                            {
                                startTimer = "5 วันที่ผ่านมา";
                            }
                        }
                        else if (item.StartTime.Value.Year == dateTime.Year && item.StartTime.Value.Month == dateTime.Month && item.StartTime.Value.Date.AddDays(6) == dateTime.Date)
                        {
                            if (languageID == "1")
                            {
                                startTimer = "6 days ago";
                            }
                            else if (languageID == "2")
                            {
                                startTimer = "6 วันที่ผ่านมา";
                            }
                        }
                        else if (item.StartTime.Value.Year == dateTime.Year && item.StartTime.Value.Month == dateTime.Month && item.StartTime.Value.Date.AddDays(7) == dateTime.Date)
                        {
                            if (languageID == "1")
                            {
                                startTimer = "7 days ago";
                            }
                            else if (languageID == "2")
                            {
                                startTimer = "7 วันที่ผ่านมา";
                            }
                        }
                        else if (item.StartTime.Value.Year == dateTime.Year)
                        {
                            var shortTime = "MMM d";
                            switch (languageID)
                            {
                                case "2":
                                    Thread.CurrentThread.CurrentCulture = new CultureInfo("th-TH");
                                    shortTime = GetLocaleStringResource.getStringResourceByResourceName("Noti_CurrentYearFormat_TH");
                                    break;

                                case "1":
                                default:
                                    Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                                    shortTime = GetLocaleStringResource.getStringResourceByResourceName("Noti_CurrentYearFormat_NonTH");
                                    break;

                            }

                            startTimer = item.StartTime.Value.ToString(shortTime);
                        }
                        else
                        {
                            var shortTime = "MMM-dd-yyyy";
                            switch (languageID)
                            {
                                case "2":
                                    Thread.CurrentThread.CurrentCulture = new CultureInfo("th-TH");
                                    shortTime = GetLocaleStringResource.getStringResourceByResourceName("Noti_NonCurrentYearFormat_TH");
                                    break;

                                case "1":
                                default:
                                    Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                                    shortTime = GetLocaleStringResource.getStringResourceByResourceName("Noti_NonCurrentYearFormat_NonTH");
                                    break;

                            }

                            startTimer = item.StartTime.Value.ToString(shortTime);
                        }
                    }
                    #endregion

                    message = replace.ReplaceStringValueInHtmlText(nortification.ToString(), htmlStr);

                    int result3 = System.Text.RegularExpressions.Regex.Matches(message, "<br/>").Cast<System.Text.RegularExpressions.Match>().Count();
                    if (message != null && message != "")
                    {
                        ListMessage.Add(new NotificationModel()
                        {
                            NotificationID = item.NotificationID,
                            TopicID = item.TopicID ?? 0,
                            NotificationKey = item.NotificationKey,
                            SystemID = item.SystemID ?? 0,
                            NotificationData = message,
                            StartTime = startTimer,  //item.StartTime == null ? "" : item.StartTime.Value.AddMinutes(Convert.ToDouble(timeZone.MinuteOffset)).ToString(),
                            StopTime = stopTime,
                            isRead = item.isRead ?? 0,
                            isDeleted = item.isDeleted ?? 0,
                            GroupID = dataTopic.GroupID,
                            TopicURL = FullUrl,
                            HeaderLength = dataTopic.HeaderLength,
                            countdownTimer = TCD == null ? "" : TCD.Value.ToString("d MMMM yyyy HH:mm:ss", new CultureInfo("en-US")),
                            CheckCountdown = TCD == null ? false : DTNow < TCD.Value,
                            CheckTimeOut = item.StartTime == null ? false : item.StopTime == null ? false :
                                           DateTime.UtcNow < item.StopTime && DateTime.UtcNow > item.StartTime


                        });
                    }
                }


            }
            string NewFeed = _repository.GetCountNewFeed(userID, dateTime, TopicType);
            string AllFeed = _repository.GetCountAllFeed(userID, dateTime, TopicType);
            ViewBag.count_new_feed = NewFeed;
            ViewBag.count_all_feed = AllFeed;
            ViewBag.SelectNotificationTypeID = TopicType;
            ViewBag.Skip = skip + 10;
            //return PartialView("_NotipicatopnTopicPatial", ListMessage);
            return PartialView(@"~/Views/Notification/_NotipicatopnTopicPatial.cshtml", ListMessage);
        }

        //public ActionResult SearchNotification(NotificationTopic model)
        public ActionResult SearchNotification(int TopicID = 0)
        {
            string languageID = GetCurrentLanguageHelper.GetStringLanguageIDCurrent();

            var model = new NotificationTopic();

            model.TopicID = TopicID;
            string username = "";
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }
            int supplierID = _organizationRepository.GetSupplierIDByUsername(username);

            ViewData["SearchNotiList"] = _notificationTopicRepository.GetComboboxTopicList(supplierID, int.Parse(languageID));

            ViewBag.searchNotiList = _notificationTopicRepository.GetComboboxTopicList(supplierID, int.Parse(languageID));

            //return PartialView("_searchNotification", model);
            return PartialView(@"~/Views/Notification/_searchNotification.cshtml", model);
        }

        public ActionResult NotificationBox()
        {
            string username = ""; //Metro-Admin
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }
            var NotiMenuList = _menuPortalRepository.GetNotificationMenu();
            var dataNotibox = new List<NotificationDataMenuPortalModel>();
            foreach (var item in NotiMenuList)
            {
                //int? NotiData = _menuPortalService.GetNotificationBoxByUserNameAndMenuID(username, item.MenuID);

                dataNotibox.Add(new NotificationDataMenuPortalModel()
                {
                    MenuID = item.MenuID,
                    MenuLevel = item.MenuLevel,
                    SeqNo = item.SeqNo,
                    SystemGrpID = item.SystemGrpID,
                    //NotificationData = NotiData ?? 0,
                    MenuName = item.MenuName,
                    Section = item.Section,
                    Icon = item.Icon,
                    ResourceName = item.ResourceName,
                    NotificationID = item.NotificationID
                });
            }
            //return PartialView("_NotificationBoxPartial", dataNotibox);
            return PartialView(@"~/Views/Notification/_NotificationBoxPartial.cshtml", dataNotibox);
        }

        [HttpPost]
        public ActionResult GetNotificationByMenuID(int menuID)
        {
            // int notificationValue = 0;

            string username = Session["username"].ToString();

            //var notificationBox = new NotificationBoxModels();

            try
            {

                //notificationValue = _menuPortalService.GetNotificationBoxByUserNameAndMenuID(username, menuID);


                var notificationBox = _menuPortalService.GetNotificationBoxAllByUserNameAndMenuID(username, menuID);

                notificationBox.GetNotificationShow = true;


                return Json(notificationBox);

                //return Json(new
                //{
                //    isGetNotification = true,
                //    notificationValue = notificationValue
                //});
            }
            catch (Exception ex)
            {

                return Json(new
                {
                    GetNotificationShow = false
                });
            }

        }

        public void isReadNoti(int NotiID)
        {
            string username = ""; //Metro-Admin
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }
            var user = _userRepository.FindByUsername(username);
            // var dataNoti = _notificationsRepository.GetDataByNotiID(NotiID);
            _notificationsRepository.changeIsRead(NotiID);

            var dataNoti = _notificationsRepository.GetDataByNotiID(NotiID);
            var dataTopic = _notificationTopicRepository.GetDataByTopicID(dataNoti.TopicID ?? 0);
            if (dataTopic.isAddStopTimeWhenRead == 1)
            {
                //var timeZone = _timeZoneRepository.GetTimeZoneByTimeZoneIDString(user.TimeZone);
                var StopTimeWhenRead = DateTime.UtcNow.AddMilliseconds(Convert.ToDouble(dataTopic.StopTimeAdding));

                _notificationsRepository.UpdateStopTimeWhenRead(NotiID, StopTimeWhenRead);
            }

        }

        public void isDelete(int NotiID)
        {
            _notificationsRepository.changeIsDelete(NotiID);
        }

        public void isReadNotiList(List<int> NotiID)
        {
            foreach (var item in NotiID)
            {
                _notificationsRepository.changeIsRead(item);
            }
        }

        public void isDeleteNotiList(List<int> NotiID)
        {
            foreach (var item in NotiID)
            {
                _notificationsRepository.changeIsDelete(item);
            }
        }
    }
}
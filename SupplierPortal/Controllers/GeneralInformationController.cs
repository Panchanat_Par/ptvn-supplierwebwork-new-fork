﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SupplierPortal.Data.Models.Repository.Organization;
using SupplierPortal.Data.Models.Repository.CustomerDocType;
using SupplierPortal.Data.Models.Repository.CustomerDocName;
using SupplierPortal.Data.Models.Repository.CustomerDocNameMapping;
using SupplierPortal.Data.Models.Repository.OrgAttachment;
using SupplierPortal.Data.Models.Repository.AppConfig;
using SupplierPortal.Data.Models.Repository.LogOrgAttachment;
using SupplierPortal.Data.Models.Repository.Country;
using SupplierPortal.Data.Models.Repository.BusinessEntity;
using SupplierPortal.Data.Models.Repository.Function;
using SupplierPortal.Data.Models.Repository.User;
using SupplierPortal.Services.CompanyProfileManage;
using System.IO;
using System.Web.Routing;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.CompanyProfile;
using SupplierPortal.Framework.Localization;
using SupplierPortal.Framework.MethodHelper;
using SupplierPortal.Core.Extension;
using SupplierPortal.Data.Models.Repository.TrackingComprofile;
using System.Web.Configuration;
using SupplierPortal.Data.Models.Repository.OrgContactPerson;
using SupplierPortal.Data.Models.Repository.ContactPerson;
using SupplierPortal.Data.Models.Repository.TrackingOrgAttachment;
using SupplierPortal.Data.Models.Repository.TrackingRequest;
using SupplierPortal.Data.Models.Repository.PortalFieldConfig;
using SupplierPortal.Data.Helper;


namespace SupplierPortal.Controllers
{
    public class GeneralInformationController : ControllerBase
    {

        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        IOrganizationRepository _organizationRepository;
        ICustomerDocTypeRepository _customerDocTypeRepository;
        ICustomerDocNameRepository _customerDocNameRepository;
        IOrgAttachmentRepository _orgAttachmentRepository;
        IAppConfigRepository _appConfigRepository;
        ILogOrgAttachmentRepository _logOrgAttachmentRepository;
        IComProfileDataService _comProfileDataService;
        ICountryRepository _countryRepository;
        IBusinessEntityRepository _businessEntityRepository;
        IFunctionRepository _functionRepository;
        IUserRepository _userRepository;
        ITrackingComprofileRepository _trackingComprofileRepository;
        IOrgContactPerson _orgContactPerson;
        IContactPersonRepository _contactPersonRepository;
        IComProfileTrackingService _comProfileTrackingService;
        ITrackingOrgAttachmentRepository _trackingOrgAttachmentRepository;
        ITrackingRequestRepository _trackingRequestRepository;
        IPortalFieldConfigRepository _portalFieldConfigRepository;

        public GeneralInformationController()
        {
            _organizationRepository = new OrganizationRepository();
            _customerDocTypeRepository = new CustomerDocTypeRepository();
            _customerDocNameRepository = new CustomerDocNameRepository();
            _orgAttachmentRepository = new OrgAttachmentRepository();
            _appConfigRepository = new AppConfigRepository();
            _logOrgAttachmentRepository = new LogOrgAttachmentRepository();
            _comProfileDataService = new ComProfileDataService();
            _countryRepository = new CountryRepository();
            _businessEntityRepository = new BusinessEntityRepository();
            _functionRepository = new FunctionRepository();
            _userRepository = new UserRepository();
            _trackingComprofileRepository = new TrackingComprofileRepository();
            _orgContactPerson = new OrgContactPerson();
            _contactPersonRepository = new ContactPersonRepository();
            _comProfileTrackingService = new ComProfileTrackingService();
            _trackingOrgAttachmentRepository = new TrackingOrgAttachmentRepository();
            _trackingRequestRepository = new TrackingRequestRepository();
            _portalFieldConfigRepository = new PortalFieldConfigRepository();
        }

        public GeneralInformationController
            (
            OrganizationRepository organizationRepository,
            CustomerDocTypeRepository customerDocTypeRepository,
            CustomerDocNameRepository customerDocNameRepository,
            OrgAttachmentRepository orgAttachmentRepository,
            AppConfigRepository appConfigRepository,
            LogOrgAttachmentRepository logOrgAttachmentRepository,
            ComProfileDataService comProfileDataService,
            CountryRepository countryRepository,
            BusinessEntityRepository businessEntityRepository,
            FunctionRepository functionRepository,
            UserRepository userRepository,
            TrackingComprofileRepository trackingComprofileRepository,
            OrgContactPerson orgContactPerson,
            ContactPersonRepository contactPersonRepository,
            ComProfileTrackingService comProfileTrackingService,
            TrackingOrgAttachmentRepository trackingOrgAttachmentRepository,
            TrackingRequestRepository trackingRequestRepository,
            PortalFieldConfigRepository portalFieldConfigRepository
            )
        {
            _organizationRepository = organizationRepository;
            _customerDocTypeRepository = customerDocTypeRepository;
            _customerDocNameRepository = customerDocNameRepository;
            _orgAttachmentRepository = orgAttachmentRepository;
            _appConfigRepository = appConfigRepository;
            _logOrgAttachmentRepository = logOrgAttachmentRepository;
            _comProfileDataService = comProfileDataService;
            _countryRepository = countryRepository;
            _businessEntityRepository = businessEntityRepository;
            _functionRepository = functionRepository;
            _userRepository = userRepository;
            _trackingComprofileRepository = trackingComprofileRepository;
            _orgContactPerson = orgContactPerson;
            _contactPersonRepository = contactPersonRepository;
            _comProfileTrackingService = comProfileTrackingService;
            _trackingOrgAttachmentRepository = trackingOrgAttachmentRepository;
            _trackingRequestRepository = trackingRequestRepository;
            _portalFieldConfigRepository = portalFieldConfigRepository;
        }

        // GET: GeneralInformation
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult General(string alertMessage = "")
        {
            string username = "";
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }

            int supplierID = _organizationRepository.GetSupplierIDByUsername(username);

            var model = _comProfileDataService.GetGeneralComProfileBySupplierID(supplierID);
            var Orgdata = _organizationRepository.GetDataBySupplierID(supplierID);
            //int isInter = LanguageHelper.GetLanguageIsInter();
            //get data trackingstatus
            model.trackingFields = model.GetTrackingStatus<Tbl_Organization>(Orgdata.SupplierID);
            //ViewBag.TrackingModel = trackingModel;


            if (model.trackingFields["CompanyTypeID"].TrackingStatusID == 2)
                model.CompanyTypeID = int.Parse(model.trackingFields["CompanyTypeID"].NewKeyValue);
            if (model.trackingFields["CountryCode"].TrackingStatusID == 2)
            {
                model.CountryCode = model.trackingFields["CountryCode"].NewKeyValue;
                string countryName = _countryRepository.GetCountryNameByCountryCode(model.CountryCode);
                model.CountryName = countryName;
            }

            if (model.trackingFields["TaxID"].TrackingStatusID == 2)
                model.TaxID = model.trackingFields["TaxID"].NewKeyValue;
            if (model.trackingFields["DUNSNumber"].TrackingStatusID == 2)
                model.DUNSNumber = model.trackingFields["DUNSNumber"].NewKeyValue;
            if (model.trackingFields["BusinessEntityID"].TrackingStatusID == 2)
                model.BusinessEntityID = int.Parse(model.trackingFields["BusinessEntityID"].NewKeyValue);

            if (model.BusinessEntityID == -1)
            {
                var otherData = _trackingComprofileRepository.GetDataBySupplierIDandTrackingKey(model.SupplierID, "BusinessEntityID", "Tbl_Organization");
                foreach (var item in otherData)
                {
                    model.OtherBusinessEntity = item.NewKeyValue.Split('|').LastOrDefault();
                }

            }


            if (model.trackingFields["CompanyName_Local"].TrackingStatusID == 2)
                model.CompanyName_Local = model.trackingFields["CompanyName_Local"].NewKeyValue;
            if (model.trackingFields["CompanyName_Inter"].TrackingStatusID == 2)
                model.CompanyName_Inter = model.trackingFields["CompanyName_Inter"].NewKeyValue;
            if (model.trackingFields["BranchNo"].TrackingStatusID == 2)
                model.BranchNo = model.trackingFields["BranchNo"].NewKeyValue;
            if (model.trackingFields["BranchName_Local"].TrackingStatusID == 2)
                model.BranchName_Local = model.trackingFields["BranchName_Local"].NewKeyValue;
            if (model.trackingFields["BranchName_Inter"].TrackingStatusID == 2)
                model.BranchName_Inter = model.trackingFields["BranchName_Inter"].NewKeyValue;


            ViewBag.AlertMessage = alertMessage;
            return View(model);
        }

        [HttpPost]
        public ActionResult GeneralSave(CompanyGeneralModel model)
        {
            try
            {
                //bool checkDup;

                //model.trackingFields = model.GetTrackingStatus<Tbl_Organization>(model.SupplierID);
                //foreach (var item in model.trackingFields)
                //{
                //    if (item.Value.TrackingStatusID == 1)
                //    {


                //        checkDup = true;
                //    }
                //}

                int trackingReqID = 0;

                logger.Debug("(before Insert)TrackingReqID : " + trackingReqID);
                #region Update Data

                var userContact = _userRepository.FindAllByUsername(User.Identity.Name);
                var contactData = _contactPersonRepository.GetContactPersonByContectID(userContact.ContactID ?? 0);
                //test tracking check
                var itemtrackingStatus = _organizationRepository.GetTrackingStatus();

                //insert Req
                //trackingReqID = _trackingComprofileRepository.InsertReqAndRetrun(userContact.UserID, model.SupplierID);
                var trackingRequestModel = new Tbl_TrackingRequest()
                {
                    SupplierID = model.SupplierID,
                    ReqBy = userContact.UserID,
                    ReqDate = DateTime.UtcNow,
                    TrackingGrpID = 1,
                    TrackingStatusID = 2,
                    TrackingStatusDate = DateTime.UtcNow,
                    Remark = "",
                    isDeleted = 0
                };

                trackingReqID = _trackingRequestRepository.InsertReturnTrackingReqID(trackingRequestModel, userContact.UserID);

                //check ReqData      
                //var dataReq = _trackingComprofileRepository.GetDataByUser(userContact.UserID);

                //if(dataReq != null)
                //{
                //    trackingReqID = dataReq.TrackingReqID;
                //    logger.Debug("(after Insert)TrackingReqID : " + trackingReqID);
                //}
                //ลูปเอาค่า tbl.field เพื่อไปหาใน Tbl_PortalFieldConfig
                foreach (var Variable in itemtrackingStatus)
                {
                    var value = string.Empty;
                    if (value != null)
                    {
                        try
                        {
                            value = (model.GetType().GetProperty(Variable.Key).GetValue(model) ?? string.Empty).ToString();
                        }
                        catch (Exception)
                        {
                        }
                    }
                    var Orgdata = _organizationRepository.GetDataBySupplierID(model.SupplierID);
                    var Oldval = Orgdata.GetType().GetProperty(Variable.Key.ToString()).GetValue(Orgdata).ToString();


                    var trackingKey = Variable.Value.Split('.').LastOrDefault(); //ตัดเอาเฉพาะชื่อฟิว
                    var item = _trackingComprofileRepository.GetDataBySupplierIDandTrackingKey(model.SupplierID, trackingKey, "Tbl_Organization");

                    //ถ้าไม่มีข้อมูลใน db ทำการ insert ถ้ามีแล้วให้ทำการ update
                    if (item.Count() == 0)
                    {
                        //check Newvalue vs Oldvalue
                        if (Oldval != value)
                        {

                            //get check other
                            var dataCheckOther = _portalFieldConfigRepository.GetdataCheckOtherField(Variable.Value);
                            if (dataCheckOther != null)
                            {
                                var dataFieldOther = _portalFieldConfigRepository.GetdataByID(dataCheckOther.OtherFieldID ?? 0);
                                var splitData = dataFieldOther.Variable.Split('.').LastOrDefault();

                                var valueOther = (model.GetType().GetProperty(splitData).GetValue(model) ?? string.Empty).ToString();
                                if (valueOther != null)
                                {
                                    if (valueOther != "")
                                    {
                                        value += "|" + valueOther;
                                    }
                                    _trackingComprofileRepository.Insert(Variable.Value, model.SupplierID, value, trackingReqID, model.SupplierID, 2);
                                }
                            }
                            else
                                _trackingComprofileRepository.Insert(Variable.Value, model.SupplierID, value, trackingReqID, model.SupplierID, 2);
                            //if (Variable.Key == "BusinessEntityID" && model.BusinessEntityID == -1)
                            //{
                            //    value += "|" + model.OtherBusinessEntity;
                            //    _trackingComprofileRepository.Insert(Variable.Value, model.SupplierID, value, dataReq.TrackingReqID, model.SupplierID);
                            //}


                            //else
                            //{
                            //    _trackingComprofileRepository.Insert(Variable.Value, model.SupplierID, value, 0, model.SupplierID);
                            //}
                        }
                        //else
                        //{
                        //    _trackingComprofileRepository.Insert(Variable.Value, model.SupplierID, value, 0, model.SupplierID);
                        //}
                    }

                    var OtherFiled = _trackingComprofileRepository.GetDataOtherFiled(Variable.Value);

                    if (OtherFiled != null)
                    {
                        var splitOther = OtherFiled.Variable.Split('.').LastOrDefault();
                        var itemOther = _trackingComprofileRepository.GetDataBySupplierIDandTrackingKey(model.SupplierID, splitOther, "Tbl_Organization");
                        if (itemOther.Count() == 0)
                        {
                            var dataOther = _trackingComprofileRepository.GetDataOtherFiledByID(OtherFiled.OtherFieldID ?? 0);
                            var spit = dataOther.Variable.Split('.').LastOrDefault();
                            var valueOther = string.Empty;
                            if (valueOther != null)
                            {
                                try
                                {
                                    valueOther = (model.GetType().GetProperty(spit).GetValue(model) ?? string.Empty).ToString();
                                }
                                catch (Exception)
                                {
                                }
                            }
                            var OrgdataOther = _organizationRepository.GetDataBySupplierID(model.SupplierID);
                            var OldvalueOther = OrgdataOther.GetType().GetProperty(spit).GetValue(OrgdataOther).ToString();

                            if (valueOther != OldvalueOther)
                            {

                                if (OtherFiled.Variable == "Tbl_Organization.BusinessEntityID")
                                {
                                    valueOther = "-1|" + valueOther;
                                }

                                _trackingComprofileRepository.Insert(OtherFiled.Variable, model.SupplierID, valueOther, trackingReqID, model.SupplierID, 2);

                            }
                        }

                    }
                    //else
                    //{
                    //    //check Newvalue vs Oldvalue
                    //    if (Oldval != value)
                    //    {
                    //        //check ReqData
                    //        var dataReq = _trackingComprofileRepository.GetDataByUser(userContact.UserID);
                    //        if (dataReq != null)
                    //        {
                    //            _trackingComprofileRepository.Update(Variable.Value, model.SupplierID, value, dataReq.TrackingReqID, model.SupplierID);
                    //        }
                    //        //else
                    //        //{
                    //        //    _trackingComprofileRepository.Update(Variable.Value, model.SupplierID, value, 0, model.SupplierID);
                    //        //}
                    //    }
                    //    //else
                    //    //{
                    //    //    _trackingComprofileRepository.Update(Variable.Value, model.SupplierID, value, 0, model.SupplierID);
                    //    //}
                    //}

                }
                #region TrackingCompofileCheckRef
                var bundleRefPortalCompAddress = new List<Tbl_PortalFieldConfig>();
                var dataTrackingCompAddress = _trackingComprofileRepository.GetdataRefByAddressID(model.SupplierID);

                foreach (var itemCompare in dataTrackingCompAddress)
                {
                    var dataTrackingRef = _portalFieldConfigRepository.GetdataForcheckRef(itemCompare.FieldID ?? 0);
                    string bundleRef = dataTrackingRef.BundleFieldID;
                    string[] ListBundle = bundleRef.Split(',');
                    foreach (var list in ListBundle)
                    {
                        if (!string.IsNullOrEmpty(list))
                        {
                            int i = Convert.ToInt32(list);

                            var dataTrackingRef2 = _portalFieldConfigRepository.GetdataForcheckRef(i);
                            if (dataTrackingRef2 != null)
                            {
                                bundleRefPortalCompAddress.Add(dataTrackingRef2);
                                foreach (var iitem in bundleRefPortalCompAddress)
                                {
                                    var dataTrackingComPro = _trackingComprofileRepository.GetdataRefByfieldID_AddressID(iitem.FieldID, model.SupplierID);
                                    //if (dataTrackingComPro != null)
                                    //{
                                    var valueCompare = string.Empty;
                                    var dataVal = string.Empty;
                                    if (valueCompare != null)
                                    {
                                        dataVal = iitem.Variable.Split('.').LastOrDefault();
                                        try
                                        {
                                            valueCompare = (model.GetType().GetProperty(dataVal).GetValue(model) ?? string.Empty).ToString();
                                        }
                                        catch (Exception)
                                        {
                                        }
                                    }
                                    var dataTacking2 = _trackingComprofileRepository.GetDataBySupplierIDandTrackingKey(model.SupplierID, dataVal, "Tbl_Organization");
                                    var dataTacking5 = _trackingComprofileRepository.GetDataRefBySupplierIDandTrackingKey(model.SupplierID, dataVal, "Tbl_Organization");

                                    var dataReqCom = _trackingComprofileRepository.GetDataByUser(userContact.UserID);
                                    if (dataReqCom != null)
                                    {
                                        if (dataTacking2.Count() == 0 && dataTacking5.Count() == 0)
                                        {
                                            _trackingComprofileRepository.Insert(iitem.Variable, model.SupplierID, valueCompare, trackingReqID, model.SupplierID, 5);
                                        }
                                    }
                                    //}
                                }
                            }
                        }
                    }
                }
                #endregion
                string username = "";
                if (Session["username"] != null)
                {
                    username = Session["username"].ToString();
                }
                var user = _userRepository.FindByUsername(username);
                int userID = user.UserID;

                var orgModel = _organizationRepository.GetDataBySupplierID(model.SupplierID);

                if (orgModel != null)
                {
                    //เช็คเซฟเฉพาะข้อมูลที่มีค่า isTrackModify = 0
                    if (!itemtrackingStatus.ContainsKey("CompanyTypeID"))
                        orgModel.CompanyTypeID = model.CompanyTypeID;
                    if (!itemtrackingStatus.ContainsKey("CountryCode"))
                        orgModel.CountryCode = model.CountryCode ?? "";
                    if (!itemtrackingStatus.ContainsKey("TaxID"))
                        orgModel.TaxID = model.TaxID ?? "";
                    if (!itemtrackingStatus.ContainsKey("DUNSNumber"))
                        orgModel.DUNSNumber = model.DUNSNumber ?? "";
                    if (!itemtrackingStatus.ContainsKey("BusinessEntityID"))
                        orgModel.BusinessEntityID = model.BusinessEntityID;
                    if (!itemtrackingStatus.ContainsKey("OtherBusinessEntity"))
                        orgModel.OtherBusinessEntity = model.OtherBusinessEntity ?? "";
                    if (!itemtrackingStatus.ContainsKey("CompanyName_Local"))
                    {
                        model.CompanyName_Local = model.CompanyName_Local ?? "";
                        orgModel.CompanyName_Local = model.CompanyName_Local.ToUpper();
                    }
                    if (!itemtrackingStatus.ContainsKey("CompanyName_Inter"))
                    {
                        model.CompanyName_Inter = model.CompanyName_Inter ?? "";
                        orgModel.CompanyName_Inter = model.CompanyName_Inter.ToUpper();
                    }
                    if (!itemtrackingStatus.ContainsKey("BranchNo"))
                        orgModel.BranchNo = model.BranchNo ?? "";
                    if (!itemtrackingStatus.ContainsKey("BranchName_Local"))
                    {
                        model.BranchName_Local = model.BranchName_Local ?? "";
                        orgModel.BranchName_Local = model.BranchName_Local.ToUpper();
                    }
                    if (!itemtrackingStatus.ContainsKey("BranchName_Inter"))
                    {
                        model.BranchName_Inter = model.BranchName_Inter ?? "";
                        orgModel.BranchName_Inter = model.BranchName_Inter.ToUpper();
                    }


                    _organizationRepository.Update(orgModel, userID);
                    //Update Tbl_Organization.LastUpdate
                    _organizationRepository.UpdateLastUpdate(orgModel.SupplierID, userID);
                }
                #endregion

                string languageID = GetCurrentLanguageHelper.GetStringLanguageIDCurrent();
                string messageReturn = "";

                string alertMessage = messageReturn.GetStringResource("_ComPro.Msg.TrackingSuccess", languageID);

                _comProfileTrackingService.SendingEmailVerifyTracking(trackingReqID, languageID);

                return RedirectToAction("General", "GeneralInformation", new { alertMessage = alertMessage });

            }
            catch (Exception ex)
            {
                logger.Error(ex);

                Response.Redirect("~/Error/Error?ErrorMessage=" + ex.Message);
            }

            return RedirectToAction("General");
        }


        public ActionResult BusinessEntityPartialNew(CompanyGeneralModel model)
        {
            //,Dictionary<string,object> customAttr
            var corperate = Request.Params["CorperateValue"] as string ?? string.Empty;

            int companyTypeID = 1;
            if (!string.IsNullOrEmpty(corperate))
            {
                companyTypeID = Convert.ToInt32(corperate);
            }
            else if (model.CompanyTypeID != null)
            {
                companyTypeID = model.CompanyTypeID ?? 1;
            }

            string languageID = GetCurrentLanguageHelper.GetStringLanguageIDCurrent();

            int languageId = Convert.ToInt32(languageID);

            ViewBag.BusinessEntityList = _businessEntityRepository.GetMultiLangBusinessEntity(companyTypeID, languageId);

            var data = model.GetTrackingStatus<Tbl_Organization>(model.SupplierID);

            ViewBag.cusAttr = data;

            return PartialView(@"~/Views/GeneralInformation/_BusinessEntityPartialNew.cshtml", model);
        }

        public ActionResult ValidateTaxID(string TaxID, string CountryCode_VI, string CompanyTypeID)
        {
            string languageID = GetCurrentLanguageHelper.GetStringLanguageIDCurrent();

            bool IsValid = true;

            string messageReturn = "";

            string errorMessage = "";

            if (CountryCode_VI == "TH")
            {
                var sqlQuery = "select dbo.VerifyTaxNumber('" + TaxID + "')";
                var result = _functionRepository.GetResultByQuery(sqlQuery);

                if (result)
                {
                    return Json(IsValid, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (CompanyTypeID == "1")
                    {
                        errorMessage = messageReturn.GetStringResource("_ComPro.ValidateMsg.InvalidFormatTaxID.Corp", languageID);

                    }
                    else
                    {
                        errorMessage = messageReturn.GetStringResource("_ComPro.ValidateMsg.InvalidFormatTaxID.Ordinary", languageID);

                    }

                    return Json(errorMessage, JsonRequestBehavior.AllowGet);
                }

            }


            if (IsValid)
            {
                return Json(IsValid, JsonRequestBehavior.AllowGet);
            }

            return Json(errorMessage, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ValidatDuplicateTexID(string OldTaxID, string TaxID, string BranchNo)
        {
            string languageID = GetCurrentLanguageHelper.GetStringLanguageIDCurrent();

            bool IsValid = true;

            string errorMessage = "";

            if (OldTaxID.Trim().ToUpper() == TaxID.Trim().ToUpper())
            {
                return Json(IsValid, JsonRequestBehavior.AllowGet);
            }


            bool checkDuplicate = _organizationRepository.CheckDuplicateOrganizationByTaxIDAndBranchNo(TaxID.Trim(), BranchNo.Trim());

            if (checkDuplicate)
            {
                return Json(errorMessage, JsonRequestBehavior.AllowGet);
            }

            if (IsValid)
            {
                return Json(IsValid, JsonRequestBehavior.AllowGet);
            }

            return Json(errorMessage, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ValidateDUNSNumber(string DUNSNumber)
        {
            string languageID = GetCurrentLanguageHelper.GetStringLanguageIDCurrent();

            bool IsValid = true;

            string messageReturn = "";

            string errorMessage = "";

            if (!CryptoExtension.IsdunsNumberValid(DUNSNumber))
            {
                errorMessage = messageReturn.GetStringResource("_ComPro.ValidateMsg.InvalidFormatDUNSNumber", languageID);

                return Json(errorMessage, JsonRequestBehavior.AllowGet);
            }

            if (IsValid)
            {
                return Json(IsValid, JsonRequestBehavior.AllowGet);
            }

            return Json(errorMessage, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Document(string alertMessage = "")
        {

            string username = "";
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }

            int supplierID = _organizationRepository.GetSupplierIDByUsername(username);

            var orgModel = _organizationRepository.GetDataBySupplierID(supplierID);
            CompanyDocumentModel model = new CompanyDocumentModel();
            model.SupplierID = orgModel.SupplierID;
            model.CompanyTypeID = orgModel.CompanyTypeID;
            model.CountryCode = orgModel.CountryCode;
            model.CheckModify = "0";

            ViewBag.AlertMessage = alertMessage;
            return View(model);
        }

        public ActionResult AttachmentPartial(int CompanyTypeID, string CountryCode, int SupplierID)
        {
            var model = _customerDocTypeRepository.GetDocumentTypeByCompanyTypeIDAndCountrycode(CompanyTypeID, CountryCode);

            ViewData["SupplierID"] = SupplierID;

            return PartialView(@"~/Views/GeneralInformation/_AttachmentPartial.cshtml", model);
        }

        public ActionResult CustomerDocNamePartialNew(int DocumentTypeID, string Countrycode, int CompanyTypeID)
        {

            var model = _customerDocNameRepository.GetCustomerDocNameByDocumentTypeIDAndCountryCode(DocumentTypeID, Countrycode, CompanyTypeID);

            return PartialView(@"~/Views/GeneralInformation/_CustomerDocNamePartialNew.cshtml", model);
        }

        public ActionResult AttachmentItemListPartial(int DocumentTypeID, int SupplierID, int CompanyTypeID)
        {
            var model = _comProfileTrackingService.GetOrgAttachmentListBySupplierIDAndDocumentTypeID(SupplierID, DocumentTypeID, CompanyTypeID);

            return PartialView(@"~/Views/GeneralInformation/_AttachmentItemListPartial.cshtml", model);
        }

        [HttpPost]
        public ActionResult DocumentSave(CompanyDocumentModel model)
        {
            bool isTrackingModifyField = false;
            try
            {
                string username = "";
                if (Session["username"] != null)
                {
                    username = Session["username"].ToString();
                }
                var user = _userRepository.FindByUsername(username);
                int userID = user.UserID;
                int supplierID = user.SupplierID ?? 0;

                var documentName = _customerDocNameRepository.GetCustomerDocNameAll();

                isTrackingModifyField = _portalFieldConfigRepository.CheckPortalFieldConfigIsTrackingByFieldName("GeneralInfo.CompanyAffidavit");

                int trackingReqID = 0;
                //มีการแก้ไขไฟล์แนบ
                if (model.CheckModify == "1" && isTrackingModifyField)
                {
                    var trackingRequestModel = new Tbl_TrackingRequest()
                    {
                        SupplierID = supplierID,
                        ReqBy = userID,
                        ReqDate = DateTime.UtcNow,
                        TrackingGrpID = 2,
                        TrackingStatusID = 2,
                        TrackingStatusDate = DateTime.UtcNow,
                        Remark = "",
                        isDeleted = 0
                    };

                    trackingReqID = _trackingRequestRepository.InsertReturnTrackingReqID(trackingRequestModel, userID);
                }

                foreach (var item in documentName)
                {
                    var hidFile = Request.Params["hidFile_" + item.DocumentNameID + "[]"] ?? string.Empty;

                    var hidGuid = Request.Params["hidGuid_" + item.DocumentNameID + "[]"] ?? string.Empty;

                    var hidOtherDocument = Request.Params["hidOtherDocument_" + item.DocumentNameID + "[]"] ?? string.Empty;

                    var splitHidFile = hidFile.Split(',');

                    var splitHidGuid = hidGuid.Split(',');

                    var splitHidOtherDocument = hidOtherDocument.Split(',');

                    #region-----------------Insert Attachment------------------
                    if (splitHidFile.Length > 0 && (!string.IsNullOrEmpty(hidFile)))
                    {

                        for (var i = 0; i < splitHidFile.Length; i++)
                        {

                            string fileName = splitHidFile[i];

                            string guidName = splitHidGuid[i];

                            string otherDocument = "";

                            if (item.DocumentTypeID == -1)
                            {
                                otherDocument = splitHidOtherDocument[i];
                            }

                            UploadAttachment(fileName, guidName, model.SupplierID, item.DocumentNameID, otherDocument, trackingReqID, userID);

                        }
                    }
                    #endregion End Insert Attachment

                }


                #region-----------------Remove Attachment------------------
                var hidRemoveFile = Request.Params["hidRemoveGuid[]"] ?? string.Empty;

                var splitHidRemoveFile = hidRemoveFile.Split(',');

                if (splitHidRemoveFile.Length > 0 && (!string.IsNullOrEmpty(hidRemoveFile)))
                {
                    foreach (var itemlist in splitHidRemoveFile)
                    {
                        if (isTrackingModifyField)
                        {
                            TrackingRemoveAttachment(itemlist, trackingReqID, userID);
                        }
                        else
                        {
                            RemoveAttachment(itemlist, userID);
                        }


                    }
                }
                #endregion End Remove Attachment


                #region-----------------Remove Attachment By ChangedDocumentType------------------
                //var listRemove = _customerDocTypeRepository.GetDocumentTypeForRemove(model.CompanyTypeID ?? 1);

                var listRemove = _customerDocNameRepository.GetCustomerDocNameForRemoveByCountryCode(model.CompanyTypeID ?? 1, model.CountryCode);

                foreach (var listDoc in listRemove)
                {
                    var listRegAttachment = _orgAttachmentRepository.GetOrgAttachmentBySupplierIDAndDocumentNameID(model.SupplierID, listDoc.DocumentNameID);

                    foreach (var listattachment in listRegAttachment)
                    {

                        if (isTrackingModifyField)
                        {
                            TrackingRemoveAttachment(listattachment.AttachmentNameUnique, trackingReqID, userID);
                        }
                        else
                        {
                            RemoveAttachment(listattachment.AttachmentNameUnique, userID);
                        }


                    }
                }
                #endregion End Remove Attachment By ChangedDocumentType

                //End Remove Attachment By ChangedDocumentType

                #region Update Tbl_Organization.LastUpdate

                _organizationRepository.UpdateLastUpdate(supplierID, userID);

                #endregion

                if (model.CheckModify == "1" && isTrackingModifyField)
                {
                    string languageID = GetCurrentLanguageHelper.GetStringLanguageIDCurrent();
                    string messageReturn = "";

                    string alertMessage = messageReturn.GetStringResource("_ComPro.Msg.TrackingSuccess", languageID);

                    _comProfileTrackingService.SendingEmailVerifyTracking(trackingReqID, languageID);


                    return RedirectToAction("Document", "GeneralInformation", new { alertMessage = alertMessage });
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex);

                Response.Redirect("~/Error/Error?ErrorMessage=" + ex.Message);
            }


            return RedirectToAction("Document");
        }

        [HttpPost]
        public ActionResult UploadFileAttachment(FormCollection collection)
        {
            string languageID = GetCurrentLanguageHelper.GetStringLanguageIDCurrent();

            string hidDocumentTypeID = collection["hidDocumentTypeID"];

            string attachmentName = "FileAttachment_" + hidDocumentTypeID;

            string tbxdocname = collection["tbx-doc-name_" + hidDocumentTypeID];

            string hidDocumentNameID = collection["hidDocumentNameID"];

            string customerDocName = "";

            if (!string.IsNullOrEmpty(hidDocumentNameID))
            {
                customerDocName = _customerDocNameRepository.GetCustomerDocNameByDocumentNameID(Convert.ToInt32(hidDocumentNameID));
            }


            string fileName = "";

            string fileGuidName = "";

            string htmlText = "";

            HttpPostedFileBase myFile = Request.Files[attachmentName];
            bool isUploaded = false;
            string message = "File upload failed";

            if (myFile != null && myFile.ContentLength != 0)
            {
                string fileSize = this.GetFileSize(myFile.ContentLength);

                string pathTemp = _appConfigRepository.GetAppConfigByName("Portal_AttachmentPathTemp").Value;
                string pathAttachment = WebConfigurationManager.AppSettings["pathAttachment"];
                string pathTempSupplier = "Supplier";

                string pathForSaving = pathAttachment + pathTemp + pathTempSupplier;
                string pathForDownload = pathTemp + pathTempSupplier;
                if (this.CreateFolderIfNeeded(pathForSaving))
                {
                    try
                    {
                        string guid = SupplierPortal.Framework.MethodHelper.GetGenerateGuidHelper.GetGenerateRandomGuid();

                        fileGuidName = guid + myFile.FileName.Substring(myFile.FileName.LastIndexOf('.'));

                        myFile.SaveAs(Path.Combine(pathForSaving, fileGuidName));

                        string virtualFilePath = pathForDownload + "/" + fileGuidName;

                        isUploaded = true;
                        message = "File uploaded successfully!";
                        fileName = myFile.FileName;
                        //fileGuidName = SupplierPortal.Framework.MethodHelper.GetGenerateGuidHelper.GetGenerateRandomGuid();
                        htmlText = GetHtmlText(hidDocumentNameID, hidDocumentTypeID, fileName, virtualFilePath, fileGuidName, guid, tbxdocname, fileSize, customerDocName);
                    }
                    catch (Exception ex)
                    {
                        message = string.Format("File upload failed: {0}", ex.Message);
                    }
                }
            }
            return Json(new
            {
                isUploaded = isUploaded,
                message = message,
                documentTypeID = hidDocumentTypeID,
                documentNameID = hidDocumentNameID,
                fileName = fileName,
                htmlText = htmlText,
                fileGuidName = fileGuidName
            }, "text/html");
        }

        public ActionResult Download(string filePath, string fileName = "")
        {
            string pathAttachment = WebConfigurationManager.AppSettings["pathAttachment"];
            filePath = pathAttachment + filePath;

            if (!System.IO.File.Exists(@filePath))
            {
                filePath = @pathAttachment + @"\SWWDocuments\UserManualForDownload\EPInstruction.pdf";
            }
            byte[] fileBytes = System.IO.File.ReadAllBytes(@filePath);

            if (string.IsNullOrEmpty(fileName))
            {
                fileName = "download";
            }

            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        public ActionResult ViewDocumentTracking(int trackingItemID)
        {
            var trackingOrgAttachmentModel = _trackingOrgAttachmentRepository.GetTrackingOrgAttachmentByTrackingItemID(trackingItemID);


            return PartialView(@"~/Views/GeneralInformation/_DocumentTrackingPartial.cshtml");
        }

        #region------------------------------------------------Function Helper------------------------------------------------------

        private string GetFileSize(int contentLength)
        {

            string[] sizes = { "B", "KB", "MB", "GB" };
            int len = contentLength;
            int order = 0;
            while (len >= 1024 && order + 1 < sizes.Length)
            {
                order++;
                len = len / 1024;
            }

            // Adjust the format string to your preferences. For example "{0:0.#}{1}" would
            // show a single decimal place, and no space.
            string result = String.Format("{0:0.##} {1}", len, sizes[order]);
            return result;
        }

        private bool CreateFolderIfNeeded(string path)
        {
            bool result = true;
            if (!Directory.Exists(path))
            {
                try
                {
                    Directory.CreateDirectory(path);
                }
                catch (Exception)
                {
                    /*TODO: You must process this exception.*/
                    result = false;
                }
            }
            return result;
        }

        private string GetHtmlText(string documentNameID, string documentTypeID, string fileName, string virtualFilePath, string fileGuidName, string guid, string tbxdocname = "", string fileSize = "", string customerDocName = "")
        {

            string link = HtmlHelper.GenerateLink(this.ControllerContext.RequestContext, System.Web.Routing.RouteTable.Routes, fileName, "Default", "Download", "GeneralInformation", new RouteValueDictionary(new { filePath = virtualFilePath, fileName = fileName }), new Dictionary<string, object> { { "class", "register" } });

            string linkDelete = "<img src=\"/Content/images/icon/ico-sp_deluser2.gif\" alt=\"Submit\" id=\"deleteAttachment\" data-toggle=\"tooltip\" data-placement=\"left\" title=\"Delete file\" docType = " + documentTypeID + "  class = \"iconDeleteDoc\">";

            string docName = "";

            if (documentNameID == "-1")
            {
                docName = tbxdocname;
            }
            else
            {
                docName = customerDocName;
            }
            string otherDocumentType = tbxdocname;

            string htmlTextTable = "";
            htmlTextTable += "<tr class=\"main_table_row docName_" + documentNameID + "\">";
            htmlTextTable += "<td align=\"left\" class=\"newAttachment\">";
            htmlTextTable += "<div  class=\"file-attachment-download\">" + link + "</div>";
            htmlTextTable += "<input type=\"hidden\" name=\"hidFile_" + documentNameID + "[]\" value=\"" + fileName + "\" />";
            htmlTextTable += "<input type=\"hidden\" name=\"hidGuid_" + documentNameID + "[]\" value=\"" + fileGuidName + "\" />";
            htmlTextTable += "<input type=\"hidden\" name=\"hidOtherDocument_" + documentNameID + "[]\" value=\"" + otherDocumentType + "\" />";
            htmlTextTable += "</td>";
            htmlTextTable += "<td align=\"right\">" + fileSize + "</td>";
            htmlTextTable += "<td align=\"left\">" + docName + "</td>";
            htmlTextTable += "<td align=\"center\">" + linkDelete + "</td>";
            htmlTextTable += "<\tr>";

            return htmlTextTable;
        }

        private void UploadAttachment(string fileName, string guidName, int supplierID, int documentNameID, string otherDocument, int trackingReqID, int updateBy)
        {
            try
            {
                if (!_orgAttachmentRepository.OrgAttachmentExists(guidName, supplierID))
                {
                    string username = "";
                    if (Session["username"] != null)
                    {
                        username = Session["username"].ToString();
                    }
                    var user = _userRepository.FindByUsername(username);
                    int userID = user.UserID;

                    string pathTemp = _appConfigRepository.GetAppConfigByName("Portal_AttachmentPathTemp").Value;
                    string pathAttachment = WebConfigurationManager.AppSettings["pathAttachment"];

                    string pathTempSupplier = "Supplier";

                    string pathForSavingTemp = pathAttachment + pathTemp + pathTempSupplier;
                    //string pathForSavingTemp = Server.MapPath("~/Register_AttachmentPathTemp/" + regID);

                    string filePathTemp = pathForSavingTemp + "/" + guidName;

                    if (System.IO.File.Exists(filePathTemp))
                    {

                        byte[] fileBytes = System.IO.File.ReadAllBytes(filePathTemp);


                        string virtualFilePath = filePathTemp;
                        string fileName2 = Path.GetFileName(virtualFilePath);
                        var fileTemp = File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName2);

                        if (fileTemp != null && fileTemp.FileContents.Length != 0)
                        {
                            string pathSave = _appConfigRepository.GetAppConfigByName("Portal_SupplierDocumentPath").Value;
                            string pathForSaving = pathAttachment + pathSave + supplierID;
                            //string pathForSaving = Server.MapPath("~/Register_AttachmentPath/" + regID);

                            string filePath = pathForSaving + "/" + guidName;

                            if (this.CreateFolderIfNeeded(pathForSaving))
                            {
                                // Ensure that the target does not exist. 
                                if (System.IO.File.Exists(filePath))
                                    System.IO.File.Delete(filePath);

                                // Move the file.
                                System.IO.File.Move(filePathTemp, filePath);

                                var models = new Tbl_OrgAttachment
                                {
                                    SupplierID = supplierID,
                                    AttachmentName = fileName,
                                    AttachmentNameUnique = guidName,
                                    SizeAttach = fileTemp.FileContents.Length.ToString(),
                                    DocumentNameID = documentNameID,
                                    OtherDocument = otherDocument,
                                    isDeleted = 0

                                };

                                //_orgAttachmentRepository.Insert(models);

                                int attachmentID = _orgAttachmentRepository.InsertReturnAttachmentID(models, updateBy);

                                //trackingReqID != 0 แสดงว่า มีการ Tracking ใน Tbl_PortalFieldConfig ให้ทำกระบวนการ Tracking
                                if (trackingReqID != 0)
                                {
                                    //#region-----------------Check Tracking DocumentNameID------------------

                                    //#endregion End
                                    TrackingInsertAttachment(attachmentID, trackingReqID, userID);


                                }



                                //_logOrgAttachmentRepository.InsertLogAdd(supplierID, guidName, "Add");

                            }


                        }

                    }

                }

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void RemoveAttachment(string guidName, int updateBy)
        {
            try
            {

                var attachmentModel = _orgAttachmentRepository.GetOrgAttachmentByGuidName(guidName);

                if (attachmentModel != null)
                {
                    string pathSave = _appConfigRepository.GetAppConfigByName("Portal_SupplierDocumentPath").Value;
                    string pathDelete = _appConfigRepository.GetAppConfigByName("Portal_SupplierDocumentDelPath").Value;
                    string pathAttachment = WebConfigurationManager.AppSettings["pathAttachment"];
                    string pathForSaving = pathAttachment + pathSave + attachmentModel.SupplierID;
                    string pathForDelete = pathAttachment + pathDelete + attachmentModel.SupplierID;
                    //string pathForSaving = Server.MapPath("~/Register_AttachmentPath/" + attachmentModel.RegID);

                    string filePath = pathForSaving + "/" + guidName;

                    if (System.IO.File.Exists(filePath))
                    {
                        byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);

                        string virtualFilePath = filePath;
                        string fileName2 = Path.GetFileName(virtualFilePath);

                        var fileTemp = File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName2);

                        if (fileTemp != null && fileTemp.FileContents.Length != 0)
                        {

                            string filePathDel = pathForDelete + "/" + guidName;

                            if (this.CreateFolderIfNeeded(pathForDelete))
                            {
                                // Ensure that the target does not exist. 
                                if (System.IO.File.Exists(filePathDel))
                                    System.IO.File.Delete(filePathDel);

                                // Move the file.
                                System.IO.File.Move(filePath, filePathDel);
                            }
                        }
                    }

                    //_logOrgAttachmentRepository.InsertLogDelete(attachmentModel.SupplierID, attachmentModel.AttachmentID, "Delete");
                    _orgAttachmentRepository.DeleteUpdateFlag(attachmentModel.SupplierID, attachmentModel.AttachmentID, updateBy);

                }

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void TrackingRemoveAttachment(string guidName, int trackingReqID, int updateBy)
        {
            try
            {
                var attachmentModel = _orgAttachmentRepository.GetOrgAttachmentByGuidName(guidName);

                if (attachmentModel != null)
                {
                    int attachSupplierID = attachmentModel.SupplierID;
                    int attachDocumentNameID = attachmentModel.DocumentNameID ?? 0;

                    var orgAttachmentList = _orgAttachmentRepository.GetOrgAttachmentBySupplierIDAndDocumentNameID(attachSupplierID, attachDocumentNameID);

                    //*** โดยที่ DocumentNameID != -1 ถ้า DocumentNameID == -1 จะเป็นอีกกรณี
                    if (attachDocumentNameID != -1)
                    {
                        if (orgAttachmentList.Count() == 1)
                        {
                            //ลบโดยที่ไม่ได้เพิ่ม DocumentNameID ใหม่ กรณีที่มีการเพิ่มใหม่ ค่านี้จะถูกเพิ่มในฟังชั่น TrackingInsertAttachment  (จะไม่มีทางเป็น 0 เพราะจะมีอย่างน้อย 1 ตัวถึงจะกดลบได้)

                            var trackingModel = new Tbl_TrackingOrgAttachment()
                            {
                                TrackingReqID = trackingReqID,
                                OldAttachmentID = attachmentModel.AttachmentID,
                                NewAttachmentID = 0,
                                TrackingStatusID = 2,
                                TrackingStatusDate = DateTime.UtcNow,
                                Remark = "",
                                DocumentNameID = attachmentModel.DocumentNameID
                            };

                            _trackingOrgAttachmentRepository.Insert(trackingModel, updateBy);

                        }
                    }
                    else
                    {
                        //กรณีที่ DocumentNameID == -1 จะเพิ่มได้หลายไฟล์ ก็จะ Update ให้ NewAttachmentID == 0
                        var trackingModel = new Tbl_TrackingOrgAttachment()
                        {
                            TrackingReqID = trackingReqID,
                            OldAttachmentID = attachmentModel.AttachmentID,
                            NewAttachmentID = 0,
                            TrackingStatusID = 2,
                            TrackingStatusDate = DateTime.UtcNow,
                            Remark = "",
                            DocumentNameID = attachmentModel.DocumentNameID
                        };

                        _trackingOrgAttachmentRepository.Insert(trackingModel, updateBy);

                    }

                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void TrackingInsertAttachment(int attachmentID, int trackingReqID, int updateBy)
        {
            try
            {
                var orgAttachmentModel = _orgAttachmentRepository.GetOrgAttachmentByAttachmentID(attachmentID);

                if (orgAttachmentModel != null)
                {
                    int attachSupplierID = orgAttachmentModel.SupplierID;
                    int attachDocumentNameID = orgAttachmentModel.DocumentNameID ?? 0;

                    //*** โดยที่ DocumentNameID != -1 ถ้า DocumentNameID == -1 จะเป็นอีกกรณี
                    if (attachDocumentNameID != -1)
                    {
                        var orgAttachmentList = _orgAttachmentRepository.GetOrgAttachmentBySupplierIDAndDocumentNameID(attachSupplierID, attachDocumentNameID);
                        if (orgAttachmentList.Count() == 1)
                        {
                            // เพิ่มเข้ามาใหม่โดยที่ยังไม่มี DocumentNameID ใน Supplier นี้มาก่อน case เพิ่มใหม่จากไม่เคยมี
                            var trackingModel = new Tbl_TrackingOrgAttachment()
                            {
                                TrackingReqID = trackingReqID,
                                OldAttachmentID = 0,
                                NewAttachmentID = attachmentID,
                                TrackingStatusID = 2,
                                TrackingStatusDate = DateTime.UtcNow,
                                Remark = "",
                                DocumentNameID = attachDocumentNameID
                            };
                            _trackingOrgAttachmentRepository.Insert(trackingModel, updateBy);

                        }
                        else
                        {
                            // มีมากกว่า 1 แสดงว่า เคยมี DocumentNameID ใน Supplier มาก่อน case ลบแล้วเพิ่มเข้ามาใหม่ (จะไม่มีทางเป็น 0 เพราะจะมีอย่างน้อย 1 ตัวที่เพิ่งเพิ่ม)
                            var oldOrgAttachmentModel = orgAttachmentList.Where(m => m.AttachmentID != attachmentID).FirstOrDefault();
                            var trackingModel = new Tbl_TrackingOrgAttachment()
                            {
                                TrackingReqID = trackingReqID,
                                OldAttachmentID = oldOrgAttachmentModel.AttachmentID,
                                NewAttachmentID = attachmentID,
                                TrackingStatusID = 2,
                                TrackingStatusDate = DateTime.UtcNow,
                                Remark = "",
                                DocumentNameID = attachDocumentNameID
                            };
                            _trackingOrgAttachmentRepository.Insert(trackingModel, updateBy);

                        }
                    }
                    else
                    {
                        //ถ้าเป็นกรณี DocumentNameID == -1 แล้วเพิ่มใหม่เข้ามา จะเป็นการเพิ่ม TrackingItem ภายใต้ trackingReqID นี้
                        var trackingModel = new Tbl_TrackingOrgAttachment()
                        {
                            TrackingReqID = trackingReqID,
                            OldAttachmentID = 0,
                            NewAttachmentID = attachmentID,
                            TrackingStatusID = 2,
                            TrackingStatusDate = DateTime.UtcNow,
                            Remark = "",
                            DocumentNameID = attachDocumentNameID
                        };
                        _trackingOrgAttachmentRepository.Insert(trackingModel, updateBy);
                    }

                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        #endregion


    }
}

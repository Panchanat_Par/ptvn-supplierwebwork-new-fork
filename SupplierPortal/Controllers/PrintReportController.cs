﻿using SupplierPortal.Models;
using SupplierPortal.ReportSupport;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SupplierPortal.Controllers
{
    public class PrintReportController : Controller
    {
        // GET: PrintReport

        public ActionResult Index()
        {
            return View();
        }

        #region No. of PO (3D Pie Chart)

        [ChildActionOnly]
        public PartialViewResult _NumPO(int year, string buyerGroupName)
        {
            //3D Pie Chart
            DataTable dtNumPO_Chart;

            string chart_NumPO;

            DateTime dt = DateTime.Now;
            if (year == -1)
            {
                year = dt.Year;
            }

            ClsReportChart oRptChart = new ClsReportChart();
            dtNumPO_Chart = oRptChart.getDtNumPO_Chart(Session["suppShortName"].ToString(), buyerGroupName, year);
            chart_NumPO = dtNumPO_Chart.Rows[0]["NumPO_Chart"].ToString();

            DataTable dtNumPO;
            dtNumPO = oRptChart.getDtNumPO(Session["suppShortName"].ToString(), buyerGroupName, year);
            int numPORound = dtNumPO.Rows.Count;

            //ChartsModels_NumPO model = new ChartsModels_NumPO();
            ChartsModels_NumPO model = new ChartsModels_NumPO
            {
                data_NumPO = Enumerable.Range(1, numPORound).Select(x => new NumPO
                {
                    //buyerGroup = Convert.ToString(dtNumPO.Rows[x - 1]["BuyerGroup"]),
                    //buyerName = Convert.ToString(dtNumPO.Rows[x - 1]["BuyerName"]),
                    buyer = Convert.ToString(dtNumPO.Rows[x - 1]["Buyer"]),
                    numPO = String.Format("{0:#,##0}", dtNumPO.Rows[x - 1]["NumPO"]),
                    percentNumPO = Convert.ToDecimal(dtNumPO.Rows[x - 1]["PercentNumPO"])
                })
            };
            model.json_NumPO = chart_NumPO;

            //return PartialView("_NumPO", model);
            return PartialView(@"~/Views/PrintReport/_NumPO.cshtml", model);
        }

        [HttpPost]
        public PartialViewResult _NumPO(DashboardModels dModel)
        {
            //3D Pie Chart
            DataTable dtNumPO_Chart;
            string chart_NumPO;

            ClsReportChart oRptChart = new ClsReportChart();
            dtNumPO_Chart = oRptChart.getDtNumPO_Chart(Session["suppShortName"].ToString(), dModel.selectedBuyerGrp, dModel.selectedYear);

            chart_NumPO = dtNumPO_Chart.Rows[0]["NumPO_Chart"].ToString();

            DataTable dtNumPO;
            dtNumPO = oRptChart.getDtNumPO(Session["suppShortName"].ToString(), dModel.selectedBuyerGrp, dModel.selectedYear);
            int numPORound = dtNumPO.Rows.Count;

            //ChartsModels_NumPO model = new ChartsModels_NumPO();
            ChartsModels_NumPO model = new ChartsModels_NumPO
            {
                data_NumPO = Enumerable.Range(1, numPORound).Select(x => new NumPO
                {
                    //buyerGroup = Convert.ToString(dtNumPO.Rows[x - 1]["BuyerGroup"]),
                    //buyerName = Convert.ToString(dtNumPO.Rows[x - 1]["BuyerName"]),
                    buyer = Convert.ToString(dtNumPO.Rows[x - 1]["Buyer"]),
                    numPO = String.Format("{0:#,##0}", dtNumPO.Rows[x - 1]["NumPO"]),
                    percentNumPO = Convert.ToDecimal(dtNumPO.Rows[x - 1]["PercentNumPO"])
                })
            };
            model.json_NumPO = chart_NumPO;
            //return PartialView("_NumPO", model);
            return PartialView(@"~/Views/PrintReport/_NumPO.cshtml", model);
        }

        #endregion

        #region PO Volume (3D Pie Chart)

        [ChildActionOnly]
        public PartialViewResult _POVolume(int year, string buyerGroupName)
        {
            //3D Pie Chart
            DataTable dtPOVolume_Chart;
            string chart_POVolume;

            DateTime dt = DateTime.Now;
            if (year == -1)
            {
                year = dt.Year;
            }

            ClsReportChart oRptChart = new ClsReportChart();
            dtPOVolume_Chart = oRptChart.getDtPOVolume_Chart(Session["suppShortName"].ToString(), buyerGroupName, year);

            chart_POVolume = dtPOVolume_Chart.Rows[0]["POVolume_Chart"].ToString();

            DataTable dtPOVolume;
            dtPOVolume = oRptChart.getDtPOVolume(Session["suppShortName"].ToString(), buyerGroupName, year);
            int POVolumeRound = dtPOVolume.Rows.Count;

            //ChartsModels_NumPO model = new ChartsModels_NumPO();
            ChartsModels_POVolume model = new ChartsModels_POVolume
            {
                data_POVolume = Enumerable.Range(1, POVolumeRound).Select(x => new POVolume
                {
                    buyer = Convert.ToString(dtPOVolume.Rows[x - 1]["Buyer"]),
                    volume = String.Format("{0:#,##0.00}", dtPOVolume.Rows[x - 1]["POVolume"]),
                    percentVolume = Convert.ToDecimal(dtPOVolume.Rows[x - 1]["PercentVolume"])
                })
            };
            model.json_POVolume = chart_POVolume;

           // return PartialView("_POVolume", model);
            return PartialView(@"~/Views/PrintReport/_POVolume.cshtml", model);
        }

        [HttpPost]
        public PartialViewResult _POVolume(DashboardModels dModel)
        {
            //3D Pie Chart
            DataTable dtPOVolume_Chart;
            string chart_POVolume;

            ClsReportChart oRptChart = new ClsReportChart();
            dtPOVolume_Chart = oRptChart.getDtPOVolume_Chart(Session["suppShortName"].ToString(), dModel.selectedBuyerGrp, dModel.selectedYear);

            chart_POVolume = dtPOVolume_Chart.Rows[0]["POVolume_Chart"].ToString();

            DataTable dtPOVolume;
            dtPOVolume = oRptChart.getDtPOVolume(Session["suppShortName"].ToString(), dModel.selectedBuyerGrp, dModel.selectedYear);
            int POVolumeRound = dtPOVolume.Rows.Count;

            //ChartsModels_NumPO model = new ChartsModels_NumPO();
            ChartsModels_POVolume model = new ChartsModels_POVolume
            {
                data_POVolume = Enumerable.Range(1, POVolumeRound).Select(x => new POVolume
                {
                    buyer = Convert.ToString(dtPOVolume.Rows[x - 1]["Buyer"]),
                    volume = String.Format("{0:#,##0.00}", dtPOVolume.Rows[x - 1]["POVolume"]),
                    percentVolume = Convert.ToDecimal(dtPOVolume.Rows[x - 1]["PercentVolume"])
                })
            };
            model.json_POVolume = chart_POVolume;
            //return PartialView("_POVolume", model);
            return PartialView(@"~/Views/PrintReport/_POVolume.cshtml", model);
        }

        #endregion

        #region PO Volume by Status (3D Pie Chart)

        [ChildActionOnly]
        public PartialViewResult _POStatus(int year, string buyerGroupName)
        {
            //3D Pie Chart
            DataTable dtPOStatus_Chart;
            string chart_POStatus;

            DateTime dt = DateTime.Now;
            if (year == -1)
            {
                year = dt.Year;
            }

            ClsReportChart oRptChart = new ClsReportChart();
            dtPOStatus_Chart = oRptChart.getDtPOStatus_Chart(Session["suppShortName"].ToString(), buyerGroupName, year);

            chart_POStatus = dtPOStatus_Chart.Rows[0]["POVolumeByStatus_Chart"].ToString();

            DataTable dtPOStatus;
            dtPOStatus = oRptChart.getDtPOStatus(Session["suppShortName"].ToString(), buyerGroupName, year);
            int POStatusRound = dtPOStatus.Rows.Count;

            //ChartsModels_NumPO model = new ChartsModels_NumPO();
            ChartsModels_POStatus model = new ChartsModels_POStatus
            {
                data_POStatus = Enumerable.Range(1, POStatusRound).Select(x => new POStatus
                {
                    status = Convert.ToString(dtPOStatus.Rows[x - 1]["StatusName"]),
                    volume = String.Format("{0:#,##0.00}", dtPOStatus.Rows[x - 1]["POVolume"]),
                    numPO = String.Format("{0:#,##0}", dtPOStatus.Rows[x - 1]["NumPO"]),
                    percentNumPO = Convert.ToDecimal(dtPOStatus.Rows[x - 1]["PercentNumPO"])
                })
            };
            model.json_POStatus = chart_POStatus;

            //return PartialView("_POStatus", model);
            return PartialView(@"~/Views/PrintReport/_POStatus.cshtml", model);
        }

        [HttpPost]
        public PartialViewResult _POStatus(DashboardModels dModel)
        {
            //3D Pie Chart
            DataTable dtPOStatus_Chart;
            string chart_POStatus;

            ClsReportChart oRptChart = new ClsReportChart();
            dtPOStatus_Chart = oRptChart.getDtPOStatus_Chart(Session["suppShortName"].ToString(), dModel.selectedBuyerGrp, dModel.selectedYear);

            chart_POStatus = dtPOStatus_Chart.Rows[0]["POVolumeByStatus_Chart"].ToString();

            DataTable dtPOStatus;
            dtPOStatus = oRptChart.getDtPOStatus(Session["suppShortName"].ToString(), dModel.selectedBuyerGrp, dModel.selectedYear);
            int POStatusRound = dtPOStatus.Rows.Count;

            //ChartsModels_NumPO model = new ChartsModels_NumPO();
            ChartsModels_POStatus model = new ChartsModels_POStatus
            {
                data_POStatus = Enumerable.Range(1, POStatusRound).Select(x => new POStatus
                {
                    status = Convert.ToString(dtPOStatus.Rows[x - 1]["StatusName"]),
                    volume = String.Format("{0:#,##0.00}", dtPOStatus.Rows[x - 1]["POVolume"]),
                    numPO = String.Format("{0:#,##0}", dtPOStatus.Rows[x - 1]["NumPO"]),
                    percentNumPO = Convert.ToDecimal(dtPOStatus.Rows[x - 1]["PercentNumPO"])
                })
            };
            model.json_POStatus = chart_POStatus;
            //return PartialView("_POStatus", model);
            return PartialView(@"~/Views/PrintReport/_POStatus.cshtml", model);
        }

        #endregion

        #region Support Decryption

        //Function for Convert Hex String to Byte Array
        public static byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }

        //DecryptString
        private static string DecryptStringFromBytes(byte[] SID, byte[] key)
        {
            // Check arguments. 
            if (SID == null || SID.Length <= 0)
                throw new ArgumentNullException("SID");
            if (key == null || key.Length <= 0)
                throw new ArgumentNullException("key");

            byte[] DataToDecrypt = SID;

            var objTripleDESCryptoService = new TripleDESCryptoServiceProvider();

            objTripleDESCryptoService.Key = key;
            objTripleDESCryptoService.Mode = CipherMode.ECB;
            objTripleDESCryptoService.Padding = PaddingMode.PKCS7;

            var objCrytpoTransform = objTripleDESCryptoService.CreateDecryptor();
            byte[] resultArray = objCrytpoTransform.TransformFinalBlock(DataToDecrypt, 0, DataToDecrypt.Length);
            objTripleDESCryptoService.Clear();

            //Convert and return the decrypted data/byte into string format.
            return UTF8Encoding.UTF8.GetString(resultArray);

        }

        #endregion


        public ActionResult TimeOutError()
        {
            //return PartialView("TimeOutError");
            return PartialView(@"~/Views/Shared/TimeOutError.cshtml");
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SupplierPortal.Data.Models.Repository.Organization;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.CompanyProfile;
using SupplierPortal.Data.Models.Repository.User;

namespace SupplierPortal.Controllers
{
    public class FooterPartialController : Controller
    {
        IUserRepository _userRepository;
        IOrganizationRepository _organizationRepository;
        //private SupplierPortalEntities context;
        // GET: _FooterPartial
        public FooterPartialController()
        {
            _organizationRepository = new OrganizationRepository();
            //context = new SupplierPortalEntities();
            _userRepository = new UserRepository();
        }
        //SupplierPortalEntities context, 
        public FooterPartialController(OrganizationRepository organizationRepository, UserRepository userRepository)
        {
            //this.context = context;
            _userRepository = userRepository;
            _organizationRepository = organizationRepository;
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult FooterPortalPartial()
        {
            string username = "Metro-Admin";
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }
            int supID = _organizationRepository.GetSupplierIDByUsername(username);

            var orgModel = _organizationRepository.GetDataBySupplierID(supID);
            var model = new GeneralInfosModel();
            model.SupplierID = orgModel.SupplierID;
            model.isPTVNVerified = orgModel.isPTVNVerified;
            return PartialView(@"~/Views/FooterPartial/_FooterPortalPartial.cshtml", model);
            //   return View();
        }

        [HttpPost]
        public ActionResult _FooterPartial(GeneralInfosModel model, Tbl_Organization Orgmodel)
        {
            var orgModel = _organizationRepository.GetDataBySupplierID(Orgmodel.SupplierID);
            int userID = 0;
            string username = ""; //Metro-Admin
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }
            var user = _userRepository.FindByUsername(username);
            if (user != null)
            {
                userID = user.UserID;
            }

            if (orgModel != null)
            {
                orgModel.isPTVNVerified = Orgmodel.isPTVNVerified;
            }


            //var m = new GeneralInfosModel();          
            //var Orgmodel = _organizationRepository.GetDataBySupplierID(model.SupplierID);
            //m.SupplierID = Orgmodel.SupplierID;
            return View(@"~/Views/FooterPartial/_FooterPartial.cshtml", model);
        }
    }
}
﻿using Microsoft.Owin.Security;
using SupplierPortal.Core.Caching;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using SupplierPortal.ServiceClients;



namespace SupplierPortal.Controllers
{
    public class BillingConditionController: BaseController
    {
        private readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly ICacheManager _cacheManager;
        private readonly UserBillingConditionService _userBillingConditionService;

        public BillingConditionController()
        {
            _cacheManager = new MemoryCacheManager();
            _userBillingConditionService = new UserBillingConditionService();
        }

        public IAuthenticationManager Authentication
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        [System.Web.Mvc.AllowAnonymous]
        public async Task<ActionResult> AcceptBilling()
        {
            string guid = Session["GUID"].ToString();
           
            bool result = await _userBillingConditionService.UpdateBillingCondition(guid);
            Session["isAcceptBillingCondition"] = result;

            return Json(true);
        }


        [System.Web.Mvc.AllowAnonymous]
        public ActionResult UpdateSeesionBilling()
        {
            Session["isAcceptBillingCondition"] = true;
            return Json(true);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SupplierPortal.Data.Models.SupportModel.Register;
using SupplierPortal.Data.Models.Repository.BusinessEntity;
using SupplierPortal.Data.Models.Repository.Country;
using SupplierPortal.Data.Models.Repository.AppConfig;
using SupplierPortal.Data.Models.Repository.NameTitle;
using SupplierPortal.Data.Models.Repository.JobTitle;
using SupplierPortal.Data.Models.Repository.AutoComplete_State;
using SupplierPortal.Data.Models.Repository.CustomerDocType;
using SupplierPortal.Data.Models.Repository.CustomerDocName;
using SupplierPortal.Data.Models.Repository.RegEmailTicket;
using SupplierPortal.Data.Models.Repository.RegContactRequest;
using SupplierPortal.Data.Models.Repository.ServiceType;
using SupplierPortal.Data.Models.Repository.RegContactRequestServiceTypeMapping;
using SupplierPortal.Data.Models.Repository.RegAttachment;
using SupplierPortal.Data.Models.Repository.LogRegAttachment;
using SupplierPortal.Data.Models.Repository.Language;
using SupplierPortal.Data.Models.Repository.RegAddress;
using SupplierPortal.Data.Models.Repository.RegContact;
using SupplierPortal.Data.Models.Repository.RegInfo;
using SupplierPortal.Data.Models.Repository.RegServiceTypeMapping;
using SupplierPortal.Data.Models.Repository.RegContactAddressMapping;
using SupplierPortal.Data.Models.Repository.OPAdoptionNotify;
using SupplierPortal.Framework.Localization;
using SupplierPortal.Data.Models.Repository.Function;
using SupplierPortal.Data.Models.Repository.LogRegContactRequest;
using SupplierPortal.Data.Models.Repository.RegContactRequestHistory;
using SupplierPortal.Data.Models.Repository.Organization;
using System.IO;
using System.Web.Routing;
using SupplierPortal.Services.RegisterPortalManage;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Core.Extension;
using System.Collections;
using CaptchaMvc.Attributes;
using CaptchaMvc.HtmlHelpers;
using CaptchaMvc.Interface;
using SupplierPortal.Data.Models.SupportModel.AccountPortal;
using System.Net.Http;
using System.Web.Configuration;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SupplierPortal.Framework.MethodHelper;
using SupplierPortal.Data.Models.Repository.RegBusinessType;
using SupplierPortal.Data.Models.Repository.BusinessType;
using SupplierPortal.Data.Models.Repository.RegProductKeyword;
using SupplierPortal.Data.Models.Repository.ProductCategory;
using SupplierPortal.Data.Models.Repository.RegisProductCategory;
using System.Globalization;
using SupplierPortal.Data.Models.Repository.ADSBuyer;
using SupplierPortal.Data.Models.SupportModel;
using SupplierPortal.Data.Models.Repository.Currency;
using SupplierPortal.ServiceClients;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.Repository.OPNBuyer;
using System.IdentityModel.Tokens.Jwt;
using SupplierPortal.MainFunction.Crypto;

namespace SupplierPortal.Controllers
{
    public class RegisterController : BaseController
    {
        private readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private IBusinessEntityRepository _businessEntityRepository;
        private ICountryRepository _countryRepository;
        private IFunctionRepository _functionRepository;
        private IAppConfigRepository _appConfigRepository;
        private INameTitleRepository _nameTitleRepository;
        private IJobTitleRepository _jobTitleRepository;
        private IAutoComplete_StateRepository _autoComplete_StateRepository;
        private ICustomerDocTypeRepository _customerDocTypeRepository;
        private ICustomerDocNameRepository _customerDocNameRepository;
        private IRegEmailTicketRepository _regEmailTicketRepository;
        private IRegisterDataService _registerDataService;
        private IRegContactRequestRepository _regContactRequestRepository;
        private IServiceTypeRepository _serviceTypeRepository;
        private IRegContactRequestServiceTypeMappingRepository _regContactReqServMapping;
        private IRegAttachmentRepository _regAttachmentRepository;
        private ILogRegAttachmentRepository _logRegAttachmentRepository;
        private ILanguageRepository _languageRepository;
        private IRegAddressRepository _regAddressRepository;
        private IRegContactRepository _regContactRepository;
        private IRegInfoRepository _regInfoRepository;
        private IRegServiceTypeMappingRepository _regServiceTypeMappingRepository;
        private IRegContactAddressMappingRepository _regContactAddressMappingRepository;
        private IOPAdoptionNotifyRepository _oPAdoptionNotifyRepository;
        private ILogRegContactRequestRepository _logRegContactRequestRepository;
        private IRegContactRequestHistoryRepository _regContactRequestHistoryRepository;
        private IOrganizationRepository _organizationRepository;
        private IRegBusinessTypeRepository _regBusinessTypeRepository;
        private IBusinessTypeRepository _businessTypeRepository;
        private IRegProductKeywordRepository _regProductKeywordRepository;
        private IProductCategoryRepository _productCategoryRepository;
        private IRegisProductCategoryRepository _regisProductCategoryRepository;
        private IADSBuyerRepository _adsbuyerRepository;
        private IOPNBuyerRepository _opnBuyerRepository;
        private ICurrencyRepository _currencyRepository;
        private OPNSupplierService _opnSupplierService;

        public RegisterController()
        {
            _businessEntityRepository = new BusinessEntityRepository();
            _countryRepository = new CountryRepository();
            _functionRepository = new FunctionRepository();
            _appConfigRepository = new AppConfigRepository();
            _nameTitleRepository = new NameTitleRepository();
            _jobTitleRepository = new JobTitleRepository();
            _autoComplete_StateRepository = new AutoComplete_StateRepository();
            _customerDocTypeRepository = new CustomerDocTypeRepository();
            _customerDocNameRepository = new CustomerDocNameRepository();
            _regEmailTicketRepository = new RegEmailTicketRepository();
            _registerDataService = new RegisterDataService();
            _regContactRequestRepository = new RegContactRequestRepository();
            _serviceTypeRepository = new ServiceTypeRepository();
            _regContactReqServMapping = new RegContactRequestServiceTypeMappingRepository();
            _regAttachmentRepository = new RegAttachmentRepository();
            _logRegAttachmentRepository = new LogRegAttachmentRepository();
            _languageRepository = new LanguageRepository();
            _regAddressRepository = new RegAddressRepository();
            _regContactRepository = new RegContactRepository();
            _regInfoRepository = new RegInfoRepository();
            _regServiceTypeMappingRepository = new RegServiceTypeMappingRepository();
            _regContactAddressMappingRepository = new RegContactAddressMappingRepository();
            _oPAdoptionNotifyRepository = new OPAdoptionNotifyRepository();
            _logRegContactRequestRepository = new LogRegContactRequestRepository();
            _regContactRequestHistoryRepository = new RegContactRequestHistoryRepository();
            _organizationRepository = new OrganizationRepository();
            _regBusinessTypeRepository = new RegBusinessTypeRepository();
            _businessTypeRepository = new BusinessTypeRepository();
            _regProductKeywordRepository = new RegProductKeywordRepository();
            _productCategoryRepository = new ProductCategoryRepository();
            _regisProductCategoryRepository = new RegisProductCategoryRepository();
            _adsbuyerRepository = new ADSBuyerRepository();
            _opnBuyerRepository = new OPNBuyerRepository();
            _currencyRepository = new CurrencyRepository();
            _opnSupplierService = new OPNSupplierService();
        }

        public RegisterController(
            BusinessEntityRepository businessEntityRepository,
            CountryRepository countryRepository,
            FunctionRepository functionRepository,
            AppConfigRepository appConfigRepository,
            NameTitleRepository nameTitleRepository,
            JobTitleRepository jobTitleRepository,
            AutoComplete_StateRepository autoComplete_StateRepository,
            CustomerDocTypeRepository customerDocTypeRepository,
            CustomerDocNameRepository customerDocNameRepository,
            RegEmailTicketRepository regEmailTicketRepository,
            RegisterDataService registerDataService,
            RegContactRequestRepository regContactRequestRepository,
            ServiceTypeRepository serviceTypeRepository,
            RegContactRequestServiceTypeMappingRepository regContactReqServMapping,
            RegAttachmentRepository regAttachmentRepository,
            LogRegAttachmentRepository logRegAttachmentRepository,
            LanguageRepository languageRepository,
            RegAddressRepository regAddressRepository,
            RegContactRepository regContactRepository,
            RegInfoRepository regInfoRepository,
            RegServiceTypeMappingRepository regServiceTypeMappingRepository,
            RegContactAddressMappingRepository regContactAddressMappingRepository,
            OPAdoptionNotifyRepository oPAdoptionNotifyRepository,
            LogRegContactRequestRepository logRegContactRequestRepository,
            RegContactRequestHistoryRepository regContactRequestHistoryRepository,
            OrganizationRepository organizationRepository,
            RegBusinessTypeRepository regBusinessTypeRepository,
            BusinessTypeRepository businessTypeRepository,
            RegProductKeywordRepository regProductKeywordRepository,
            ProductCategoryRepository productCategoryRepository,
            RegisProductCategoryRepository regisProductCategoryRepository,
            ADSBuyerRepository adsbuyerRepository,
            OPNBuyerRepository opnBuyerRepository,
            CurrencyRepository currencyRepository,
            OPNSupplierService opnSupplierService
        )
        {
            _businessEntityRepository = businessEntityRepository;
            _countryRepository = countryRepository;
            _functionRepository = functionRepository;
            _appConfigRepository = appConfigRepository;
            _nameTitleRepository = nameTitleRepository;
            _jobTitleRepository = jobTitleRepository;
            _autoComplete_StateRepository = autoComplete_StateRepository;
            _customerDocTypeRepository = customerDocTypeRepository;
            _customerDocNameRepository = customerDocNameRepository;
            _regEmailTicketRepository = regEmailTicketRepository;
            _registerDataService = registerDataService;
            _regContactRequestRepository = regContactRequestRepository;
            _serviceTypeRepository = serviceTypeRepository;
            _regContactReqServMapping = regContactReqServMapping;
            _regAttachmentRepository = regAttachmentRepository;
            _logRegAttachmentRepository = logRegAttachmentRepository;
            _languageRepository = languageRepository;
            _regAddressRepository = regAddressRepository;
            _regContactRepository = regContactRepository;
            _regInfoRepository = regInfoRepository;
            _regServiceTypeMappingRepository = regServiceTypeMappingRepository;
            _regContactAddressMappingRepository = regContactAddressMappingRepository;
            _oPAdoptionNotifyRepository = oPAdoptionNotifyRepository;
            _logRegContactRequestRepository = logRegContactRequestRepository;
            _regContactRequestHistoryRepository = regContactRequestHistoryRepository;
            _organizationRepository = organizationRepository;
            _regBusinessTypeRepository = regBusinessTypeRepository;
            _businessTypeRepository = businessTypeRepository;
            _regProductKeywordRepository = regProductKeywordRepository;
            _productCategoryRepository = productCategoryRepository;
            _regisProductCategoryRepository = regisProductCategoryRepository;
            _adsbuyerRepository = adsbuyerRepository;
            _opnBuyerRepository = opnBuyerRepository;
            _currencyRepository = currencyRepository;
            _opnSupplierService = opnSupplierService;
        }

        [HttpPost]
        public ActionResult RenderViewByTicketcode(TicketcodeRenderView model)
        {
            string optionTicketCode = WebConfigurationManager.AppSettings["use_sc_ticket_code"];
            bool redirectToSC = false;
            if(!string.IsNullOrEmpty(optionTicketCode))
            {
                redirectToSC = Convert.ToBoolean(optionTicketCode);
            }

            if (redirectToSC)
            {
                string baseURL = WebConfigurationManager.AppSettings["baseURL"];
                string ticketKey = WebConfigurationManager.AppSettings["sc_ticket_secret_key"];
                string ticketCodeEncrypt = CryptographyManage.EncryptAESBase32(model.Ticketcode, ticketKey, true);

                HttpClient verifyTokenHttp = new HttpClient();
                string verifyTicket = string.Format("{0}api/sc/verifyTicketCode?ticketCode={1}", baseURL, ticketCodeEncrypt);
                HttpResponseMessage responseCoc = verifyTokenHttp.GetAsync(verifyTicket).Result;
                if (responseCoc.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    return Json(new
                    {
                        Response = "Success",
                        IsRedirectToSC = redirectToSC,
                        RedirectURL = HttpUtility.UrlDecode(Url.Action("RedirectSC", "Redirect", new RouteValueDictionary(new { ticketCode = ticketCodeEncrypt }))
                        )
                    });
                }
                else
                {
                    HttpCookie cultureCookie = Request.Cookies["_culture"];
                    var languageId = cultureCookie.Value;

                    string errorMessage = "";
                    errorMessage = errorMessage.GetStringResource("_Regis.Msg_TicketCodeWrong", languageId);
                    return Json(new { Response = errorMessage });
                }
            }
            else
            {
                var regresult = _regInfoRepository.GetRegInfoListByTicketcode(model.Ticketcode);
                //_regInfoRepository.Dispose();

                if (regresult.Count() > 0)
                {
                    //return Json("Success");
                    return Json(new { Response = "Success" });
                }
                else
                {
                    HttpCookie cultureCookie = Request.Cookies["_culture"];
                    var languageId = cultureCookie.Value;

                    string errorMessage = "";
                    errorMessage = errorMessage.GetStringResource("_Regis.Msg_TicketCodeWrong", languageId);
                    return Json(new { Response = errorMessage });
                    //return Json(errorMessage);
                }
            }
        }

        public ActionResult SupplierRegister(int? aaa, string a)
        {
            string buyerShortName = null;
            string email = null;
            int? buyerSupplierId = null;
            int? buyerSupplierContactId = null;
            int? supplierUserId = null;

            if (!string.IsNullOrEmpty(a))
            {
                var stream = a;
                var handler = new JwtSecurityTokenHandler();
                var jsonToken = handler.ReadToken(stream);
                var tokenS = handler.ReadToken(stream) as JwtSecurityToken;
                buyerShortName = tokenS.Claims.First(claim => claim.Type == "buyerShortName").Value;
                email = tokenS.Claims.First(claim => claim.Type == "email").Value;
                buyerSupplierId = string.IsNullOrEmpty(tokenS.Claims.First(claim => claim.Type == "buyerSupplierId").Value) == true ? 0 : int.Parse(tokenS.Claims.First(claim => claim.Type == "buyerSupplierId").Value);
                buyerSupplierContactId = string.IsNullOrEmpty(tokenS.Claims.First(claim => claim.Type == "buyerSupplierContactId").Value) == true ? 0 : int.Parse(tokenS.Claims.First(claim => claim.Type == "buyerSupplierContactId").Value);
                supplierUserId = string.IsNullOrEmpty(tokenS.Claims.First(claim => claim.Type == "supplierUserId").Value) == true ? 0 : int.Parse(tokenS.Claims.First(claim => claim.Type == "supplierUserId").Value);

            }

            HttpCookie cultureCookie = Request.Cookies["_culture"];
            string languageID = "";

            if (cultureCookie != null)
            {
                languageID = cultureCookie.Value.ToString();
            }
            if (string.IsNullOrEmpty(languageID))
            {
                languageID = CultureHelper.GetImplementedCulture(languageID); // This is safe
            }
            int languageId = Convert.ToInt32(languageID);

            var model = new RegisterModels();

            List<int> list = new List<int>();

            string languageCode = _languageRepository.GetLanguageCodeById(languageId).ToString().ToUpper();
            string countryName = _countryRepository.GetCountryNameByCountryCode(languageCode);

            model.CountryCode = languageCode;
            model.CountryName = countryName;
            model.Proc = "Add";

            //model.CompanyTypeID = 1;

            ViewBag.TitleNameList = _nameTitleRepository.GetMultiLangNameTitleByLangid(languageId);

            ViewBag.JobTitleList = _jobTitleRepository.GetJobTitleList(languageId);

            ViewBag.ServiceTypeList = _serviceTypeRepository.GetServiceTypeList(languageId);

            //var data = new List<LocalizedModel>();
            //var dataTH = new LocalizedModel()
            //{
            //    EntityID = 1,
            //    EntityValue = "THB"
            //};
            //var dataUSD = new LocalizedModel()
            //{
            //    EntityID = 2,
            //    EntityValue = "USD"
            //};
            //data.Add(dataTH);
            //data.Add(dataUSD);
            model.CurrencyCode = "THB";
            ViewBag.CurrencyList = _currencyRepository.CurrencyList().Where(w => new[] { "THB", "USD", "VND" }.Any(s => s == w.CurrencyCode));
            //ViewBag.CurrencyList = data;

            model.OrgBusinessType = new List<BusinessTypeRegModel>();

            if (languageId == 1)
            {
                ViewBag.Category1 = _productCategoryRepository.GetProductCat1(1);
            }
            else
            {
                ViewBag.Category1 = _productCategoryRepository.GetProductCat1(2);
            }

            var dataRegisCat = _regisProductCategoryRepository
                   .GetProductCatRegis(0)
                   .Select(s => s.CategoryID_Lev3)
                   .ToArray();

            if (dataRegisCat != null)
            {
                if (languageId == 1)
                {
                    ViewBag.RegisCategory = _productCategoryRepository.GetItemCat1ByCat3(dataRegisCat).OrderBy(o => o.CategoryName_Inter).ToList();
                }
                else
                {
                    ViewBag.RegisCategory = _productCategoryRepository.GetItemCat1ByCat3(dataRegisCat).OrderBy(o => o.CategoryName_Local).ToList();
                }
            }

            //Buyer OPN Register
            ViewData["isOPNSupplier"] = false;
            if (buyerSupplierId != null)
            {
                if (buyerSupplierId > 0)
                {
                    ViewData["isOPNSupplier"] = true;
                    OPNSupplierRegister opnSupplierRegister = new OPNSupplierRegister();
                    opnSupplierRegister.buyerShortName = buyerShortName;
                    opnSupplierRegister.email = email;
                    opnSupplierRegister.buyerSupplierId = buyerSupplierId;
                    opnSupplierRegister.buyerSupplierContactId = buyerSupplierContactId;
                    opnSupplierRegister.supplierUserId = supplierUserId;

                    //set fix email for OPN Supplier
                    model.opnSupplierRegister = opnSupplierRegister;
                    model.InvetationCode = buyerShortName;
                    model.ConfirmEmail = email;
                    model.Email = email;
                    list.Add(2);
                }
            }
            int[] serviceTypeList = list.ToArray();
            model.ServiceTypeList = serviceTypeList;

            return View(model);
        }

        public ActionResult SupplierRegisterByTicket(string ticketCode)
        {
            try
            {
                HttpCookie cultureCookie = Request.Cookies["_culture"];

                string languageID = "";

                if (cultureCookie != null)
                {
                    languageID = cultureCookie.Value.ToString();
                }
                if (string.IsNullOrEmpty(languageID))
                {
                    languageID = CultureHelper.GetImplementedCulture(languageID); // This is safe
                }
                int languageId = Convert.ToInt32(languageID);

                //var model = new RegisterModels();

                var model = _registerDataService.GetRegisterRegInfoDataByTicketCode(ticketCode);

                //List<int> list = new List<int>();
                //int[] serviceTypeList = list.ToArray();
                //model.ServiceTypeList = serviceTypeList;

                #region-------------------------------------ViewBag -------------------------------------
                ViewBag.TitleNameList = _nameTitleRepository.GetMultiLangNameTitleByLangid(languageId);
                ViewBag.JobTitleList = _jobTitleRepository.GetJobTitleList(languageId);
                ViewBag.ServiceTypeList = _serviceTypeRepository.GetServiceTypeList(languageId);
                //var data = new List<LocalizedModel>();
                //var dataTH = new LocalizedModel()
                //{
                //    EntityID = 1,
                //    EntityValue = "THB"
                //};
                //var dataUSD = new LocalizedModel()
                //{
                //    EntityID = 2,
                //    EntityValue = "USD"
                //};
                //data.Add(dataTH);
                //data.Add(dataUSD);
                if (string.IsNullOrEmpty(model.CurrencyCode))
                    model.CurrencyCode = "THB";
                //ViewBag.CurrencyList = data;
                ViewBag.CurrencyList = _currencyRepository.CurrencyList().Where(w => new[] { "THB", "USD", "VND" }.Any(s => s == w.CurrencyCode));
                #endregion-------------------------------------End ViewBag-----------------------------------

                var dataBusinessType = _regBusinessTypeRepository.GetRegBusinessTypeByRegID(model.RegID ?? 0);
                var ListBusinessType = new List<BusinessTypeRegModel>();
                foreach (var item in dataBusinessType)
                {
                    var dt = new BusinessTypeRegModel()
                    {
                        BusinessTypeID = item.BusinessTypeID,
                        RegID = item.RegID
                    };
                    ListBusinessType.Add(dt);
                }

                model.OrgBusinessType = ListBusinessType;

                if (languageId == 1)
                {
                    ViewBag.Category1 = _productCategoryRepository.GetProductCat1(1);
                }
                else
                {
                    ViewBag.Category1 = _productCategoryRepository.GetProductCat1(2);
                }

                var dataKeyword = _regProductKeywordRepository.GetRegProductKeywordByRegID(model.RegID ?? 0);
                foreach (var item in dataKeyword)
                {
                    if (dataKeyword.Count() <= 1)
                    {
                        model.ProductKeyword += item.Keyword;
                    }
                    else
                    {
                        model.ProductKeyword += "," + item.Keyword;
                    }
                }

                var dataRegisCat = _regisProductCategoryRepository
                    .GetProductCatRegis(model.RegID ?? 0)
                    .Select(s => s.CategoryID_Lev3)
                    .ToArray();

                if (dataRegisCat != null)
                {
                    if (languageId == 1)
                    {
                        ViewBag.RegisCategory = _productCategoryRepository.GetItemCat1ByCat3(dataRegisCat).OrderBy(o => o.CategoryName_Inter).ToList();
                    }
                    else
                    {
                        ViewBag.RegisCategory = _productCategoryRepository.GetItemCat1ByCat3(dataRegisCat).OrderBy(o => o.CategoryName_Local).ToList();
                    }
                }

                return View("SupplierRegister", model);
            }
            catch (Exception ex)
            {
                return RedirectToAction("RegisterPortalError", "Error", new { ErrorMessage = ex.Message });
            }
        }

        [HttpPost]
        public async Task<ActionResult> SupplierRegister(RegisterModels model)
        {
            try
            {
                var productCategory = new List<int>();
                if (model.CategoryID_Lev3 != null)
                {
                    productCategory = JsonConvert.DeserializeObject<List<int>>(model.CategoryID_Lev3);
                }

                HttpCookie cultureCookie = Request.Cookies["_culture"];
                string languageID = "";

                if (cultureCookie != null)
                {
                    languageID = cultureCookie.Value.ToString();
                }
                if (string.IsNullOrEmpty(languageID))
                {
                    languageID = CultureHelper.GetImplementedCulture(languageID); // This is safe
                }
                int languageId = Convert.ToInt32(languageID);

                //model.ServiceTypeList = CheckBoxListExtension.GetSelectedValues<int>("ServiceType");

                #region-------------------------------------Generate TicketCode -------------------------------------
                var tbl_DocumentNoFormat = "RegisterTicket".GetPatternByDocName();
                string _charRandom = "", _ticketCode = "";
                foreach (var list in tbl_DocumentNoFormat)
                {
                    _charRandom = list.IsRandomCode == 1 ? list.Length.ToString().GetRandomChar(list.Pattern) : "";
                    _ticketCode = list.Suffix == "" ? list.Prefix + _charRandom : list.Pattern + _charRandom + list.Suffix;
                }

                string languageCode = _languageRepository.GetLanguageCodeById(languageId);

                model.TicketCode = _ticketCode;

                #endregion-------------------------------------End Generate TicketCode-----------------------------------
                logger.Debug("Success Gecerate TicketCode (RegID) " + model.RegID);
                #region-------------------------------------Insert Main Contact -------------------------------------
                var mainContactModel = new Tbl_RegContact()
                {
                    TitleID = model.NameTitleID,
                    FirstName_Local = (model.FirstName ?? "").Trim(),
                    FirstName_Inter = (model.FirstName ?? "").Trim(),
                    LastName_Local = (model.LastName ?? "").Trim(),
                    LastName_Inter = (model.LastName ?? "").Trim(),
                    JobTitleID = model.JobTitleID,
                    OtherJobTitle = (model.OtherJobTitle ?? "").Trim(),
                    Department = "",
                    PhoneCountryCode = (model.PhoneNoCountryCode ?? "").Trim(),
                    PhoneNo = (model.PhoneNo ?? "").Trim(),
                    PhoneExt = (model.PhoneExt ?? "").Trim(),
                    FaxNo = "",
                    FaxExt = "",
                    MobileCountryCode = (model.MobileNoCountryCode ?? "").Trim(),
                    MobileNo = (model.MobileNo ?? "").Trim(),
                    Email = (model.Email ?? "").Trim()
                };

                model.MainContactID = _regContactRepository.InsertReturnContactID(mainContactModel);

                #endregion-------------------------------------End Main Contact--------------------------------------
                logger.Debug("Success insert Main Contact (RegID) " + model.RegID);
                #region-------------------------------------Insert Address -------------------------------------
                var addressModel = new Tbl_RegAddress()
                {
                    HouseNo_Local = (model.RegCompanyAddress_HouseNo ?? "").Trim(),
                    HouseNo_Inter = "",
                    VillageNo_Local = (model.RegCompanyAddress_VillageNo ?? "").Trim(),
                    VillageNo_Inter = "",
                    Lane_Local = (model.RegCompanyAddress_Lane ?? "").Trim(),
                    Lane_Inter = "",
                    Road_Local = (model.RegCompanyAddress_Road ?? "").Trim(),
                    Road_Inter = "",
                    SubDistrict_Local = (model.RegCompanyAddress_SubDistrict ?? "").Trim(),
                    SubDistrict_Inter = "",
                    City_Local = (model.RegCompanyAddress_City ?? "").Trim(),
                    City_Inter = "",
                    State_Local = (model.RegCompanyAddress_State ?? "").Trim(),
                    State_Inter = "",
                    CountryCode = (model.RegCompanyAddress_CountryCode ?? "").Trim(),
                    PostalCode = (model.RegCompanyAddress_PostalCode ?? "").Trim(),
                };

                model.CompanyAddrID = _regAddressRepository.InsertReturnAddressID(addressModel);
                _regContactAddressMappingRepository.Insert(model.MainContactID ?? 0, model.CompanyAddrID ?? 0);
                #endregion-------------------------------------End Insert Address-------------------------------
                logger.Debug("Success insert Address (RegID) " + model.RegID);
                #region-------------------------------------Insert RegInfo -------------------------------------------
                var regInfoModel = new Tbl_RegInfo()
                {
                    TicketCode = model.TicketCode.Trim(),
                    //FirstName = "",
                    //LastName = "",
                    RegStatusID = 2,
                    CountryCode = (model.CountryCode ?? "").Trim(),
                    TaxID = (model.TaxID ?? "").Trim(),
                    DUNSNumber = "",
                    CompanyTypeID = model.CompanyTypeID,
                    BusinessEntityID = model.BusinessEntityID,
                    OtherBusinessEntity = (model.OtherBusinessEntity ?? "").Trim(),
                    CompanyName_Local = (model.CompanyName_Local.ToUpper() ?? "").Trim(),
                    CompanyName_Inter = (model.CompanyName_Inter.ToUpper() ?? "").Trim(),
                    BranchNo = (model.BranchNo ?? "").Trim(),
                    BranchName_Local = "",
                    BranchName_Inter = "",
                    MainBusinessCode = "00000000",
                    MainBusiness = (model.MainBusiness ?? "").Trim(),
                    CompanyAddrID = model.CompanyAddrID,
                    DeliveredAddrID = model.CompanyAddrID,
                    MainContactID = model.MainContactID,
                    PhoneNo = (model.PhoneNo ?? "").Trim(),
                    PhoneExt = (model.PhoneExt ?? "").Trim(),
                    MobileNo = (model.MobileNo ?? "").Trim(),
                    FaxNo = "",
                    FaxExt = "",
                    WebSite = "",
                    Latitude = 0,
                    Longtitude = 0,
                    LastUpdateDate = DateTime.UtcNow,
                    LastRejectDate = DateTime.UtcNow,
                    isDeleted = 0,
                    RegisteredCapital = model.RegisteredCapital ?? 0,
                    CurrencyCode = (model.CurrencyCode ?? "").Trim(),
                    ApprovedBy = 0,
                    ApprovedDate = new DateTime(1900, 01, 01),
                    RegisteredEmail = (model.Email ?? "").Trim(),
                    LanguageCode = (languageCode ?? "").Trim(),
                    RequestDate = DateTime.UtcNow
                };

                #region Validate Duplicate

                string tmpConcatLocal = "";
                tmpConcatLocal = model.TaxID + model.CompanyName_Local + model.BranchNo ?? "";

                tmpConcatLocal = tmpConcatLocal.Trim().ToUpper();
                tmpConcatLocal = GetStringFunctionHelper.GetMakeVerifyString(tmpConcatLocal);

                if (_regInfoRepository.CheckDuplicateRegInfoVerifyString(tmpConcatLocal, model.RegID.ToString()))
                {
                    string message = "";
                    message = message.GetStringResource("_Regis.ValidateMsg.DuplicateRegister", languageID);
                    return RedirectToAction("RegisterPortalError", "Error", new { ErrorMessage = message });
                }

                #endregion

                model.RegID = _regInfoRepository.InsertReturnRegID(regInfoModel);

                #endregion-------------------------------------End Insert RegInfo--------------------------------------
                logger.Debug("Success insert RegInfo (RegID) " + model.RegID);
                #region-------------------------------------Insert TicketCode -------------------------------------

                var emailTicket = new Tbl_RegEmailTicket()
                {
                    RegID = model.RegID ?? 0,
                    TicketCode = (model.TicketCode ?? "").Trim(),
                    RegisteredEmail = (model.Email ?? "").Trim(),
                    LanguageCode = (languageCode ?? "").Trim()
                };

                _regEmailTicketRepository.Insert(emailTicket);

                #endregion-------------------------------------End Insert TicketCode-----------------------------------
                logger.Debug("Success insert TicketCode (RegID) " + model.RegID);
                #region-------------------------------------Insert RegServiceType -------------------------------------------
                foreach (int item in model.ServiceTypeList)
                {
                    _regServiceTypeMappingRepository.InsertByValue(model.RegID ?? 0, item, item == 2 ? (model.InvetationCode ?? "") : "");
                }
                #endregion-------------------------------------End Insert RegServiceType--------------------------------------
                logger.Debug("Success insert RegServiceType (RegID) " + model.RegID);
                #region-------------------------------------Attachment Upload-------------------------------------

                try
                {
                    var documentName = _customerDocNameRepository.GetCustomerDocNameAll();

                    foreach (var item in documentName)
                    {
                        var hidFile = Request.Params["hidFile_" + item.DocumentNameID + "[]"] ?? string.Empty;

                        var hidGuid = Request.Params["hidGuid_" + item.DocumentNameID + "[]"] ?? string.Empty;

                        var hidOtherDocument = Request.Params["hidOtherDocument_" + item.DocumentNameID + "[]"] ?? string.Empty;

                        var splitHidFile = hidFile.Split(',');

                        var splitHidGuid = hidGuid.Split(',');

                        var splitHidOtherDocument = hidOtherDocument.Split(',');

                        if (splitHidFile.Length > 0 && (!string.IsNullOrEmpty(hidFile)))
                        {
                            for (var i = 0; i < splitHidFile.Length; i++)
                            {
                                string fileName = splitHidFile[i];

                                string guidName = splitHidGuid[i];

                                string otherDocument = "";

                                if (item.DocumentTypeID == -1)
                                {
                                    otherDocument = splitHidOtherDocument[i];
                                }

                                UploadAttachment(fileName, guidName, model.RegID ?? 0, item.DocumentNameID, otherDocument);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    return RedirectToAction("RegisterPortalError", "Error", new { ErrorMessage = ex.Message });
                }

                #endregion-------------------------------------Attachment Upload-------------------------------------
                logger.Debug("Success insert Attachmet Upload (RegID) " + model.RegID);
                #region --------------------------------Insert RegBusinessType--------------------------------

                var businessType = _businessTypeRepository.GetBusinessTypeAll();

                if (businessType != null)
                {
                    foreach (var itemList in businessType)
                    {
                        var businessTypeID = Request.Params["businessType_" + itemList.BusinessTypeID] ?? string.Empty;

                        if (!string.IsNullOrEmpty(businessTypeID))
                        {
                            var data = new Tbl_RegBusinessType();

                            data.RegID = model.RegID ?? 0;
                            data.BusinessTypeID = itemList.BusinessTypeID;

                            _regBusinessTypeRepository.InsertRegBusinessType(data);
                        }
                    }
                }

                #endregion
                logger.Debug("Success insert RegBusinessType (RegID) " + model.RegID);
                #region ---------------------------------------------Insert KeyWord---------------------------------------------

                if (model.ProductKeyword != null)
                {
                    var dataKey = model.ProductKeyword.Split(',').ToList();
                    if (model.ProductKeyword != "")
                    {
                        foreach (var Key in dataKey)
                        {
                            //_orgProductKeywordRepository.Insert(model.SupplierID, Key.ToString() , userID);
                            var data = new Tbl_RegProductKeyword();
                            data.RegID = model.RegID ?? 0;
                            data.Keyword = (Key.ToString() ?? "").Trim();

                            _regProductKeywordRepository.Insert(data);
                        }
                    }
                }
                #endregion
                logger.Debug("Success insert KeyWord (RegID) " + model.RegID);
                #region ---------------------------------------Insert Category---------------------------------------
                if (productCategory != null)
                {
                    foreach (var item in productCategory)
                    {
                        var data = new Tbl_RegProductCategory();
                        data.RegID = model.RegID ?? 0;
                        data.CategoryID_Lev3 = item;
                        data.ProductTypeID = 3;

                        _regisProductCategoryRepository.Insert(data);
                    }
                }

                #endregion
                logger.Debug("Success insert Category (RegID) " + model.RegID);

                //อย่าลืมเปิดส่งเมล์เมื่อแก้เสร็จ
                _registerDataService.SendingEmailRegisterSubmit(model.RegID ?? 0, languageID);
                _registerDataService.SendingEmailRegisterSubmitPIS(model.RegID ?? 0, languageID);

                logger.Debug("Success SendEmail");
                string messageReturn = "";
                string email = "<a href=\"mailto:" + model.Email + "\"> " + model.Email + " </a>";
                messageReturn = messageReturn.GetStringResource("_Regis.InfoMsg.RegisterSuccess", languageID);
                messageReturn = messageReturn.Replace("<%email_address%>", email);

                ViewBag.Message = messageReturn;
                model.OrgBusinessType = new List<BusinessTypeRegModel>();

                if (languageId == 1)
                {
                    ViewBag.Category1 = _productCategoryRepository.GetProductCat1(1);
                }
                else if (languageId == 2)
                {
                    ViewBag.Category1 = _productCategoryRepository.GetProductCat1(2);
                }

                var dataRegisCat = _regisProductCategoryRepository
                       .GetProductCatRegis(0)
                       .Select(s => s.CategoryID_Lev3)
                       .ToArray();

                if (dataRegisCat != null)
                {
                    if (languageId == 1)
                    {
                        ViewBag.RegisCategory = _productCategoryRepository.GetItemCat1ByCat3(dataRegisCat).OrderBy(o => o.CategoryName_Inter).ToList();
                    }
                    else if (languageId == 2)
                    {
                        ViewBag.RegisCategory = _productCategoryRepository.GetItemCat1ByCat3(dataRegisCat).OrderBy(o => o.CategoryName_Local).ToList();
                    }
                }

                #region ---------------------- OPN Supplier Register -------------------------

                // verify OPN invitation code or not ?
                bool isOPNInvitationCode = _opnBuyerRepository.verifyInvitationCode(model.InvetationCode);

                // OPN Supplier Register
                if (model.opnSupplierRegister != null && model.RegID != null && isOPNInvitationCode == true)
                {
                    if (model.opnSupplierRegister.buyerSupplierId > 0 && model.RegID > 0)
                    {
                        logger.Info("------- Start Insert OPNBuyerSupplierMapping & OPNSupplierMapping -------");
                        logger.Info("RegId : " + model.RegID);
                        logger.Info("InvitationCode : " + model.opnSupplierRegister.buyerShortName);
                        logger.Info("email : " + model.opnSupplierRegister.email);
                        logger.Info("buyerSupplierId : " + model.opnSupplierRegister.buyerSupplierId);
                        logger.Info("buyerSupplierContactId : " + model.opnSupplierRegister.buyerSupplierContactId);
                        logger.Info("supplierUserId : " + model.opnSupplierRegister.supplierUserId);

                        model.opnSupplierRegister.regId = model.RegID.Value;
                        model.opnSupplierRegister.email = model.Email;
                        model.opnSupplierRegister.buyerShortName = model.InvetationCode;
                        bool insertOPNBuyerSupplierMappingResult = await _opnSupplierService.InsertOPNBuyerSupplierMapping(model.opnSupplierRegister);
                        if (insertOPNBuyerSupplierMappingResult)
                        {
                            bool insertOPNSupplierMappingResult = await _opnSupplierService.InsertOPNSupplierMapping(model.opnSupplierRegister);
                            if (insertOPNSupplierMappingResult)
                            {
                                logger.Info("Insert OPNBuyerSupplierMapping & OPNSupplierMapping success");
                            }
                            else
                            {
                                logger.Error("Cannot insert OPNSupplierMapping");
                            }
                        }
                        else
                        {
                            logger.Error("Cannot insert OPNBuyerSupplierMapping");
                        }
                    }
                }
                else if (model.opnSupplierRegister == null && model.RegID != null && isOPNInvitationCode == true)
                {
                    OPNSupplierRegister opnSupplierRegister = new OPNSupplierRegister();

                    opnSupplierRegister.regId = model.RegID.Value;
                    opnSupplierRegister.email = model.Email;
                    opnSupplierRegister.buyerShortName = model.InvetationCode;
                    model.opnSupplierRegister = opnSupplierRegister;

                    bool insertOPNBuyerSupplierMappingResult = await _opnSupplierService.InsertOPNBuyerSupplierMapping(model.opnSupplierRegister);
                    if (insertOPNBuyerSupplierMappingResult)
                    {
                        logger.Info("Insert OPNBuyerSupplierMapping success");
                    }
                    else
                    {
                        logger.Error("Cannot insert OPNBuyerSupplierMapping");
                    }
                }

                #endregion


                #region-------------------------------------ViewBag -------------------------------------
                ViewBag.TitleNameList = _nameTitleRepository.GetMultiLangNameTitleByLangid(languageId);
                ViewBag.JobTitleList = _jobTitleRepository.GetJobTitleList(languageId);
                ViewBag.ServiceTypeList = _serviceTypeRepository.GetServiceTypeList(languageId);
                //var currency = new List<LocalizedModel>();
                //var currencyTH = new LocalizedModel()
                //{
                //    EntityID = 1,
                //    EntityValue = "THB"
                //};
                //var currencyUSD = new LocalizedModel()
                //{
                //    EntityID = 2,
                //    EntityValue = "USD"
                //};
                //currency.Add(currencyTH);
                //currency.Add(currencyUSD);
                if (string.IsNullOrEmpty(model.CurrencyCode))
                    model.CurrencyCode = "THB";
                //ViewBag.CurrencyList = currency;
                ViewBag.CurrencyList = _currencyRepository.CurrencyList().Where(w => new[] { "THB", "USD", "VND" }.Any(s => s == w.CurrencyCode));
                #endregion-------------------------------------End ViewBag-----------------------------------
            }
            catch (Exception ex)
            {
                logger.Error(ex);

                return RedirectToAction("RegisterPortalError", "Error", new { ErrorMessage = ex.Message });
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult SupplierRegisterEdit(RegisterModels model)
        {
            try
            {
                var productCategory = new List<int>();
                if (model.CategoryID_Lev3 != null)
                {
                    productCategory = JsonConvert.DeserializeObject<List<int>>(model.CategoryID_Lev3);
                }

                HttpCookie cultureCookie = Request.Cookies["_culture"];
                string languageID = "";

                if (cultureCookie != null)
                {
                    languageID = cultureCookie.Value.ToString();
                }
                if (string.IsNullOrEmpty(languageID))
                {
                    languageID = CultureHelper.GetImplementedCulture(languageID); // This is safe
                }
                int languageId = Convert.ToInt32(languageID);

                //model.ServiceTypeList = CheckBoxListExtension.GetSelectedValues<int>("ServiceType");

                string languageCode = _languageRepository.GetLanguageCodeById(languageId);

                #region-------------------------------------Update RegInfo -------------------------------------

                var regInfoModel = _regInfoRepository.GetRegInfoByRegID(model.RegID ?? 0).FirstOrDefault();

                if (regInfoModel != null)
                {
                    regInfoModel.CountryCode = (model.CountryCode ?? "").Trim();
                    regInfoModel.TaxID = (model.TaxID ?? "").Trim();
                    regInfoModel.CompanyTypeID = model.CompanyTypeID;
                    regInfoModel.BusinessEntityID = model.BusinessEntityID;
                    regInfoModel.OtherBusinessEntity = (model.OtherBusinessEntity ?? "").Trim();
                    regInfoModel.CompanyName_Local = (model.CompanyName_Local.ToUpper() ?? "").Trim();
                    regInfoModel.CompanyName_Inter = (model.CompanyName_Inter.ToUpper() ?? "").Trim();
                    regInfoModel.BranchNo = (model.BranchNo ?? "").Trim();
                    regInfoModel.MainBusiness = (model.MainBusiness ?? "").Trim();
                    regInfoModel.PhoneNo = (model.PhoneNo ?? "").Trim();
                    regInfoModel.PhoneExt = (model.PhoneExt ?? "").Trim();
                    regInfoModel.MobileNo = (model.MobileNo ?? "").Trim();
                    regInfoModel.LastUpdateDate = DateTime.UtcNow;
                    regInfoModel.ApprovedBy = 0;
                    regInfoModel.ApprovedDate = new DateTime(1900, 01, 01);
                    regInfoModel.RegisteredCapital = model.RegisteredCapital ?? 0;
                    regInfoModel.CurrencyCode = (model.CurrencyCode ?? "").Trim();
                    regInfoModel.RegisteredEmail = (model.Email ?? "").Trim();
                    regInfoModel.LanguageCode = (languageCode ?? "").Trim();
                    regInfoModel.RequestDate = DateTime.UtcNow;

                    if (regInfoModel.RegStatusID == 3)
                    {
                        regInfoModel.RegStatusID = 2;
                    }

                    _regInfoRepository.UpdateRegInfo(regInfoModel);
                }

                #endregion----------------------------------End RegInfo-----------------------------------
                logger.Debug("Success Update RegInfo (RegID) " + model.RegID);
                #region-------------------------------------Update Address -------------------------------------

                var addressModel = _regAddressRepository.GetRegAddressByaddrid(model.CompanyAddrID ?? 0).FirstOrDefault();
                if (addressModel != null)
                {
                    addressModel.HouseNo_Local = (model.RegCompanyAddress_HouseNo ?? "").Trim();
                    addressModel.VillageNo_Local = (model.RegCompanyAddress_VillageNo ?? "").Trim();
                    addressModel.Lane_Local = (model.RegCompanyAddress_Lane ?? "").Trim();
                    addressModel.Road_Local = (model.RegCompanyAddress_Road ?? "").Trim();
                    addressModel.SubDistrict_Local = (model.RegCompanyAddress_SubDistrict ?? "").Trim();
                    addressModel.City_Local = (model.RegCompanyAddress_City ?? "").Trim();
                    addressModel.State_Local = (model.RegCompanyAddress_State ?? "").Trim();
                    addressModel.CountryCode = (model.RegCompanyAddress_CountryCode ?? "").Trim();
                    addressModel.PostalCode = (model.RegCompanyAddress_PostalCode ?? "").Trim();

                    _regAddressRepository.UpdateRegAddress(addressModel);
                }

                #endregion----------------------------------End Address-----------------------------------
                logger.Debug("Success Update Address (RegID) " + model.RegID);
                #region-------------------------------------Update Main Contact -------------------------------------

                var mainContactModel = _regContactRepository.GetRegContactByContactid(model.MainContactID ?? 0).FirstOrDefault();
                if (mainContactModel != null)
                {
                    mainContactModel.TitleID = model.NameTitleID;
                    mainContactModel.FirstName_Local = (model.FirstName ?? "").Trim();
                    mainContactModel.LastName_Local = (model.LastName ?? "").Trim();
                    mainContactModel.JobTitleID = model.JobTitleID;
                    mainContactModel.OtherJobTitle = (model.OtherJobTitle ?? "").Trim();
                    mainContactModel.PhoneCountryCode = (model.PhoneNoCountryCode ?? "").Trim();
                    mainContactModel.PhoneNo = (model.PhoneNo ?? "").Trim();
                    mainContactModel.PhoneExt = (model.PhoneExt ?? "").Trim();
                    mainContactModel.MobileCountryCode = (model.MobileNoCountryCode ?? "").Trim();
                    mainContactModel.MobileNo = (model.MobileNo ?? "").Trim();
                    mainContactModel.Email = (model.Email ?? "").Trim();

                    _regContactRepository.UpdateRegContact(mainContactModel);
                }
                #endregion----------------------------------End Main Contact-----------------------------------
                logger.Debug("Success Update Main Contact (RegID) " + model.RegID);
                #region-------------------------------------Update RegEmailTicket ------------------------------

                var regEmailTicket = _regEmailTicketRepository.GetEmailTicketDataListByTicketcode(model.TicketCode).FirstOrDefault();

                if (regEmailTicket != null)
                {
                    regEmailTicket.RegisteredEmail = (model.Email ?? "").Trim();
                    _regEmailTicketRepository.Update(regEmailTicket, 1);
                }

                #endregion----------------------------------End Update RegEmailTicket---------------------------
                logger.Debug("Success Update RegEmailTicket (RegID) " + model.RegID);
                #region-------------------------------------Update RegServiceType -------------------------------------------
                var serviceTypeList = _serviceTypeRepository.GetServiceTypeList(languageId);
                foreach (var item in serviceTypeList)
                {
                    if (model.ServiceTypeList.Contains(item.EntityID))
                    {
                        if (!_regServiceTypeMappingRepository.CheckExistsServiceMappingByValue(model.RegID ?? 0, item.EntityID))
                        {
                            //_regServiceTypeMappingRepository.InsertByValue(model.RegID ?? 0, item.EntityID, model.InvetationCode);
                            _regServiceTypeMappingRepository.InsertByValue(model.RegID ?? 0, item.EntityID, item.EntityID == 2 ? (model.InvetationCode ?? "") : "");
                        }
                        else
                        {
                            _regServiceTypeMappingRepository.UpdateByValue(model.RegID ?? 0, item.EntityID, item.EntityID == 2 ? (model.InvetationCode ?? "") : "");
                        }
                    }
                    else
                    {
                        if (_regServiceTypeMappingRepository.CheckExistsServiceMappingByValue(model.RegID ?? 0, item.EntityID))
                        {
                            _regServiceTypeMappingRepository.RemoveByValue(model.RegID ?? 0, item.EntityID);
                        }
                    }
                }
                //foreach (int item in model.ServiceTypeList)
                //{
                //    _regServiceTypeMappingRepository.InsertByValue(model.RegID ?? 0, item);
                //}
                #endregion-------------------------------------End Update RegServiceType--------------------------------------
                logger.Debug("Success Update RegServiceType (RegID) " + model.RegID);
                #region-------------------------------------Update Attachment Upload-------------------------------------

                try
                {
                    var documentName = _customerDocNameRepository.GetCustomerDocNameAll();

                    foreach (var item in documentName)
                    {
                        var hidFile = Request.Params["hidFile_" + item.DocumentNameID + "[]"] ?? string.Empty;

                        var hidGuid = Request.Params["hidGuid_" + item.DocumentNameID + "[]"] ?? string.Empty;

                        var hidOtherDocument = Request.Params["hidOtherDocument_" + item.DocumentNameID + "[]"] ?? string.Empty;

                        var splitHidFile = hidFile.Split(',');

                        var splitHidGuid = hidGuid.Split(',');

                        var splitHidOtherDocument = hidOtherDocument.Split(',');

                        if (splitHidFile.Length > 0 && (!string.IsNullOrEmpty(hidFile)))
                        {
                            for (var i = 0; i < splitHidFile.Length; i++)
                            {
                                string fileName = splitHidFile[i];

                                string guidName = splitHidGuid[i];

                                string otherDocument = "";

                                if (item.DocumentTypeID == -1)
                                {
                                    otherDocument = splitHidOtherDocument[i];
                                }

                                UploadAttachment(fileName, guidName, model.RegID ?? 0, item.DocumentNameID, otherDocument);
                            }
                        }
                    }

                    //Remove Attachment
                    var hidRemoveFile = Request.Params["hidRemoveGuid[]"] ?? string.Empty;

                    var splitHidRemoveFile = hidRemoveFile.Split(',');

                    if (splitHidRemoveFile.Length > 0)
                    {
                        foreach (var itemlist in splitHidRemoveFile)
                        {
                            RemoveAttachment(itemlist);
                        }
                    }
                    //End Remove Attachment

                    //Remove Attachment By ChangedDocumentType
                    //var listRemove = _customerDocTypeRepository.GetDocumentTypeForRemove(model.CompanyTypeID ?? 1);

                    var listRemove = _customerDocNameRepository.GetCustomerDocNameForRemove(model.CompanyTypeID ?? 1);

                    foreach (var listDoc in listRemove)
                    {
                        var listRegAttachment = _regAttachmentRepository.GetRegAttachmentByRegIDAndDocumentNameID(model.RegID ?? 0, listDoc.DocumentNameID);

                        foreach (var listattachment in listRegAttachment)
                        {
                            RemoveAttachment(listattachment.AttachmentNameUnique);
                            //_regAttachmentRepository.Delete(listattachment.RegID, listattachment.AttachmentID);
                        }
                    }

                    //End Remove Attachment By ChangedDocumentType
                }
                catch (Exception ex)
                {
                    throw;
                    //return RedirectToAction("RegisterPortalError", "Error", new { ErrorMessage = ex.Message });
                }

                #endregion-------------------------------------End Update Attachment Upload-------------------------------------
                logger.Debug("Success Update Attachment Upload (RegID) " + model.RegID);
                #region --------------------------------Update RegBusinessType--------------------------------

                var businessType = _businessTypeRepository.GetBusinessTypeAll();

                if (businessType != null)
                {
                    _regBusinessTypeRepository.RemoveRegBusinessTypeByRegID(model.RegID ?? 0);

                    foreach (var itemList in businessType)
                    {
                        var businessTypeID = Request.Params["businessType_" + itemList.BusinessTypeID] ?? string.Empty;

                        if (!string.IsNullOrEmpty(businessTypeID))
                        {
                            var data = new Tbl_RegBusinessType();

                            data.RegID = model.RegID ?? 0;
                            data.BusinessTypeID = itemList.BusinessTypeID;

                            _regBusinessTypeRepository.UpdateRegBusinessType(data);
                        }
                    }
                }

                #endregion
                logger.Debug("Success Update RegBusinessType (RegID) " + model.RegID);
                #region ---------------------------------------------Update KeyWord---------------------------------------------

                if (model.ProductKeyword != null)
                {
                    var dtProductKeyword = _regProductKeywordRepository.GetRegProductKeywordByRegID(model.RegID ?? 0);
                    _regProductKeywordRepository.removeKeyWordList(dtProductKeyword);
                    var dataKey = model.ProductKeyword.Split(',').ToList();
                    if (model.ProductKeyword != "")
                    {
                        foreach (var Key in dataKey)
                        {
                            //_orgProductKeywordRepository.Insert(model.SupplierID, Key.ToString() , userID);
                            var data = new Tbl_RegProductKeyword();
                            data.RegID = model.RegID ?? 0;
                            data.Keyword = (Key.ToString() ?? "").Trim();

                            _regProductKeywordRepository.Edit(data);
                        }
                    }
                }
                #endregion
                logger.Debug("Success Update KeyWord (RegID) " + model.RegID);
                #region ---------------------------------------Update Category---------------------------------------
                if (productCategory != null)
                {
                    _regisProductCategoryRepository.RemoveDataOrgProductCategory(model.RegID ?? 0);
                    foreach (var item in productCategory)
                    {
                        var data = new Tbl_RegProductCategory();
                        data.RegID = model.RegID ?? 0;
                        data.CategoryID_Lev3 = item;
                        data.ProductTypeID = 3;

                        _regisProductCategoryRepository.Edit(data);
                    }
                }
                #endregion
                logger.Debug("Success Update Category (RegID) " + model.RegID);

                model.OrgBusinessType = new List<BusinessTypeRegModel>();

                if (languageId == 1)
                {
                    ViewBag.Category1 = _productCategoryRepository.GetProductCat1(1);
                }
                else if (languageId == 2)
                {
                    ViewBag.Category1 = _productCategoryRepository.GetProductCat1(2);
                }

                var dataRegisCat = _regisProductCategoryRepository
                       .GetProductCatRegis(0)
                       .Select(s => s.CategoryID_Lev3)
                       .ToArray();

                if (dataRegisCat != null)
                {
                    if (languageId == 1)
                    {
                        ViewBag.RegisCategory = _productCategoryRepository.GetItemCat1ByCat3(dataRegisCat).OrderBy(o => o.CategoryName_Inter).ToList();
                    }
                    else if (languageId == 2)
                    {
                        ViewBag.RegisCategory = _productCategoryRepository.GetItemCat1ByCat3(dataRegisCat).OrderBy(o => o.CategoryName_Local).ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);

                return RedirectToAction("RegisterPortalError", "Error", new { ErrorMessage = ex.Message });
            }

            return RedirectToAction("SupplierRegisterByTicket", "Register", new { ticketcode = model.TicketCode });
        }

        public ActionResult BusinessEntityPartialNew(RegisterModels model)
        {
            var corperate = Request.Params["CorperateValue"] as string ?? string.Empty;

            int companyTypeID = 1;
            if (!string.IsNullOrEmpty(corperate))
            {
                companyTypeID = Convert.ToInt32(corperate);
            }
            else if (model.CompanyTypeID != null)
            {
                companyTypeID = model.CompanyTypeID ?? 1;
            }

            HttpCookie cultureCookie = Request.Cookies["_culture"];
            string languageID = "";

            if (cultureCookie != null)
            {
                languageID = cultureCookie.Value.ToString();
            }
            if (string.IsNullOrEmpty(languageID))
            {
                languageID = CultureHelper.GetImplementedCulture(languageID); // This is safe
            }
            int languageId = Convert.ToInt32(languageID);

            ViewBag.BusinessEntityList = _businessEntityRepository.GetMultiLangBusinessEntity(companyTypeID, languageId);

            //return PartialView("_BusinessEntityPartialNew", model);
            return PartialView(@"~/Views/Register/_BusinessEntityPartialNew.cshtml", model);
        }

        public ActionResult AttachmentPartial(int CompanyTypeID, string CountryCode, int RegID)
        {
            var model = _customerDocTypeRepository.GetDocumentTypeByCompanyTypeIDAndCountrycode(CompanyTypeID, CountryCode);

            ViewData["RegID"] = RegID;

            //return PartialView("_AttachmentPartial", model);
            return PartialView(@"~/Views/Register/_AttachmentPartial.cshtml", model);
        }

        [HttpGet]
        public ActionResult AttachmentChangedPartial(int CompanyTypeID, string CountryCode, string RegID = "")
        {
            int tempRegID = 0;
            if (!string.IsNullOrEmpty(RegID))
            {
                tempRegID = Convert.ToInt32(RegID);
            }

            var model = _customerDocTypeRepository.GetDocumentTypeByCompanyTypeIDAndCountrycode(CompanyTypeID, CountryCode);

            ViewData["RegID"] = tempRegID;

            //return PartialView("_AttachmentPartial", model);
            return PartialView(@"~/Views/Register/_AttachmentPartial.cshtml", model);
        }

        public ActionResult AttachmentItemListPartial(int DocumentTypeID, int RegID, int CompanyTypeID)
        {
            var model = _regAttachmentRepository.GetRegAttachmentListByRegIDAndDocumentTypeID(RegID, DocumentTypeID, CompanyTypeID);

            //return PartialView("_AttachmentItemListPartial", model);
            return PartialView(@"~/Views/Register/_AttachmentItemListPartial.cshtml", model);
        }

        public ActionResult CustomerDocNamePartialNew(int DocumentTypeID, string Countrycode, int CompanyTypeID)
        {
            var model = _customerDocNameRepository.GetCustomerDocNameByDocumentTypeIDAndCountryCode(DocumentTypeID, Countrycode, CompanyTypeID);
            ViewBag.CustomerDocNameList = _customerDocNameRepository.GetCustomerDocNameByDocumentTypeIDAndCountryCode(DocumentTypeID, Countrycode, CompanyTypeID);

            //return PartialView("_CustomerDocNamePartialNew", model);
            return PartialView(@"~/Views/Register/_CustomerDocNamePartialNew.cshtml", model);
        }

        [HttpPost]
        public ActionResult UploadFileAttachment(FormCollection collection)
        {
            string languageId = "";
            HttpCookie cultureCookie = Request.Cookies["_culture"];
            languageId = cultureCookie.Value;

            //string hidRegID = collection["hidRegID"];

            string hidDocumentTypeID = collection["hidDocumentTypeID"];

            string attachmentName = "FileAttachment_" + hidDocumentTypeID;

            string tbxdocname = collection["tbx-doc-name_" + hidDocumentTypeID];

            string tbxfilename = collection["tbx-file-path_" + hidDocumentTypeID];

            string hidDocumentNameID = collection["hidDocumentNameID"];

            string customerDocName = "";

            if (!string.IsNullOrEmpty(hidDocumentNameID))
            {
                customerDocName = _customerDocNameRepository.GetCustomerDocNameByDocumentNameID(Convert.ToInt32(hidDocumentNameID));
            }

            string fileName = "";

            string fileGuidName = "";

            string htmlText = "";

            HttpPostedFileBase myFile = Request.Files[attachmentName];
            bool isUploaded = false;
            string message = "File upload failed";

            var attachmentSize = _appConfigRepository.GetAppConfigByName("Register_AttachmentSize");
            var maxFileSize = Convert.ToInt32(attachmentSize.Value);

            if (myFile != null && myFile.ContentLength != 0)
            {
                if (myFile.ContentLength > maxFileSize)
                {
                    message = message.GetStringResource("_Regis.ValidateMsg.LimitAttachFileSize", languageId);
                    return Json(new { isUploaded = isUploaded, message = message, documentTypeID = hidDocumentTypeID }, "text/html");
                }

                string fileSize = this.GetFileSize(myFile.ContentLength);

                string pathTemp = _appConfigRepository.GetAppConfigByName("Register_AttachmentPathTemp").Value;
                string pathAttachment = WebConfigurationManager.AppSettings["pathAttachment"];
                string pathTempRegis = "Register";

                string pathForSaving = pathAttachment + pathTemp + pathTempRegis;
                string pathForDownload = pathTemp + pathTempRegis;
                //string pathForSaving = Server.MapPath("~/Register_AttachmentPathTemp/" + hidRegID);

                if (this.CreateFolderIfNeeded(pathForSaving))
                {
                    try
                    {
                        string guid = SupplierPortal.Framework.MethodHelper.GetGenerateGuidHelper.GetGenerateRandomGuid();

                        //fileGuidName = guid + myFile.;
                        fileGuidName = guid + myFile.FileName.Substring(myFile.FileName.LastIndexOf('.'));

                        myFile.SaveAs(Path.Combine(pathForSaving, fileGuidName));

                        string virtualFilePath = pathForDownload + "/" + fileGuidName;

                        isUploaded = true;
                        message = "File uploaded successfully!";
                        //fileName = myFile.FileName;
                        fileName = tbxfilename;
                        //fileGuidName = SupplierPortal.Framework.MethodHelper.GetGenerateGuidHelper.GetGenerateRandomGuid();
                        htmlText = GetHtmlText(hidDocumentNameID, hidDocumentTypeID, fileName, virtualFilePath, fileGuidName, guid, tbxdocname, fileSize, customerDocName);
                    }
                    catch (Exception ex)
                    {
                        message = string.Format("File upload failed: {0}", ex.Message);
                    }
                }
            }
            return Json(new
            {
                isUploaded = isUploaded,
                message = message,
                documentTypeID = hidDocumentTypeID,
                documentNameID = hidDocumentNameID,
                fileName = fileName,
                htmlText = htmlText,
                fileGuidName = fileGuidName
            }, "text/html");
        }

        public ActionResult ValidateTaxID(string TaxID, string CountryCode_VI, string CompanyTypeID)
        {
            string languageId = "";

            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (cultureCookie != null)
            {
                languageId = cultureCookie.Value.ToString();
            }
            else
            {
                languageId = CultureHelper.GetImplementedCulture(languageId);// This is safe
            }

            bool IsValid = true;

            string messageReturn = "";

            string errorMessage = "";

            if (CountryCode_VI == "TH")
            {
                var sqlQuery = "select dbo.VerifyTaxNumber('" + TaxID.Trim() + "')";
                var result = _functionRepository.GetResultByQuery(sqlQuery);
                //--Debug
                //result = true;
                //--Debug
                if (result)
                {
                    return Json(IsValid, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (CompanyTypeID == "1")
                    {
                        errorMessage = messageReturn.GetStringResource("_Regis.ValidateMsg.InvalidFormatTaxID.Corp", languageId);
                    }
                    else
                    {
                        errorMessage = messageReturn.GetStringResource("_Regis.ValidateMsg.InvalidFormatTaxID.Ordinary", languageId);
                    }

                    return Json(errorMessage, JsonRequestBehavior.AllowGet);
                }
            }

            if (IsValid)
            {
                return Json(IsValid, JsonRequestBehavior.AllowGet);
            }

            return Json(errorMessage, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CheckDuplicateTexID(ValidRegisModels model)
        {
            string languageID = "";

            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (cultureCookie != null)
            {
                languageID = cultureCookie.Value.ToString();
            }
            if (string.IsNullOrEmpty(languageID))
            {
                languageID = CultureHelper.GetImplementedCulture(languageID); // This is safe
            }
            int languageId = Convert.ToInt32(languageID);

            if (model.Proc == "Edit")
            {
                string tmpConcatOld = "";
                string tmpConcatNew = "";

                tmpConcatOld = model.OldConcat;
                tmpConcatNew = model.TaxID + model.CompanyName_Local + model.CompanyName_Inter + model.BranchNo ?? "";

                if (tmpConcatOld.Trim().ToUpper() == tmpConcatNew.Trim().ToUpper())
                {
                    var modelDup = new ValidRegisModels
                    {
                        Status = true
                        //TelCountryCode = telCountryCode
                    };

                    return Json(modelDup);
                }
            }

            string message = "";

            #region-----------------------CheckDuplicate For Level 1 ----------------------
            // Check By TaxID , BranchNo For CountryCode "TH"
            if (model.CountryCode == "TH")
            {
                if (_organizationRepository.CheckDuplicateOrganizationByTaxIDAndBranchNo(model.TaxID, model.BranchNo ?? ""))
                {
                    string supplierID = _registerDataService.GetSupplierIDOrganizationByTaxIDAndBranchNo(model.TaxID, model.BranchNo ?? "");
                    var orgModel = _organizationRepository.GetDataBySupplierID(Convert.ToInt32(supplierID));

                    var modelDup = new ValidRegisModels
                    {
                        Status = false,
                        SupplierID = supplierID,
                        DuplicateCompanyName = orgModel.CompanyName_Local,
                        DuplicateType = "DuplicateOrg"
                    };

                    return Json(modelDup);
                }
                else if (_regInfoRepository.CheckDuplicateRegInfoByTaxIDAndBranchNo(model.TaxID, model.BranchNo ?? "", model.RegID ?? ""))
                {
                    message = message.GetStringResource("_Regis.ValidateMsg.DuplicateRegister", languageID);

                    var modelDup = new ValidRegisModels
                    {
                        Status = false,
                        DuplicateType = "DuplicateReg",
                        Message = message
                    };

                    return Json(modelDup);
                }
            }

            #endregion

            #region-----------------------CheckDuplicate For Level 2 ----------------------

            string tmpConcatLocal = "";
            string tmpConcatInter = "";

            tmpConcatLocal = model.TaxID + model.CompanyName_Local + model.BranchNo ?? "";
            tmpConcatInter = model.TaxID + model.CompanyName_Inter + model.BranchNo ?? "";

            tmpConcatLocal = tmpConcatLocal.Trim().ToUpper();
            tmpConcatLocal = GetStringFunctionHelper.GetMakeVerifyString(tmpConcatLocal);
            tmpConcatInter = tmpConcatInter.Trim().ToUpper();
            tmpConcatInter = GetStringFunctionHelper.GetMakeVerifyString(tmpConcatInter);

            if (_registerDataService.CheckDuplicateOrganization(tmpConcatLocal))
            {
                string supplierID = _registerDataService.GetSupplierIDOrganizationByConcatRegis(tmpConcatLocal);
                var orgModel = _organizationRepository.GetDataBySupplierID(Convert.ToInt32(supplierID));

                var modelDup = new ValidRegisModels
                {
                    Status = false,
                    SupplierID = supplierID,
                    DuplicateCompanyName = orgModel.CompanyName_Local,
                    DuplicateType = "DuplicateOrg"
                };

                return Json(modelDup);
            }
            else if (_registerDataService.CheckDuplicateOrganization(tmpConcatInter))
            {
                string supplierID = _registerDataService.GetSupplierIDOrganizationByConcatRegis(tmpConcatInter);
                var orgModel = _organizationRepository.GetDataBySupplierID(Convert.ToInt32(supplierID));

                var modelDup = new ValidRegisModels
                {
                    Status = false,
                    SupplierID = supplierID,
                    DuplicateCompanyName = orgModel.CompanyName_Inter,
                    DuplicateType = "DuplicateOrg"
                };

                return Json(modelDup);
            }
            else if (_registerDataService.CheckDuplicateRegInfo(tmpConcatLocal, model.RegID ?? ""))
            {
                message = message.GetStringResource("_Regis.ValidateMsg.DuplicateRegister", languageID);

                var modelDup = new ValidRegisModels
                {
                    Status = false,
                    DuplicateType = "DuplicateReg",
                    Message = message
                };

                return Json(modelDup);
            }
            else if (_registerDataService.CheckDuplicateRegInfo(tmpConcatInter, model.RegID ?? ""))
            {
                message = message.GetStringResource("_Regis.ValidateMsg.DuplicateRegister", languageID);

                var modelDup = new ValidRegisModels
                {
                    Status = false,
                    DuplicateType = "DuplicateReg",
                    Message = message
                };

                return Json(modelDup);
            }
            else
            {
                string telCountryCode = "";
                var countryModel = _countryRepository.GetCountryByCountryCode(model.CountryCode);
                if (countryModel != null)
                {
                    telCountryCode = countryModel.TelCountryCode;
                }

                var modelDup = new ValidRegisModels
                {
                    Status = true,
                    TelCountryCode = telCountryCode
                };

                return Json(modelDup);
            }
            #endregion

            return Json(new { Response = "Error" });
        }

        public ActionResult PostalCodeValidateCompany(string RegCompanyAddress_PostalCode, string RegCompanyAddress_CountryCode)
        {
            string languageId = "";

            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (cultureCookie != null)
            {
                languageId = cultureCookie.Value.ToString();
            }
            else
            {
                languageId = CultureHelper.GetImplementedCulture(languageId);// This is safe
            }

            bool IsValid = true;

            string messageReturn = "";

            string errorMessage = "";
            if (string.IsNullOrEmpty(RegCompanyAddress_PostalCode))
            {
                RegCompanyAddress_PostalCode = "";
            }
            if (string.IsNullOrEmpty(RegCompanyAddress_CountryCode))
            {
                RegCompanyAddress_CountryCode = "";
            }

            var sqlQuery = "select dbo.VerifyPostalCode('" + RegCompanyAddress_PostalCode.ToString() + "','" + RegCompanyAddress_CountryCode.ToString() + "')";
            IFunctionRepository _repo = new FunctionRepository();

            var result = _repo.GetResultByQuery(sqlQuery);
            _repo.Dispose();
            if (result)
            {
                return Json(IsValid, JsonRequestBehavior.AllowGet);
            }
            else
            {
                errorMessage = messageReturn.GetStringResource("_Regis.ValidateMsg.InvalidFormatPostalCode", languageId);

                return Json(errorMessage, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult IsEmailAvailableConfig(string email, string initialEmail = "")
        {
            string languageId = "";

            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (cultureCookie != null)
            {
                languageId = cultureCookie.Value.ToString();
            }
            else
            {
                languageId = CultureHelper.GetImplementedCulture(languageId);// This is safe
            }

            var IsAllowDuplicateEmail = _appConfigRepository.GetAppConfigByName("Register_IsAllowDuplicateContactEmail").Value;

            var IsAllowMultipleEmail = _appConfigRepository.GetAppConfigByName("Register_IsAllowMultipleContactEmail").Value;

            string messageReturn = "";

            bool IsValid = false;

            if (IsAllowDuplicateEmail != "1")
            {
                //bool chk = false;
                bool chk = _regEmailTicketRepository.EmailExists(email.Trim(), initialEmail.Trim());

                if (!chk)
                {
                    IsValid = true;
                }
                else
                {
                    string errorMessage = messageReturn.GetStringResource("_Regis.RequestUser.ValidateMsg.DuplicateContactEmail", languageId);
                    return Json(errorMessage, JsonRequestBehavior.AllowGet);
                }
            }

            if (IsAllowMultipleEmail == "1")
            {
                string emailSeparator = _appConfigRepository.GetAppConfigByName("Portal_EmailSeparator").Value;

                foreach (string value in email.Split(Char.Parse(emailSeparator)))
                {
                    if (string.IsNullOrEmpty(value))
                    {
                        IsValid = false;
                        string errorMessage = messageReturn.GetStringResource("_Regis.ValidateMsg.InvalidFormatEmail", languageId);
                        return Json(errorMessage, JsonRequestBehavior.AllowGet);
                    }
                    string pattern = @"[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?";

                    //if (IsValidEmailAddress(value))
                    if (IsValidEmailAddressRegular(value, pattern))
                    {
                        IsValid = true;
                    }
                    else
                    {
                        string errorMessage = messageReturn.GetStringResource("_Regis.ValidateMsg.InvalidFormatEmail", languageId);
                        return Json(errorMessage, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else
            {
                string pattern = @"[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?";

                if (IsValidEmailAddressRegular(email, pattern))
                {
                    IsValid = true;
                }
                else
                {
                    string errorMessage = messageReturn.GetStringResource("_Regis.ValidateMsg.InvalidFormatEmail", languageId);
                    return Json(errorMessage, JsonRequestBehavior.AllowGet);
                }
            }

            if (IsValid)
            {
                return Json(IsValid, JsonRequestBehavior.AllowGet);
            }
            return Json(messageReturn, JsonRequestBehavior.AllowGet);
        }

        public ActionResult IsContactEmailAvailableConfig(string ContactEmail, string initialEmail = "")
        {
            string languageId = "";

            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (cultureCookie != null)
            {
                languageId = cultureCookie.Value.ToString();
            }
            else
            {
                languageId = CultureHelper.GetImplementedCulture(languageId);// This is safe
            }

            var IsAllowDuplicateEmail = _appConfigRepository.GetAppConfigByName("Register_IsAllowDuplicateContactEmail").Value;

            var IsAllowMultipleEmail = _appConfigRepository.GetAppConfigByName("Register_IsAllowMultipleContactEmail").Value;

            string messageReturn = "";

            bool IsValid = false;

            if (IsAllowDuplicateEmail != "1")
            {
                //bool chk = false;
                bool chk = _regEmailTicketRepository.EmailExists(ContactEmail.Trim(), initialEmail.Trim());

                if (!chk)
                {
                    IsValid = true;
                }
                else
                {
                    string errorMessage = messageReturn.GetStringResource("_Regis.RequestUser.ValidateMsg.DuplicateContactEmail", languageId);
                    return Json(errorMessage, JsonRequestBehavior.AllowGet);
                }
            }

            if (IsAllowMultipleEmail == "1")
            {
                string emailSeparator = _appConfigRepository.GetAppConfigByName("Portal_EmailSeparator").Value;

                foreach (string value in ContactEmail.Split(Char.Parse(emailSeparator)))
                {
                    if (string.IsNullOrEmpty(value))
                    {
                        IsValid = false;
                        string errorMessage = messageReturn.GetStringResource("_Regis.ValidateMsg.InvalidFormatEmail", languageId);
                        return Json(errorMessage, JsonRequestBehavior.AllowGet);
                    }
                    string pattern = @"[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?";

                    //if (IsValidEmailAddress(value))
                    if (IsValidEmailAddressRegular(value, pattern))
                    {
                        IsValid = true;
                    }
                    else
                    {
                        string errorMessage = messageReturn.GetStringResource("_Regis.ValidateMsg.InvalidFormatEmail", languageId);
                        return Json(errorMessage, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else
            {
                string pattern = @"[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?";

                if (IsValidEmailAddressRegular(ContactEmail, pattern))
                {
                    IsValid = true;
                }
                else
                {
                    string errorMessage = messageReturn.GetStringResource("_Regis.ValidateMsg.InvalidFormatEmail", languageId);
                    return Json(errorMessage, JsonRequestBehavior.AllowGet);
                }
            }

            if (IsValid)
            {
                return Json(IsValid, JsonRequestBehavior.AllowGet);
            }
            return Json(messageReturn, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CaptchaPartial()
        {
            //return PartialView("_CaptchaPartial");
            return PartialView(@"~/Views/Register/_CaptchaPartial.cshtml");
        }

        [HttpPost, CaptchaVerify("Captcha is not valid")]
        public ActionResult CheckCaptcha()
        {
            if (ModelState.IsValid)
            {
                ModelState.Clear();
                if (Request.IsAjaxRequest())
                    return Json(new
                    {
                        Message = "Success",
                        IsOk = bool.TrueString
                    });
            }

            if (Request.IsAjaxRequest())
            {
                string languageId = "";

                HttpCookie cultureCookie = Request.Cookies["_culture"];
                if (cultureCookie != null)
                {
                    languageId = cultureCookie.Value.ToString();
                }
                else
                {
                    languageId = CultureHelper.GetImplementedCulture(languageId);// This is safe
                }
                string messageReturn = "";
                string errorMessage = messageReturn.GetStringResource("_Regis.ValidateMsg.CaptchaNotValid", languageId);
                IUpdateInfoModel captchaValue = this.GenerateCaptchaValue(5);
                return Json(new
                {
                    Message = errorMessage,
                    IsOk = bool.FalseString,
                    Captcha =
                new Dictionary<string, string>
                                    {
                                        {captchaValue.ImageElementId, captchaValue.ImageUrl},
                                        {captchaValue.TokenElementId, captchaValue.TokenValue}
                                    }
                });
            }

            return Json(new
            {
                Message = "Error",
                IsOk = bool.FalseString
            });
        }

        [HttpPost]
        public ActionResult CheckDuplicateOrgContactPerson(ValidOrgContactPersonModels model)
        {
            try
            {
                string languageId = "";

                HttpCookie cultureCookie = Request.Cookies["_culture"];
                if (cultureCookie != null)
                {
                    languageId = cultureCookie.Value.ToString();
                }
                else
                {
                    languageId = CultureHelper.GetImplementedCulture(languageId);// This is safe
                }

                string messageReturn = "";

                //string emailAdminBSP = "";

                string emailReturn = "";

                string stringResourceReturn = "";

                string usernameDuplicate = "";

                string duplicateType = "";// ประเภทที่ซ้ำ 0 = Message only, 1 = Send mail .Register.ContactBSPAdmin.ReqUser,2 = Send mail . Register.AdoptionNotify , 3 = Send mail .ForgotPassword ,4 = Show Alert InvitationCodeNotFound

                int SupplierID = 0;

                if (!string.IsNullOrEmpty(model.hidSupplierID))
                {
                    SupplierID = Convert.ToInt32(model.hidSupplierID);
                }

                string invitationCode = "";
                invitationCode = model.hidInvitationCode ?? "";

                if (!string.IsNullOrEmpty(model.hidService2))
                {
                    if (_registerDataService.CheckFullnameContactDuplicateOP(model.FirstName_Contact, model.LastName_Contact, SupplierID, invitationCode.Trim(), out emailReturn, out stringResourceReturn, out duplicateType))
                    {
                        string errorMessage = messageReturn.GetStringResource(stringResourceReturn, languageId);
                        var modelDup = new ValidOrgContactPersonModels
                        {
                            hidSupplierID = model.hidSupplierID,
                            Status = false,
                            Message = errorMessage,
                            emailReturn = emailReturn,
                            DuplicateType = duplicateType
                        };
                        return Json(modelDup);
                    }
                }
                else
                {
                    if (_registerDataService.CheckFullnameContactDuplicateNonOP(model.FirstName_Contact, model.LastName_Contact, SupplierID, out stringResourceReturn, out usernameDuplicate, out duplicateType))
                    {
                        string errorMessage = messageReturn.GetStringResource(stringResourceReturn, languageId);
                        var modelDup = new ValidOrgContactPersonModels
                        {
                            hidSupplierID = model.hidSupplierID,
                            Status = false,
                            Message = errorMessage,
                            DuplicateType = duplicateType,
                            UsernameDuplicate = usernameDuplicate
                        };
                        return Json(modelDup);
                    }
                }

                var modelDupTrue = new ValidOrgContactPersonModels
                {
                    Status = true,
                    Message = ""
                };
                return Json(modelDupTrue);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }

        [HttpPost]
        public ActionResult AddContactRequest(RegContactRequestModels contactModel)
        {
            string languageId = GetCurrentLanguageHelper.GetStringLanguageIDCurrent();

            string message = "";

            try
            {
                #region-------- Insert RegContactRequest And log -----------------

                var model = new Tbl_RegContactRequest()
                {
                    SupplierID = Convert.ToInt32(contactModel.SupplierID),
                    UserReqStatusID = 2,
                    RequestDate = DateTime.UtcNow,
                    TitleID = contactModel.ContactTitleName,
                    FirstName_Local = contactModel.FirstName_Contact,
                    FirstName_Inter = contactModel.FirstName_Contact,
                    LastName_Local = contactModel.LastName_Contact,
                    LastName_Inter = contactModel.LastName_Contact,
                    JobTitleID = contactModel.ContactJobTitle,
                    OtherJobTitle = contactModel.ContactOtherJobTitle ?? "",
                    Department = "",
                    PhoneCountryCode = contactModel.ContactPhoneNoCountryCode ?? "",
                    PhoneNo = contactModel.ContactPhoneNo,
                    PhoneExt = contactModel.ContactPhoneExt ?? "",
                    MobileCountryCode = contactModel.ContactMobileCountryCode ?? "",
                    MobileNo = contactModel.ContactMobile ?? "",
                    FaxNo = "",
                    FaxExt = "",
                    Email = contactModel.ContactEmail,
                    UserID = 0,
                    isDeleted = 0
                };

                int userReqId = _regContactRequestRepository.InsertReturnUserReqId(model);

                _logRegContactRequestRepository.Insert(userReqId, "Add", 1);

                #endregion

                #region-------- Insert RegContactRequestHistory -----------------
                var historyModel = new Tbl_RegContactRequestHistory()
                {
                    UserReqId = userReqId,
                    UserReqStatusID = 2,
                    Remark = "",
                    UpdateBy = 1,
                    UpdateDate = DateTime.UtcNow
                };

                _regContactRequestHistoryRepository.Insert(historyModel);
                #endregion

                #region-------- Insert RegContactRequestServiceTypeMapping -----------------

                if (!string.IsNullOrEmpty(contactModel.Service1))
                {
                    var mappingModel = new Tbl_RegContactRequestServiceTypeMapping()
                    {
                        UserReqId = userReqId,
                        ServiceTypeID = Convert.ToInt32(contactModel.Service1),
                        InvitationCode = ""
                    };

                    _regContactReqServMapping.Insert(mappingModel);
                }

                if (!string.IsNullOrEmpty(contactModel.Service2))
                {
                    var mappingModel = new Tbl_RegContactRequestServiceTypeMapping()
                    {
                        UserReqId = userReqId,
                        ServiceTypeID = Convert.ToInt32(contactModel.Service2),
                        InvitationCode = contactModel.ContactInvitationCode ?? ""
                    };

                    _regContactReqServMapping.Insert(mappingModel);
                }

                if (!string.IsNullOrEmpty(contactModel.Service3))
                {
                    var mappingModel = new Tbl_RegContactRequestServiceTypeMapping()
                    {
                        UserReqId = userReqId,
                        ServiceTypeID = Convert.ToInt32(contactModel.Service3),
                        InvitationCode = contactModel.ContactInvitationCode ?? ""
                    };

                    _regContactReqServMapping.Insert(mappingModel);
                }

                #endregion

                _registerDataService.SendingEmailNewRequestUser(userReqId, languageId, contactModel.ContactEmail);

                _registerDataService.SendingEmailNewRequestUserPIS(userReqId, languageId);
                //if (!string.IsNullOrEmpty(contactModel.EmailAdminBSP))
                //{
                //    _registerDataService.SendingEmailBSPAdminRequestContact(contactModel.EmailAdminBSP, languageId);
                //}

                message = message.GetStringResource("_Regis.RequestUser.InfoMsg.RequestUserSuccess", languageId);

                string email = "";
                email = "<a href=\"mailto:" + contactModel.ContactEmail + "\"> " + contactModel.ContactEmail + " </a>";

                message = @message.Replace("<%email_address%>", email);

                return Json(new { Message = message });
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                //message = message.GetStringResource("_Regis.Msg_SendEmailToAdminUnSuccess", languageId);
                message = message.GetStringResource(ex.Message, languageId);
                return Json(new { Message = message });
            }

            return Json(new { Response = "Error" });
        }

        [HttpPost]
        public ActionResult SendEmailByDuplicateContact(SendEmailDuplicateContactModels model)
        {
            string languageId = "";

            string message = "";

            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (cultureCookie != null)
            {
                languageId = cultureCookie.Value.ToString();
            }
            else
            {
                languageId = CultureHelper.GetImplementedCulture(languageId);// This is safe
            }

            try
            {
                // model.DuplicateType // ประเภทที่ซ้ำ 0 = Message only, 1 = Send mail .Register.ContactBSPAdmin.ReqUser,2 = Send mail . Register.AdoptionNotify , 3 = Send mail .ForgotPassword ,4 = Show Alert InvitationCodeNotFound
                if (!string.IsNullOrEmpty(model.DuplicateType))
                {
                    if (model.DuplicateType == "1")
                    {
                        //_registerDataService.SendingEmailBSPAdminRequestContact(model.EmailAdminBSP, languageId, model.ContactEmail);
                        _registerDataService.SendingEmailBSPAdminRequestContact(model, languageId, model.ContactEmail);

                        message = message.GetStringResource("_Regis.RequestUser.InfoMsg.NotifyBSPAdminMailSuccess", languageId);

                        message = message.Replace("<%email_address%>", model.ContactEmail);

                        return Json(new { Message = message });
                    }
                    else if (model.DuplicateType == "2")
                    {
                        int adsActionFlag = 0;
                        if (!string.IsNullOrEmpty(model.ContactInvitationCode))
                        {
                            adsActionFlag = 1;
                        }

                        var opNotifyModel = new Tbl_OPAdoptionNotify()
                        {
                            SupplierID = Convert.ToInt32(model.SupplierID),
                            RequestDate = DateTime.UtcNow,
                            TitleID = model.ContactTitleName,
                            FirstName_Local = model.FirstName_Contact,
                            FirstName_Inter = "",
                            LastName_Local = model.LastName_Contact,
                            LastName_Inter = "",
                            JobTitleID = model.ContactJobTitle,
                            OtherJobTitle = model.ContactOtherJobTitle ?? "",
                            Department = "",
                            PhoneCountryCode = model.ContactPhoneNoCountryCode ?? "",
                            PhoneNo = model.ContactPhoneNo,
                            PhoneExt = model.ContactPhoneExt ?? "",
                            MobileCountryCode = model.ContactMobileCountryCode ?? "",
                            MobileNo = model.ContactMobile ?? "",
                            FaxNo = "",
                            FaxExt = "",
                            Email = model.ContactEmail,
                            RefRegID = 0,
                            RefUserReqID = 0,
                            isSendMail = 0,
                            SAFReqNo = "",
                            SAFActionFlag = 0,
                            ADSActionFlag = adsActionFlag,
                            InvitationCode = model.ContactInvitationCode ?? ""
                        };

                        int adoptionReqID = _oPAdoptionNotifyRepository.InsertReturnAdoptionReqID(opNotifyModel);

                        //_registerDataService.SendingEmailAdoptionNotifyExistingUser(adoptionReqID,languageId, model.ContactEmail);

                        //_oPAdoptionNotifyRepository.UpdateFlagIsSendMailByAdoptionReqID(adoptionReqID);

                        message = message.GetStringResource("_Regis.RequestUser.InfoMsg.NotifyOPAdoptionMailSuccess", languageId);
                        string email = "";
                        email = "<a href=\"mailto:" + model.ContactEmail + "\"> " + model.ContactEmail + " </a>";
                        message = message.Replace("<%email_address%>", email);

                        return Json(new { Message = message });
                    }
                    else if (model.DuplicateType == "3")
                    {
                        var forgotPasswordModel = new ForgotPasswordModel()
                        {
                            Username = model.UsernameDuplicate
                        };

                        HttpClient client = new HttpClient();
                        string baseURL = WebConfigurationManager.AppSettings["baseURL"];

                        string languageURL = "?languageId=" + languageId;

                        client.BaseAddress = new Uri(baseURL);
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                        string Jsonstr = JsonConvert.SerializeObject(forgotPasswordModel);
                        HttpContent content = new StringContent(Jsonstr);
                        content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                        string urlAll = string.Format("{0}api/AccountPortal/ForgotPassword{1}", baseURL, languageURL);

                        HttpResponseMessage response = client.PostAsync(urlAll, content).Result;

                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            var responseBody = response.Content.ReadAsStringAsync().Result;
                            JObject _response = JObject.Parse(responseBody);

                            var query = from c in _response["data"].Children()
                                        select c;
                            query = query.ToList();

                            var responseResult = (from x in query
                                                  select new
                                                  {
                                                      respMessage = (string)x.SelectToken("Message"),
                                                      respResult = (bool)x.SelectToken("Result"),
                                                      respEmail = (string)x.SelectToken("Email")
                                                  }).ToList();

                            var respResult = responseResult.Select(x => x.respResult).FirstOrDefault();
                            message = responseResult.Select(x => x.respMessage).FirstOrDefault();

                            if (respResult)
                            {
                                message = responseResult.Select(x => x.respMessage).FirstOrDefault();
                                return Json(new { Message = message });
                            }
                            else
                            {
                                message = message.GetStringResource("_Regis.RequestUser.InfoMsg.NotifyForgotPasswordMailFail", languageId);

                                return Json(new { Message = message });
                            }
                        }
                        else
                        {
                            message = message.GetStringResource("_Regis.RequestUser.InfoMsg.NotifyForgotPasswordMailFail", languageId);

                            return Json(new { Message = message });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);

                message = message.GetStringResource("_Regis.RequestUser.InfoMsg.NotifyForgotPasswordMailFail", languageId);

                return Json(new { Message = message });
            }
            return Json(new { Response = "Error" });
        }

        public FileResult DownloadAgreement()
        {
            string LanguageID = "";
            LanguageID = CurrentTime.getLanguageID();
            string file = "";
            string pathTempTH = _appConfigRepository.GetAppConfigByName("Register_AgreementTermPath_TH").Value;
            string pathTempEN = _appConfigRepository.GetAppConfigByName("Register_AgreementTermPath_EN").Value;
            string pathAttachment = WebConfigurationManager.AppSettings["pathAttachment"];

            switch (LanguageID)
            {
                case "2":
                    file = @pathAttachment + @pathTempTH;
                    break;

                case "1":
                default:
                    file = @pathAttachment + @pathTempEN;
                    break;
            }
            byte[] fileBytes = System.IO.File.ReadAllBytes(@file);
            string virtualFilePath = @file;
            string fileName2 = Path.GetFileName(virtualFilePath);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName2);
        }

        public ActionResult Download(string filePath, string fileName = "")
        {
            string pathAttachment = WebConfigurationManager.AppSettings["pathAttachment"];
            filePath = pathAttachment + filePath;

            if (!System.IO.File.Exists(@filePath))
            {
                filePath = @pathAttachment + @"\SWWDocuments\UserManualForDownload\EPInstruction.pdf";
            }
            byte[] fileBytes = System.IO.File.ReadAllBytes(@filePath);

            if (string.IsNullOrEmpty(fileName))
            {
                fileName = "download";
            }

            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        public ActionResult GetProductCategory2(int id = 0)
        {
            string LanguageID = "";
            LanguageID = CurrentTime.getLanguageID();
            var data = new List<Tbl_ProductCategoryLev2>();
            if (LanguageID == "1")
            {
                data = _productCategoryRepository.GetProductCat2(id, 1).ToList();
            }
            else if (LanguageID == "2")
            {
                data = _productCategoryRepository.GetProductCat2(id, 2).ToList();
            }

            return View(data);
        }

        public ActionResult GetProductCategory3(int id = 0)
        {
            string LanguageID = "";
            LanguageID = CurrentTime.getLanguageID();
            var data = new List<Tbl_ProductCategoryLev3>();
            if (LanguageID == "1")
            {
                data = _productCategoryRepository.GetProductCat3(id, 1).ToList();
            }
            else if (LanguageID == "2")
            {
                data = _productCategoryRepository.GetProductCat3(id, 2).ToList();
            }

            return View(data);
        }

        [HttpPost]
        public ActionResult CheckInvitationCode(string invitationCode)
        {
            var InvitationNO = true;

            var dataBuyer = _adsbuyerRepository.GetBuyerOrgIDByInvitationCode(invitationCode);

            if (dataBuyer != null && dataBuyer != "")
            {
                InvitationNO = true;
            }
            else
            {
                bool result = _opnBuyerRepository.verifyInvitationCode(invitationCode);
                if (!result)
                {
                    InvitationNO = false;
                }
            }

            return Json(InvitationNO);
        }

        [HttpPost]
        public ActionResult GetProductCategory(params int?[] productCategorys)
        {
            string LanguageID = "";
            LanguageID = CurrentTime.getLanguageID();
            var ListCategory = new List<CategoryModel>();

            if (productCategorys != null)
            {
                if (LanguageID == "1")
                {
                    ListCategory = _productCategoryRepository.GetItemCat1ByCat3(productCategorys).OrderBy(o => o.CategoryName_Inter).ToList();
                }
                else if (LanguageID == "2")
                {
                    ListCategory = _productCategoryRepository.GetItemCat1ByCat3(productCategorys).OrderBy(o => o.CategoryName_Local).ToList();
                }
            }

            //ViewBag.ListLv1 = ListL1;

            return View(ListCategory);
        }

        #region------------------------------------------------Function Helper------------------------------------------------------

        private bool IsValidEmailAddressRegular(string emailAddress, string paatten)
        {
            return new System.ComponentModel.DataAnnotations
                .RegularExpressionAttribute(paatten)
                .IsValid(emailAddress);
        }

        private string GetFileSize(int contentLength)
        {
            string[] sizes = { "B", "KB", "MB", "GB" };
            int len = contentLength;
            int order = 0;
            while (len >= 1024 && order + 1 < sizes.Length)
            {
                order++;
                len = len / 1024;
            }

            // Adjust the format string to your preferences. For example "{0:0.#}{1}" would
            // show a single decimal place, and no space.
            string result = String.Format("{0:0.##} {1}", len, sizes[order]);
            return result;
        }

        private bool CreateFolderIfNeeded(string path)
        {
            bool result = true;
            if (!Directory.Exists(path))
            {
                try
                {
                    Directory.CreateDirectory(path);
                }
                catch (Exception)
                {
                    /*TODO: You must process this exception.*/
                    result = false;
                }
            }
            return result;
        }

        private string GetHtmlText(string documentNameID, string documentTypeID, string fileName, string virtualFilePath, string fileGuidName, string guid, string tbxdocname = "", string fileSize = "", string customerDocName = "")
        {
            string link = HtmlHelper.GenerateLink(this.ControllerContext.RequestContext, System.Web.Routing.RouteTable.Routes, fileName, "Default", "Download", "Register", new RouteValueDictionary(new { filePath = virtualFilePath, fileName = fileName }), new Dictionary<string, object> { { "class", "register" } });

            string linkDelete = "<img src=\"/Content/images/icon/ico-sp_deluser2.gif\" alt=\"Submit\" id=\"deleteAttachment\" data-toggle=\"tooltip\" data-placement=\"left\" title=\"Delete file\" docType = " + documentTypeID + " class = \"iconDeleteDoc\">";

            string docName = "";

            if (documentNameID == "-1")
            {
                docName = tbxdocname;
            }
            else
            {
                docName = customerDocName;
            }
            string otherDocumentType = tbxdocname;

            string htmlTextTable = "";
            htmlTextTable += "<tr class=\"main_table_row docName_" + documentNameID + "\">";
            htmlTextTable += "<td align=\"left\">";
            htmlTextTable += "<div  class=\"file-attachment-download\">" + link + "</div>";
            htmlTextTable += "<input type=\"hidden\" name=\"hidFile_" + documentNameID + "[]\" value=\"" + fileName + "\" />";
            htmlTextTable += "<input type=\"hidden\" name=\"hidGuid_" + documentNameID + "[]\" value=\"" + fileGuidName + "\" />";
            htmlTextTable += "<input type=\"hidden\" name=\"hidOtherDocument_" + documentNameID + "[]\" value=\"" + otherDocumentType + "\" />";
            htmlTextTable += "</td>";
            htmlTextTable += "<td align=\"right\">" + fileSize + "</td>";
            htmlTextTable += "<td align=\"left\">" + docName + "</td>";
            htmlTextTable += "<td align=\"center\">" + linkDelete + "</td>";
            htmlTextTable += "</tr>";

            return htmlTextTable;
        }

        private void UploadAttachment(string fileName, string guidName, int regID, int documentNameID, string otherDocument)
        {
            try
            {
                if (!_regAttachmentRepository.RegAttachmentExists(guidName, regID))
                {
                    string pathTemp = _appConfigRepository.GetAppConfigByName("Register_AttachmentPathTemp").Value;
                    string pathAttachment = WebConfigurationManager.AppSettings["pathAttachment"];

                    string pathTempRegis = "Register";

                    string pathForSavingTemp = pathAttachment + pathTemp + pathTempRegis;
                    //string pathForSavingTemp = Server.MapPath("~/Register_AttachmentPathTemp/" + regID);

                    string filePathTemp = pathForSavingTemp + "/" + guidName;

                    if (System.IO.File.Exists(filePathTemp))
                    {
                        byte[] fileBytes = System.IO.File.ReadAllBytes(filePathTemp);

                        string virtualFilePath = filePathTemp;
                        string fileName2 = Path.GetFileName(virtualFilePath);
                        var fileTemp = File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName2);

                        if (fileTemp != null && fileTemp.FileContents.Length != 0)
                        {
                            string pathSave = _appConfigRepository.GetAppConfigByName("Register_AttachmentPath").Value;
                            string pathForSaving = pathAttachment + pathSave + regID;
                            //string pathForSaving = Server.MapPath("~/Register_AttachmentPath/" + regID);

                            string filePath = pathForSaving + "/" + guidName;

                            if (this.CreateFolderIfNeeded(pathForSaving))
                            {
                                // Ensure that the target does not exist.
                                if (System.IO.File.Exists(filePath))
                                    System.IO.File.Delete(filePath);

                                // Move the file.
                                System.IO.File.Move(filePathTemp, filePath);

                                var models = new Tbl_RegAttachment
                                {
                                    RegID = regID,
                                    AttachmentName = fileName,
                                    AttachmentNameUnique = guidName,
                                    SizeAttach = fileTemp.FileContents.Length.ToString(),
                                    DocumentNameID = documentNameID,
                                    OtherDocument = otherDocument
                                };

                                _regAttachmentRepository.Insert(models);

                                _logRegAttachmentRepository.InsertLogAdd(regID, guidName, "Add");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void RemoveAttachment(string guidName)
        {
            try
            {
                var attachmentModel = _regAttachmentRepository.GetRegAttachmentByGuidName(guidName);

                if (attachmentModel != null)
                {
                    string pathSave = _appConfigRepository.GetAppConfigByName("Register_AttachmentPath").Value;
                    string pathAttachment = WebConfigurationManager.AppSettings["pathAttachment"];
                    string pathForSaving = pathAttachment + pathSave + attachmentModel.RegID;
                    //string pathForSaving = Server.MapPath("~/Register_AttachmentPath/" + attachmentModel.RegID);

                    string filePath = pathForSaving + "/" + guidName;

                    if (System.IO.File.Exists(filePath))
                        System.IO.File.Delete(filePath);

                    _logRegAttachmentRepository.InsertLogDelete(attachmentModel.RegID, attachmentModel.AttachmentID, "Delete");
                    _regAttachmentRepository.Delete(attachmentModel.RegID, attachmentModel.AttachmentID);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        #endregion
    }
}
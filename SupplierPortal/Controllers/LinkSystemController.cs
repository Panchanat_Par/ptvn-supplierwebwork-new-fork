﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SupplierPortal.Data.Models.Repository.MenuPortal;
using SupplierPortal.Data.Models.Repository.UserSession;
using SupplierPortal.Data.Models.Repository.User;
using SupplierPortal.Data.Models.Repository.APILogs;
using SupplierPortal.Data.Models.SupportModel.AccountPortal;
using SupplierPortal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Framework.MethodHelper;

namespace SupplierPortal.Controllers
{
    public class LinkSystemController : Controller
    {
        private IMenuPortalRepository _menuPortalRepository;
        private IUserSessionRepository _userSessionRepository;
        private IUserRepository _userRepository;
        private IAPILogsRepository _apiLogsRepository;

        public LinkSystemController()
        {
            _menuPortalRepository = new MenuPortalRepository();
            _userSessionRepository = new UserSessionRepository();
            _userRepository = new UserRepository();
            _apiLogsRepository = new APILogsRepository();
        }

        public LinkSystemController(
            MenuPortalRepository menuPortalRepository,
            UserSessionRepository userSessionRepository,
            UserRepository userRepository,
            APILogsRepository apiLogsRepository
            )
        {
            _menuPortalRepository = menuPortalRepository;
            _userSessionRepository = userSessionRepository;
            _userRepository = userRepository;
            _apiLogsRepository = apiLogsRepository;
        }

        // GET: LinkSystem
        public ActionResult Index()
        {
            return View();
        }

        // GET: /LinkSystem/ReturnHome
        [AllowAnonymous]
        public ActionResult ReturnHome(string UID, int MENU)
        {
            try
            {
                #region Insert API Log

                string visitorsIPAddr = GetClientIPAddressHelper.GetLocalIPAddress();
                string dataLogs = @"UID: " + UID + ";MENU: " + MENU;
                var _APILogs = new Tbl_APILogs()
                {
                    ReqID = "",
                    SystemID = 0,
                    Events = "ReturnHome",
                    Events_Time = DateTime.UtcNow,
                    IPAddress_Client = visitorsIPAddr,
                    APIKey = "",
                    APIName = "",
                    Return_Code = "",
                    Return_Status = "",
                    Return_Message = "",
                    Data = dataLogs,
                    UserGUID = UID,
                };

                _apiLogsRepository.Insert(_APILogs);

                #endregion Insert API Log

                if (Session["GUID"] == null)
                {
                    Response.Redirect("~/Error/Timeout");
                }
                else
                {
                    #region Check ActiveUserSession

                    string guid = Session["GUID"].ToString();
                    string username = Session["username"].ToString();

                    CheckTimeoutModel model = new CheckTimeoutModel
                    {
                        UserGuid = guid
                    };

                    HttpClient client = new HttpClient();

                    string baseURL = WebConfigurationManager.AppSettings["baseURL"];

                    client.BaseAddress = new Uri(baseURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    string Jsonstr = JsonConvert.SerializeObject(model);
                    HttpContent content = new StringContent(Jsonstr);
                    content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                    string urlAll = string.Format("{0}api/AccountPortal/ActiveUserSession", baseURL);

                    HttpResponseMessage response = client.PostAsync(urlAll, content).Result;

                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var responseBody = response.Content.ReadAsStringAsync().Result;
                        try
                        {
                            JObject _response = JObject.Parse(responseBody);

                            var query = from c in _response["data"].Children()
                                        select c;
                            query = query.ToList();

                            var responseResult = (from x in query
                                                  select new
                                                  {
                                                      isSessionTimeout = (int)x.SelectToken("IsSessionTimeout"),
                                                  }).ToList();

                            var respResult = responseResult.Select(x => x.isSessionTimeout).FirstOrDefault();

                            if (respResult != 0)
                            {
                                Response.Redirect("~/Error/Timeout");
                            }
                        }
                        catch (Exception ex)
                        {
                            Response.Redirect("~/Error/Error?ErrorMessage=" + ex.Message);
                        }

                        var ChkisAcceptTermOfUse = _userRepository.FindByUsername(username);
                        if (ChkisAcceptTermOfUse.isAcceptTermOfUse == 0)
                        {
                            Response.Redirect("~/Agreement/Index");
                        }

                        if (ChkisAcceptTermOfUse.IsActivated == 0)
                        {
                            Response.Redirect("~/Activated/ChangePassword");
                        }
                    }
                    else
                    {
                        Response.Redirect("~/Error/Timeout");
                    }

                    #endregion Check ActiveUserSession
                }

                var userSession = _userSessionRepository.GetUserSession(UID);

                if (userSession == null)
                {
                    return View();
                }
                string userGuid = userSession.UserGuid;

                DateTime lastAccessed = userSession.LastAccessed ?? DateTime.Now;

                DateTime utcNow = DateTime.UtcNow;

                long elapsedTicks = utcNow.Ticks - lastAccessed.Ticks;
                TimeSpan elapsedSpan = new TimeSpan(elapsedTicks);

                double totalDateDiff = elapsedSpan.TotalMilliseconds;

                string strTimeout = System.Configuration.ConfigurationManager.AppSettings.Get("TimeOut");

                long timeOut = Convert.ToInt64(strTimeout);

                if (timeOut < totalDateDiff)
                {
                    Response.Redirect("~/Error/Timeout");
                }

                if (userGuid != UID)
                {
                    return View();
                }

                var resultMenu = _menuPortalRepository.GetMenuById(MENU);
                if (resultMenu != null)
                {
                    return RedirectToAction(resultMenu.ActionName, resultMenu.ControllerName);
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("~/Error/Error?ErrorMessage=" + ex.Message);
            }

            return View();
        }

        [HttpPost]
        public ActionResult CheckUserSessionTimeOut(string userguid)
        {
            if (string.IsNullOrEmpty(userguid))
            {
                return Json("TimeOut");
            }
            else
            {
                string guid = userguid;

                CheckTimeoutModel model = new CheckTimeoutModel
                {
                    UserGuid = guid
                };

                HttpClient client = new HttpClient();

                string baseURL = WebConfigurationManager.AppSettings["baseURL"];

                client.BaseAddress = new Uri(baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                string Jsonstr = JsonConvert.SerializeObject(model);
                HttpContent content = new StringContent(Jsonstr);
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                string urlAll = string.Format("{0}api/AccountPortal/ActiveUserSession", baseURL);

                HttpResponseMessage response = client.PostAsync(urlAll, content).Result;

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var responseBody = response.Content.ReadAsStringAsync().Result;
                    try
                    {
                        JObject _response = JObject.Parse(responseBody);

                        var query = from c in _response["data"].Children()
                                    select c;
                        query = query.ToList();

                        var responseResult = (from x in query
                                              select new
                                              {
                                                  isSessionTimeout = (int)x.SelectToken("IsSessionTimeout"),
                                              }).ToList();

                        var respResult = responseResult.Select(x => x.isSessionTimeout).FirstOrDefault();

                        if (respResult != 0)
                        {
                            return Json("TimeOut");
                        }
                    }
                    catch (Exception ex)
                    {
                        return Json("TimeOut");
                    }
                }
                else
                {
                    return Json("TimeOut");
                }
            }

            return Json("success");
        }
    }
}
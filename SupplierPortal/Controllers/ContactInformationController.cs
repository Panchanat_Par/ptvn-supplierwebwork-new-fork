﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.CompanyProfile;
using SupplierPortal.Data.Models.Repository.Organization;
using SupplierPortal.Data.Models.Repository.User;
using SupplierPortal.Data.Models.Repository.Address;
using SupplierPortal.Data.Models.Repository.Language;
using SupplierPortal.Data.Models.Repository.OrgAddress;
using SupplierPortal.Data.Models.Repository.Country;
using SupplierPortal.Data.Models.Repository.AutoComplete_SubDistrict;
using SupplierPortal.Data.Models.Repository.AutoComplete_City;
using SupplierPortal.Data.Models.Repository.AutoComplete_State;
using SupplierPortal.Data.Models.Repository.TrackingRequest;
using SupplierPortal.Services.CompanyProfileManage;
using SupplierPortal.Framework.Localization;
//using System.Transactions;
using SupplierPortal.Data.Models.Repository.Function;
using System.Text.RegularExpressions;
using SupplierPortal.Data.Models.Repository.ContactPerson;
using SupplierPortal.Data.Models.Repository.TrackingOrgAddress;
using SupplierPortal.Data.Models.Repository.TrackingComprofile;
using SupplierPortal.Data.Models.Repository.PortalFieldConfig;
using SupplierPortal.Framework.MethodHelper;

namespace SupplierPortal.Controllers
{
    public class ContactInformationController : ControllerBase
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        IOrganizationRepository _organizationRepository;
        IUserRepository _userRepository;
        ILanguageRepository _languageRepository;
        IOrgAddressRepository _orgAddressRepository;
        IAddressRepository _addressRepository;
        ICountryRepository _countryRepository;
        IAutoComplete_SubDistrictRepository _subDistrictRepository;
        IAutoComplete_CityRepository _cityRepository;
        IAutoComplete_StateRepository _stateRepository;
        IContactPersonRepository _contactPersonRepository;
        ITrackingOrgAddressRepository _trackingOrgAddressRepository;
        ITrackingComprofileRepository _trackingComprofileRepository;
        IPortalFieldConfigRepository _portalFieldConfigRepository;
        ITrackingRequestRepository _trackingRequestRepository;
        IComProfileTrackingService _comProfileTrackingService;

        public ContactInformationController()
        {
            _organizationRepository = new OrganizationRepository();
            _userRepository = new UserRepository();
            _languageRepository = new LanguageRepository();
            _orgAddressRepository = new OrgAddressRepository();
            _addressRepository = new AddressRepository();
            _countryRepository = new CountryRepository();
            _subDistrictRepository = new AutoComplete_SubDistrictRepository();
            _cityRepository = new AutoComplete_CityRepository();
            _stateRepository = new AutoComplete_StateRepository();
            _contactPersonRepository = new ContactPersonRepository();
            _trackingOrgAddressRepository = new TrackingOrgAddressRepository();
            _trackingComprofileRepository = new TrackingComprofileRepository();
            _portalFieldConfigRepository = new PortalFieldConfigRepository();
            _trackingRequestRepository = new TrackingRequestRepository();
            _comProfileTrackingService = new ComProfileTrackingService();
        }
        public ContactInformationController
            (
                OrganizationRepository organizationRepository,
                UserRepository userRepository,
                LanguageRepository languageRepository,
                OrgAddressRepository orgAddressRepository,
                AddressRepository addressRepository,
                ICountryRepository countryRepository,
                AutoComplete_SubDistrictRepository subDistrictRepository,
                AutoComplete_CityRepository cityRepository,
                AutoComplete_StateRepository stateRepository,
                ContactPersonRepository contactPersonRepository,
                TrackingOrgAddressRepository trackingOrgAddressRepository,
                TrackingComprofileRepository trackingComprofileRepository,
                PortalFieldConfigRepository portalFieldConfigRepository,
                TrackingRequestRepository trackingRequestRepository,
                ComProfileTrackingService comProfileTrackingService
            )
        {
            _organizationRepository = organizationRepository;
            _userRepository = userRepository;
            _languageRepository = languageRepository;
            _orgAddressRepository = orgAddressRepository;
            _addressRepository = addressRepository;
            _countryRepository = countryRepository;
            _subDistrictRepository = subDistrictRepository;
            _cityRepository = cityRepository;
            _stateRepository = stateRepository;
            _contactPersonRepository = contactPersonRepository;
            _trackingOrgAddressRepository = trackingOrgAddressRepository;
            _trackingComprofileRepository = trackingComprofileRepository;
            _portalFieldConfigRepository = portalFieldConfigRepository;
            _trackingRequestRepository = trackingRequestRepository;
            _comProfileTrackingService = comProfileTrackingService;
        }

        //SupplierPortalEntities db;
        // GET: ContactInformation

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Address(string alertMessage = "")
        {

            ViewBag.AlertMessage = alertMessage;
            return View();
        }

        [HttpPost]
        public ActionResult Address(CompanyAddress model)
        {
            string username = "";
            int userID = 0;
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }
            var user = _userRepository.FindByUsername(username);
            if (user != null)
            {
                userID = user.UserID;
            }

            int supplierID = _organizationRepository.GetSupplierIDByUsername(username);

            int chkTrackingReqID = 0;

            try
            {
                //using (TransactionScope scope = new TransactionScope())
                //{
                var userContact = _userRepository.FindAllByUsername(User.Identity.Name);
                //var contactData = _contactPersonRepository.GetContactPersonByContectID(userContact.ContactID ?? 0);
                //test tracking check
                var itemtrackingStatus = _addressRepository.GetTrackingStatus();

                if (itemtrackingStatus.Count() > 0)
                {
                    #region Update Data
                    var addressData = _addressRepository.GetAddressByUsername(User.Identity.Name);
                    var OrgAdd = _orgAddressRepository.GetAddress(supplierID, model.CompanyAddressID ?? 0, 1);
                    var dataTrackingOrg = _trackingOrgAddressRepository.GetDataBySupplierID(model.SupplierID ?? 0);
                    //tracking Req

                    bool checkInsertReq = false;
                    if (model.CompanyAddressID != model.DeliverAddressID && model.CheckB)
                    {
                        checkInsertReq = true;
                    }
                    else
                    {
                        checkInsertReq = false;
                    }
                    if (checkInsertReq == false || model.chkDataChange)
                    {
                        logger.Debug("(insert reqId in ContactInfomationControllers [before Insert])TrackingReqID : " + chkTrackingReqID);

                        //chkTrackingReqID = _trackingOrgAddressRepository.InsertReqReturnTrackingReqID(userContact.UserID, supplierID);

                        var _trackingRequestModel = new Tbl_TrackingRequest()
                        {
                            SupplierID = model.SupplierID,
                            ReqBy = userContact.UserID,
                            ReqDate = DateTime.UtcNow,
                            TrackingGrpID = 3,
                            TrackingStatusID = 2,
                            TrackingStatusDate = DateTime.UtcNow,
                            Remark = "",
                            isDeleted = 0
                        };

                        chkTrackingReqID = _trackingRequestRepository.InsertReturnTrackingReqID(_trackingRequestModel, userContact.UserID);

                        logger.Debug("(insert reqId in ContactInfomationControllers [After Insert])TrackingReqID : " + chkTrackingReqID);
                    }
                    else if (checkInsertReq)
                    {
                        //chkTrackingReqID = _trackingOrgAddressRepository.InsertReq(userContact.UserID, supplierID);
                    }



                    if (OrgAdd != null)
                    {

                        var CompanyAdd = _addressRepository.GetAddressByAddressID(model.CompanyAddressID ?? 0);
                        if (CompanyAdd != null)
                        {
                            //trackingCompofile
                            #region trackingCompofile
                            //var dataTrackingOrg = _trackingOrgAddressRepository.GetDataBySupplierID(model.SupplierID ?? 0);
                            foreach (var Variable in itemtrackingStatus)
                            {
                                if (Variable.Key.Contains("CompanyAddress"))
                                {
                                    var value = string.Empty;
                                    if (value != null)
                                    {
                                        try
                                        {
                                            value = (model.GetType().GetProperty(Variable.Key).GetValue(model) ?? string.Empty).ToString();
                                        }
                                        catch (Exception)
                                        {
                                        }
                                    }
                                    var split = "";
                                    if (Variable.Key.Contains("CompanyAddress"))
                                    {
                                        split = Variable.Key.Replace(string.Format("{0}_", "CompanyAddress"), string.Empty);
                                    }
                                    else if (Variable.Key.Contains("DeliverAddress"))
                                    {
                                        split = Variable.Key.Replace(string.Format("{0}_", "DeliverAddress"), string.Empty);
                                    }
                                    else
                                    {
                                        split = Variable.Key.Replace(string.Format("{0}_", "CompanyAddress"), string.Empty);
                                    }
                                    var Oldval = string.Empty;
                                    Oldval = (addressData.GetType().GetProperty(split).GetValue(addressData) ?? string.Empty).ToString();

                                    var trackingKey = Variable.Value.Split('.').LastOrDefault(); //ตัดเอาเฉพาะชื่อฟิว
                                    var item = _trackingComprofileRepository.GetDataBySupplierIDandTrackingKey(model.CompanyAddressID ?? 0, split, "Tbl_Address");

                                    if (item.Count() == 0)
                                    {
                                        if (Oldval != value)
                                        {
                                            //var dataReq = _trackingComprofileRepository.GetDataByUser(userContact.UserID);
                                            //if (dataReq != null)
                                            //{
                                            _trackingComprofileRepository.Insert(Variable.Value, supplierID, value, chkTrackingReqID, OrgAdd.AddressID, 2);
                                            //}
                                        }
                                    }
                                }
                            }
                            #region TrackingCompofileCheckRef
                            var bundleRefPortalCompAddress = new List<Tbl_PortalFieldConfig>();
                            var dataTrackingCompAddress = _trackingComprofileRepository.GetdataRefByAddressID(model.CompanyAddressID ?? 0);

                            foreach (var itemCompare in dataTrackingCompAddress)
                            {
                                var dataTrackingRef = _portalFieldConfigRepository.GetdataForcheckRef(itemCompare.FieldID ?? 0);
                                string bundleRef = dataTrackingRef.BundleFieldID;
                                string[] ListBundle = bundleRef.Split(',');
                                foreach (var list in ListBundle)
                                {
                                    if (!string.IsNullOrEmpty(list))
                                    {
                                        int i = Convert.ToInt32(list);

                                        var dataTrackingRef2 = _portalFieldConfigRepository.GetdataForcheckRef(i);
                                        if (dataTrackingRef2 != null)
                                        {
                                            if (!bundleRefPortalCompAddress.Contains(dataTrackingRef2))
                                            {
                                                bundleRefPortalCompAddress.Add(dataTrackingRef2);
                                            }
                                        }
                                    }
                                }
                            }
                            foreach (var iitem in bundleRefPortalCompAddress)
                            {
                                var dataTrackingComPro = _trackingComprofileRepository.GetdataRefByfieldID_AddressID(iitem.FieldID, model.CompanyAddressID ?? 0);
                                //if (dataTrackingComPro != null)
                                //{
                                var valueCompare = string.Empty;
                                var dataVal = string.Empty;
                                if (valueCompare != null)
                                {
                                    //dataVal = iitem.Variable.Split('.').LastOrDefault();
                                    var splits = iitem.Variable.Replace("CompanyAddress.", string.Empty);
                                    //dataVal = iitem.Variable.Split('.').LastOrDefault();
                                    if (splits.Contains("CompanyAddress"))
                                    {
                                        dataVal = splits.Replace("CompanyAddress_", string.Empty);
                                    }
                                    else if (splits.Contains("DeliverAddress"))
                                    {
                                        dataVal = splits.Replace("DeliverAddress_", string.Empty);
                                    }
                                    else
                                    {
                                        dataVal = splits.Replace("CompanyAddress_", string.Empty);
                                    }
                                    try
                                    {
                                        valueCompare = (addressData.GetType().GetProperty(dataVal).GetValue(addressData) ?? string.Empty).ToString();
                                    }
                                    catch (Exception)
                                    {
                                    }
                                }
                                var item1 = _trackingComprofileRepository.GetDataBySupplierIDandTrackingKey(model.CompanyAddressID ?? 0, dataVal, "Tbl_Address");

                                if (item1.Count() == 0)
                                {
                                    if (iitem.Variable.Contains("CompanyAddress"))
                                    {
                                        _trackingComprofileRepository.Insert(iitem.Variable, supplierID, valueCompare, chkTrackingReqID, model.CompanyAddressID ?? 0, 2);
                                    }
                                }
                            }
                            #endregion

                            var prop = CompanyAdd.GetType().GetProperties();
                            var dataTrackingAddress = model.GetTrackingStatus<Tbl_Address>(CompanyAdd.AddressID);
                            var isChange = prop.Any(c =>
                            {

                                if (dataTrackingAddress.ContainsKey(c.Name) && dataTrackingAddress[c.Name].TrackingStatusID == 1)
                                {
                                    var key = string.Format("CompanyAddress_{0}", c.Name);
                                    var objValue = (c.GetValue(CompanyAdd) ?? string.Empty);
                                    var modelValue = model.GetType().GetProperty(key);

                                    return modelValue == null ? false : !objValue.Equals(modelValue.GetValue(model) ?? string.Empty);
                                }
                                return false;

                            });

                            #endregion
                            //เช็คเซฟเฉพาะข้อมูลที่มีค่า isTrackModify = 0
                            if (!itemtrackingStatus.ContainsKey("CompanyAddress_HouseNo_Local"))
                                CompanyAdd.HouseNo_Local = model.CompanyAddress_HouseNo_Local ?? "";
                            if (!itemtrackingStatus.ContainsKey("CompanyAddress_HouseNo_Inter"))
                                CompanyAdd.HouseNo_Inter = model.CompanyAddress_HouseNo_Inter ?? "";
                            if (!itemtrackingStatus.ContainsKey("CompanyAddress_VillageNo_Local"))
                                CompanyAdd.VillageNo_Local = model.CompanyAddress_VillageNo_Local ?? "";
                            if (!itemtrackingStatus.ContainsKey("CompanyAddress_VillageNo_Inter"))
                                CompanyAdd.VillageNo_Inter = model.CompanyAddress_VillageNo_Inter ?? "";
                            if (!itemtrackingStatus.ContainsKey("CompanyAddress_Lane_Local"))
                                CompanyAdd.Lane_Local = model.CompanyAddress_Lane_Local ?? "";
                            if (!itemtrackingStatus.ContainsKey("CompanyAddress_Lane_Inter"))
                                CompanyAdd.Lane_Inter = model.CompanyAddress_Lane_Inter ?? "";
                            if (!itemtrackingStatus.ContainsKey("CompanyAddress_Road_Local"))
                                CompanyAdd.Road_Local = model.CompanyAddress_Road_Local ?? "";
                            if (!itemtrackingStatus.ContainsKey("CompanyAddress_Road_Inter"))
                                CompanyAdd.Road_Inter = model.CompanyAddress_Road_Inter ?? "";
                            if (!itemtrackingStatus.ContainsKey("CompanyAddress_SubDistrict_Local"))
                                CompanyAdd.SubDistrict_Local = model.CompanyAddress_SubDistrict_Local ?? "";
                            if (!itemtrackingStatus.ContainsKey("CompanyAddress_SubDistrict_Inter"))
                                CompanyAdd.SubDistrict_Inter = model.CompanyAddress_SubDistrict_Inter ?? "";
                            if (!itemtrackingStatus.ContainsKey("CompanyAddress_City_Local"))
                                CompanyAdd.City_Local = model.CompanyAddress_City_Local ?? "";
                            if (!itemtrackingStatus.ContainsKey("CompanyAddress_City_Inter"))
                                CompanyAdd.City_Inter = model.CompanyAddress_City_Inter ?? "";
                            if (!itemtrackingStatus.ContainsKey("CompanyAddress_State_Local"))
                                CompanyAdd.State_Local = model.CompanyAddress_State_Local ?? "";
                            if (!itemtrackingStatus.ContainsKey("CompanyAddress_State_Inter"))
                                CompanyAdd.State_Inter = model.CompanyAddress_State_Inter ?? "";
                            if (!itemtrackingStatus.ContainsKey("CompanyAddress_CountryCode"))
                                CompanyAdd.CountryCode = model.CompanyAddress_CountryCode ?? "";
                            //update companyAdd
                            _addressRepository.UpdateAddress(CompanyAdd, userID);
                            if (isChange)
                                _trackingOrgAddressRepository.InsertTrackingOrg("update", supplierID, userContact.UserID, CompanyAdd.AddressID, CompanyAdd.AddressID,chkTrackingReqID);

                        }

                    }
                    else
                    {

                        //trackingCompofile
                        #region trackingCompofile
                        //var dataTrackingOrg = _trackingOrgAddressRepository.GetDataBySupplierID(model.SupplierID ?? 0);
                        foreach (var Variable in itemtrackingStatus)
                        {
                            if (Variable.Key.Contains("CompanyAddress"))
                            {
                                var value = string.Empty;
                                if (value != null)
                                {
                                    try
                                    {
                                        value = (model.GetType().GetProperty(Variable.Key).GetValue(model) ?? string.Empty).ToString();
                                    }
                                    catch (Exception)
                                    {
                                    }
                                }
                                var split = "";
                                if (Variable.Key.Contains("CompanyAddress"))
                                {
                                    split = Variable.Key.Replace(string.Format("{0}_", "CompanyAddress"), string.Empty);
                                }
                                else if (Variable.Key.Contains("DeliverAddress"))
                                {
                                    split = Variable.Key.Replace(string.Format("{0}_", "DeliverAddress"), string.Empty);
                                }
                                else
                                {
                                    split = Variable.Key.Replace(string.Format("{0}_", "CompanyAddress"), string.Empty);
                                }

                                var Oldval = string.Empty;
                                Oldval = (addressData.GetType().GetProperty(split).GetValue(addressData) ?? string.Empty).ToString();

                                var trackingKey = Variable.Value.Split('.').LastOrDefault(); //ตัดเอาเฉพาะชื่อฟิว
                                var item = _trackingComprofileRepository.GetDataBySupplierIDandTrackingKey(model.CompanyAddressID ?? 0, split, "Tbl_Address");

                                if (item.Count() == 0)
                                {
                                    if (Oldval != value)
                                    {
                                        //var dataReq = _trackingComprofileRepository.GetDataByUser(userContact.UserID);
                                        //if (dataReq != null)
                                        //{
                                        _trackingComprofileRepository.Insert(Variable.Value, supplierID, value, chkTrackingReqID, OrgAdd.AddressID, 2);


                                        //}
                                    }
                                }
                            }
                        }

                        #region TrackingCompofileCheckRef
                        var bundleRefPortalCompAddress = new List<Tbl_PortalFieldConfig>();
                        var dataTrackingCompAddress = _trackingComprofileRepository.GetdataRefByAddressID(model.CompanyAddressID ?? 0);

                        foreach (var itemCompare in dataTrackingCompAddress)
                        {
                            var dataTrackingRef = _portalFieldConfigRepository.GetdataForcheckRef(itemCompare.FieldID ?? 0);
                            string bundleRef = dataTrackingRef.BundleFieldID;
                            string[] ListBundle = bundleRef.Split(',');
                            foreach (var list in ListBundle)
                            {
                                if (!string.IsNullOrEmpty(list))
                                {
                                    int i = Convert.ToInt32(list);

                                    var dataTrackingRef2 = _portalFieldConfigRepository.GetdataForcheckRef(i);
                                    if (dataTrackingRef2 != null)
                                    {
                                        if (!bundleRefPortalCompAddress.Contains(dataTrackingRef2))
                                        {
                                            bundleRefPortalCompAddress.Add(dataTrackingRef2);
                                        }
                                    }
                                }
                            }
                        }
                        foreach (var iitem in bundleRefPortalCompAddress)
                        {
                            var dataTrackingComPro = _trackingComprofileRepository.GetdataRefByfieldID_AddressID(iitem.FieldID, model.CompanyAddressID ?? 0);
                            //if (dataTrackingComPro != null)
                            //{
                            var valueCompare = string.Empty;
                            var dataVal = string.Empty;
                            if (valueCompare != null)
                            {
                                //dataVal = iitem.Variable.Split('.').LastOrDefault();
                                var splits = iitem.Variable.Replace("CompanyAddress.", string.Empty);
                                if (splits.Contains("CompanyAddress"))
                                {
                                    dataVal = splits.Replace("CompanyAddress_", string.Empty);
                                }
                                else if (splits.Contains("DeliverAddress"))
                                {
                                    dataVal = splits.Replace("DeliverAddress_", string.Empty);
                                }
                                else
                                {
                                    dataVal = splits.Replace("CompanyAddress_", string.Empty);
                                }
                                try
                                {
                                    valueCompare = (addressData.GetType().GetProperty(dataVal).GetValue(addressData) ?? string.Empty).ToString();
                                }
                                catch (Exception)
                                {
                                }
                            }
                            var item1 = _trackingComprofileRepository.GetDataBySupplierIDandTrackingKey(model.CompanyAddressID ?? 0, dataVal, "Tbl_Address");

                            if (item1.Count() == 0)
                            {
                                if (iitem.Variable.Contains("CompanyAddress"))
                                {
                                    _trackingComprofileRepository.Insert(iitem.Variable, supplierID, valueCompare, chkTrackingReqID, model.CompanyAddressID ?? 0, 2);
                                }
                            }
                        }
                        #endregion
                        var comAdd = new Tbl_Address();

                        var prop = comAdd.GetType().GetProperties();
                        var dataTrackingAddress = model.GetTrackingStatus<Tbl_Address>(comAdd.AddressID);
                        var isChange = prop.Any(c =>
                        {

                            if (dataTrackingAddress.ContainsKey(c.Name) && dataTrackingAddress[c.Name].TrackingStatusID == 1)
                            {
                                var key = string.Format("CompanyAddress_{0}", c.Name);
                                var objValue = (c.GetValue(comAdd) ?? string.Empty);
                                var modelValue = model.GetType().GetProperty(key);

                                return modelValue == null ? false : !objValue.Equals(modelValue.GetValue(model));
                            }
                            return false;

                        });

                        #endregion

                        //เช็คเซฟเฉพาะข้อมูลที่มีค่า isTrackModify = 0
                        if (!itemtrackingStatus.ContainsKey("CompanyAddress_HouseNo_Local"))
                            comAdd.HouseNo_Local = model.CompanyAddress_HouseNo_Local ?? "";
                        if (!itemtrackingStatus.ContainsKey("CompanyAddress_HouseNo_Inter"))
                            comAdd.HouseNo_Inter = model.CompanyAddress_HouseNo_Inter ?? "";
                        if (!itemtrackingStatus.ContainsKey("CompanyAddress_VillageNo_Local"))
                            comAdd.VillageNo_Local = model.CompanyAddress_VillageNo_Local ?? "";
                        if (!itemtrackingStatus.ContainsKey("CompanyAddress_VillageNo_Inter"))
                            comAdd.VillageNo_Inter = model.CompanyAddress_VillageNo_Inter ?? "";
                        if (!itemtrackingStatus.ContainsKey("CompanyAddress_Lane_Local"))
                            comAdd.Lane_Local = model.CompanyAddress_Lane_Local ?? "";
                        if (!itemtrackingStatus.ContainsKey("CompanyAddress_Lane_Inter"))
                            comAdd.Lane_Inter = model.CompanyAddress_Lane_Inter ?? "";
                        if (!itemtrackingStatus.ContainsKey("CompanyAddress_Road_Local"))
                            comAdd.Road_Local = model.CompanyAddress_Road_Local ?? "";
                        if (!itemtrackingStatus.ContainsKey("CompanyAddress_Road_Inter"))
                            comAdd.Road_Inter = model.CompanyAddress_Road_Inter ?? "";
                        if (!itemtrackingStatus.ContainsKey("CompanyAddress_SubDistrict_Local"))
                            comAdd.SubDistrict_Local = model.CompanyAddress_SubDistrict_Local ?? "";
                        if (!itemtrackingStatus.ContainsKey("CompanyAddress_SubDistrict_Inter"))
                            comAdd.SubDistrict_Inter = model.CompanyAddress_SubDistrict_Inter ?? "";
                        if (!itemtrackingStatus.ContainsKey("CompanyAddress_City_Local"))
                            comAdd.City_Local = model.CompanyAddress_City_Local ?? "";
                        if (!itemtrackingStatus.ContainsKey("CompanyAddress_City_Inter"))
                            comAdd.City_Inter = model.CompanyAddress_City_Inter ?? "";
                        if (!itemtrackingStatus.ContainsKey("CompanyAddress_State_Local"))
                            comAdd.State_Local = model.CompanyAddress_State_Local ?? "";
                        if (!itemtrackingStatus.ContainsKey("CompanyAddress_State_Inter"))
                            comAdd.State_Inter = model.CompanyAddress_State_Inter ?? "";
                        if (!itemtrackingStatus.ContainsKey("CompanyAddress_CountryCode"))
                            comAdd.CountryCode = model.CompanyAddress_CountryCode ?? "";
                        //instert companyAdd
                        int comOrg = 0;
                        comOrg = _addressRepository.InsertAddress(comAdd);

                        model.CompanyAddressID = comOrg;

                        var orgAdd = new Tbl_OrgAddress()
                        {
                            SupplierID = model.SupplierID ?? 0,
                            AddressID = model.CompanyAddressID ?? 0,
                            AddressTypeID = 1,
                            SeqNo = 1
                        };
                        _orgAddressRepository.InsertAddress(orgAdd);

                        if (isChange)
                            _trackingOrgAddressRepository.InsertTrackingOrg("insert", supplierID, userContact.UserID, comAdd.AddressID, comAdd.AddressID, chkTrackingReqID);
                    }

                    if (model.CheckB)
                    {

                        if (model.CompanyAddressID == model.DeliverAddressID)
                        {
                            var orgDeliverAdd = _orgAddressRepository.GetAddress(model.SupplierID ?? 0, model.DeliverAddressID ?? 0, 2);
                            if (orgDeliverAdd != null)
                            {
                                //trackingCompofile

                                //update OrgAddress
                                _orgAddressRepository.UpdateAddress(orgDeliverAdd, model.CompanyAddressID ?? 0);
                                // _trackingOrgAddressRepository.UpdateTrackingOrg("Remove", supplierID, user.UserID, model.DeliverAddressID ?? 0, model.DeliverAddressID ?? 0);
                            }
                        }
                        else
                        {
                            var orgDeliverAdd = _orgAddressRepository.GetAddress(model.SupplierID ?? 0, model.DeliverAddressID ?? 0, 2);
                            var orgDeli = _orgAddressRepository.GetAddress(model.SupplierID ?? 0, model.CompanyAddressID ?? 0, 2);
                            if (orgDeliverAdd != null)
                            {
                                //trackingCompofile

                                //update OrgAddress
                                _orgAddressRepository.UpdateAddress(orgDeliverAdd, model.CompanyAddressID ?? 0);
                                //_trackingOrgAddressRepository.UpdateTrackingOrg("Remove", supplierID, user.UserID, model.DeliverAddressID ?? 0, model.DeliverAddressID ?? 0);
                                //_trackingOrgAddressRepository.InsertTrackingOrg("Checkbox", supplierID, userContact.ContactID ?? 0);
                            }
                            else if (orgDeli != null)
                            {
                                _trackingOrgAddressRepository.UpdateTrackingOrg("Remove", supplierID, user.UserID, model.DeliverAddressID ?? 0, model.DeliverAddressID ?? 0, chkTrackingReqID);
                            }
                            else
                            {
                                //insert OrgAddress
                                var orgAdd = new Tbl_OrgAddress()
                                {
                                    SupplierID = model.SupplierID ?? 0,
                                    AddressID = model.CompanyAddressID ?? 0,
                                    AddressTypeID = 2,
                                    SeqNo = 1
                                };
                                _orgAddressRepository.InsertAddress(orgAdd);
                                _trackingOrgAddressRepository.UpdateTrackingOrg("Remove", supplierID, user.UserID, orgDeliverAdd.AddressID, orgDeliverAdd.AddressID, chkTrackingReqID);
                                //_trackingOrgAddressRepository.InsertTrackingOrg("Checkbox", supplierID, userContact.ContactID ?? 0);
                            }

                        }
                    }
                    else
                    {

                        var orgDeliverAdd = _orgAddressRepository.GetAddress(model.SupplierID ?? 0, model.DeliverAddressID ?? 0, 2);

                        if (model.CompanyAddressID == model.DeliverAddressID)
                        {
                            var checkDataTrackingOrgAddress = _trackingOrgAddressRepository.GetDataforCheckUpdateBySupplierID(supplierID);
                            var deliverAddress = new Tbl_Address();
                            int deAddID = 0;
                            if (checkDataTrackingOrgAddress != null)
                            {
                                //เช็คเซฟเฉพาะข้อมูลที่มีค่า isTrackModify = 0
                                if (!itemtrackingStatus.ContainsKey("DeliverAddress_HouseNo_Local"))
                                    deliverAddress.HouseNo_Local = model.DeliverAddress_HouseNo_Local ?? "";
                                if (!itemtrackingStatus.ContainsKey("DeliverAddress_HouseNo_Inter"))
                                    deliverAddress.HouseNo_Inter = model.DeliverAddress_HouseNo_Inter ?? "";
                                if (!itemtrackingStatus.ContainsKey("DeliverAddress_VillageNo_Local"))
                                    deliverAddress.VillageNo_Local = model.DeliverAddress_VillageNo_Local ?? "";
                                if (!itemtrackingStatus.ContainsKey("DeliverAddress_VillageNo_Inter"))
                                    deliverAddress.VillageNo_Inter = model.DeliverAddress_VillageNo_Inter ?? "";
                                if (!itemtrackingStatus.ContainsKey("DeliverAddress_Lane_Local"))
                                    deliverAddress.Lane_Local = model.DeliverAddress_Lane_Local ?? "";
                                if (!itemtrackingStatus.ContainsKey("DeliverAddress_Lane_Inter"))
                                    deliverAddress.Lane_Inter = model.DeliverAddress_Lane_Inter ?? "";
                                if (!itemtrackingStatus.ContainsKey("DeliverAddress_Road_Local"))
                                    deliverAddress.Road_Local = model.DeliverAddress_Road_Local ?? "";
                                if (!itemtrackingStatus.ContainsKey("DeliverAddress_Road_Inter"))
                                    deliverAddress.Road_Inter = model.DeliverAddress_Road_Inter ?? "";
                                if (!itemtrackingStatus.ContainsKey("DeliverAddress_SubDistrict_Local"))
                                    deliverAddress.SubDistrict_Local = model.DeliverAddress_SubDistrict_Local ?? "";
                                if (!itemtrackingStatus.ContainsKey("DeliverAddress_SubDistrict_Inter"))
                                    deliverAddress.SubDistrict_Inter = model.DeliverAddress_SubDistrict_Inter ?? "";
                                if (!itemtrackingStatus.ContainsKey("DeliverAddress_City_Local"))
                                    deliverAddress.City_Local = model.DeliverAddress_City_Local ?? "";
                                if (!itemtrackingStatus.ContainsKey("DeliverAddress_City_Inter"))
                                    deliverAddress.City_Inter = model.DeliverAddress_City_Inter ?? "";
                                if (!itemtrackingStatus.ContainsKey("DeliverAddress_State_Local"))
                                    deliverAddress.State_Local = model.DeliverAddress_State_Local ?? "";
                                if (!itemtrackingStatus.ContainsKey("DeliverAddress_State_Inter"))
                                    deliverAddress.State_Inter = model.DeliverAddress_State_Inter ?? "";
                                if (!itemtrackingStatus.ContainsKey("DeliverAddress_CountryCode"))
                                    deliverAddress.CountryCode = model.DeliverAddress_CountryCode ?? "";
                                if (!itemtrackingStatus.ContainsKey("DeliverAddress_PostalCode"))
                                    deliverAddress.PostalCode = model.DeliverAddress_PostalCode ?? "";

                                // insert Tbl_Address
                                deAddID = _addressRepository.InsertAddress(deliverAddress);
                            }
                            else
                            {
                                //เช็คเซฟเฉพาะข้อมูลที่มีค่า isTrackModify = 0
                                //if (!itemtrackingStatus.ContainsKey("DeliverAddress_HouseNo_Local"))
                                deliverAddress.HouseNo_Local = model.DeliverAddress_HouseNo_Local ?? "";
                                //if (!itemtrackingStatus.ContainsKey("DeliverAddress_HouseNo_Inter"))
                                deliverAddress.HouseNo_Inter = model.DeliverAddress_HouseNo_Inter ?? "";
                                //if (!itemtrackingStatus.ContainsKey("DeliverAddress_VillageNo_Local"))
                                deliverAddress.VillageNo_Local = model.DeliverAddress_VillageNo_Local ?? "";
                                //if (!itemtrackingStatus.ContainsKey("DeliverAddress_VillageNo_Inter"))
                                deliverAddress.VillageNo_Inter = model.DeliverAddress_VillageNo_Inter ?? "";
                                //if (!itemtrackingStatus.ContainsKey("DeliverAddress_Lane_Local"))
                                deliverAddress.Lane_Local = model.DeliverAddress_Lane_Local ?? "";
                                //if (!itemtrackingStatus.ContainsKey("DeliverAddress_Lane_Inter"))
                                deliverAddress.Lane_Inter = model.DeliverAddress_Lane_Inter ?? "";
                                //if (!itemtrackingStatus.ContainsKey("DeliverAddress_Road_Local"))
                                deliverAddress.Road_Local = model.DeliverAddress_Road_Local ?? "";
                                //if (!itemtrackingStatus.ContainsKey("DeliverAddress_Road_Inter"))
                                deliverAddress.Road_Inter = model.DeliverAddress_Road_Inter ?? "";
                                //if (!itemtrackingStatus.ContainsKey("DeliverAddress_SubDistrict_Local"))
                                deliverAddress.SubDistrict_Local = model.DeliverAddress_SubDistrict_Local ?? "";
                                //if (!itemtrackingStatus.ContainsKey("DeliverAddress_SubDistrict_Inter"))
                                deliverAddress.SubDistrict_Inter = model.DeliverAddress_SubDistrict_Inter ?? "";
                                //if (!itemtrackingStatus.ContainsKey("DeliverAddress_City_Local"))
                                deliverAddress.City_Local = model.DeliverAddress_City_Local ?? "";
                                //if (!itemtrackingStatus.ContainsKey("DeliverAddress_City_Inter"))
                                deliverAddress.City_Inter = model.DeliverAddress_City_Inter ?? "";
                                //if (!itemtrackingStatus.ContainsKey("DeliverAddress_State_Local"))
                                deliverAddress.State_Local = model.DeliverAddress_State_Local ?? "";
                                //if (!itemtrackingStatus.ContainsKey("DeliverAddress_State_Inter"))
                                deliverAddress.State_Inter = model.DeliverAddress_State_Inter ?? "";
                                //if (!itemtrackingStatus.ContainsKey("DeliverAddress_CountryCode"))
                                deliverAddress.CountryCode = model.DeliverAddress_CountryCode ?? "";
                                //if (!itemtrackingStatus.ContainsKey("DeliverAddress_PostalCode"))
                                deliverAddress.PostalCode = model.DeliverAddress_PostalCode ?? "";

                                // insert Tbl_Address
                                deAddID = _addressRepository.InsertAddress(deliverAddress);
                            }

                            if (orgDeliverAdd == null)
                            {
                                //var orgAdd = new Tbl_OrgAddress()
                                //{
                                //    SupplierID = model.SupplierID ?? 0,
                                //    AddressID = deAddID,
                                //    AddressTypeID = 2,
                                //    SeqNo = 1
                                //};
                                //_orgAddressRepository.InsertAddress(orgAdd);

                                if (checkDataTrackingOrgAddress != null)
                                {
                                    //==================================
                                    _trackingOrgAddressRepository.UpdateTrackingOrg("insert", supplierID, userContact.UserID, OrgAdd.AddressID, deliverAddress.AddressID, chkTrackingReqID);
                                }
                                else
                                {
                                    //==================================
                                    _trackingOrgAddressRepository.InsertTrackingOrg("insert", supplierID, userContact.UserID, OrgAdd.AddressID, deliverAddress.AddressID, chkTrackingReqID);
                                }
                            }
                            else
                            {
                                //_orgAddressRepository.UpdateAddress(orgDeliverAdd, deAddID);

                                if (checkDataTrackingOrgAddress != null)
                                {
                                    //==================================
                                    _trackingOrgAddressRepository.UpdateTrackingOrg("insert", supplierID, userContact.UserID, OrgAdd.AddressID, deliverAddress.AddressID, chkTrackingReqID);
                                }
                                else
                                {
                                    //==================================
                                    _trackingOrgAddressRepository.InsertTrackingOrg("insert", supplierID, userContact.UserID, OrgAdd.AddressID, deliverAddress.AddressID, chkTrackingReqID);
                                }
                            }

                        }
                        else
                        {

                            var deliAdd = _addressRepository.GetAddressByAddressID(model.DeliverAddressID ?? 0);
                            //var dataOrgAddress = _orgAddressRepository.GetAddresType(supplierID, 2);
                            if (deliAdd != null)
                            {
                                var dataOrg = false;
                                foreach (var item in deliAdd.Tbl_OrgAddress)
                                {
                                    if (item.AddressTypeID == 2)
                                    {
                                        dataOrg = item.AddressID == model.DeliverAddressID;
                                        if (dataOrg)
                                        {
                                            break;
                                        }
                                    }
                                }

                                if (dataOrg)
                                {
                                    //trackingCompofile
                                    #region trackingCompofile

                                    var DeliverData = _addressRepository.GetAddressByAddressID(model.DeliverAddressID ?? 0);
                                    //var dataTrackingOrg = _trackingOrgAddressRepository.GetDataBySupplierID(model.SupplierID ?? 0);
                                    foreach (var Variable in itemtrackingStatus)
                                    {
                                        if (Variable.Key.Contains("DeliverAddress"))
                                        {
                                            var value = string.Empty;
                                            if (value != null)
                                            {
                                                try
                                                {
                                                    value = (model.GetType().GetProperty(Variable.Key).GetValue(model) ?? string.Empty).ToString();
                                                }
                                                catch (Exception)
                                                {
                                                }
                                            }
                                            var split = "";
                                            if (Variable.Key.Contains("CompanyAddress"))
                                            {
                                                split = Variable.Key.Replace(string.Format("{0}_", "CompanyAddress"), string.Empty);
                                            }
                                            else if (Variable.Key.Contains("DeliverAddress"))
                                            {
                                                split = Variable.Key.Replace(string.Format("{0}_", "DeliverAddress"), string.Empty);
                                            }
                                            else
                                            {
                                                split = Variable.Key.Replace(string.Format("{0}_", "CompanyAddress"), string.Empty);
                                            }

                                            var Oldval = string.Empty;
                                            Oldval = (DeliverData.GetType().GetProperty(split).GetValue(DeliverData) ?? string.Empty).ToString();

                                            var trackingKey = Variable.Value.Split('.').LastOrDefault(); //ตัดเอาเฉพาะชื่อฟิว
                                            var item = _trackingComprofileRepository.GetDataBySupplierIDandTrackingKey(model.DeliverAddressID ?? 0, split, "Tbl_Address");

                                            if (item.Count() == 0)
                                            {
                                                if (Oldval != value)
                                                {

                                                    _trackingComprofileRepository.Insert(Variable.Value, supplierID, value, chkTrackingReqID, deliAdd.AddressID, 2);

                                                }
                                            }
                                        }
                                    }

                                    #region TrackingCompofileCheckRef
                                    var bundleRefPortalCompDeliver = new List<Tbl_PortalFieldConfig>();
                                    var dataTrackingCompDeliver = _trackingComprofileRepository.GetdataRefByAddressID(model.DeliverAddressID ?? 0);
                                    var deliverData = _addressRepository.GetAddressByAddressID(model.DeliverAddressID ?? 0);
                                    foreach (var item in dataTrackingCompDeliver)
                                    {
                                        var dataTrackingRef = _portalFieldConfigRepository.GetdataForcheckRef(item.FieldID ?? 0);
                                        string bundleRef = dataTrackingRef.BundleFieldID;
                                        string[] ListBundle = bundleRef.Split(',');
                                        foreach (var list in ListBundle)
                                        {
                                            if (!string.IsNullOrEmpty(list))
                                            {
                                                int i = Convert.ToInt32(list);

                                                var dataTrackingRef2 = _portalFieldConfigRepository.GetdataForcheckRef(i);
                                                if (dataTrackingRef2 != null)
                                                {
                                                    if (!bundleRefPortalCompDeliver.Contains(dataTrackingRef2))
                                                    {
                                                        bundleRefPortalCompDeliver.Add(dataTrackingRef2);
                                                    }
                                                }
                                            }
                                        }

                                    }
                                    foreach (var iitem in bundleRefPortalCompDeliver)
                                    {
                                        var dataTrackingComPro = _trackingComprofileRepository.GetdataRefByfieldID_AddressID(iitem.FieldID, model.DeliverAddressID ?? 0);
                                        var valueCompare = string.Empty;
                                        var dataVal = string.Empty;
                                        if (valueCompare != null)
                                        {
                                            var splits = iitem.Variable.Replace("CompanyAddress.", string.Empty);
                                            //dataVal = iitem.Variable.Split('.').LastOrDefault();
                                            if (splits.Contains("CompanyAddress"))
                                            {
                                                dataVal = splits.Replace("CompanyAddress_", string.Empty);
                                            }
                                            else if (splits.Contains("DeliverAddress"))
                                            {
                                                dataVal = splits.Replace("DeliverAddress_", string.Empty);
                                            }
                                            else
                                            {
                                                dataVal = splits.Replace("CompanyAddress_", string.Empty);
                                            }
                                            try
                                            {
                                                valueCompare = (deliverData.GetType().GetProperty(dataVal).GetValue(deliverData) ?? string.Empty).ToString();
                                            }
                                            catch (Exception)
                                            {
                                            }
                                        }
                                        var item1 = _trackingComprofileRepository.GetDataBySupplierIDandTrackingKey(model.DeliverAddressID ?? 0, dataVal, "Tbl_Address");
                                        if (item1.Count() == 0)
                                        {
                                            if (iitem.Variable.Contains("DeliverAddress"))
                                            {
                                                _trackingComprofileRepository.Insert(iitem.Variable, supplierID, valueCompare, chkTrackingReqID, model.DeliverAddressID ?? 0, 2);
                                            }
                                        }
                                    }
                                    #endregion
                                    var dataDelivers = _trackingComprofileRepository.GetdataRefByAddressID(DeliverData.AddressID);
                                    bool isChangeDeli = false;
                                    foreach (var item in dataDelivers)
                                    {
                                        if (item.OldKeyValue != item.NewKeyValue)
                                        {
                                            isChangeDeli = true;
                                            break;
                                        }
                                    }
                                    //var prop = DeliverData.GetType().GetProperties();
                                    //var dataTrackingAddress = model.GetTrackingStatus<Tbl_Address>(DeliverData.AddressID);

                                    //var isChangeDeli = prop.Any(c =>
                                    //{

                                    //    if (dataTrackingAddress.ContainsKey(c.Name) && dataTrackingAddress[c.Name].TrackingStatusID == 1)
                                    //    {
                                    //        var key = string.Format("DeliverAddress_{0}", c.Name);
                                    //        var objValue = (c.GetValue(DeliverData) ?? string.Empty);
                                    //        var modelValue = model.GetType().GetProperty(key);

                                    //        return modelValue == null ? false : !objValue.Equals(modelValue.GetValue(model) ?? string.Empty);
                                    //    }
                                    //    return false;

                                    //});

                                    var checkDataTrackingOrgAddress = _trackingOrgAddressRepository.GetDataforCheckUpdateBySupplierID(supplierID);

                                    if (checkDataTrackingOrgAddress == null)
                                    {
                                        //เช็คเซฟเฉพาะข้อมูลที่มีค่า isTrackModify = 0
                                        if (!itemtrackingStatus.ContainsKey("DeliverAddress_HouseNo_Local"))
                                            deliAdd.HouseNo_Local = model.DeliverAddress_HouseNo_Local ?? "";
                                        if (!itemtrackingStatus.ContainsKey("DeliverAddress_HouseNo_Inter"))
                                            deliAdd.HouseNo_Inter = model.DeliverAddress_HouseNo_Inter ?? "";
                                        if (!itemtrackingStatus.ContainsKey("DeliverAddress_VillageNo_Local"))
                                            deliAdd.VillageNo_Local = model.DeliverAddress_VillageNo_Local ?? "";
                                        if (!itemtrackingStatus.ContainsKey("DeliverAddress_VillageNo_Inter"))
                                            deliAdd.VillageNo_Inter = model.DeliverAddress_VillageNo_Inter ?? "";
                                        if (!itemtrackingStatus.ContainsKey("DeliverAddress_Lane_Local"))
                                            deliAdd.Lane_Local = model.DeliverAddress_Lane_Local ?? "";
                                        if (!itemtrackingStatus.ContainsKey("DeliverAddress_Lane_Inter"))
                                            deliAdd.Lane_Inter = model.DeliverAddress_Lane_Inter ?? "";
                                        if (!itemtrackingStatus.ContainsKey("DeliverAddress_Road_Local"))
                                            deliAdd.Road_Local = model.DeliverAddress_Road_Local ?? "";
                                        if (!itemtrackingStatus.ContainsKey("DeliverAddress_Road_Inter"))
                                            deliAdd.Road_Inter = model.DeliverAddress_Road_Inter ?? "";
                                        if (!itemtrackingStatus.ContainsKey("DeliverAddress_SubDistrict_Local"))
                                            deliAdd.SubDistrict_Local = model.DeliverAddress_SubDistrict_Local ?? "";
                                        if (!itemtrackingStatus.ContainsKey("DeliverAddress_SubDistrict_Inter"))
                                            deliAdd.SubDistrict_Inter = model.DeliverAddress_SubDistrict_Inter ?? "";
                                        if (!itemtrackingStatus.ContainsKey("DeliverAddress_City_Local"))
                                            deliAdd.City_Local = model.DeliverAddress_City_Local ?? "";
                                        if (!itemtrackingStatus.ContainsKey("DeliverAddress_City_Inter"))
                                            deliAdd.City_Inter = model.DeliverAddress_City_Inter ?? "";
                                        if (!itemtrackingStatus.ContainsKey("DeliverAddress_State_Local"))
                                            deliAdd.State_Local = model.DeliverAddress_State_Local ?? "";
                                        if (!itemtrackingStatus.ContainsKey("DeliverAddress_State_Inter"))
                                            deliAdd.State_Inter = model.DeliverAddress_State_Inter ?? "";
                                        if (!itemtrackingStatus.ContainsKey("DeliverAddress_CountryCode"))
                                            deliAdd.CountryCode = model.DeliverAddress_CountryCode ?? "";
                                        if (!itemtrackingStatus.ContainsKey("DeliverAddress_PostalCode"))
                                            deliAdd.PostalCode = model.DeliverAddress_PostalCode ?? "";

                                        _addressRepository.UpdateAddress(deliAdd, userID);

                                        var checkdatainDb = _trackingOrgAddressRepository.GetDataCheckDelivery(supplierID);
                                        if (checkdatainDb == null)
                                        {
                                            if (isChangeDeli)
                                                _trackingOrgAddressRepository.InsertTrackingOrg("insert", supplierID, userContact.UserID, model.DeliverAddressID ?? 0, model.DeliverAddressID ?? 0, chkTrackingReqID);
                                        }

                                    }
                                    else
                                    {
                                        //เช็คเซฟเฉพาะข้อมูลที่มีค่า isTrackModify = 0
                                        //if (!itemtrackingStatus.ContainsKey("DeliverAddress_HouseNo_Local"))
                                        deliAdd.HouseNo_Local = model.DeliverAddress_HouseNo_Local ?? "";
                                        //if (!itemtrackingStatus.ContainsKey("DeliverAddress_HouseNo_Inter"))
                                        deliAdd.HouseNo_Inter = model.DeliverAddress_HouseNo_Inter ?? "";
                                        //if (!itemtrackingStatus.ContainsKey("DeliverAddress_VillageNo_Local"))
                                        deliAdd.VillageNo_Local = model.DeliverAddress_VillageNo_Local ?? "";
                                        //if (!itemtrackingStatus.ContainsKey("DeliverAddress_VillageNo_Inter"))
                                        deliAdd.VillageNo_Inter = model.DeliverAddress_VillageNo_Inter ?? "";
                                        //if (!itemtrackingStatus.ContainsKey("DeliverAddress_Lane_Local"))
                                        deliAdd.Lane_Local = model.DeliverAddress_Lane_Local ?? "";
                                        //if (!itemtrackingStatus.ContainsKey("DeliverAddress_Lane_Inter"))
                                        deliAdd.Lane_Inter = model.DeliverAddress_Lane_Inter ?? "";
                                        //if (!itemtrackingStatus.ContainsKey("DeliverAddress_Road_Local"))
                                        deliAdd.Road_Local = model.DeliverAddress_Road_Local ?? "";
                                        //if (!itemtrackingStatus.ContainsKey("DeliverAddress_Road_Inter"))
                                        deliAdd.Road_Inter = model.DeliverAddress_Road_Inter ?? "";
                                        //if (!itemtrackingStatus.ContainsKey("DeliverAddress_SubDistrict_Local"))
                                        deliAdd.SubDistrict_Local = model.DeliverAddress_SubDistrict_Local ?? "";
                                        //if (!itemtrackingStatus.ContainsKey("DeliverAddress_SubDistrict_Inter"))
                                        deliAdd.SubDistrict_Inter = model.DeliverAddress_SubDistrict_Inter ?? "";
                                        //if (!itemtrackingStatus.ContainsKey("DeliverAddress_City_Local"))
                                        deliAdd.City_Local = model.DeliverAddress_City_Local ?? "";
                                        //if (!itemtrackingStatus.ContainsKey("DeliverAddress_City_Inter"))
                                        deliAdd.City_Inter = model.DeliverAddress_City_Inter ?? "";
                                        //if (!itemtrackingStatus.ContainsKey("DeliverAddress_State_Local"))
                                        deliAdd.State_Local = model.DeliverAddress_State_Local ?? "";
                                        //if (!itemtrackingStatus.ContainsKey("DeliverAddress_State_Inter"))
                                        deliAdd.State_Inter = model.DeliverAddress_State_Inter ?? "";
                                        //if (!itemtrackingStatus.ContainsKey("DeliverAddress_CountryCode"))
                                        deliAdd.CountryCode = model.DeliverAddress_CountryCode ?? "";
                                        //if (!itemtrackingStatus.ContainsKey("DeliverAddress_PostalCode"))
                                        deliAdd.PostalCode = model.DeliverAddress_PostalCode ?? "";

                                        _addressRepository.UpdateAddress(deliAdd, userID);

                                        //==================================
                                        //_trackingOrgAddressRepository.InsertTrackingOrg("insert", supplierID, userContact.UserID, OrgAdd.AddressID, deliAdd.AddressID);
                                    }
                                    #endregion
                                }

                            }
                            else
                            {
                                var checkDataTrackingOrgAddress = _trackingOrgAddressRepository.GetDataforCheckUpdateBySupplierID(supplierID);

                                var deliverAddress = new Tbl_Address();
                                int deAddID = 0;
                                if (checkDataTrackingOrgAddress == null)
                                {
                                    //เช็คเซฟเฉพาะข้อมูลที่มีค่า isTrackModify = 0
                                    if (!itemtrackingStatus.ContainsKey("DeliverAddress_HouseNo_Local"))
                                        deliverAddress.HouseNo_Local = model.DeliverAddress_HouseNo_Local ?? "";
                                    if (!itemtrackingStatus.ContainsKey("DeliverAddress_HouseNo_Inter"))
                                        deliverAddress.HouseNo_Inter = model.DeliverAddress_HouseNo_Inter ?? "";
                                    if (!itemtrackingStatus.ContainsKey("DeliverAddress_VillageNo_Local"))
                                        deliverAddress.VillageNo_Local = model.DeliverAddress_VillageNo_Local ?? "";
                                    if (!itemtrackingStatus.ContainsKey("DeliverAddress_VillageNo_Inter"))
                                        deliverAddress.VillageNo_Inter = model.DeliverAddress_VillageNo_Inter ?? "";
                                    if (!itemtrackingStatus.ContainsKey("DeliverAddress_Lane_Local"))
                                        deliverAddress.Lane_Local = model.DeliverAddress_Lane_Local ?? "";
                                    if (!itemtrackingStatus.ContainsKey("DeliverAddress_Lane_Inter"))
                                        deliverAddress.Lane_Inter = model.DeliverAddress_Lane_Inter ?? "";
                                    if (!itemtrackingStatus.ContainsKey("DeliverAddress_Road_Local"))
                                        deliverAddress.Road_Local = model.DeliverAddress_Road_Local ?? "";
                                    if (!itemtrackingStatus.ContainsKey("DeliverAddress_Road_Inter"))
                                        deliverAddress.Road_Inter = model.DeliverAddress_Road_Inter ?? "";
                                    if (!itemtrackingStatus.ContainsKey("DeliverAddress_SubDistrict_Local"))
                                        deliverAddress.SubDistrict_Local = model.DeliverAddress_SubDistrict_Local ?? "";
                                    if (!itemtrackingStatus.ContainsKey("DeliverAddress_SubDistrict_Inter"))
                                        deliverAddress.SubDistrict_Inter = model.DeliverAddress_SubDistrict_Inter ?? "";
                                    if (!itemtrackingStatus.ContainsKey("DeliverAddress_City_Local"))
                                        deliverAddress.City_Local = model.DeliverAddress_City_Local ?? "";
                                    if (!itemtrackingStatus.ContainsKey("DeliverAddress_City_Inter"))
                                        deliverAddress.City_Inter = model.DeliverAddress_City_Inter ?? "";
                                    if (!itemtrackingStatus.ContainsKey("DeliverAddress_State_Local"))
                                        deliverAddress.State_Local = model.DeliverAddress_State_Local ?? "";
                                    if (!itemtrackingStatus.ContainsKey("DeliverAddress_State_Inter"))
                                        deliverAddress.State_Inter = model.DeliverAddress_State_Inter ?? "";
                                    if (!itemtrackingStatus.ContainsKey("DeliverAddress_CountryCode"))
                                        deliverAddress.CountryCode = model.DeliverAddress_CountryCode ?? "";
                                    if (!itemtrackingStatus.ContainsKey("DeliverAddress_PostalCode"))
                                        deliverAddress.PostalCode = model.DeliverAddress_PostalCode ?? "";

                                    // insert Tbl_Address
                                    deAddID = _addressRepository.InsertAddress(deliverAddress);
                                }
                                else
                                {
                                    //เช็คเซฟเฉพาะข้อมูลที่มีค่า isTrackModify = 0
                                    //if (!itemtrackingStatus.ContainsKey("DeliverAddress_HouseNo_Local"))
                                    deliverAddress.HouseNo_Local = model.DeliverAddress_HouseNo_Local ?? "";
                                    //if (!itemtrackingStatus.ContainsKey("DeliverAddress_HouseNo_Inter"))
                                    deliverAddress.HouseNo_Inter = model.DeliverAddress_HouseNo_Inter ?? "";
                                    //if (!itemtrackingStatus.ContainsKey("DeliverAddress_VillageNo_Local"))
                                    deliverAddress.VillageNo_Local = model.DeliverAddress_VillageNo_Local ?? "";
                                    //if (!itemtrackingStatus.ContainsKey("DeliverAddress_VillageNo_Inter"))
                                    deliverAddress.VillageNo_Inter = model.DeliverAddress_VillageNo_Inter ?? "";
                                    //if (!itemtrackingStatus.ContainsKey("DeliverAddress_Lane_Local"))
                                    deliverAddress.Lane_Local = model.DeliverAddress_Lane_Local ?? "";
                                    //if (!itemtrackingStatus.ContainsKey("DeliverAddress_Lane_Inter"))
                                    deliverAddress.Lane_Inter = model.DeliverAddress_Lane_Inter ?? "";
                                    //if (!itemtrackingStatus.ContainsKey("DeliverAddress_Road_Local"))
                                    deliverAddress.Road_Local = model.DeliverAddress_Road_Local ?? "";
                                    //if (!itemtrackingStatus.ContainsKey("DeliverAddress_Road_Inter"))
                                    deliverAddress.Road_Inter = model.DeliverAddress_Road_Inter ?? "";
                                    //if (!itemtrackingStatus.ContainsKey("DeliverAddress_SubDistrict_Local"))
                                    deliverAddress.SubDistrict_Local = model.DeliverAddress_SubDistrict_Local ?? "";
                                    //if (!itemtrackingStatus.ContainsKey("DeliverAddress_SubDistrict_Inter"))
                                    deliverAddress.SubDistrict_Inter = model.DeliverAddress_SubDistrict_Inter ?? "";
                                    //if (!itemtrackingStatus.ContainsKey("DeliverAddress_City_Local"))
                                    deliverAddress.City_Local = model.DeliverAddress_City_Local ?? "";
                                    //if (!itemtrackingStatus.ContainsKey("DeliverAddress_City_Inter"))
                                    deliverAddress.City_Inter = model.DeliverAddress_City_Inter ?? "";
                                    //if (!itemtrackingStatus.ContainsKey("DeliverAddress_State_Local"))
                                    deliverAddress.State_Local = model.DeliverAddress_State_Local ?? "";
                                    //if (!itemtrackingStatus.ContainsKey("DeliverAddress_State_Inter"))
                                    deliverAddress.State_Inter = model.DeliverAddress_State_Inter ?? "";
                                    //if (!itemtrackingStatus.ContainsKey("DeliverAddress_CountryCode"))
                                    deliverAddress.CountryCode = model.DeliverAddress_CountryCode ?? "";
                                    //if (!itemtrackingStatus.ContainsKey("DeliverAddress_PostalCode"))
                                    deliverAddress.PostalCode = model.DeliverAddress_PostalCode ?? "";

                                    // insert Tbl_Address
                                    deAddID = _addressRepository.InsertAddress(deliverAddress);
                                }

                                if (orgDeliverAdd == null)
                                {
                                    //var orgAdd = new Tbl_OrgAddress()
                                    //{
                                    //    SupplierID = model.SupplierID ?? 0,
                                    //    AddressID = deAddID,
                                    //    AddressTypeID = 2,
                                    //    SeqNo = 1
                                    //};
                                    //_orgAddressRepository.InsertAddress(orgAdd);

                                    if (checkDataTrackingOrgAddress != null)
                                    {
                                        _trackingOrgAddressRepository.UpdateTrackingOrg("insert", supplierID, userContact.UserID, OrgAdd.AddressID, deliAdd.AddressID, chkTrackingReqID);
                                    }
                                    else
                                    {
                                        _trackingOrgAddressRepository.InsertTrackingOrg("insert", supplierID, userContact.UserID, OrgAdd.AddressID, deliAdd.AddressID, chkTrackingReqID);
                                    }
                                }
                                else
                                {
                                    _orgAddressRepository.UpdateAddress(orgDeliverAdd, deAddID);

                                    if (checkDataTrackingOrgAddress != null)
                                    {
                                        _trackingOrgAddressRepository.UpdateTrackingOrg("insert", supplierID, userContact.UserID, deliAdd.AddressID, deliAdd.AddressID, chkTrackingReqID);
                                    }
                                    else
                                    {
                                        _trackingOrgAddressRepository.InsertTrackingOrg("insert", supplierID, userContact.UserID, deliAdd.AddressID, deliAdd.AddressID, chkTrackingReqID);
                                    }
                                }
                            }
                        }
                    }

                    #endregion
                }
                else
                {
                    #region Update Data
                    var OrgAdd = _orgAddressRepository.GetAddress(supplierID, model.CompanyAddressID ?? 0, 1);

                    if (OrgAdd != null)
                    {
                        var CompanyAdd = _addressRepository.GetAddressByAddressID(model.CompanyAddressID ?? 0);
                        if (CompanyAdd != null)
                        {
                            CompanyAdd.HouseNo_Local = model.CompanyAddress_HouseNo_Local ?? "";
                            CompanyAdd.HouseNo_Inter = model.CompanyAddress_HouseNo_Inter ?? "";
                            CompanyAdd.VillageNo_Local = model.CompanyAddress_VillageNo_Local ?? "";
                            CompanyAdd.VillageNo_Inter = model.CompanyAddress_VillageNo_Inter ?? "";
                            CompanyAdd.Lane_Local = model.CompanyAddress_Lane_Local ?? "";
                            CompanyAdd.Lane_Inter = model.CompanyAddress_Lane_Inter ?? "";
                            CompanyAdd.Road_Local = model.CompanyAddress_Road_Local ?? "";
                            CompanyAdd.Road_Inter = model.CompanyAddress_Road_Inter ?? "";
                            CompanyAdd.SubDistrict_Local = model.CompanyAddress_SubDistrict_Local ?? "";
                            CompanyAdd.SubDistrict_Inter = model.CompanyAddress_SubDistrict_Inter ?? "";
                            CompanyAdd.City_Local = model.CompanyAddress_City_Local ?? "";
                            CompanyAdd.City_Inter = model.CompanyAddress_City_Inter ?? "";
                            CompanyAdd.State_Local = model.CompanyAddress_State_Local ?? "";
                            CompanyAdd.State_Inter = model.CompanyAddress_State_Inter ?? "";
                            CompanyAdd.CountryCode = model.CompanyAddress_CountryCode ?? "";

                            //update companyAdd
                            _addressRepository.UpdateAddress(CompanyAdd, userID);
                        }

                    }
                    else
                    {
                        var comAdd = new Tbl_Address();

                        comAdd.HouseNo_Local = model.CompanyAddress_HouseNo_Local ?? "";
                        comAdd.HouseNo_Inter = model.CompanyAddress_HouseNo_Inter ?? "";
                        comAdd.VillageNo_Local = model.CompanyAddress_VillageNo_Local ?? "";
                        comAdd.VillageNo_Inter = model.CompanyAddress_VillageNo_Inter ?? "";
                        comAdd.Lane_Local = model.CompanyAddress_Lane_Local ?? "";
                        comAdd.Lane_Inter = model.CompanyAddress_Lane_Inter ?? "";
                        comAdd.Road_Local = model.CompanyAddress_Road_Local ?? "";
                        comAdd.Road_Inter = model.CompanyAddress_Road_Inter ?? "";
                        comAdd.SubDistrict_Local = model.CompanyAddress_SubDistrict_Local ?? "";
                        comAdd.SubDistrict_Inter = model.CompanyAddress_SubDistrict_Inter ?? "";
                        comAdd.City_Local = model.CompanyAddress_City_Local ?? "";
                        comAdd.City_Inter = model.CompanyAddress_City_Inter ?? "";
                        comAdd.State_Local = model.CompanyAddress_State_Local ?? "";
                        comAdd.State_Inter = model.CompanyAddress_State_Inter ?? "";
                        comAdd.CountryCode = model.CompanyAddress_CountryCode ?? "";

                        //instert companyAdd
                        int comOrg = 0;
                        comOrg = _addressRepository.InsertAddress(comAdd);
                        model.CompanyAddressID = comOrg;

                        var orgAdd = new Tbl_OrgAddress()
                        {
                            SupplierID = model.SupplierID ?? 0,
                            AddressID = model.CompanyAddressID ?? 0,
                            AddressTypeID = 1,
                            SeqNo = 1
                        };
                        _orgAddressRepository.InsertAddress(orgAdd);
                    }

                    if (model.CheckB)
                    {
                        if (model.CompanyAddressID == model.DeliverAddressID)
                        {
                            var orgDeliverAdd = _orgAddressRepository.GetAddress(model.SupplierID ?? 0, model.DeliverAddressID ?? 0, 2);
                            if (orgDeliverAdd != null)
                            {
                                //update OrgAddress

                                _orgAddressRepository.UpdateAddress(orgDeliverAdd, model.CompanyAddressID ?? 0);
                            }


                        }
                        else
                        {
                            var orgDeliverAdd = _orgAddressRepository.GetAddress(model.SupplierID ?? 0, model.DeliverAddressID ?? 0, 2);
                            if (orgDeliverAdd != null)
                            {
                                //update OrgAddress
                                _orgAddressRepository.UpdateAddress(orgDeliverAdd, model.CompanyAddressID ?? 0);
                            }
                            else
                            {
                                //insert OrgAddress
                                var orgAdd = new Tbl_OrgAddress()
                                {
                                    SupplierID = model.SupplierID ?? 0,
                                    AddressID = model.CompanyAddressID ?? 0,
                                    AddressTypeID = 2,
                                    SeqNo = 1
                                };
                                _orgAddressRepository.InsertAddress(orgAdd);
                            }

                        }
                    }
                    else
                    {
                        var orgDeliverAdd = _orgAddressRepository.GetAddress(model.SupplierID ?? 0, model.DeliverAddressID ?? 0, 2);

                        if (model.CompanyAddressID == model.DeliverAddressID)
                        {
                            var deliverAddress = new Tbl_Address();

                            deliverAddress.HouseNo_Local = model.DeliverAddress_HouseNo_Local ?? "";
                            deliverAddress.HouseNo_Inter = model.DeliverAddress_HouseNo_Inter ?? "";
                            deliverAddress.VillageNo_Local = model.DeliverAddress_VillageNo_Local ?? "";
                            deliverAddress.VillageNo_Inter = model.DeliverAddress_VillageNo_Inter ?? "";
                            deliverAddress.Lane_Local = model.DeliverAddress_Lane_Local ?? "";
                            deliverAddress.Lane_Inter = model.DeliverAddress_Lane_Inter ?? "";
                            deliverAddress.Road_Local = model.DeliverAddress_Road_Local ?? "";
                            deliverAddress.Road_Inter = model.DeliverAddress_Road_Inter ?? "";
                            deliverAddress.SubDistrict_Local = model.DeliverAddress_SubDistrict_Local ?? "";
                            deliverAddress.SubDistrict_Inter = model.DeliverAddress_SubDistrict_Inter ?? "";
                            deliverAddress.City_Local = model.DeliverAddress_City_Local ?? "";
                            deliverAddress.City_Inter = model.DeliverAddress_City_Inter ?? "";
                            deliverAddress.State_Local = model.DeliverAddress_State_Local ?? "";
                            deliverAddress.State_Inter = model.DeliverAddress_State_Inter ?? "";
                            deliverAddress.CountryCode = model.DeliverAddress_CountryCode ?? "";
                            deliverAddress.PostalCode = model.DeliverAddress_PostalCode ?? "";


                            // insert Tbl_Address
                            int deAddID = 0;
                            deAddID = _addressRepository.InsertAddress(deliverAddress);

                            if (orgDeliverAdd == null)
                            {
                                var orgAdd = new Tbl_OrgAddress()
                                {
                                    SupplierID = model.SupplierID ?? 0,
                                    AddressID = deAddID,
                                    AddressTypeID = 2,
                                    SeqNo = 1
                                };
                                _orgAddressRepository.InsertAddress(orgAdd);
                            }
                            else
                            {
                                _orgAddressRepository.UpdateAddress(orgDeliverAdd, deAddID);
                            }

                        }
                        else
                        {
                            var deliAdd = _addressRepository.GetAddressByAddressID(model.DeliverAddressID ?? 0);

                            if (deliAdd != null)
                            {
                                deliAdd.HouseNo_Local = model.DeliverAddress_HouseNo_Local ?? "";
                                deliAdd.HouseNo_Inter = model.DeliverAddress_HouseNo_Inter ?? "";
                                deliAdd.VillageNo_Local = model.DeliverAddress_VillageNo_Local ?? "";
                                deliAdd.VillageNo_Inter = model.DeliverAddress_VillageNo_Inter ?? "";
                                deliAdd.Lane_Local = model.DeliverAddress_Lane_Local ?? "";
                                deliAdd.Lane_Inter = model.DeliverAddress_Lane_Inter ?? "";
                                deliAdd.Road_Local = model.DeliverAddress_Road_Local ?? "";
                                deliAdd.Road_Inter = model.DeliverAddress_Road_Inter ?? "";
                                deliAdd.SubDistrict_Local = model.DeliverAddress_SubDistrict_Local ?? "";
                                deliAdd.SubDistrict_Inter = model.DeliverAddress_SubDistrict_Inter ?? "";
                                deliAdd.City_Local = model.DeliverAddress_City_Local ?? "";
                                deliAdd.City_Inter = model.DeliverAddress_City_Inter ?? "";
                                deliAdd.State_Local = model.DeliverAddress_State_Local ?? "";
                                deliAdd.State_Inter = model.DeliverAddress_State_Inter ?? "";
                                deliAdd.CountryCode = model.DeliverAddress_CountryCode ?? "";
                                deliAdd.PostalCode = model.DeliverAddress_PostalCode ?? "";

                                _addressRepository.UpdateAddress(deliAdd, userID);
                            }
                            else
                            {
                                var deliverAddress = new Tbl_Address();

                                deliverAddress.HouseNo_Local = model.DeliverAddress_HouseNo_Local ?? "";
                                deliverAddress.HouseNo_Inter = model.DeliverAddress_HouseNo_Inter ?? "";
                                deliverAddress.VillageNo_Local = model.DeliverAddress_VillageNo_Local ?? "";
                                deliverAddress.VillageNo_Inter = model.DeliverAddress_VillageNo_Inter ?? "";
                                deliverAddress.Lane_Local = model.DeliverAddress_Lane_Local ?? "";
                                deliverAddress.Lane_Inter = model.DeliverAddress_Lane_Inter ?? "";
                                deliverAddress.Road_Local = model.DeliverAddress_Road_Local ?? "";
                                deliverAddress.Road_Inter = model.DeliverAddress_Road_Inter ?? "";
                                deliverAddress.SubDistrict_Local = model.DeliverAddress_SubDistrict_Local ?? "";
                                deliverAddress.SubDistrict_Inter = model.DeliverAddress_SubDistrict_Inter ?? "";
                                deliverAddress.City_Local = model.DeliverAddress_City_Local ?? "";
                                deliverAddress.City_Inter = model.DeliverAddress_City_Inter ?? "";
                                deliverAddress.State_Local = model.DeliverAddress_State_Local ?? "";
                                deliverAddress.State_Inter = model.DeliverAddress_State_Inter ?? "";
                                deliverAddress.CountryCode = model.DeliverAddress_CountryCode ?? "";
                                deliverAddress.PostalCode = model.DeliverAddress_PostalCode ?? "";


                                // insert Tbl_Address
                                int deAddID = 0;
                                deAddID = _addressRepository.InsertAddress(deliverAddress);

                                if (orgDeliverAdd == null)
                                {
                                    var orgAdd = new Tbl_OrgAddress()
                                    {
                                        SupplierID = model.SupplierID ?? 0,
                                        AddressID = deAddID,
                                        AddressTypeID = 2,
                                        SeqNo = 1
                                    };
                                    _orgAddressRepository.InsertAddress(orgAdd);
                                }
                                else
                                {
                                    _orgAddressRepository.UpdateAddress(orgDeliverAdd, deAddID);
                                }
                            }
                        }
                    }

                    #endregion
                }


                // The Complete method commits the transaction. If an exception has been thrown, 
                // Complete is not  called and the transaction is rolled back.
                //    scope.Complete();
                //}

                #region Update Tbl_Organization.LastUpdate
                _organizationRepository.UpdateLastUpdate(supplierID, userID);
                #endregion


                #region Send Email

                var trackingRequestModel = _trackingRequestRepository.GetTrackingRequestByTrackingReqID(chkTrackingReqID);

                if (trackingRequestModel != null)
                {
                    string languageID = GetCurrentLanguageHelper.GetStringLanguageIDCurrent();
                    string messageReturn = "";

                    string alertMessage = messageReturn.GetStringResource("_ComPro.Msg.TrackingSuccess", languageID);

                    _comProfileTrackingService.SendingEmailVerifyTracking(chkTrackingReqID, languageID);


                    return RedirectToAction("Address", "ContactInformation", new { alertMessage = alertMessage });
                }

                #endregion

            }
            catch (Exception)
            {

                throw;
            }

            return View(@"~/Views/ContactInformation/Address.cshtml");
        }

        public ActionResult _AddressPartial()
        {
            int supplierID = 0;
            string username = "";
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }
            supplierID = _organizationRepository.GetSupplierIDByUsername(username);
            //db = new SupplierPortalEntities();
            var model = new CompanyAddress();
            int compAddressID = 0;
            int deliverAddressID = 0;
            model.SupplierID = supplierID;
            var orgCompanyAddressMapping = _orgAddressRepository.GetAddresType(supplierID, 1);
            var orgDeliverAddressMapping = _orgAddressRepository.GetAddresType(supplierID, 2);
            if (orgCompanyAddressMapping != null)
            {
                compAddressID = orgCompanyAddressMapping.AddressID;
            }
            if (orgDeliverAddressMapping != null)
            {
                var deliverID = _trackingOrgAddressRepository.GetLastDataBySupplierID(supplierID);
                if (deliverID != null)
                {
                    deliverAddressID = deliverID.NewAddressID ?? 0;
                }
                else
                {
                    deliverAddressID = orgDeliverAddressMapping.AddressID;
                }
            }
            else
            {
                deliverAddressID = orgCompanyAddressMapping.AddressID;
            }
            //get data trackingstatus
            model.trackingFields = model.GetTrackingStatus<Tbl_Address>(compAddressID);


            var orgCompanyAddress = _addressRepository.GetAddressByAddressID(compAddressID);



            if (orgCompanyAddress != null)
            {
                model.CompanyAddressID = compAddressID;
                if (model.trackingFields["HouseNo_Local"].TrackingStatusID == 2)
                    model.CompanyAddress_HouseNo_Local = model.trackingFields["HouseNo_Local"].NewKeyValue ?? "";
                else model.CompanyAddress_HouseNo_Local = orgCompanyAddress.HouseNo_Local ?? "";

                if (model.trackingFields["HouseNo_Inter"].TrackingStatusID == 2)
                    model.CompanyAddress_HouseNo_Inter = model.trackingFields["HouseNo_Inter"].NewKeyValue ?? "";
                else model.CompanyAddress_HouseNo_Inter = orgCompanyAddress.HouseNo_Inter ?? "";

                if (model.trackingFields["VillageNo_Local"].TrackingStatusID == 2)
                    model.CompanyAddress_VillageNo_Local = model.trackingFields["VillageNo_Local"].NewKeyValue ?? "";
                else model.CompanyAddress_VillageNo_Local = orgCompanyAddress.VillageNo_Local ?? "";

                if (model.trackingFields["VillageNo_Inter"].TrackingStatusID == 2)
                    model.CompanyAddress_VillageNo_Inter = model.trackingFields["VillageNo_Inter"].NewKeyValue ?? "";
                else model.CompanyAddress_VillageNo_Inter = orgCompanyAddress.VillageNo_Inter ?? "";

                if (model.trackingFields["Lane_Local"].TrackingStatusID == 2)
                    model.CompanyAddress_Lane_Local = model.trackingFields["Lane_Local"].NewKeyValue ?? "";
                else model.CompanyAddress_Lane_Local = orgCompanyAddress.Lane_Local ?? "";

                if (model.trackingFields["Lane_Inter"].TrackingStatusID == 2)
                    model.CompanyAddress_Lane_Inter = model.trackingFields["Lane_Inter"].NewKeyValue ?? "";
                else model.CompanyAddress_Lane_Inter = orgCompanyAddress.Lane_Inter ?? "";

                if (model.trackingFields["Road_Local"].TrackingStatusID == 2)
                    model.CompanyAddress_Road_Local = model.trackingFields["Road_Local"].NewKeyValue ?? "";
                else model.CompanyAddress_Road_Local = orgCompanyAddress.Road_Local ?? "";

                if (model.trackingFields["Road_Inter"].TrackingStatusID == 2)
                    model.CompanyAddress_Road_Inter = model.trackingFields["Road_Inter"].NewKeyValue ?? "";
                else model.CompanyAddress_Road_Inter = orgCompanyAddress.Road_Inter ?? "";

                if (model.trackingFields["SubDistrict_Local"].TrackingStatusID == 2)
                    model.CompanyAddress_SubDistrict_Local = model.trackingFields["SubDistrict_Local"].NewKeyValue ?? "";
                else model.CompanyAddress_SubDistrict_Local = orgCompanyAddress.SubDistrict_Local ?? "";

                if (model.trackingFields["SubDistrict_Inter"].TrackingStatusID == 2)
                    model.CompanyAddress_SubDistrict_Inter = model.trackingFields["SubDistrict_Inter"].NewKeyValue ?? "";
                else model.CompanyAddress_SubDistrict_Inter = orgCompanyAddress.SubDistrict_Inter ?? "";

                if (model.trackingFields["City_Local"].TrackingStatusID == 2)
                    model.CompanyAddress_City_Local = model.trackingFields["City_Local"].NewKeyValue ?? "";
                else model.CompanyAddress_City_Local = orgCompanyAddress.City_Local ?? "";

                if (model.trackingFields["City_Inter"].TrackingStatusID == 2)
                    model.CompanyAddress_City_Inter = model.trackingFields["City_Inter"].NewKeyValue ?? "";
                else model.CompanyAddress_City_Inter = orgCompanyAddress.City_Inter ?? "";

                if (model.trackingFields["State_Local"].TrackingStatusID == 2)
                    model.CompanyAddress_State_Local = model.trackingFields["State_Local"].NewKeyValue ?? "";
                else model.CompanyAddress_State_Local = orgCompanyAddress.State_Local ?? "";

                if (model.trackingFields["State_Inter"].TrackingStatusID == 2)
                    model.CompanyAddress_State_Inter = model.trackingFields["State_Inter"].NewKeyValue ?? "";
                else model.CompanyAddress_State_Inter = orgCompanyAddress.State_Inter ?? "";

                if (model.trackingFields["PostalCode"].TrackingStatusID == 2)
                    model.CompanyAddress_PostalCode = model.trackingFields["PostalCode"].NewKeyValue ?? "";
                else model.CompanyAddress_PostalCode = orgCompanyAddress.PostalCode ?? "";

                if (model.trackingFields["CountryCode"].TrackingStatusID == 2)
                    model.CompanyAddress_CountryCode = model.trackingFields["CountryCode"].NewKeyValue ?? "";
                else model.CompanyAddress_CountryCode = orgCompanyAddress.CountryCode ?? "";
            }

            if (deliverAddressID != compAddressID)
            {
                //get data trackingstatus
                model.trackingFieldsDel = model.GetTrackingStatus<Tbl_Address>(deliverAddressID);
            }
            var orgDeliverAddress = _addressRepository.GetAddressByAddressID(deliverAddressID);

            if (model.trackingFieldsDel != null)
            {
                model.DeliverAddressID = deliverAddressID;

                if (model.trackingFieldsDel["HouseNo_Local"].TrackingStatusID == 2)
                    model.DeliverAddress_HouseNo_Local = model.trackingFieldsDel["HouseNo_Local"].NewKeyValue ?? "";
                else model.DeliverAddress_HouseNo_Local = orgDeliverAddress.HouseNo_Local ?? "";

                if (model.trackingFieldsDel["HouseNo_Inter"].TrackingStatusID == 2)
                    model.DeliverAddress_HouseNo_Inter = model.trackingFieldsDel["HouseNo_Inter"].NewKeyValue ?? "";
                else model.DeliverAddress_HouseNo_Inter = orgDeliverAddress.HouseNo_Inter ?? "";

                if (model.trackingFieldsDel["VillageNo_Local"].TrackingStatusID == 2)
                    model.DeliverAddress_VillageNo_Local = model.trackingFieldsDel["VillageNo_Local"].NewKeyValue ?? "";
                else model.DeliverAddress_VillageNo_Local = orgDeliverAddress.VillageNo_Local ?? "";

                if (model.trackingFieldsDel["VillageNo_Inter"].TrackingStatusID == 2)
                    model.DeliverAddress_VillageNo_Inter = model.trackingFieldsDel["VillageNo_Inter"].NewKeyValue ?? "";
                else model.DeliverAddress_VillageNo_Inter = orgDeliverAddress.VillageNo_Inter ?? "";

                if (model.trackingFieldsDel["Lane_Local"].TrackingStatusID == 2)
                    model.DeliverAddress_Lane_Local = model.trackingFieldsDel["Lane_Local"].NewKeyValue ?? "";
                else model.DeliverAddress_Lane_Local = orgDeliverAddress.Lane_Local ?? "";

                if (model.trackingFieldsDel["Lane_Inter"].TrackingStatusID == 2)
                    model.DeliverAddress_Lane_Inter = model.trackingFieldsDel["Lane_Inter"].NewKeyValue ?? "";
                else model.DeliverAddress_Lane_Inter = orgDeliverAddress.Lane_Inter ?? "";

                if (model.trackingFieldsDel["Road_Local"].TrackingStatusID == 2)
                    model.DeliverAddress_Road_Local = model.trackingFieldsDel["Road_Local"].NewKeyValue ?? "";
                else model.DeliverAddress_Road_Local = orgDeliverAddress.Road_Local ?? "";

                if (model.trackingFieldsDel["Road_Inter"].TrackingStatusID == 2)
                    model.DeliverAddress_Road_Inter = model.trackingFieldsDel["Road_Inter"].NewKeyValue ?? "";
                else model.DeliverAddress_Road_Inter = orgDeliverAddress.Road_Inter ?? "";

                if (model.trackingFieldsDel["SubDistrict_Local"].TrackingStatusID == 2)
                    model.DeliverAddress_SubDistrict_Local = model.trackingFieldsDel["SubDistrict_Local"].NewKeyValue ?? "";
                else model.DeliverAddress_SubDistrict_Local = orgDeliverAddress.SubDistrict_Local ?? "";

                if (model.trackingFieldsDel["SubDistrict_Inter"].TrackingStatusID == 2)
                    model.DeliverAddress_SubDistrict_Inter = model.trackingFieldsDel["SubDistrict_Inter"].NewKeyValue ?? "";
                else model.DeliverAddress_SubDistrict_Inter = orgDeliverAddress.SubDistrict_Inter ?? "";

                if (model.trackingFieldsDel["City_Local"].TrackingStatusID == 2)
                    model.DeliverAddress_City_Local = model.trackingFieldsDel["City_Local"].NewKeyValue ?? "";
                else model.DeliverAddress_City_Local = orgDeliverAddress.City_Local ?? "";

                if (model.trackingFieldsDel["City_Inter"].TrackingStatusID == 2)
                    model.DeliverAddress_City_Inter = model.trackingFieldsDel["City_Inter"].NewKeyValue ?? "";
                else model.DeliverAddress_City_Inter = orgDeliverAddress.City_Inter ?? "";

                if (model.trackingFieldsDel["State_Local"].TrackingStatusID == 2)
                    model.DeliverAddress_State_Local = model.trackingFieldsDel["State_Local"].NewKeyValue ?? "";
                else model.DeliverAddress_State_Local = orgDeliverAddress.State_Local ?? "";

                if (model.trackingFieldsDel["State_Inter"].TrackingStatusID == 2)
                    model.DeliverAddress_State_Inter = model.trackingFieldsDel["State_Inter"].NewKeyValue ?? "";
                else model.DeliverAddress_State_Inter = orgDeliverAddress.State_Inter ?? "";

                if (model.trackingFieldsDel["PostalCode"].TrackingStatusID == 2)
                    model.DeliverAddress_PostalCode = model.trackingFieldsDel["PostalCode"].NewKeyValue ?? "";
                else model.DeliverAddress_PostalCode = orgDeliverAddress.PostalCode ?? "";

                if (model.trackingFieldsDel["CountryCode"].TrackingStatusID == 2)
                    model.DeliverAddress_CountryCode = model.trackingFieldsDel["CountryCode"].NewKeyValue ?? "";
                else model.DeliverAddress_CountryCode = orgDeliverAddress.CountryCode ?? "";

            }
            else
            {
                //get data trackingstatus
                model.trackingFieldsDel = model.GetTrackingStatus<Tbl_Address>(0);
                // model.trackingFieldsDel = new Dictionary<string, TrackingCompProfile>();

                model.DeliverAddressID = deliverAddressID;
                model.DeliverAddress_HouseNo_Local = orgDeliverAddress.HouseNo_Local ?? "";
                model.DeliverAddress_HouseNo_Inter = orgDeliverAddress.HouseNo_Inter ?? "";
                model.DeliverAddress_VillageNo_Inter = orgDeliverAddress.VillageNo_Inter ?? "";
                model.DeliverAddress_Lane_Local = orgDeliverAddress.Lane_Local ?? "";
                model.DeliverAddress_Lane_Inter = orgDeliverAddress.Lane_Inter ?? "";
                model.DeliverAddress_Road_Local = orgDeliverAddress.Road_Local ?? "";
                model.DeliverAddress_Road_Inter = orgDeliverAddress.Road_Inter ?? "";
                model.DeliverAddress_SubDistrict_Local = orgDeliverAddress.SubDistrict_Local ?? "";
                model.DeliverAddress_SubDistrict_Inter = orgDeliverAddress.SubDistrict_Inter ?? "";
                model.DeliverAddress_City_Local = orgDeliverAddress.City_Local ?? "";
                model.DeliverAddress_City_Inter = orgDeliverAddress.City_Inter ?? "";
                model.DeliverAddress_State_Local = orgDeliverAddress.State_Local ?? "";
                model.DeliverAddress_State_Inter = orgDeliverAddress.State_Inter ?? "";
                model.DeliverAddress_PostalCode = orgDeliverAddress.PostalCode ?? "";
                model.DeliverAddress_CountryCode = orgDeliverAddress.CountryCode ?? "";


            }
            //var dMLanguageList = _languageRepository.GetDynamicLanguage();
            //model.DynamicLanguageList = dMLanguageList;
            if (compAddressID == deliverAddressID)
            {
                model.CheckB = true;
            }
            else
            {
                model.CheckB = false;
            }


            model.trackingOrgAddress = _trackingOrgAddressRepository.GetLastDataBySupplierID(supplierID);
            if (model.trackingOrgAddress != null)
            {
                if (model.trackingOrgAddress.NewAddressID == model.trackingOrgAddress.OldAddressID)
                {
                    model.trackingOrgAddress = new Tbl_TrackingOrgAddress { TrackingStatusID = 1 };
                }
            }
            else
            {
                model.trackingOrgAddress = new Tbl_TrackingOrgAddress { TrackingStatusID = 1 };
            }

            model.trackingAddress = _trackingOrgAddressRepository.GetDataCheckAddress(supplierID);
            if (model.trackingAddress != null)
            {
                if (model.trackingAddress.NewAddressID != model.trackingAddress.OldAddressID)
                {
                    model.trackingAddress = new Tbl_TrackingOrgAddress { TrackingStatusID = 1 };
                }
            }
            else
            {
                model.trackingAddress = new Tbl_TrackingOrgAddress { TrackingStatusID = 1 };
            }
            model.trackingDelivery = _trackingOrgAddressRepository.GetDataCheckDelivery(supplierID);
            if (model.trackingDelivery != null)
            {
                if (model.trackingDelivery.NewAddressID != model.trackingDelivery.OldAddressID)
                {
                    model.trackingDelivery = new Tbl_TrackingOrgAddress { TrackingStatusID = 1 };
                }
            }
            else
            {
                model.trackingDelivery = new Tbl_TrackingOrgAddress { TrackingStatusID = 1 };
            }

            return View(@"~/Views/ContactInformation/_AddressPartial.cshtml", model);
        }

        public ActionResult ContactCompany()
        {
            string username = "Metro-Admin"; //Metro-Admin
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }
            int supID = _organizationRepository.GetSupplierIDByUsername(username);

            var orgModel = _organizationRepository.GetDataBySupplierID(supID);
            var model = new GeneralInfosModel();
            model.SupplierID = orgModel.SupplierID;
            model.PhoneNo = orgModel.PhoneNo;
            model.PhoneExt = orgModel.PhoneExt;
            model.MobileNo = orgModel.MobileNo;
            model.FaxExt = orgModel.FaxExt;
            model.FaxNo = orgModel.FaxNo;
            model.WebSite = orgModel.WebSite;


            return View(@"~/Views/ContactInformation/ContactCompany.cshtml", model);

        }

        [HttpPost]
        public ActionResult ContactCompany(GeneralInfosModel model)
        {
            var OrgModel = _organizationRepository.GetDataBySupplierID(model.SupplierID);
            int userID = 0;
            string username = ""; //Metro-Admin
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }
            var user = _userRepository.FindByUsername(username);
            if (user != null)
            {
                userID = user.UserID;
            }

            if (OrgModel != null)
            {
                OrgModel.PhoneNo = model.PhoneNo;
                OrgModel.PhoneExt = model.PhoneExt;
                OrgModel.MobileNo = model.MobileNo;
                OrgModel.FaxNo = model.FaxNo;
                OrgModel.FaxExt = model.FaxExt;
                OrgModel.WebSite = model.WebSite;
                _organizationRepository.Update(OrgModel, userID); //Orgmodel.SupplierID

                #region Update Tbl_Organization.LastUpdate
                _organizationRepository.UpdateLastUpdate(OrgModel.SupplierID, userID);
                #endregion
            }

            return View(@"~/Views/ContactInformation/ContactCompany.cshtml", model);
        }

        public ActionResult ContactPerson()
        {
            return View();
        }

        public ActionResult _MapAPIPartial()
        {

            return View();
        }

        public ActionResult Map(int supplierID = 0)
        {
            string username = "";
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }
            supplierID = _organizationRepository.GetSupplierIDByUsername(username);

            var model = _organizationRepository.GetDataBySupplierID(supplierID);
            return View(@"~/Views/ContactInformation/Map.cshtml", model);
        }

        [HttpPost]
        public ActionResult Map(Tbl_Organization model)
        {

            string username = "";
            int userID = 0;
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }
            var user = _userRepository.FindByUsername(username);
            if (user != null)
            {
                userID = user.UserID;
            }

            var OrgModel = _organizationRepository.GetDataBySupplierID(model.SupplierID);

            if (OrgModel != null)
            {
                OrgModel.Latitude = model.Latitude;
                OrgModel.Longtitude = model.Longtitude;

                _organizationRepository.Update(OrgModel, userID);
            }

            var Orgmodel = _organizationRepository.GetDataBySupplierID(model.SupplierID);

            #region Update Tbl_Organization.LastUpdate
            _organizationRepository.UpdateLastUpdate(model.SupplierID, userID);
            #endregion

            return View(@"~/Views/ContactInformation/Map.cshtml", Orgmodel);

        }

        public ActionResult LatValidate(double Latitude)
        {
            string languageId = "";

            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (cultureCookie != null)
            {
                languageId = cultureCookie.Value.ToString();

            }
            else
            {
                languageId = CultureHelper.GetImplementedCulture(languageId);// This is safe
            }

            bool IsValid = true;

            string messageReturn = "";

            string errorMessage = "";

            if (Latitude >= -90 && Latitude <= 90)
            {
                return Json(IsValid, JsonRequestBehavior.AllowGet);
            }
            else
            {
                errorMessage = messageReturn.GetStringResource("_Regis.ValidateMsg.NumberOnly.Title", languageId);

                return Json(errorMessage, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult LongValidate(double Longtitude)
        {
            string languageId = "";

            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (cultureCookie != null)
            {
                languageId = cultureCookie.Value.ToString();

            }
            else
            {
                languageId = CultureHelper.GetImplementedCulture(languageId);// This is safe
            }

            bool IsValid = true;


            string messageReturn = "";

            string errorMessage = "";

            if (Longtitude >= -180 && Longtitude <= 180)
            {
                return Json(IsValid, JsonRequestBehavior.AllowGet);
            }
            else
            {
                errorMessage = messageReturn.GetStringResource("_Regis.ValidateMsg.NumberOnly.Title", languageId);

                return Json(errorMessage, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult CompanyContryCode(CompanyAddress model)
        {
            //ICountryRepository _repo = new CountryRepository();
            //ViewData["CountryList2"] = _repo.GetCountryList();
            //_repo.Dispose();
            int? supId = model.SupplierID;
            var data = model.GetTrackingStatus<Tbl_Address>(model.CompanyAddressID ?? 0);
            ViewBag.cusAttr = data;
            ViewData["CountryList2"] = _countryRepository.GetCountryList();

            ViewBag.ContryCodeListCompany = new SelectList(_countryRepository.GetCountryList(), "CountryCode", "CountryName");

            return PartialView(@"~/Views/ContactInformation/CompanyContryCode.cshtml", model);

        }

        public ActionResult DeliveContryCode(CompanyAddress model)
        {
            //ICountryRepository _repo = new CountryRepository();
            //ViewData["CountryList2"] = _repo.GetCountryList();
            //_repo.Dispose();
            int? supId = model.SupplierID;
            if (model.DeliverAddressID != model.CompanyAddressID)
            {
                var data = model.GetTrackingStatus<Tbl_Address>(model.DeliverAddressID ?? 0);
                ViewBag.cusAttr = data;
            }
            else
            {
                var data = model.GetTrackingStatus<Tbl_Address>(0);
                ViewBag.cusAttr = data;
            }

            ViewData["CountryList2"] = _countryRepository.GetCountryList();

            ViewBag.ContryCodeListDelive = new SelectList(_countryRepository.GetCountryList(), "CountryCode", "CountryName");

            return PartialView(@"~/Views/ContactInformation/DeliveContryCode.cshtml", model);

        }


        public ActionResult SubCompLocalPartial(string term)
        {
            var SubCompLocals = _subDistrictRepository.GetSubDistrictList(term);
            return Json(SubCompLocals, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SubCompInterPartial(string term)
        {
            var SubCompInters = _subDistrictRepository.GetSubDistrictList(term);
            return Json(SubCompInters, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SubDelvLocalPartial(string term)
        {
            var SubDelvLocals = _subDistrictRepository.GetSubDistrictList(term);
            return Json(SubDelvLocals, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SubDelvInterPartial(string term)
        {
            var SubDelvInters = _subDistrictRepository.GetSubDistrictList(term);
            return Json(SubDelvInters, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CityCompLocalPartial(string term)
        {
            var CityCompLocals = _cityRepository.GetCityList(term);
            return Json(CityCompLocals, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CityCompInterPartial(string term)
        {
            var CityCompInters = _cityRepository.GetCityList(term);
            return Json(CityCompInters, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CityDelvLocalPartial(string term)
        {
            var CityDelvLocals = _cityRepository.GetCityList(term);
            return Json(CityDelvLocals, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CityDelvInterPartial(string term)
        {
            var CityDelvInters = _cityRepository.GetCityList(term);
            return Json(CityDelvInters, JsonRequestBehavior.AllowGet);
        }


        public ActionResult StateCompLocalPartial(string term)
        {
            var StateCompLocals = _stateRepository.GetStateList(term);
            return Json(StateCompLocals, JsonRequestBehavior.AllowGet);
        }

        public ActionResult StateCompInterPartial(string term)
        {
            var StateCompInters = _stateRepository.GetStateList(term);
            return Json(StateCompInters, JsonRequestBehavior.AllowGet);
        }

        public ActionResult StateDelvLocalPartial(string term)
        {
            var StateDelvLocals = _stateRepository.GetStateList(term);
            return Json(StateDelvLocals, JsonRequestBehavior.AllowGet);
        }

        public ActionResult StateDelvInterPartial(string term)
        {
            var StateDelvInters = _stateRepository.GetStateList(term);
            return Json(StateDelvInters, JsonRequestBehavior.AllowGet);
        }


        public ActionResult PostalCodeValidateComp(string CompanyAddress_PostalCode, string CompanyAddress_CountryCode_VI)
        {
            string languageId = "";

            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (cultureCookie != null)
            {
                languageId = cultureCookie.Value.ToString();

            }
            else
            {
                languageId = CultureHelper.GetImplementedCulture(languageId);// This is safe
            }

            bool IsValid = true;

            string messageReturn = "";

            string errorMessage = "";
            if (string.IsNullOrEmpty(CompanyAddress_PostalCode))
            {
                CompanyAddress_PostalCode = "";
            }
            if (string.IsNullOrEmpty(CompanyAddress_CountryCode_VI))
            {
                CompanyAddress_CountryCode_VI = "";
            }

            var sqlQuery = "select dbo.VerifyPostalCode('" + CompanyAddress_PostalCode.ToString() + "','" + CompanyAddress_CountryCode_VI.ToString() + "')";
            IFunctionRepository _repo = new FunctionRepository();

            var result = _repo.GetResultByQuery(sqlQuery);
            _repo.Dispose();
            if (result)
            {
                return Json(IsValid, JsonRequestBehavior.AllowGet);
            }
            else
            {
                errorMessage = messageReturn.GetStringResource("_Regis.ValidateMsg.InvalidFormatPostalCode", languageId);

                return Json(errorMessage, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult PostalCodeValidateDelv(string DeliverAddress_PostalCode, string DeliverAddress_CountryCode_VI)
        {
            string languageId = "";

            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (cultureCookie != null)
            {
                languageId = cultureCookie.Value.ToString();

            }
            else
            {
                languageId = CultureHelper.GetImplementedCulture(languageId);// This is safe
            }

            bool IsValid = true;

            string messageReturn = "";

            string errorMessage = "";
            if (string.IsNullOrEmpty(DeliverAddress_PostalCode))
            {
                DeliverAddress_PostalCode = "";
            }
            if (string.IsNullOrEmpty(DeliverAddress_CountryCode_VI))
            {
                DeliverAddress_CountryCode_VI = "";
            }

            var sqlQuery = "select dbo.VerifyPostalCode('" + DeliverAddress_PostalCode.ToString() + "','" + DeliverAddress_CountryCode_VI.ToString() + "')";
            IFunctionRepository _repo = new FunctionRepository();

            var result = _repo.GetResultByQuery(sqlQuery);
            _repo.Dispose();
            if (result)
            {
                return Json(IsValid, JsonRequestBehavior.AllowGet);
            }
            else
            {
                errorMessage = messageReturn.GetStringResource("_Regis.ValidateMsg.InvalidFormatPostalCode", languageId);

                return Json(errorMessage, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult WebsiteValidate(string WebSite)
        {
            string languageId = "";

            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (cultureCookie != null)
            {
                languageId = cultureCookie.Value.ToString();

            }
            else
            {
                languageId = CultureHelper.GetImplementedCulture(languageId);// This is safe
            }

            bool IsValid = true;

            string messageReturn = "";

            string errorMessage = "";
            Regex rgx = new Regex(@"^((http|https)://)?([\w-]+\.)+[\w]+(/[\w- ./?]*)?$");
            if (rgx.IsMatch(WebSite))
            {
                return Json(IsValid, JsonRequestBehavior.AllowGet);
            }
            else
            {
                errorMessage = messageReturn.GetStringResource("_ComPro.ValidateMsg.InvalidFormatWebsite", languageId);

                return Json(errorMessage, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SupplierPortal.Data.Models.Repository.Product;
using SupplierPortal.Framework.MethodHelper;
using Newtonsoft.Json;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;
using SupplierPortal.Data.Models.Repository.Organization;
using SupplierPortal.Data.Models.Repository.OrgProduct;
using SupplierPortal.Data.Models.Repository.ProductType;
using SupplierPortal.Data.Models.Repository.BusinessType;
using SupplierPortal.Data.Models.Repository.User;
using SupplierPortal.Data.Models.Repository.OrgProductKeyword;
using SupplierPortal.Services.CompanyProfileManage;
using SupplierPortal.Data.Models.SupportModel.CompanyProfile;
using SupplierPortal.Data.Helper;
using SupplierPortal.Data.Models.Repository.ProductCategory;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.Register;

namespace SupplierPortal.Controllers
{
    public class ProductInformationController : ControllerBase
    {
        IProductRepository _productRepository;
        IOrganizationRepository _organizationRepository;
        IComProfileDataService _comProfileDataService;
        IOrgProductRepository _orgProductRepository;
        IProductTypeRepository _productTypeRepository;
        IBusinessTypeRepository _businessTypeRepository;
        IUserRepository _userRepository;
        IOrgProductKeywordRepository _orgProductKeywordRepository;
        IProductCategoryRepository _productCategoryRepository;

        public ProductInformationController()
        {
            _productRepository = new ProductRepository();
            _organizationRepository = new OrganizationRepository();
            _comProfileDataService = new ComProfileDataService();
            _orgProductRepository = new OrgProductRepository();
            _productTypeRepository = new ProductTypeRepository();
            _businessTypeRepository = new BusinessTypeRepository();
            _userRepository = new UserRepository();
            _orgProductKeywordRepository = new OrgProductKeywordRepository();
            _productCategoryRepository = new ProductCategoryRepository();
        }

        public ProductInformationController
            (
            ProductRepository productRepository,
            OrganizationRepository organizationRepository,
            ComProfileDataService comProfileDataService,
            OrgProductRepository orgProductRepository,
            ProductTypeRepository productTypeRepository,
            BusinessTypeRepository businessTypeRepository,
            UserRepository userRepository,
            OrgProductKeywordRepository orgProductKeywordRepository,
            ProductCategoryRepository productCategoryRepository
            )
        {
            _productRepository = productRepository;
            _organizationRepository = organizationRepository;
            _comProfileDataService = comProfileDataService;
            _orgProductRepository = orgProductRepository;
            _productTypeRepository = productTypeRepository;
            _businessTypeRepository = businessTypeRepository;
            _userRepository = userRepository;
            _orgProductKeywordRepository = orgProductKeywordRepository;
            _productCategoryRepository = productCategoryRepository;
        }
        // GET: ProductInformation
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ProductService()
        {
            string languageID = "";
            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (cultureCookie != null)
            {
                languageID = cultureCookie.Value.ToString();

            }
            if (string.IsNullOrEmpty(languageID))
            {
                languageID = CultureHelper.GetImplementedCulture(languageID); // This is safe
            }
            int languageId = Convert.ToInt32(languageID);

            string username = "";
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }

            int supplierID = _organizationRepository.GetSupplierIDByUsername(username);

            var model = _comProfileDataService.GetProductServiceBySupplierID(supplierID);


            var dataKey = _orgProductKeywordRepository.GetProductKeywordBySupplierID(model.SupplierID);

            foreach (var item in dataKey)
            {
                if (dataKey.Count() <= 1)
                {
                    model.ProductKeyword += item.Keyword;
                }
                else
                {
                    if (model.ProductKeyword != null)
                    {
                        model.ProductKeyword += "," + item.Keyword;
                    }
                    else
                    {
                        model.ProductKeyword += item.Keyword;
                    }

                }
            }
            if (languageId == 1)
            {
                ViewBag.Category1 = _productCategoryRepository.GetProductCat1(1);
            }
            else if (languageId == 2)
            {
                ViewBag.Category1 = _productCategoryRepository.GetProductCat1(2);
            }

            var dataOrgCat = _productCategoryRepository
               .GetOrgCatDataBySupplierID(supplierID)
               .Select(s => s.CategoryID_Lev3)
               .ToArray();

            if (dataOrgCat != null)
            {
                if (languageId == 1)
                {
                    ViewBag.ListCategory = _productCategoryRepository.GetItemCat1ByCat3(dataOrgCat).OrderBy(o => o.CategoryName_Inter).ToList();
                }
                else if (languageId == 2)
                {
                    ViewBag.ListCategory = _productCategoryRepository.GetItemCat1ByCat3(dataOrgCat).OrderBy(o => o.CategoryName_Local).ToList();
                }

            }

            var getProductType = _productCategoryRepository
            .GetOrgCatDataBySupplierID(supplierID)
            .ToList();
            if (getProductType != null)
            {
                ViewBag.ProductType = getProductType;
            }

            return View(model);
        }

        public ActionResult ProductListPartial(int supplierID)
        {
            string languageID = GetCurrentLanguageHelper.GetStringLanguageIDCurrent();

            int languageId = Convert.ToInt32(languageID);

            var modelsTemp = _orgProductRepository.GetOrgProductBySupplierIDAndLanguageID(supplierID, languageId);

            List<OrgProductGridViewModel> models = new List<OrgProductGridViewModel>();

            foreach (var itemList in modelsTemp)
            {
                if (!models.Exists(m => m.ProductCode == itemList.ProductCode))
                {
                    var itemAdd = new OrgProductGridViewModel
                    {
                        SupplierID = supplierID,
                        ProdTypeID = itemList.ProdTypeID,
                        OldProdTypeID = GetOldProductType(supplierID, itemList.ProductCode),
                        ProductCode = itemList.ProductCode,
                        ProductName = itemList.ProductName,

                    };

                    models.Add(itemAdd);
                }

            }

            //return PartialView("_ProductListPartial", models);
            return PartialView(@"~/Views/ProductInformation/_ProductListPartial.cshtml", models);
        }

        [HttpPost]
        public ActionResult AddProductMulti(string productArray, string productType, string dataAdd)
        {
            List<ProductTypeAddModel> productTypeAdd = JsonConvert.DeserializeObject<List<ProductTypeAddModel>>(productType);

            string languageID = GetCurrentLanguageHelper.GetStringLanguageIDCurrent();

            int languageId = Convert.ToInt32(languageID);

            List<ProductSearchResultModel> productList = JsonConvert.DeserializeObject<List<ProductSearchResultModel>>(productArray);

            List<ProductAddModel> productAddRows = JsonConvert.DeserializeObject<List<ProductAddModel>>(dataAdd);

            string htmlText = "";

            foreach (var item in productList)
            {
                int chk = productAddRows.Where(m => m.ProductCode == item.ProductCode).Count();

                if (chk == 0)
                {
                    htmlText += GetHtmlAddProductText(item.ProductName, item.ProductCode, productTypeAdd, languageId);
                }
            }



            bool isSuccess = true;

            string productCode = "";

            return Json(new
            {
                isSuccess = isSuccess,
                htmlText = @htmlText,
                productCode = productCode
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ProductServiceSave(CompanyProductModel model)
        {
            try
            {
                string username = "";
                if (Session["username"] != null)
                {
                    username = Session["username"].ToString();
                }
                var user = _userRepository.FindByUsername(username);
                int userID = user.UserID;

                model.SupplierID = user.SupplierID ?? 0;
                List<OrgProductGridViewModel> addProductList = new List<OrgProductGridViewModel>();
                List<BusinessTypeModel> addBusinessTypeList = new List<BusinessTypeModel>();

                var businessType = _businessTypeRepository.GetBusinessTypeAll();


                foreach (var itemList in businessType)
                {
                    var businessTypeID = Request.Params["businessType_" + itemList.BusinessTypeID] ?? string.Empty;

                    if (!string.IsNullOrEmpty(businessTypeID))
                    {
                        var businessTypeModel = new BusinessTypeModel()
                        {
                            BusinessTypeID = itemList.BusinessTypeID
                        };
                        addBusinessTypeList.Add(businessTypeModel);
                    }
                }

                model.OrgBusinessType = addBusinessTypeList;

                var hidProduct = Request.Params["hidCode[]"] ?? string.Empty;

                var splitHidProduct = hidProduct.Split(',');


                #region productTypeList
                var productTypeList = _productTypeRepository.GetProductTypeAll();
                if (splitHidProduct.Length > 0)
                {
                    for (var i = 0; i < splitHidProduct.Length; i++)
                    {
                        string productCode = splitHidProduct[i];
                        var oldChkProductTypeID = Request.Params["hidOldProID_" + productCode] ?? string.Empty;
                        if (!string.IsNullOrEmpty(oldChkProductTypeID))
                        {
                            IList<CheckUpdateProductModel> addProductCheckList = new List<CheckUpdateProductModel>();
                            string chkUpdateProduct = "";

                            foreach (var productList in productTypeList)
                            {
                                var productTypeID = Request.Params["radio" + productCode + "_" + productList.ProductTypeID] ?? string.Empty;

                                if (!string.IsNullOrEmpty(productTypeID))
                                {
                                    chkUpdateProduct += "1";
                                    var itemsCheck = new CheckUpdateProductModel()
                                    {
                                        StatusUpdate = 1,
                                        ProductTypeID = productList.ProductTypeID
                                    };

                                    addProductCheckList.Add(itemsCheck);

                                }
                                else
                                {
                                    chkUpdateProduct += "0";
                                    var itemsCheck = new CheckUpdateProductModel()
                                    {
                                        StatusUpdate = 0,
                                        ProductTypeID = productList.ProductTypeID
                                    };

                                    addProductCheckList.Add(itemsCheck);
                                }
                            }// End  foreach (var productList in productTypeList)
                            if (chkUpdateProduct != oldChkProductTypeID)
                            {
                                foreach (var productCheckList in addProductCheckList)
                                {
                                    if (productCheckList.StatusUpdate == 0)
                                    {
                                        if (_orgProductRepository.OrgProductExists(model.SupplierID, productCode, productCheckList.ProductTypeID))
                                        {
                                            var items = new OrgProductGridViewModel()
                                            {
                                                ProdTypeID = productCheckList.ProductTypeID,
                                                ProductType = "",
                                                ProductCode = productCode,
                                                ProductName = "",
                                                ActionLog = "Remove",
                                            };

                                            addProductList.Add(items);
                                        }
                                    }
                                    else
                                    {
                                        if (!_orgProductRepository.OrgProductExists(model.SupplierID, productCode, productCheckList.ProductTypeID))
                                        {
                                            var items = new OrgProductGridViewModel()
                                            {
                                                ProdTypeID = productCheckList.ProductTypeID,
                                                ProductType = "",
                                                ProductCode = productCode,
                                                ProductName = "",
                                                ActionLog = "Add",
                                            };

                                            addProductList.Add(items);
                                        }
                                    }
                                }
                            }
                        }// End  if (!string.IsNullOrEmpty(oldChkProductTypeID))
                        else
                        {
                            foreach (var productList in productTypeList)
                            {
                                var productTypeID = Request.Params["radio" + productCode + "_" + productList.ProductTypeID] ?? string.Empty;

                                if (!string.IsNullOrEmpty(productTypeID))
                                {
                                    if (!_orgProductRepository.OrgProductExists(model.SupplierID, productCode, Convert.ToInt32(productTypeID)))
                                    {
                                        var items = new OrgProductGridViewModel()
                                        {
                                            ProdTypeID = Convert.ToInt32(productTypeID),
                                            ProductType = "",
                                            ProductCode = productCode,
                                            ProductName = "",
                                            ActionLog = "Add",
                                        };

                                        addProductList.Add(items);
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion

                //Remove Product
                var hidRemoveProduct = Request.Params["hidRemoveProduct[]"] ?? string.Empty;
                var splitHidRemoveProduct = hidRemoveProduct.Split(',');

                if (splitHidRemoveProduct.Length > 0)
                {
                    foreach (var itemlist in splitHidRemoveProduct)
                    {
                        if (_orgProductRepository.OrgProductExists(model.SupplierID, itemlist))
                        {
                            var items = new OrgProductGridViewModel()
                            {
                                //ProdTypeID = null,
                                ProductType = "",
                                ProductCode = itemlist,
                                ProductName = "",
                                ActionLog = "RemoveAll",
                            };

                            addProductList.Add(items);
                        }

                    }
                } //End Remove Product

                model.OrgProductList = addProductList;

                _comProfileDataService.UpdateProductService(model, userID);

                #region productKeyword

                if (model.ProductKeyword != null)
                {
                    var dataKeyWord = _orgProductKeywordRepository.GetProductKeywordBySupplierID(model.SupplierID);
                    _orgProductKeywordRepository.removeKeyWordList(dataKeyWord);
                    var dataKey = model.ProductKeyword.Split(',').ToList();
                    if (model.ProductKeyword != "")
                    {
                        foreach (var Key in dataKey)
                        {
                            _orgProductKeywordRepository.Insert(model.SupplierID, Key.ToString(), userID);
                        }
                    }
                }

                #region ---------------------------------------Insert Category---------------------------------------

                var productCategory = new List<int>();
                //if (model.CategoryID_Lev_3 != null)
                //{
                //    productCategory = JsonConvert.DeserializeObject<List<int>>(model.CategoryID_Lev_3);
                //}

                //if (productCategory != null)

                if (model.ProductCategory != null)
                {
                    _productCategoryRepository.RemoveDataOrgProductCategory(model.SupplierID);
                    var dataOrgCat = new List<Tbl_OrgProductCategory>();
                    foreach (var ProductCate in model.ProductCategory)
                    {
                        if (ProductCate.ProductsTypeID != null)
                        {
                            foreach (var productTypeId in ProductCate.ProductsTypeID)
                            {
                                var data = new Tbl_OrgProductCategory
                                {
                                    CategoryID_Lev3 = ProductCate.CategoryID_Lev_3,
                                    ProductTypeID = productTypeId,
                                    SupplierID = model.SupplierID
                                };
                                dataOrgCat.Add(data);
                            }
                        }
                        else
                        {
                            var data = new Tbl_OrgProductCategory
                            {
                                CategoryID_Lev3 = ProductCate.CategoryID_Lev_3,
                                ProductTypeID = 0,
                                SupplierID = model.SupplierID
                            };
                            dataOrgCat.Add(data);
                        }

                    }

                    _productCategoryRepository.Insert(dataOrgCat);
                }

                #endregion

                //if (dataProduct != null)
                //{
                //    _orgProductKeywordRepository.Delete(model.SupplierID, model.ProductKeyword, userID);
                //}
                //else
                //{
                //    _orgProductKeywordRepository.Insert(model.SupplierID, model.ProductKeyword, userID);
                //}
                #endregion

                #region Update Tbl_Organization.LastUpdate

                _organizationRepository.UpdateLastUpdate(model.SupplierID, userID);

                #endregion

            }
            catch (Exception ex)
            {
                Response.Redirect("~/Error/Error?ErrorMessage=" + ex.Message);
            }

            return RedirectToAction("ProductService");
        }

        [HttpPost]
        public ActionResult AddProductKeyword(string productKeyword)
        {
            string htmlText = "";

            htmlText = GetHtmlAddProductKeywordText(productKeyword.Trim());

            bool isSuccess = true;

            return Json(new
            {
                isSuccess = isSuccess,
                htmlText = @htmlText

            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ProductKeyword()
        {

            string username = "Metro-Admin";
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }

            int supplierID = _organizationRepository.GetSupplierIDByUsername(username);

            var model = _comProfileDataService.GetProductKeywordBySupplierID(supplierID);

            return View(model);
        }

        [HttpPost]
        public ActionResult ProductKeywordSave(CompanyProductKeywordModel model)
        {
            try
            {
                string username = "";
                if (Session["username"] != null)
                {
                    username = Session["username"].ToString();
                }
                var user = _userRepository.FindByUsername(username);
                int userID = user.UserID;

                #region-----------------------Remove ProductKeyword------------------------

                var hidRemoveProductKeyword = Request.Params["hidRemoveProductKeyword[]"] ?? string.Empty;
                var splitHidRemoveProductKeyword = hidRemoveProductKeyword.Split(',');

                if (splitHidRemoveProductKeyword.Length > 0)
                {
                    foreach (var itemlist in splitHidRemoveProductKeyword)
                    {
                        if (!string.IsNullOrEmpty(itemlist.Trim()))
                        {
                            string decodeProductKeyword = StringHelper.GetMakeASCIIStringToComma(itemlist.Trim());

                            if (_orgProductKeywordRepository.OrgProductKeywordExists(model.SupplierID, decodeProductKeyword.Trim()))
                            {
                                _orgProductKeywordRepository.Delete(model.SupplierID, decodeProductKeyword.Trim(), userID);
                            }
                        }
                    }
                }

                #endregion

                #region-----------------------Insert ProductKeyword------------------------

                var hidProductKeyword = Request.Params["hidProductKeyword[]"] ?? string.Empty;
                var splitHidProductKeyword = hidProductKeyword.Split(',');

                if (splitHidProductKeyword.Length > 0)
                {
                    foreach (var itemlist in splitHidProductKeyword)
                    {
                        if (!string.IsNullOrEmpty(itemlist.Trim()))
                        {
                            string decodeProductKeyword = StringHelper.GetMakeASCIIStringToComma(itemlist.Trim());

                            if (!_orgProductKeywordRepository.OrgProductKeywordExists(model.SupplierID, decodeProductKeyword.Trim()))
                            {
                                _orgProductKeywordRepository.Insert(model.SupplierID, decodeProductKeyword.Trim(), userID);
                            }
                        }
                    }
                }

                #endregion

                #region Update Tbl_Organization.LastUpdate

                _organizationRepository.UpdateLastUpdate(model.SupplierID, userID);

                #endregion
            }
            catch (Exception ex)
            {
                Response.Redirect("~/Error/Error?ErrorMessage=" + ex.Message);
            }

            return RedirectToAction("ProductKeyword");
        }

        public ActionResult Product_Keyword(CompanyProductModel model)
        {
            //ViewData["BuyerList"] = _saleInfoRepository.GetBuyerList();
            //return PartialView("_ProductKeyword", model);
            return PartialView(@"~/Views/ProductInformation/_ProductKeyword.cshtml", model);
        }


        public ActionResult GetProductCategory2(int id = 0)
        {
            string LanguageID = "";
            LanguageID = CurrentTime.getLanguageID();
            var data = new List<Tbl_ProductCategoryLev2>();
            if (LanguageID == "1")
            {
                data = _productCategoryRepository.GetProductCat2(id, 1).ToList();
            }
            else if (LanguageID == "2")
            {
                data = _productCategoryRepository.GetProductCat2(id, 2).ToList();
            }


            return View(data);
        }

        public ActionResult GetProductCategory3(int id = 0)
        {
            string LanguageID = "";
            LanguageID = CurrentTime.getLanguageID();
            var data = new List<Tbl_ProductCategoryLev3>();
            if (LanguageID == "1")
            {
                data = _productCategoryRepository.GetProductCat3(id, 1).ToList();
            }
            else if (LanguageID == "2")
            {
                data = _productCategoryRepository.GetProductCat3(id, 2).ToList();
            }


            return View(data);
        }


        [HttpPost]
        public ActionResult GetProductCategory(List<ProductCategory> ProductCategory, int?[] productCategorys = null)
        {
            string username = "Metro-Admin";
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }

            int supplierID = _organizationRepository.GetSupplierIDByUsername(username);
            string LanguageID = "";
            LanguageID = CurrentTime.getLanguageID();
            var ListCategory = new List<CategoryModel>();
            //var dataOrgCat = _productCategoryRepository
            //    .GetOrgCatDataBySupplierID(supplierID)
            //    .Select(s => s.CategoryID_Lev3)
            //    .ToArray()
            //    .Union(productCategorys ?? new int[1])
            //    .Distinct()
            //    .ToArray();

            if (productCategorys != null)
            {
                if (LanguageID == "1")
                {
                    ListCategory = _productCategoryRepository.GetItemCat1ByCat3(productCategorys).OrderBy(o => o.CategoryName_Inter).ToList();
                }
                else if (LanguageID == "2")
                {
                    ListCategory = _productCategoryRepository.GetItemCat1ByCat3(productCategorys).OrderBy(o => o.CategoryName_Local).ToList();
                }

            }



            //var getProductType = _productCategoryRepository
            //.GetOrgCatDataBySupplierID(supplierID)
            //.ToList();

            if (ProductCategory != null)
            {
                var ListDataCate = new List<Tbl_OrgProductCategory>();

                foreach (var item in ProductCategory)
                {
                    foreach (var itemType in item.ProductsTypeID)
                    {
                        var dataCate = new Tbl_OrgProductCategory()
                        {
                            CategoryID_Lev3 = item.CategoryID_Lev_3,
                            ProductTypeID = itemType
                        };

                        ListDataCate.Add(dataCate);
                    }

                }
                //ViewBag.ProductType =( from pt in ProductCategory
                //                       let pc = getProductType.Where(w=>w.CategoryID_Lev3 ==pt.CategoryID_Lev_3).FirstOrDefault()
                //                      //join pc in ProductCategory on pt.CategoryID_Lev3 equals pc.CategoryID_Lev_3
                //                      select new Tbl_OrgProductCategory
                //                      {
                //                          CategoryID_Lev3= 
                //                      });
                ViewBag.ProductType = ListDataCate;
            }
            else
            {
                ViewBag.ProductType = new List<Tbl_OrgProductCategory>();
            }


            //ViewBag.ListLv1 = ListL1;

            return View(ListCategory);
        }


        #region------------------------------------------------Function Helper------------------------------------------------------

        private string GetHtmlAddProductText(string productName, string productCode, List<ProductTypeAddModel> productType, int languageId)
        {

            var productTypeList = SupplierPortal.Services.RegisterPortalManage.RegisterService.GetProductTypeGroup(languageId);

            string textHtml = "";

            string textHidden = "<input type=\"hidden\" name=\"hidCode[]\" value=\"" + productCode + "\" />";



            string linkDelete = "<input type=\"image\" src=\"/Content/images/icon/ico-sp_deluser2.gif\" alt=\"Submit\" onclick=\"return deleteDataProduct('" + productCode + "');\" data-toggle=\"tooltip\" data-placement=\"left\" title=\"Delete Product\" >";

            string textProductType = "";

            textProductType += "<div id=\"radio\">";

            foreach (var productList in productTypeList)
            {
                string checkedRadio = "";

                if (productType.Exists(x => x.ProductTypeID == productList.EntityID))
                {
                    checkedRadio = "checked=\"checked\"";
                }
                else
                {

                    checkedRadio = "";
                }

                textProductType += " <input type=\"checkbox\" id=\"radio" + productCode + "_" + productList.EntityID + "\" name=\"radio" + productCode + "_" + productList.EntityID + "\" value=\"" + productList.EntityID + "\" " + checkedRadio + "> ";
                textProductType += " <label for=\"radio" + productCode + "_" + productList.EntityID + "\">" + productList.EntityValue + "</label> ";
            }

            textProductType += "</div>";

            textHtml += "<tr id =\"" + productCode + "\" class=\"numbered_row\" >";
            textHtml += "<td class=\"row_number\"></td>";
            textHtml += "<td>" + productName + " " + textHidden + "</td>";
            textHtml += "<td>" + textProductType + "</td>";
            textHtml += "<td align=\"center\">" + linkDelete + "</td>";
            textHtml += "</tr>";

            return textHtml;
        }

        private string GetOldProductType(int supplierID, string productCode)
        {
            var productTypeList = _productTypeRepository.GetProductTypeAll();

            string chkUpdateProduct = "";

            foreach (var productList in productTypeList)
            {
                if (_orgProductRepository.OrgProductExists(supplierID, productCode, productList.ProductTypeID))
                {
                    chkUpdateProduct += "1";
                }
                else
                {
                    chkUpdateProduct += "0";
                }
            }

            return chkUpdateProduct;
        }

        private string GetHtmlAddProductKeywordText(string productKeyword)
        {
            string textHtml = "";

            string encodeProductKeyword = StringHelper.GetMakeCommaToASCIIString(productKeyword.Trim());

            string linkDelete = "<input type=\"image\" src=\"/Content/images/icon/ico-sp_deluser2.gif\"  id=\"deleteKeyword\" data-toggle=\"tooltip\" data-placement=\"left\" title=\"Delete Keyword\" prodKeyword = " + productKeyword.Trim() + " >";

            textHtml += "<tr class=\"main_table_row numbered_row\" >";
            textHtml += "<td align=\"center\" class=\"row_number\"></td>";
            textHtml += "<td align=\"left\">" + productKeyword.Trim() + "</td>";
            textHtml += "<td align=\"center\">" + linkDelete + " <input type=\"hidden\" name=\"hidProductKeyword[]\" value=\"" + encodeProductKeyword.Trim() + "\" /></td>";
            textHtml += "</tr>";

            return textHtml;
        }

        #endregion
    }
}

﻿using SupplierPortal.Data.Models.SupportModel.Profile;
using SupplierPortal.Framework.AccountManage;
using SupplierPortal.Framework.Localization;
using SupplierPortal.Services.AccountManage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SupplierPortal.Controllers
{
    public class ActivatedController : Controller
    {
        // GET: Activated
        public ActionResult ChangePassword()
        {
            
            return View();
        }

        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModels model)
        {

            string strUserName = "";

            if (Session["username"] != null)
            {
                strUserName = Session["username"].ToString();
            }

            RecoveryPassword.ResetPassword(strUserName, model.Password);

            return RedirectToAction("Index", "Home");
        }


    }
}
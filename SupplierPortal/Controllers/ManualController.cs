﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SupplierPortal.Services.ManualManage;
using SupplierPortal.Data.Models.SupportModel.Manual;
using System.IO;
using System.Web.Configuration;


namespace SupplierPortal.Controllers
{
    public class ManualController : ControllerBase
    {
        //Controller

        IDocumentDwnldService _documentDwnldService;

        public ManualController()
        {
            _documentDwnldService = new DocumentDwnldService();
        }

        public ManualController(DocumentDwnldService documentDwnldService)
        {
            _documentDwnldService = documentDwnldService;

        }
        // GET: Manual
        public ActionResult Index()
        {
            var model = _documentDwnldService.DocumentManualList();

            return View(model);
        }

        public ActionResult Download(string filePath)
        {
            string pathAttachment = WebConfigurationManager.AppSettings["pathAttachment"];
            filePath = pathAttachment + filePath;
            if (!System.IO.File.Exists(@filePath))
            {
                filePath = @pathAttachment + @"\SWWDocuments\UserManualForDownload\EPInstruction.pdf";
            }
            byte[] fileBytes = System.IO.File.ReadAllBytes(@filePath);


            string virtualFilePath = @filePath;
            string fileName2 = Path.GetFileName(virtualFilePath);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName2);
        }

        //public ActionResult Download2()
        //{
        //    return new DownloadResult { VirtualPath = "~/content/site.css", FileDownloadName = "TheSiteCss.css" };
        //}

        //public FileResult Download()
        //{
        //    byte[] fileBytes = System.IO.File.ReadAllBytes(@"D:\SupplierPortalDocuments\UserManualForDownload\Manual1.txt");
        //    string fileName = "Manual1.txt";
        //    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        //}


    }
}
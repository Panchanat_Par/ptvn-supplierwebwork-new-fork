﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SupplierPortal.Models
{
    public class ReplaceHtml
    {
        public string ReplaceStringValueInHtmlText(string jsonStr, string htmlText)
        {
            JObject json = JObject.Parse(jsonStr);

            foreach (JToken child in json.Children())
            {
                htmlText = findElementName(child, htmlText);
            }

            return htmlText;
        }

        public string findElementName(JToken child, string htmlText)
        {
            foreach (JToken grandChild in child)
            {
                if (grandChild.HasValues)
                {
                    htmlText = findElementName(grandChild, htmlText);
                }
                else
                {
                    var property = child as JProperty;

                    if (property != null)
                    {
                        var name = property.Name;
                        name = "<%" + name + "%>";
                        var value = property.Value.ToString();
                        htmlText = htmlText.Replace(name, value);
                    }
                }
            }
            return htmlText;
        }
    }
}
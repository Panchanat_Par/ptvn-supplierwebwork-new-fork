﻿using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.User;
using SupplierPortal.Hubs;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Web;

namespace SupplierPortal.Models
{
    public class Notification
    {
        private SupplierPortalEntities context;
        private static Byte[] LastTimeStamp { get; set; }

        public Notification()
        {
            context = new SupplierPortalEntities();
        }

        public Notification(
            SupplierPortalEntities context
            )
        {
            this.context = context;
        }

        public IEnumerable<Tbl_Notification> GetAllMessages(int userID, DateTime notificationDate, int topicType = 0, int skip = 0)
        {
            var dataNotificationNewFeed = (
                from x in context.Tbl_Notification
                where x.UserID == userID
                        && x.isDeleted == 0
                        && x.isRead == 0
                        && x.StartTime < notificationDate
                        && x.StopTime > notificationDate
                        && (topicType == 0 || x.TopicID == topicType)
                select x
                ).OrderByDescending(o => o.StartTime).Skip(skip).Take(10)
                .ToList();


            var dataNotificationAllFeed = (
                from x in context.Tbl_Notification
                where x.UserID == userID
                      && x.isDeleted == 0
                      && x.isRead == 1
                      && x.StartTime < notificationDate
                      && x.StopTime > notificationDate
                      && (topicType == 0 || x.TopicID == topicType)
                select x
                ).OrderByDescending(o => o.StartTime).Skip(skip).Take(10)
                .ToList();

            var dataNotification = dataNotificationNewFeed.Union(dataNotificationAllFeed);

            return dataNotification.ToList();

        }

        public string GetCountNewFeed(int userID, DateTime notificationDate, int topicType = 0)
        {
            var countNew = (from x in context.Tbl_Notification
                            where x.UserID == userID
                                    && x.isDeleted == 0
                                    && x.isRead == 0
                                    && x.StartTime < notificationDate
                                    && x.StopTime > notificationDate
                                    && (topicType == 0 || x.TopicID == topicType)
                            select x).Count();
            return countNew.ToString();

        }
        public string GetCountAllFeed(int userID, DateTime notificationDate, int topicType = 0)
        {
            var countAll = (from x in context.Tbl_Notification
                            where x.UserID == userID
                                    && x.isDeleted == 0
                                    && x.StartTime < notificationDate
                                    && x.StopTime > notificationDate
                                    && (topicType == 0 || x.TopicID == topicType)
                            select x).Count();
            return countAll.ToString();

        }

        public static void RegisterNotification()
        {


            using (var context = new SupplierPortalEntities())
            {
                ConnectionStringSettings dependencyConnection = ConfigurationManager.ConnectionStrings["DependencyEntities"];

                SqlDependency.Start(dependencyConnection.ConnectionString);

                SqlDependency dependency = new SqlDependency();
                dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                CallContext.SetData("MS.SqlDependencyCookie", dependency.Id);

                var query = context.Tbl_NotificationCheck.OrderBy(o => o.ModifiedTimeStamp).AsQueryable().ToString();
                var datas = context.Database.SqlQuery<Tbl_NotificationCheck>(query).LastOrDefault();

                if (datas != null)
                {
                    LastTimeStamp = datas.ModifiedTimeStamp;
                }
                else
                {
                    LastTimeStamp = new byte[0];
                }

            }


        }

        private static void dependency_OnChange(object sender, SqlNotificationEventArgs e)
        {
            if (e.Type == SqlNotificationType.Change)
            {
                var ctx = new SupplierPortalEntities();
                var query = "SELECT * FROM Tbl_Notification WHERE ModifiedTimeStamp > @LastTimeStamp";
                var param = new SqlParameter("LastTimeStamp", LastTimeStamp);
                var changedList = ctx.Database.SqlQuery<Tbl_Notification>(query, param).ToList();
                if (changedList.Count > 0)
                {
                    MessagesHub.SendMessages(changedList);
                }
            }
            RegisterNotification();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using SupplierPortal.Data.Models.MappingTable;
using System.IO;
using System.Web.Helpers;
using System.Security.Claims;

namespace SupplierPortal
{
    public class MvcApplication : System.Web.HttpApplication
    {

        protected void Application_Start()
        {
            //Database.SetInitializer<SupplierPortal3Entities>(null);
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            ViewEngines.Engines.Clear();
            IViewEngine razorEngine = new RazorViewEngine() { FileExtensions = new string[] { "cshtml" } };
            ViewEngines.Engines.Add(razorEngine);

            log4net.Config.XmlConfigurator.Configure(new FileInfo(Server.MapPath("~/Web.config")));

            SupplierPortal.Models.Notification.RegisterNotification();
        }
    }
}

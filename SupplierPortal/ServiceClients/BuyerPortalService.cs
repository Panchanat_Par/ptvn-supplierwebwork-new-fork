﻿using Newtonsoft.Json.Linq;
using SupplierPortal.Data.CustomModels.AccountPortal;
using SupplierPortal.Data.CustomModels.BuyerSuppliers;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.Profile;
using SupplierPortal.Models.Dto.DataContract;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Script.Serialization;

namespace SupplierPortal.ServiceClients
{
    public class BuyerPortalService : APIConnectionService
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private string baseURL;
        private string urlInsertSupplierUser;
        private string urlGetBuyerRolePrivileges;
        private string urlGetEPBuyerRolePrivileges;
        public BuyerPortalService()
        {
            this.baseURL = WebConfigurationManager.AppSettings["baseURL"];
            this.urlInsertSupplierUser = WebConfigurationManager.AppSettings["insert_supplier_user"];
            this.urlGetBuyerRolePrivileges = WebConfigurationManager.AppSettings["get_buyer_role_privileges"];
            this.urlGetEPBuyerRolePrivileges = WebConfigurationManager.AppSettings["get_epbuyer_role_privileges"];
        }


        #region INSERT
        public async Task<CreateResponse> InsertSupplierUser(SupplierUserRequest request)
        {
            CreateResponse response = null;
            HttpResponseMessage postTask = null;
            try
            {
                string url = string.Format("{0}{1}", baseURL, urlInsertSupplierUser);

                postTask = await this.PostApiConnection(url, request);

                if (postTask.IsSuccessStatusCode)
                {
                    Stream responseString = postTask.Content.ReadAsStreamAsync().Result;
                    StreamReader r = new StreamReader(responseString);
                    string jsonResult = r.ReadToEnd();
                    JavaScriptSerializer JsonConvert = new JavaScriptSerializer();
                    CreateResponse respond = JsonConvert.Deserialize<CreateResponse>(jsonResult);
                    return respond;
                }
                else
                {
                    response = new CreateResponse()
                    {
                        Success = false,
                        Message = "System Error"
                    };
                }

            }
            catch (Exception ex)
            {
                response = new CreateResponse()
                {
                    Success = false,
                    Message = ex.InnerException != null ? ex.InnerException.Message : ex.Message
                };
            }
            finally
            {
                postTask.Dispose();
            }

            return response;
        }
        #endregion

        #region UPDATE
        public async Task<Boolean> UpdateBuyerContactPerson(BuyerSupplierProfile model, string strSysUserID)
        {
            bool result = false;
            HttpResponseMessage httpResponse = null;
            BuyerSuppliersModel buyerSupplier = null;
            try
            {
                buyerSupplier = new BuyerSuppliersModel()
                {
                    FirstName_Local = model.FirstName_Local,
                    LastName_Local = model.LastName_Local,
                    FirstName_Inter = model.FirstName_Inter,
                    LastName_Inter = model.LastName_Inter,
                    TitleID = model.TitleID,
                    PhoneNo = model.PhoneNo,
                    PhoneExt = model.PhoneExt,
                    Email = model.Email,
                    UserID = Convert.ToInt32(model.UserID),
                    Username = model.UserLoginID,
                    CurrentID = Convert.ToInt32(strSysUserID),
                    ContactID = Convert.ToInt32(model.ContactID),
                    isPrimaryContact = model.IsPrimaryContact ? 1 : 0,
                    MobileCountryCode = model.MobileCountryCode,
                    MobileNo = model.MobileNo,
                    SupplierID = model.SupplierID
                };

                string url = "api/buyer/UpdateBuyer";
                httpResponse = await this.PostApiConnection(url, buyerSupplier);
                if (httpResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                logger.Error("UpdateBuyerContactPerson Service(WebSite) : " + ex);
            }
            finally
            {
                httpResponse.Dispose();
            }
            return result;
        }
        #endregion

        #region DELETE

        #endregion

        #region GET
        public async Task<ResultValidated> LoginValidate(string strUserName, string userPassword, string languageId = "", string browserType = "", string clientIPAddress = "")
        {
            HttpResponseMessage httpResponse = null;
            ResultValidated resultValidate = null;
            try
            {
                string languageURL = "?languageId=" + languageId;
                string browserTypeURL = "&browserType=" + browserType;
                string clientIPAddressURL = "&clientIPAddress=" + clientIPAddress;

                string urlAll = string.Format("api/BuyerAccountPortal/LoginValidate{0}{1}{2}", languageId, browserType, clientIPAddress);
                httpResponse = new HttpResponseMessage();
                httpResponse = await this.GetApiConnection(urlAll, strUserName, userPassword);

                if (httpResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var responseBody = httpResponse.Content.ReadAsStringAsync().Result;
                    JObject _response = JObject.Parse(responseBody);

                    if (Convert.ToBoolean(_response["result"].SelectToken("SuccessStatus")))
                    {
                        var query = from c in _response["data"].Children()
                                    select c;
                        query = query.ToList();

                        //result
                        var responseResult = (from x in query
                                              select new
                                              {
                                                  respUsername = (string)x.SelectToken("Username"),
                                                  respSysUserID = (string)x.SelectToken("SysUserID"),
                                                  respSessionID = (string)x.SelectToken("GUID"),
                                                  respSessionEID = (string)x.SelectToken("EID"),
                                                  respResult = (bool)x.SelectToken("Result"),
                                                  respMessage = (string)x.SelectToken("Message"),
                                                  respUser = (string)x.SelectToken("User"),
                                                  respOrganization = (string)x.SelectToken("Organization"),
                                                  respLanguageId = (string)x.SelectToken("LanguageId"),
                                                  respisAcceptTermOfUse = (string)x.SelectToken("isAcceptTermOfUse")
                                              }).ToList();

                        var respResult = responseResult.Select(x => x.respResult).FirstOrDefault();

                        resultValidate = new ResultValidated()
                        {
                            Username = responseResult.Select(x => x.respUsername).FirstOrDefault(),
                            SysUserID = responseResult.Select(x => x.respSysUserID).FirstOrDefault(),
                            GUID = responseResult.Select(x => x.respSessionID).FirstOrDefault(),
                            EID = responseResult.Select(x => x.respSessionEID).FirstOrDefault(),
                            User = responseResult.Select(x => x.respUser).FirstOrDefault(),
                            Organization = responseResult.Select(x => x.respOrganization).FirstOrDefault(),
                            isAcceptTermOfUse = responseResult.Select(x => x.respisAcceptTermOfUse).FirstOrDefault(),
                            LanguageId = responseResult.Select(x => x.respLanguageId).FirstOrDefault(),
                            Message = responseResult.Select(x => x.respMessage).FirstOrDefault(),
                            Result = respResult
                        };
                    }
                    else
                    {
                        resultValidate = new ResultValidated()
                        {
                            Username = string.Empty,
                            SysUserID = string.Empty,
                            GUID = string.Empty,
                            User = string.Empty,
                            Organization = string.Empty,
                            isAcceptTermOfUse = string.Empty,
                            LanguageId = string.Empty,
                            Message = _response["result"].SelectToken("Message").ToString(),
                            Result = Convert.ToBoolean(_response["result"].SelectToken("SuccessStatus"))
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("LoginValidate Service(WebSite) : " + ex);
            }
            finally
            {
                httpResponse.Dispose();
            }
            return resultValidate;
        }

        public async Task<ViewBuyerSupplier> GetBuyerSupplierContactDetail(int userId)
        {
            HttpResponseMessage httpResponse = null;
            ViewBuyerSupplier result = null;
            try
            {
                string url = "api/buyer/Detail";
                httpResponse = await this.PostApiConnection(url, userId);

                if (httpResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    Stream responseString = httpResponse.Content.ReadAsStreamAsync().Result;
                    StreamReader r = new StreamReader(responseString);
                    string jsonResult = r.ReadToEnd();
                    JavaScriptSerializer JsonConvert = new JavaScriptSerializer();
                    result = JsonConvert.Deserialize<ViewBuyerSupplier>(jsonResult);
                }
            }
            catch (Exception ex)
            {
                logger.Error("GetBuyerSupplierContactDetail Service(WebSite) : " + ex);
            }
            finally
            {
                httpResponse.Dispose();
            }
            return result;
        }

        public async Task<List<BuyerHistoryContactPersonModel>> GetListHistoryBuyerContactPersonByUserId(int userID)
        {
            List<BuyerHistoryContactPersonModel> result = null;
            HttpResponseMessage httpResponse = null;

            try
            {
                string url = "api/buyer/ListHistory";
                httpResponse = await this.PostApiConnection(url, userID);
                if (httpResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    Stream responseString = httpResponse.Content.ReadAsStreamAsync().Result;
                    StreamReader r = new StreamReader(responseString);
                    string jsonResult = r.ReadToEnd();
                    JavaScriptSerializer JsonConvert = new JavaScriptSerializer();
                    result = JsonConvert.Deserialize<List<BuyerHistoryContactPersonModel>>(jsonResult);
                }
            }
            catch (Exception ex)
            {
                logger.Error("GetListHistoryBuyerContactPersonByUserId Service(WebSite) : " + ex);
            }
            finally
            {
                httpResponse.Dispose();
            }
            return result;
        }

        public async Task<List<Tbl_BuyerRolePrivilege>> GetBuyerRolePrivileges(int sysUserId)
        {
            List<Tbl_BuyerRolePrivilege> response = null;
            HttpResponseMessage postTask = null;
            JavaScriptSerializer JsonConvert = new JavaScriptSerializer();

            try
            {
                string url = string.Format("{0}{1}?sysuserid={2}", baseURL, urlGetBuyerRolePrivileges, sysUserId);
                postTask = await this.GetApiConnection(url);


                if (postTask.IsSuccessStatusCode)
                {
                    Stream responseString = postTask.Content.ReadAsStreamAsync().Result;
                    StreamReader r = new StreamReader(responseString);
                    string jsonResult = r.ReadToEnd();
                    response = JsonConvert.Deserialize<List<Tbl_BuyerRolePrivilege>>(jsonResult);
                    return response;
                }
                else
                {
                    var ErrMsg = JsonConvert.Deserialize<dynamic>(postTask.Content.ReadAsStringAsync().Result);
                    logger.Error("GetBuyerRolePrivileges Service(WebSite) :" + postTask.StatusCode.ToString() + " : " + ErrMsg.Message);
                }


            }
            catch (Exception ex)
            {
                logger.Error("GetBuyerRolePrivileges Service(WebSite) : " + ex);
            }
            finally
            {
                postTask.Dispose();
            }
            return response;

        }

        public async Task<List<Tbl_BuyerRolePrivilege>> GetBuyerRolePrivilgeByEPUserId(string sessionUserId, int epUserId, int eid)
        {
            List<Tbl_BuyerRolePrivilege> response = null;
            HttpResponseMessage postTask = null;
            JavaScriptSerializer JsonConvert = new JavaScriptSerializer();

            try
            {
                string url = string.Format("{0}{1}?sessionUserId={2}&epUserId={3}&eid={4}", baseURL, urlGetEPBuyerRolePrivileges, sessionUserId, epUserId, eid);
                postTask = await this.GetApiConnection(url);


                if (postTask.IsSuccessStatusCode)
                {
                    Stream responseString = postTask.Content.ReadAsStreamAsync().Result;
                    StreamReader r = new StreamReader(responseString);
                    string jsonResult = r.ReadToEnd();
                    response = JsonConvert.Deserialize<List<Tbl_BuyerRolePrivilege>>(jsonResult);
                    return response;
                }
                else
                {
                    var ErrMsg = JsonConvert.Deserialize<dynamic>(postTask.Content.ReadAsStringAsync().Result);
                    logger.Error("GetBuyerRolePrivilgeByEPUserId Service(WebSite) :" + postTask.StatusCode.ToString() + " : " + ErrMsg.Message);
                }


            }
            catch (Exception ex)
            {
                logger.Error("GetBuyerRolePrivilgeByEPUserId Service(WebSite) : " + ex);
            }
            finally
            {
                postTask.Dispose();
            }
            return response;

        }

        public async Task<Tbl_BuyerConfig> GetBuyerConfigByEID(int eid)
        {
            Tbl_BuyerConfig response = null;
            HttpResponseMessage httpResponse = null;
            JavaScriptSerializer JsonConvert = new JavaScriptSerializer();

            try
            {

                string url = string.Format("{0}{1}?eid={2}", baseURL, "api/buyer/Epconfig", eid);
                httpResponse = await this.GetApiConnection(url);

                if (httpResponse.IsSuccessStatusCode)
                {
                    Stream responseString = httpResponse.Content.ReadAsStreamAsync().Result;
                    StreamReader r = new StreamReader(responseString);
                    string jsonResult = r.ReadToEnd();
                    response = JsonConvert.Deserialize<Tbl_BuyerConfig>(jsonResult);
                    return response;
                }
                else
                {
                    var ErrMsg = JsonConvert.Deserialize<dynamic>(httpResponse.Content.ReadAsStringAsync().Result);
                    logger.Error("GetBuyerUserWithConfigBySysUserID Service(WebSite) :" + httpResponse.StatusCode.ToString() + " : " + ErrMsg.Message);
                }
            }
            catch (Exception ex)
            {
                logger.Error("GetBuyerUserWithConfigBySysUserID Service(WebSite) : " + ex);
            }
            finally
            {
                httpResponse.Dispose();
            }
            return response;

        }
        #endregion


    }
}
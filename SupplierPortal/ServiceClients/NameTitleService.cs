﻿using Newtonsoft.Json.Linq;
using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;

namespace SupplierPortal.ServiceClients
{
    public class NameTitleService : APIConnectionService
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public async Task<List<Tbl_NameTitle>> GetNameTitleList(int languageID)
        {
            HttpResponseMessage httpResponse = new HttpResponseMessage();
            List<Tbl_NameTitle> result = null;

            try
            {
                string parameter = string.Format("?languageID={0}", languageID);
                string url = string.Format("api/NameTitle/GetNameTitleList{0}", parameter);
                httpResponse = await this.GetApiConnection(url);

                if (httpResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    Stream responseString = httpResponse.Content.ReadAsStreamAsync().Result;
                    StreamReader r = new StreamReader(responseString);
                    string jsonResult = r.ReadToEnd();
                    JavaScriptSerializer JsonConvert = new JavaScriptSerializer();
                    result = JsonConvert.Deserialize<List<Tbl_NameTitle>>(jsonResult);
                }
            }
            catch (Exception ex)
            {
                logger.Error("GetNameTitleList Service(WebSite) : " + ex);
            }
            finally
            {
                httpResponse.Dispose();
            }
            return result;
        }
    }
}
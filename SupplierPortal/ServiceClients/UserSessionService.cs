﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace SupplierPortal.ServiceClients
{
    public class UserSessionService : APIConnectionService
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public async Task<Tbl_UserSession> GetUserSession(string userGuid)
        {
            HttpResponseMessage httpResponse = new HttpResponseMessage();
            Tbl_UserSession userSession = null;
            try
            {
                string parameter = string.Format("?userGuid={0}", userGuid);
                string url = string.Format("api/UserSession/GetUserSession{0}", parameter);
                httpResponse = await this.GetApiConnection(url);

                if (httpResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var responseBody = httpResponse.Content.ReadAsStringAsync().Result;
                    JObject _response = JObject.Parse(responseBody);
                    DateTime? lastAccessed = null;
                    DateTime? loginTime = null;
                    DateTime? logoutTime = null;

                    if (!String.IsNullOrEmpty(_response["LastAccessed"].ToString()))
                        lastAccessed = DateTime.Parse(_response["LastAccessed"].ToString());

                    if (!String.IsNullOrEmpty(_response["LoginTime"].ToString()))
                        loginTime = DateTime.Parse(_response["LoginTime"].ToString());

                    if (!String.IsNullOrEmpty(_response["LogoutTime"].ToString()))
                        logoutTime = DateTime.Parse(_response["LogoutTime"].ToString());

                    userSession = new Tbl_UserSession()
                    {
                        BrowserType = _response["BrowserType"].ToString(),
                        IPAddress = _response["IPAddress"].ToString(),
                        isTimeout = int.Parse(_response["isTimeout"].ToString()),
                        SessionData = _response["SessionData"].ToString(),
                        UserGuid = _response["UserGuid"].ToString(),
                        Username = _response["Username"].ToString(),
                        LastAccessed = lastAccessed,
                        LoginTime = loginTime,
                        LogoutTime = logoutTime
                    };
                }
            }
            catch (Exception ex)
            {
                logger.Error("GetUserSession Service(WebSite) : " + ex);
            }
            finally
            {
                httpResponse.Dispose();
            }
            return userSession;
        }

        public async Task<Boolean> Update(Tbl_UserSession userSession)
        {
            bool result = false;
            HttpResponseMessage httpResponse = new HttpResponseMessage();
            try
            {
                string url = "api/UserSession/Update";
                httpResponse = await this.PostApiConnection(url, userSession);
                if (httpResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                httpResponse.Dispose();
            }
            return result;
        }
    }
}
﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace SupplierPortal.ServiceClients
{
    public class UserPDPAService : APIConnectionService
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public async Task<Boolean> InsertUserPDPA(Users_PDPA users)
        {
            bool result = false;
            HttpResponseMessage httpResponse = new HttpResponseMessage();
            try
            {
                string url = "api/pdpa/insert";
                httpResponse = await this.PostApiConnection(url, users);
                if (httpResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                httpResponse.Dispose();
            }
            return result;
        }

        public async Task<Boolean> GetUserPDPA(string username)
        {
            bool result = false;
            HttpResponseMessage httpResponse = new HttpResponseMessage();
            try
            {
                string url = "api/pdpa/get";
                httpResponse = await this.PostApiConnection(url, username);
                if (httpResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                httpResponse.Dispose();
            }
            return result;
        }
    }
}
﻿function CheckBranchNoDuplicate() {
    var companyType = $('input[name=CompanyTypeID]:checked').val();
    var countryCode = $("#dd-country option:selected").val();

    //console.log($("#BranchNo").val());
    if ($("#BranchNo").val() == "" || $("#BranchNo").val() == undefined) {
        $('input:radio[class=BranchTypeDuplicate][id=BranchTypeDuplicate0]').prop('checked', true);
    } else if ($("#BranchNo").val() == "00000") {
        $('input:radio[class=BranchTypeDuplicate][id=BranchTypeDuplicate1]').prop('checked', true);
    } else {
        $('input:radio[class=BranchTypeDuplicate][id=BranchTypeDuplicate2]').prop('checked', true);
    }
}

function OnSuccessCheckTaxID() {
    $("#RegCompanyAddress_HouseNo_valid").css("display", "none");
    $("#RegCompanyAddress_PostalCode_valid").css("display", "none");
    //$("#mainRegisterForm").valid();
    $(".done").click();

    var countryName = $("#dd-country option:selected").text();

    var countryCode = $("#dd-country option:selected").val();

    $("#RegCompanyAddress_CountryName").val(countryName);
    $("#RegCompanyAddress_CountryCode").val(countryCode);

    if (countryCode == "TH") {
        $("span#span-SubDistrict").html("*");
    } else {
        $("span#span-SubDistrict").html("");
        $("#errorCompSubDistrict").css("display", "none");
    }
}

//function OnEditTaxID() {
//    $("#divlnkCheck").css("display", "block");
//    $("#divlnkEdit").css("display", "none");

//    //CompanyTypeID.SetEnabled(true);
//    //BusinessEntityID.SetEnabled(true);
//    $("#txtOtherBusEnt").attr('readonly', false);
//    $("#CompanyName_Local").attr('readonly', false);
//    $("#CompanyName_Inter").attr('readonly', false);
//    //CountryCode.SetEnabled(true);
//    $("#TaxID").attr('readonly', false);

//    //UpdateItemCompany();

//    //BranchType.SetEnabled(true);

//    var companyType = $('input[name=CompanyTypeID]:checked').val();
//    var countryCode = $("#dd-country option:selected").val();

//    if (companyType == 1) {
//        //BusinessEntityID.SetValue(4);
//        if (countryCode == "TH") {
//            BranchTypeNotSpeci.SetEnabled(false);
//        } else {
//            BranchTypeNotSpeci.SetEnabled(true);
//        }

//    } else if (companyType == 2) {
//        BranchTypeNotSpeci.SetEnabled(true);
//    }

//    //BranchType.SetEnabled(true);
//    //BranchTypeNotSpeci.SetEnabled(true);

//    var branchType = BranchType.GetSelectedIndex();
//    if (branchType == 1) {
//        $("#BranchNo").attr('readonly', false);
//    } else {
//        $("#BranchNo").attr('readonly', true);
//    }

//    $('.divCheckVisible').css("display", "none");

//}

function AssignTelCountryCode(telCountryCode) {
    $("#PhoneNoCountryCode").val(telCountryCode);
    $("#MobileNoCountryCode").val(telCountryCode);
}

function ContactJobTitleChanged(jobTitleID) {
    if (jobTitleID == -1) {
        $("#ContactOtherJobTitleShow").css("display", "block");
    } else {
        $("#ContactOtherJobTitleShow").css("display", "none");
    }
}

function ModalMessageAlert() {
    $('#ModalMessageAlert').modal({ backdrop: 'static', keyboard: false });

    $("#ModalMessageAlert").on("hidden.bs.modal", function () {    // remove the event listeners when the dialog is dismissed
        RequestContact();
    });
}

function loadMessageSuccessAlert() {
    $('#ModalSendMessageSuccess').modal({ backdrop: 'static', keyboard: false });
}

function ModalConfirmAlert() {
    $('#ModalConfirmMessage').modal({ backdrop: 'static', keyboard: false });
}

function RequestContact() {
    $('#AddContactRequest').modal({ backdrop: 'static', keyboard: false });
}

function CheckDuplicateOrgContactPerson() {
    var check = false;

    var contactJobTitle = $("#ContactJobTitle option:selected").val();

    var contactOtherJobTitle = $("#ContactOtherJobTitle").val();
    if (contactJobTitle == -1) {
        if (contactOtherJobTitle == "" || contactOtherJobTitle == undefined) {
            $("#ContactOtherJobTitle").focus();
            $("#errorContactOtherJobTitle").css("display", "block");
            return check;
        } else {
            $("#errorContactOtherJobTitle").css("display", "none");
        }
    }
    //console.log("CheckDuplicateOrgContactPerson");
    // var indicesToSelect = ServiceTypeContact.GetSelectedIndices();
    var indicesToSelect = $('input[type=checkbox][name=ServiceTypeContact]:checked').length;
    //console.log(indicesToSelect);

    if (indicesToSelect <= 0) {
        $("#errorServiceContact").css("display", "block");
        return check;
    } else {
        $("#errorServiceContact").css("display", "none");
    }

    $("#errorContactInvitationCode").css("display", "none");

    $.ajax({
        url: '/Register/CheckDuplicateOrgContactPerson',
        dataType: "json",
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify({
            model: {
                hidSupplierID: $("#hidSupplierID").val(),
                FirstName_Contact: $("#FirstName_Contact").val(),
                LastName_Contact: $("#LastName_Contact").val(),
                hidService1: $("#hidService1").val(),
                hidService2: $("#hidService2").val(),
                hidInvitationCode: $.trim($("#ContactInvitationCode").val())
            }
        }),
        async: false,
        processData: false,
        cache: false,
        success: function (data) {
            //console.log(data);
            if (data.Status == true) {
                check = true;
                //$("#errorMessage").css("display", "none");
            } else {
                check = false;

                if (data.DuplicateType == "0") { //0 = Message only
                    $('#AddContactRequest').modal('hide');
                    $("#messageAlert").text("");
                    $("#messageAlert").text(data.Message);
                    ModalMessageAlert();//Funtion from customheadscript
                } else if (data.DuplicateType == "4") // 4 = Show Alert InvitationCodeNotFound  InvitationCode ไม่ถูกต้อง
                {
                    $("#errorContactInvitationCode").css("display", "block");
                    $("#ContactInvitationCode").focus();
                    return check;
                } else {
                    $('#AddContactRequest').modal('hide');
                    $("#confirmMessageAlert").text("");
                    $("#confirmMessageAlert").text(data.Message);
                    $("#hidEmailReturn").val(data.emailReturn);
                    $("#hidDuplicateType").val(data.DuplicateType);
                    $("#hidUsernameDuplicate").val(data.UsernameDuplicate);
                    ModalConfirmAlert();//Funtion from customheadscript
                }
            }
        },
        error: function (xhr) {
            //check = false;
        }
    });

    return check;
}

function keyThaiOnly(evt, str) {
    createAutoClosingAlert(".alert-message", 500);
    var countryCode = $("#dd-country option:selected").val();
    if (countryCode == "TH") {
        var k;
        if (window.event) k = window.event.keyCode; //  IE
        else if (evt) k = evt.which; //  Firefox
        //console.log(k);
        if (evt.ctrlKey) {
            return true;
        }
        if ((k >= 161 && k <= 255) || (k >= 3585 && k <= 3675)) {
            // THAI
            return true;
        } else {
            // Number
            if (k >= 48 && k <= 57) {
                return true;
            } else {
                if (k != 8 && k != 46 && k != 188
                    && k != 190 && k != 40 && k != 41
                    && k != 47 && k != 42 && k != 43 && k != 44
                    && k != 45 && k != 32) {
                    showAlertLocal();
                    return false;
                }
            }
        }
        // if ((k>=65 && k<=90) || (k>=97 && k<=122)) { return allowedEng; }
    }
}

function keyEngOnly(evt, str) {
    var k;
    if (window.event) k = window.event.keyCode; //  IE
    else if (evt) k = evt.which; //  Firefox
    if (evt.ctrlKey) {
        return true;
    }
    // English
    if ((k >= 65 && k <= 90) || (k >= 97 && k <= 122)) {
        return true;
    } else {
        // Number
        if (k >= 48 && k <= 57) {
            return true;
        } else {
            if (k != 8 && k != 46 && k != 188
                && k != 190 && k != 40 && k != 41
                && k != 47 && k != 42 && k != 43 && k != 44
                && k != 45 && k != 32 && k != 38) {
                showAlertInter();
                return false;
            }
        }
    }
}

function keyNumberOnly(evt, str) {
    //console.log(str);

    var k;
    if (window.event) k = window.event.keyCode; //  IE
    else if (evt) {
        k = evt.which; //  Firefox
        if (k == 8) {
            return true;
        }
    }
    // Number
    if (evt.ctrlKey) {
        return true;
    }

    if (str.length >= 10) {
        return false;
    }

    if (k >= 48 && k <= 57) {
        return true;
    } else {
        showAlertNumber();
        return false;
    }
}

function ValidatRequire(evt) {
    var check = true;

    var countryCode = $("#dd-country option:selected").val();

    var branchType = $('input[name=BranchType]:checked').val();
    //console.log(branchType);

    var textBranchNo = $("#BranchNo").val();
    var textTaxID = $("#TaxID").val();
    var textCompanyName_Local = $("#CompanyName_Local").val();
    var textOtherBusiness = $("#txtOtherBusEnt").val();

    var businessEntityID = $("#BusinessEntityID option:selected").val();

    var textCompanyName_Inter = $("#CompanyName_Inter").val();

    //$("#TaxID").keyup();
    //var elementTaxID = $("#TaxID").hasClass("input-validation-error");
    //console.log(elementTaxID);

    if (branchType == "2") {
        if (textBranchNo == "" || textBranchNo == undefined) {
            evt.preventDefault();
            $("#errorBranchNo").css("display", "block");
            $("#BranchNo").focus();
            check = false;
        } else {
            $("#errorBranchNo").css("display", "none");
        }
    } else {
        $("#errorBranchNo").css("display", "none");
    }

    if (textCompanyName_Inter == "" || textCompanyName_Inter == undefined) {
        evt.preventDefault();
        $("#errorCompanyName_Inter").css("display", "block");
        $("#CompanyName_Inter").focus();
        check = false;
    } else {
        $("#errorCompanyName_Inter").css("display", "none");
    }

    if (textCompanyName_Local == "" || textCompanyName_Local == undefined) {
        evt.preventDefault();
        $("#errorCompanyName_Local").css("display", "block");
        $("#CompanyName_Local").focus();
        check = false;
    } else {
        $("#errorCompanyName_Local").css("display", "none");
    }

    if (businessEntityID == -1) {
        if (textOtherBusiness == "" || textOtherBusiness == undefined) {
            evt.preventDefault();
            $("#errorOtherBusiness").css("display", "block");
            $("#txtOtherBusEnt").focus();
            check = false;
        } else {
            $("#errorOtherBusiness").css("display", "none");
        }
    }

    return check;
}

function ValidatTaxIDAsync(evt) {
    var check = true;

    var countryCode = $("#dd-country option:selected").val();
    var companyTypeID = $('input[name=CompanyTypeID]:checked').val();

    var textTaxID = $("#TaxID").val();

    if (textTaxID == "" || textTaxID == undefined) {
        //console.log("textTaxID undefined");
        $("#errorFormatTaxIDCorp").css("display", "none");
        $("#errorFormatTaxIDOrdinary").css("display", "none");

        evt.preventDefault();

        if (companyTypeID == 1) {
            $("#errorTaxIDCorp").css("display", "block");
            $("#errorTaxIDOrdinary").css("display", "none");
        } else if (companyTypeID == 2) {
            if (countryCode == "TH") {
                $("#errorTaxIDOrdinary").css("display", "block");
                $("#errorTaxIDCorp").css("display", "none");
            } else {
                $("#errorTaxIDCorp").css("display", "block");
                $("#errorTaxIDOrdinary").css("display", "none");
            }
        }
        $("#TaxID").focus();
        check = false;
    } else {
        $("#errorTaxIDCorp").css("display", "none");
        $("#errorTaxIDOrdinary").css("display", "none");
    }
    if (check == true) {
        $.ajax({
            url: '/Register/ValidateTaxID',
            type: 'GET',
            data: {
                TaxID: textTaxID,
                CountryCode_VI: countryCode,
                CompanyTypeID: companyTypeID
            },
            async: false,
            contentType: 'application/json',
            success: function (data) {
                //console.log(data);
                if (data != true) {
                    evt.preventDefault();
                    if (companyTypeID == 1) {
                        $("#errorFormatTaxIDCorp").css("display", "block");
                        $("#errorFormatTaxIDOrdinary").css("display", "none");
                        $("#errorTaxIDOrdinary").css("display", "none");
                        $("#errorTaxIDCorp").css("display", "none");
                    } else if (companyTypeID == 2) {
                        if (countryCode == "TH") {
                            $("#errorFormatTaxIDOrdinary").css("display", "block");
                            $("#errorFormatTaxIDCorp").css("display", "none");
                            $("#errorTaxIDOrdinary").css("display", "none");
                            $("#errorTaxIDCorp").css("display", "none");
                        } else {
                            $("#errorFormatTaxIDCorp").css("display", "block");
                            $("#errorFormatTaxIDOrdinary").css("display", "none");
                            $("#errorTaxIDOrdinary").css("display", "none");
                            $("#errorTaxIDCorp").css("display", "none");
                        }
                    }
                    $("#TaxID").focus();
                    check = false;
                } else {
                    $("#errorFormatTaxIDCorp").css("display", "none");
                    $("#errorFormatTaxIDOrdinary").css("display", "none");
                }
            },
            fail: function (event, data) {
                check = false;
            }
        });
    }

    return check;
}

function ValidatThaiOnlyRegular(evt, input, countryCode) {
    var check = true;
    if (countryCode == "TH") {
        $.ajax({
            url: '/ValidateAjax/CheckThaiOnly',
            type: 'GET',
            data: {
                input: input
            },
            async: false,
            contentType: 'application/json',
            success: function (data) {
                //console.log("CheckThaiOnly ==> " + data);
                if (data != true) {
                    evt.preventDefault();
                    $("#errorFormatCompanyName_Local").css("display", "block");
                    $("#CompanyName_Local").focus();
                    check = false;
                } else {
                    $("#errorFormatCompanyName_Local").css("display", "none");
                }
            },
            fail: function (event, data) {
                check = false;
            }
        });
    }
    return check;
}

function ValidatEngOnlyRegular(evt, input, countryCode) {
    var check = true;
    if (countryCode == "TH") {
        $.ajax({
            url: '/ValidateAjax/CheckEngOnly',
            type: 'GET',
            data: {
                input: input
            },
            async: false,
            contentType: 'application/json',
            success: function (data) {
                //console.log("CheckEngOnly ==> " + data);
                if (data != true) {
                    evt.preventDefault();
                    $("#errorFormatCompanyName_Inter").css("display", "block");
                    $("#CompanyName_Inter").focus();
                    check = false;
                } else {
                    $("#errorFormatCompanyName_Inter").css("display", "none");
                }
            },
            fail: function (event, data) {
                check = false;
            }
        });
    }
    return check;
}

function createAutoClosingAlert(selector, delay) {
    //console.log(delay);
    var alert = $(selector).alert();
    window.setTimeout(function () { alert.alert('close'); }, delay);
}

function showAlertLocal() {
    $("#myAlertLocal").css('z-index', '2000');
    $("#myAlertLocal").fadeTo(3000, 500).slideUp(500, function () {
        $("#myAlertLocal").css('z-index', '-1');
    });
}

function showAlertInter() {
    $("#myAlertInter").css('z-index', '2000');
    $("#myAlertInter").fadeTo(3000, 500).slideUp(500, function () {
        $("#myAlertInter").css('z-index', '-1');
    });
}

function showAlertNumber() {
    $("#myAlertNumber").css('z-index', '2000');
    $("#myAlertNumber").fadeTo(3000, 500).slideUp(500, function () {
        $("#myAlertNumber").css('z-index', '-1');
    });
}

function showAlertCompanyRegis() {
    $("#myAlertCompanyRegis").css('z-index', '2000');
    $("#myAlertCompanyRegis").fadeTo(3000, 500).slideUp(500, function () {
        $("#myAlertCompanyRegis").css('z-index', '-1');
    });
}

function showAlertVatRegis() {
    $("#myAlertVatRegis").css('z-index', '2000');
    $("#myAlertVatRegis").fadeTo(3000, 500).slideUp(500, function () {
        $("#myAlertVatRegis").css('z-index', '-1');
    });
}

function showAlertCompanyVatRegis() {
    $("#myAlertCompanyVatRegis").css('z-index', '2000');
    $("#myAlertCompanyVatRegis").fadeTo(3000, 500).slideUp(500, function () {
        $("#myAlertCompanyVatRegis").css('z-index', '-1');
    });
}

function showAlertIdenCard() {
    $("#myAlertIdenCard").css('z-index', '2000');
    $("#myAlertIdenCard").fadeTo(3000, 500).slideUp(500, function () {
        $("#myAlertIdenCard").css('z-index', '-1');
    });
}

function deleteData(attachmentID, attachmentNameUnique, documentNameID, docType) {
    var elementID = "item_" + attachmentID;

    $("#" + elementID).remove();

    var htmlText = "";

    htmlText += "<input type=\"hidden\" name=\"hidRemoveGuid[]\" value=\"" + attachmentNameUnique + "\" />";

    $(htmlText).appendTo("#remove_item");

    var idTr = docType;

    var rowCount = $("#myTable_" + idTr + " >tbody >tr").length;
    $("#DocumentType_" + idTr).text(rowCount);
    if (rowCount == 0) {
        var scntDiv = $('#p_scents_' + idTr);
        htmlText = "<tr class=\"main_table_row\" id=\"lastData_" + idTr + "\"><td align=\"center\" colspan=\"4\"> NoData </td></tr>";
        scntDiv.append(htmlText);
        //console.log("MM");
        //$("#myTable_" + idTr + " > tbody:last").append("<tr class=\"main_table_row\" id=\"lastData_" + idTr + "><td align=\"center\" colspan=\"4\"\">NoData</td></tr>");
    }
}

function ClearContactRequest() {
    $("#hidService1").val("");
    $("#hidService2").val("");
    $("#hidService3").val("");
    $("#FirstName_Contact").val("");
    $("#LastName_Contact").val("");
    $("#ContactOtherJobTitle").val("");
    $("#ContactPhoneNo").val("");
    $("#ContactPhoneExt").val("");
    $("#ContactMobile").val("");
    $("#ContactEmail").val("");
    $("#ConfirmContactEmail").val("");
    $("#ContactInvitationCode").val("");

    $('input[type=checkbox][name=ServiceTypeContact]').each(function () {
        $(this).prop('checked', false);
    });
}

$(document).ready(function () {
    //$.validator.setDefaults({ onkeyup: false });

    $('#ModalSendMessageSuccess').on('hidden.bs.modal', function () {    // remove the event listeners when the dialog is dismissed
        RequestContact();
    });

    $("#btnConfirmCancel").on("click", function (event) {
        RequestContact();
    });

    $("#btnConfirmSendMail").on("click", function (event) {
        var contactTitleName = $("#ContactTitleName option:selected").val();
        var contactJobTitle = $("#ContactJobTitle option:selected").val();
        var contactOtherJobTitle = "";
        if (contactJobTitle == -1) {
            contactOtherJobTitle = $("#ContactOtherJobTitle").val();
        }
        //console.log("btnConfirmSendMail");
        disablePage();
        $.ajax({
            url: '/Register/SendEmailByDuplicateContact',
            dataType: "json",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({
                model: {
                    SupplierID: $("#hidSupplierID").val(),
                    EmailAdminBSP: $("#hidEmailReturn").val(),
                    DuplicateType: $("#hidDuplicateType").val(),
                    UsernameDuplicate: $("#hidUsernameDuplicate").val(),
                    ContactTitleName: contactTitleName,
                    FirstName_Contact: $("#FirstName_Contact").val(),
                    LastName_Contact: $("#LastName_Contact").val(),
                    ContactJobTitle: contactJobTitle,
                    ContactOtherJobTitle: $("#ContactOtherJobTitle").val(),
                    ContactPhoneNoCountryCode: $("#ContactPhoneNoCountryCode").val(),
                    ContactPhoneNo: $("#ContactPhoneNo").val(),
                    ContactPhoneExt: $("#ContactPhoneExt").val(),
                    ContactMobileCountryCode: $("#ContactMobileCountryCode").val(),
                    ContactMobile: $("#ContactMobile").val(),
                    ContactEmail: $("#ContactEmail").val(),
                    ContactInvitationCode: $.trim($("#ContactInvitationCode").val())
                }
            }),
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                //console.log(data);
                $.unblockUI();
                $("#MessageSuccessAlert").text("");
                $("#MessageSuccessAlert").html(data.Message);
                ClearContactRequest();
                loadMessageSuccessAlert();//Funtion from customheadscript
            },
            error: function (xhr) {
                //check = false;
            }
        });
    });

    $("#sendRequest").on("click", function (event) {
        //console.log("sendRequest");
        //if (chk) {
        var $form = $('#sendRequestContactForm');
        if ($form.valid()) {
            var chk = CheckDuplicateOrgContactPerson();
            //console.log(chk);
            if (chk) {
                disablePage();
                //Ajax call here
                var contactTitleName = $("#ContactTitleName option:selected").val();
                var contactJobTitle = $("#ContactJobTitle option:selected").val();
                var contactOtherJobTitle = "";
                if (contactJobTitle == -1) {
                    contactOtherJobTitle = $("#ContactOtherJobTitle").val();
                }

                $.ajax({
                    url: '/Register/AddContactRequest',
                    dataType: "json",
                    type: "POST",
                    contentType: 'application/json; charset=utf-8',
                    data: JSON.stringify({
                        contactModel: {
                            SupplierID: $("#hidSupplierID").val(),
                            //EmailAdminBSP: $("#hidEmailAdminBSP").val(),
                            Service1: $("#hidService1").val(),
                            Service2: $("#hidService2").val(),
                            Service3: $("#hidService3").val(),
                            ContactTitleName: contactTitleName,
                            FirstName_Contact: $("#FirstName_Contact").val(),
                            LastName_Contact: $("#LastName_Contact").val(),
                            ContactJobTitle: contactJobTitle,
                            ContactOtherJobTitle: contactOtherJobTitle,
                            ContactPhoneNoCountryCode: $("#ContactPhoneNoCountryCode").val(),
                            ContactPhoneNo: $("#ContactPhoneNo").val(),
                            ContactPhoneExt: $("#ContactPhoneExt").val(),
                            ContactMobileCountryCode: $("#ContactMobileCountryCode").val(),
                            ContactMobile: $("#ContactMobile").val(),
                            ContactEmail: $("#ContactEmail").val(),
                            ContactInvitationCode: $.trim($("#ContactInvitationCode").val())
                        }
                    }),
                    async: true,
                    processData: false,
                    cache: false,
                    success: function (data) {
                        $.unblockUI();
                        $("#message").text("");
                        $("#message").html(data.Message);
                        ClearContactRequest();
                        loadPopup();//Funtion from customheadscript
                    },
                    error: function (xhr) {
                        return false;
                    }
                });

                $("#AddContactRequest").modal('hide');
                //$(this).off(event);
            }
        }
        //}
    });

    //$("#btnSubmit").attr('disabled', true);

    //$("#lnkEdit").click(function () {
    //    OnEditTaxID();
    //});
    $("#btnNext1").click(function (evt) {
        //evt.preventDefault();

        var countryCode = $("#dd-country option:selected").val();
        var compSubDistrict = $("#RegCompanyAddress_SubDistrict").val();
        var compCity = $("#RegCompanyAddress_City").val();
        var compState = $("#RegCompanyAddress_State").val();

        var chk = true;

        $("#RegCompanyAddress_HouseNo_valid").css("display", "block");
        $("#RegCompanyAddress_PostalCode_valid").css("display", "block");

        var ValidHouseNo = $("#RegCompanyAddress_HouseNo").valid();
        var ValidPostalCode = $("#RegCompanyAddress_PostalCode").valid();

        if (countryCode == "TH") {
            if (compSubDistrict != null) {
                compSubDistrict = $.trim(compSubDistrict);
            }
            if (compSubDistrict == "" || compSubDistrict == undefined || compSubDistrict == null) {
                evt.preventDefault();
                //RegCompanyAddress_SubDistrict.SetValue("");
                $('#RegCompanyAddress_SubDistrict').focus();
                $("#errorCompSubDistrict").css("display", "block");
                chk = false;
                return false;
            } else {
                //RegCompanyAddress_SubDistrict.SetValue(compSubDistrict);
                $("#errorCompSubDistrict").css("display", "none");
                chk = true;
            }
        }

        if (compCity != null) {
            compCity = $.trim(compCity);
        }
        if (compCity == "" || compCity == undefined || compCity == null) {
            evt.preventDefault();
            //RegCompanyAddress_City.SetValue("");
            $('#RegCompanyAddress_City').focus();
            $("#errorCompCity").css("display", "block");
            chk = false;
            return false;
        } else {
            //RegCompanyAddress_City.SetValue(compCity);
            $("#errorCompCity").css("display", "none");
            chk = true;
        }

        if (compState != null) {
            compState = $.trim(compState);
        }
        if (compState == "" || compState == undefined || compState == null) {
            evt.preventDefault();
            //RegCompanyAddress_State.SetValue("");
            $('#RegCompanyAddress_State').focus();
            $("#errorCompState").css("display", "block");
            chk = false;
            return false;
        } else {
            //RegCompanyAddress_State.SetValue(compState);
            $("#errorCompState").css("display", "none");
            chk = true;
        }

        var $form = $('#mainRegisterForm');

        var validator = $("#mainRegisterForm").validate({
            onfocusout: false,
            invalidHandler: function (form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            }
        });

        if (ValidHouseNo && ValidPostalCode && chk == true) {
            var this_li_ind = $(this).parent().parent("li").index();
            if ($('.payment-wizard li').hasClass("jump-here")) {
                $(this).parent().parent("li").removeClass("active").addClass("completed");
                $(this).parent(".wizard-content").slideUp();
                $('.payment-wizard li.jump-here').removeClass("jump-here");
                $("form").submit(function (e) {
                    e.preventDefault();
                });
            } else {
                $(this).parent().parent("li").removeClass("active").addClass("completed");
                $(this).parent(".wizard-content").slideUp();
                $(this).parent().parent("li").next("li:not('.completed')").addClass('active').children('.wizard-content').slideDown();
                $("form").submit(function (e) {
                    e.preventDefault();
                });
            }
        } else {
            validator.focusInvalid();
        }
    });
    $("#btnNext2").click(function (evt) {
        //evt.preventDefault();
        var chk = true;
        var chks = true;
        //var atLeastOneIsChecked = $('.checkBusinessType:checkbox:checked').length > 0;

        var productKeyword = $("#ProductKeyword").val();
        var productKeywordx = "";
        if (productKeyword != null) {
            productKeywordx = productKeyword.join(",");
        }

        //if (atLeastOneIsChecked == false) {
        //    $("#valideCheckB").css("display", "block");
        //    // window.location.hash = '#checkBoxBusinessType';
        //    setTimeout(function () {
        //        $("#focustext").focus();
        //    }, 1);

        //}
        //else {
        //    $("#valideCheckB").css("display", "none");
        //}

        var categoryID_Lev3 = JSON.stringify(productCategorys);

        if (categoryID_Lev3 == "[]") {
            $("#valideCategory").css("display", "block");
            //window.location.hash = '#checkBoxBusinessType';
            setTimeout(function () {
                $("#focustext").focus();
            }, 1);
            chk = false;
            //return false;
        }
        else {
            $("#valideCategory").css("display", "none");
            chk = true;
        }

        if (productKeywordx == "") {
            $("#valideProductKeyword").css("display", "block");
            $("#valideProductKeyword").focus();
            chks = false;
            return false;
        }
        else {
            $("#valideProductKeyword").css("display", "none");
            chks = true;
        }

        var $form = $('#mainRegisterForm');

        var validator = $("#mainRegisterForm").validate({
            onfocusout: false,
            invalidHandler: function (form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            }
        });
        if (chk && chks) {
            $("#FirstName_valid").css("display", "none");
            $("#LastName_valid").css("display", "none");
            $("#PhoneNo_valid").css("display", "none");
            $("#Email_valid").css("display", "none");
            $("#ConfirmEmail_valid").css("display", "none");
            var this_li_ind = $(this).parent().parent("li").index();
            if ($('.payment-wizard li').hasClass("jump-here")) {
                $(this).parent().parent("li").removeClass("active").addClass("completed");
                $(this).parent(".wizard-content").slideUp();
                $('.payment-wizard li.jump-here').removeClass("jump-here");
                $("form").submit(function (e) {
                    e.preventDefault();
                });
            } else {
                $(this).parent().parent("li").removeClass("active").addClass("completed");
                $(this).parent(".wizard-content").slideUp();
                $(this).parent().parent("li").next("li:not('.completed')").addClass('active').children('.wizard-content').slideDown();
                $("form").submit(function (e) {
                    e.preventDefault();
                });
            }
        } else {
            validator.focusInvalid();
        }
    });
    $("#btnNext3").click(function (evt) {
        //evt.preventDefault();
        var jobTitle = $("#JobTitleID option:selected").val();
        var FirstName = $("#FirstName").val();
        var LastName = $("#LastName").val();
        var PhoneNo = $("#PhoneNo").val();
        var InvitationCode = $("#InvetationCode").val();

        var chk = true;
        var chkInvitationCode = true;

        if (jobTitle == -1) {
            var otherJobTitle = $('#OtherJobTitle').val();
            if (otherJobTitle != null) {
                otherJobTitle = $.trim(otherJobTitle);
            }
            if (otherJobTitle == "" || otherJobTitle == undefined || otherJobTitle == null) {
                evt.preventDefault();
                $('#OtherJobTitle').val("");
                $('#OtherJobTitle').focus();
                $("#errorOtherJobTitle").css("display", "block");
                chk = false;
                return false;
            } else {
                $('#OtherJobTitle').val(otherJobTitle);
                $("#errorOtherJobTitle").css("display", "none");
                chk = true;
            }
        }

        var $form = $('#mainRegisterForm');

        var validator = $("#mainRegisterForm").validate({
            onfocusout: false,
            invalidHandler: function (form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            }
        });

        //if (MainBusiness != null) {
        //    MainBusiness = $.trim(MainBusiness);
        //}
        //$("#MainBusiness").val(MainBusiness);
        if (FirstName != null) {
            FirstName = $.trim(FirstName);
        }
        $("#FirstName").val(FirstName);
        if (LastName != null) {
            LastName = $.trim(LastName);
        }
        $("#LastName").val(LastName);
        if (PhoneNo != null) {
            PhoneNo = $.trim(PhoneNo);
        }
        $("#PhoneNo").val(PhoneNo);

        //var MainBusiness = $("#MainBusiness").valid();
        $("#FirstName_valid").css("display", "block");
        $("#LastName_valid").css("display", "block");
        $("#PhoneNo_valid").css("display", "block");
        $("#Email_valid").css("display", "block");
        $("#ConfirmEmail_valid").css("display", "block");

        FirstName = $("#FirstName").valid();
        LastName = $("#LastName").valid();
        PhoneNo = $("#PhoneNo").valid();
        var Email = $("#Email").valid();
        var ConfirmEmail = $("#ConfirmEmail").valid();

        //var indicesToSelect = ServiceType.GetSelectedIndices();
        var indicesToSelect = $('input[type=checkbox][name=ServiceTypeList]:checked').length;
        //console.log(indicesToSelect);
        if (indicesToSelect <= 0) {
            evt.preventDefault();
            //$('#ServiceTyp').focus();
            //$('input[type=checkbox][name=ServiceTypeList]').focus();
            $("#errorServiceType").css("display", "block");
            chk = false;
            return false;
        } else {
            $("#errorServiceType").css("display", "none");
            chk = true;
        }

        var opSelect = $('input[type=checkbox][name=ServiceTypeList][id=ServiceType_2]:checked').length;
        //console.log("OP Select ==> " + opSelect);

        if (opSelect > 0) {
            if (InvitationCode != "") {
                validateInvitationCode().done(function (data) {
                    if (data == false) {
                        $("#errorInvetationCode").css("display", "block");
                        chkInvitationCode = data;
                    }
                    else {
                        $("#errorInvetationCode").css("display", "none");
                        chkInvitationCode = data;
                    }
                });
            }
            else {
                $("#errorInvetationCode").css("display", "none");
                chkInvitationCode = true;
            }
        }
        else {
            $("#errorInvetationCode").css("display", "none");
            chkInvitationCode = true;
        }

        if (FirstName && LastName && PhoneNo && Email && ConfirmEmail && chk == true && chkInvitationCode == true) {
            if ($('.payment-wizard li').hasClass("jump-here")) {
                $(this).parent().parent("li").removeClass("active").addClass("completed");
                $(this).parent(".wizard-content").slideUp();
                $('.payment-wizard li.jump-here').removeClass("jump-here");
                $("form").submit(function (e) {
                    e.preventDefault();
                });
            } else {
                $(this).parent().parent("li").removeClass("active").addClass("completed");
                $(this).parent(".wizard-content").slideUp();
                $(this).parent().parent("li").next("li:not('.completed')").addClass('active').children('.wizard-content').slideDown();
                $("form").submit(function (e) {
                    e.preventDefault();
                });
            }
        } else {
            validator.focusInvalid();
        }
    });

    function validateInvitationCode() {
        var InvitationCode = $("#InvetationCode").val();
        return $.ajax({
            url: '/Register/CheckInvitationCode',
            type: 'POST',
            data: { 'invitationCode': InvitationCode },
            async: false,
            dataType: 'json',
            success: function (data) {
            }
        });
    }

    $("#btnSubmit").click(function (evt) {
        //evt.preventDefault();

        var countryCode = $("#dd-country option:selected").val();

        if (countryCode == "VN") {
            var companyTypeID = $('input[name=CompanyTypeID]:checked').val();
            if (companyTypeID == "1") {
                var comRegis = $("input[name='hidFile_1[]']").val();
                var vatRegis = $("input[name='hidFile_3[]']").val();
                if (vatRegis == undefined && comRegis == undefined) {
                    showAlertCompanyVatRegis();
                    return false;
                } else {
                    if (comRegis == undefined) {
                        showAlertCompanyRegis();
                        return false;
                    } else if (vatRegis == undefined) {
                        showAlertVatRegis();
                        return false;
                    }
                }
            } else {
                var idenCard = $("input[name='hidFile_11[]']").val();
                if (idenCard == undefined) {
                    showAlertIdenCard();
                    return false;
                }
            }
        }

        var compSubDistrict = $("#RegCompanyAddress_SubDistrict").val();
        var compCity = $("#RegCompanyAddress_City").val();
        var compState = $("#RegCompanyAddress_State").val();
        var jobTitle = $("#JobTitleID option:selected").val();

        //var MainBusiness = $("#MainBusiness").val();
        var FirstName = $("#FirstName").val();
        var LastName = $("#LastName").val();
        var PhoneNo = $("#PhoneNo").val();
        var chk = true;
        var dataCatagory = [];
        $.each(productCategorys, function (i, el) {
            if ($.inArray(el, dataCatagory) == -1) dataCatagory.push(el);
        });
        var categoryID_Lev3 = JSON.stringify(dataCatagory);
        if (categoryID_Lev3 == "[]") {
            $("#valideCategory").css("display", "block");
            chk = false;
            return false;
        }
        else {
            $("#CategoryID_Lev3").val(categoryID_Lev3);
            $("#valideCategory").css("display", "none");
            chk = true;
        }

        var productKeyword = $("#ProductKeyword").val();
        var productKeywordx = "";
        if (productKeyword != null) {
            productKeywordx = productKeyword.join(",");
        }

        if (productKeywordx == "") {
            $("#valideProductKeyword").css("display", "block");
            chk = false;
            return false;
        }
        else {
            $("#valideProductKeyword").css("display", "none");
            $("#hidProductKeyword").val(productKeywordx);
            chk = true;
        }

        if (countryCode == "TH") {
            if (compSubDistrict != null) {
                compSubDistrict = $.trim(compSubDistrict);
            }
            if (compSubDistrict == "" || compSubDistrict == undefined || compSubDistrict == null) {
                evt.preventDefault();
                //RegCompanyAddress_SubDistrict.SetValue("");
                $("#RegCompanyAddress_SubDistrict").val();
                $('#RegCompanyAddress_SubDistrict').focus();
                $("#errorCompSubDistrict").css("display", "block");
                chk = false;
                return false;
            } else {
                //RegCompanyAddress_SubDistrict.SetValue(compSubDistrict);
                $("#RegCompanyAddress_SubDistrict").val(compSubDistrict);
                $("#errorCompSubDistrict").css("display", "none");
                chk = true;
            }
        }

        if (compCity != null) {
            compCity = $.trim(compCity);
        }
        if (compCity == "" || compCity == undefined || compCity == null) {
            evt.preventDefault();
            //RegCompanyAddress_City.SetValue("");
            $("#RegCompanyAddress_City").val("");
            $('#RegCompanyAddress_City').focus();
            $("#errorCompCity").css("display", "block");
            chk = false;
            return false;
        } else {
            //RegCompanyAddress_City.SetValue(compCity);
            $("#RegCompanyAddress_City").val(compCity);
            $("#errorCompCity").css("display", "none");
            chk = true;
        }

        if (compState != null) {
            compState = $.trim(compState);
        }
        if (compState == "" || compState == undefined || compState == null) {
            evt.preventDefault();
            //RegCompanyAddress_State.SetValue("");
            $("#RegCompanyAddress_State").val("");
            $('#RegCompanyAddress_State').focus();
            $("#errorCompState").css("display", "block");
            chk = false;
            return false;
        } else {
            //RegCompanyAddress_State.SetValue(compState);
            $("#RegCompanyAddress_State").val(compState);
            $("#errorCompState").css("display", "none");
            chk = true;
        }

        if (jobTitle == -1) {
            var otherJobTitle = $('#OtherJobTitle').val();
            if (otherJobTitle != null) {
                otherJobTitle = $.trim(otherJobTitle);
            }
            if (otherJobTitle == "" || otherJobTitle == undefined || otherJobTitle == null) {
                evt.preventDefault();
                $('#OtherJobTitle').val("");
                $('#OtherJobTitle').focus();
                $("#errorOtherJobTitle").css("display", "block");
                chk = false;
                return false;
            } else {
                $('#OtherJobTitle').val(otherJobTitle);
                $("#errorOtherJobTitle").css("display", "none");
                chk = true;
            }
        }

        //var indicesToSelect = ServiceType.GetSelectedIndices();
        var indicesToSelect = $('input[type=checkbox][name=ServiceTypeList]:checked').length;
        //console.log(indicesToSelect);
        if (indicesToSelect <= 0) {
            evt.preventDefault();
            $('#ServiceTypeList').focus();
            $("#errorServiceType").css("display", "block");
            chk = false;
            return false;
        } else {
            $("#errorServiceType").css("display", "none");
            chk = true;
        }

        var $form = $('#mainRegisterForm');

        var validator = $("#mainRegisterForm").validate({
            onfocusout: false,
            invalidHandler: function (form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            }
        });

        //if (MainBusiness != null) {
        //    MainBusiness = $.trim(MainBusiness);
        //}
        //$("#MainBusiness").val(MainBusiness);
        if (FirstName != null) {
            FirstName = $.trim(FirstName);
        }
        $("#FirstName").val(FirstName);
        if (LastName != null) {
            LastName = $.trim(LastName);
        }
        $("#LastName").val(LastName);
        if (PhoneNo != null) {
            PhoneNo = $.trim(PhoneNo);
        }
        $("#PhoneNo").val(PhoneNo);
        //console.log(chk);

        //var atLeastOneIsChecked = $('.checkBusinessType:checkbox:checked').length > 0;

        //if (atLeastOneIsChecked == false) {
        //    $("#valideCheckB").css("display", "block");
        //    chk = false;
        //    return false;
        //}
        //else {
        //    $("#valideCheckB").css("display", "none");
        //    chk = true;
        //}

        var checkStatusCapcha = $('#hidProc').val();
        if (checkStatusCapcha == "Add") {
            var textCapcha = $("#CaptchaInputText").val();

            if (textCapcha == "" || textCapcha == undefined) {
                evt.preventDefault();
                $("#errorCaptcha").css("display", "block");
                $("#errorCaptchaNotValid").css("display", "none");
                $("#CaptchaInputText").focus();
                chk = false;
            } else {
                evt.preventDefault();
                var chkCapcha = checkCapcha(evt);
                if (chkCapcha == "True") {
                    chk = true;
                } else {
                    chk = false;
                }
            }
        }
        else if (checkStatusCapcha == "Edit") {
            chk = true;
        }

        //console.log($form.valid() + " sss " + chk);
        if ($form.valid() && chk == true) {
            disablePage();
            document.getElementById("mainRegisterForm").submit();
            //console.log("sss");
            //$form.submit();
        } else {
            validator.focusInvalid();
        }
    });

    $("#ProductKeyword").focusout(function () {
        var productKeyword = $("#ProductKeyword").val();
        var productKeywordx = "";
        if (productKeyword != null) {
            productKeywordx = productKeyword.join(",");
        }

        if (productKeywordx == "") {
            $("#valideProductKeyword").css("display", "block");
            $("#valideProductKeyword").focus();
        }
        else {
            $("#valideProductKeyword").css("display", "none");
        }
    });

    $("#RegCompanyAddress_SubDistrict").focusout(function () {
        var compSubDistrict = $("#RegCompanyAddress_SubDistrict").val();

        if (compSubDistrict == "" || compSubDistrict == undefined) {
            $("#errorCompSubDistrict").css("display", "block");
        } else {
            $("#errorCompSubDistrict").css("display", "none");
        }
    });

    $("#RegCompanyAddress_City").focusout(function () {
        var compCity = $("#RegCompanyAddress_City").val();

        if (compCity == "" || compCity == undefined) {
            $("#errorCompCity").css("display", "block");
        } else {
            $("#errorCompCity").css("display", "none");
        }
    });

    $("#RegCompanyAddress_State").focusout(function () {
        var compState = $("#RegCompanyAddress_State").val();

        if (compState == "" || compState == undefined) {
            $("#errorCompState").css("display", "block");
        } else {
            $("#errorCompState").css("display", "none");
        }
    });

    $("#BranchNo").focusout(function () {
        var countryCode = $("#dd-country option:selected").val();

        if (countryCode == "TH") {
            var textBranchNo = $("#BranchNo").val();
            if (textBranchNo.length > 0 && textBranchNo.length < 5) {
                var pad = "00000";
                var ans = pad.substring(0, pad.length - textBranchNo.length) + textBranchNo;

                $("#BranchNo").val(ans);
            }

            var numberFormat = /[^0-9]/g;
            if (numberFormat.test(textBranchNo)) {
                $("#BranchNo").val("00000");
            }
        }
    });

    $("#FirstName").focusout(function () {
        var countryCode = $("#dd-country option:selected").val();
        var input = $("#FirstName").val();
        if (countryCode == "TH") {
            $.ajax({
                url: '/ValidateAjax/CheckThaiOnly',
                type: 'GET',
                data: {
                    input: input
                },
                async: false,
                contentType: 'application/json',
                success: function (data) {
                    //console.log("CheckThaiOnly ==> " + data);
                    if (data != true) {
                        $("#errorFormatFirstName_Local").css("display", "block");
                        $("#FirstName").focus();
                    } else {
                        $("#errorFormatFirstName_Local").css("display", "none");
                    }
                },
                fail: function (event, data) {
                }
            });
        }
    });

    $("#LastName").focusout(function () {
        var countryCode = $("#dd-country option:selected").val();
        var input = $("#LastName").val();
        if (countryCode == "TH") {
            $.ajax({
                url: '/ValidateAjax/CheckThaiOnly',
                type: 'GET',
                data: {
                    input: input
                },
                async: false,
                contentType: 'application/json',
                success: function (data) {
                    //console.log("CheckThaiOnly ==> " + data);
                    if (data != true) {
                        $("#errorFormatLastName_Local").css("display", "block");
                        $("#FirstName").focus();
                    } else {
                        $("#errorFormatLastName_Local").css("display", "none");
                    }
                },
                fail: function (event, data) {
                }
            });
        }
    });

    $("#FirstName_Contact").focusout(function () {
        var countryCode = $("#dd-country option:selected").val();
        var input = $("#FirstName_Contact").val();
        if (countryCode == "TH") {
            $.ajax({
                url: '/ValidateAjax/CheckThaiOnly',
                type: 'GET',
                data: {
                    input: input
                },
                async: false,
                contentType: 'application/json',
                success: function (data) {
                    //console.log("CheckThaiOnly ==> " + data);
                    if (data != true) {
                        $("#errorFormatFirstName_Contact").css("display", "block");
                        $("#FirstName_Contact").focus();
                    } else {
                        $("#errorFormatFirstName_Contact").css("display", "none");
                    }
                },
                fail: function (event, data) {
                }
            });
        }
    });

    $("#LastName_Contact").focusout(function () {
        var countryCode = $("#dd-country option:selected").val();

        var input = $("#LastName_Contact").val();
        if (countryCode == "TH") {
            $.ajax({
                url: '/ValidateAjax/CheckThaiOnly',
                type: 'GET',
                data: {
                    input: input
                },
                async: false,
                contentType: 'application/json',
                success: function (data) {
                    //console.log("CheckThaiOnly ==> " + data);
                    if (data != true) {
                        $("#errorFormatLastName_Contact").css("display", "block");
                        $("#LastName_Contact").focus();
                    } else {
                        $("#errorFormatLastName_Contact").css("display", "none");
                    }
                },
                fail: function (event, data) {
                }
            });
        }
    });

    $("#lnkCheck").click(function (evt) {
        var countryCode = $("#dd-country option:selected").val();

        var chk = ValidatRequire(evt);

        var chk2 = ValidatTaxIDAsync(evt); // แก้ปัญหา remote validation ทำงานยังไม่เสร็จแล้วกด check

        var chk3 = ValidatThaiOnlyRegular(evt, $("#CompanyName_Local").val(), countryCode);

        var chk4 = ValidatEngOnlyRegular(evt, $("#CompanyName_Inter").val(), countryCode);

        if (chk && chk2 && chk3 && chk4) {
            //$(".done").css("display", "block");
            //disablePage();
            var concat = "";

            concat = $("#oldTaxID").val() + $("#oldCompanyName_Local").val() + $("#oldCompanyName_Inter").val() + $("#oldBranchNo").val();
            disablePage();
            $.ajax({
                url: '/Register/CheckDuplicateTexID',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({
                    model: {
                        Proc: $("#hidProc").val(),
                        RegID: $("#hidRegID").val(),
                        OldConcat: concat,
                        TaxID: $("#TaxID").val(),
                        CompanyName_Local: $("#CompanyName_Local").val(),
                        CompanyName_Inter: $("#CompanyName_Inter").val(),
                        BranchNo: $("#BranchNo").val(),
                        CountryCode: countryCode
                    }
                }),
                async: true,
                processData: false,
                cache: false,
                success: function (data) {
                    //console.log(data);

                    $.unblockUI();
                    if (data.Status) {
                        OnSuccessCheckTaxID();
                        AssignTelCountryCode(data.TelCountryCode);
                    } else {
                        if (data.DuplicateType == "DuplicateReg") {
                            $("#message").text(data.Message);
                            loadPopup();//Funtion from customheadscript
                        } else if (data.DuplicateType == "DuplicateOrg") {
                            $("#hidSupplierID").val(data.SupplierID);
                            $("#DuplicateCompanyName").val(data.DuplicateCompanyName);
                            $("#BranchNoDuplicate").val($("#BranchNo").val());
                            CheckBranchNoDuplicate();
                            $('input:radio[class=BranchTypeDuplicate]').prop('disabled', true);

                            RequestContact();
                        }
                    }
                },
                error: function (xhr) {
                    $.unblockUI();
                    return false;
                }
            });
        }
    });
});
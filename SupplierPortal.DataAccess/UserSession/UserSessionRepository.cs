﻿using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.DataAccess.Buyer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.DataAccess.UserSession
{
    public partial class UserSessionRepository : BaseRepository, IUserSessionRepository
    {
        public Boolean Update(Tbl_UserSession tbl_UserSession)
        {
            bool result = false;
            try
            {
                var usertmp = context.Tbl_UserSession.Find(tbl_UserSession.UserGuid);
                usertmp.Username = tbl_UserSession.Username;
                usertmp.IPAddress = tbl_UserSession.IPAddress;
                usertmp.BrowserType = tbl_UserSession.BrowserType;
                usertmp.LoginTime = tbl_UserSession.LoginTime;
                usertmp.LogoutTime = tbl_UserSession.LogoutTime;
                usertmp.LastAccessed = tbl_UserSession.LastAccessed;
                usertmp.isTimeout = tbl_UserSession.isTimeout;
                usertmp.SessionData = tbl_UserSession.SessionData;
                context.SaveChanges();
                result = true;
            }
            catch (Exception ex)
            {
                throw;
            }
            return result;
        }

        public Tbl_UserSession GetUserSession(string userGuid)
        {
            return context.Tbl_UserSession.Find(userGuid);
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }

    }
}

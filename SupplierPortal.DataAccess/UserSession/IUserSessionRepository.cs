﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.DataAccess.UserSession
{
    public partial interface IUserSessionRepository
    {
        Boolean Update(Tbl_UserSession tbl_UserSession);
        Tbl_UserSession GetUserSession(string userGuid);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.DataAccess;

namespace SupplierPortal.DataAccess.BuyerConfig
{
    public class BuyerConfigRepository : BaseRepository, IBuyerConfigRepository
    {
        public Tbl_BuyerConfig GetBuyerConfig(string section, string name, int EID)
        {
            try
            {
                Tbl_BuyerConfig result = context.Tbl_BuyerConfig.Where(bc => bc.Section == section
                                                                      && bc.Name == name
                                                                      && bc.EID == EID).FirstOrDefault();

                return result;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}

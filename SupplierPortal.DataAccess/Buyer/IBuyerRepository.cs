﻿
using SupplierPortal.Data.CustomModels.BuyerSuppliers;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Models.Dto.DataContract;
using System;
using System.Collections.Generic;

namespace SupplierPortal.DataAccess.Buyer
{
    public interface IBuyerRepository
    {
        #region BuyerSupplier

        #region INSERT
        int InsertBuyerUser(Tbl_BuyerUser tbl_BuyerUser);
        #endregion

        #region GET
        Tuple<List<Buyer_Suppliers_Result>, int, int> GetSuppliers(GetBuyerSuppliersRequest request);
        #endregion
        #endregion

        #region GetBuyerSupplierContact
        #region GET
        ViewBuyerSupplier GetBuyerSupplierContactDetail(int userId);
        #endregion
        #endregion

        #region GetListHistoryBuyerContactPersonByUserId
        #region GET
        List<BuyerHistoryContactPersonModel> GetListHistorySupplierContactPersonByUserId(int userId);
        #endregion
        #endregion

        #region GetHistoryBuyerContactPerson
        #region GET
        int InsertLogUserByBuyer(Tbl_LogUserByBuyer tbl_LogUserByBuyer);
        #endregion
        #endregion

        #region GetBuyerConfig
        #region GET
        Tbl_BuyerConfig GetBuyerConfigByEID(int eid);
        #endregion
        #endregion
    }
}

﻿using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.DataAccess.Buyer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.DataAccess.Language
{
    public partial class LanguageRepository : BaseRepository, ILanguageRepository
    {
        public IEnumerable<Tbl_Language> GetLanguageIdByCode(string languageCode)
        {
            IEnumerable<Tbl_Language> result = null;
            try
            {
                if (String.IsNullOrEmpty(languageCode))
                {
                    result = context.Tbl_Language.ToList();
                }
                else
                {
                    result = context.Tbl_Language.Where(b => b.LanguageCode == languageCode).ToList();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return result;
        }

        public bool IsLocalLanguage(int languageId)
        {
            try
            {
                return context.Tbl_Language.Any(m => m.isLocal == 1 && m.LanguageID == languageId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Tbl_Language> GetLanguage()
        {
            List<Tbl_Language> language = null;
            try
            {
                var query1 = (from db in this.context.Tbl_Language
                            select new {
                                LanguageID = db.LanguageID,
                                LanguageName = db.LanguageName,
                                LanguageCode = db.LanguageCode,
                                DisplayOrder = db.DisplayOrder,
                                isStandard = db.isStandard,
                                isLocal = db.isLocal,
                                isInter = db.isInter,
                                ResourceName = db.ResourceName,
                            }).ToList()
               .Select(x => new Tbl_Language
               {
                   LanguageID = x.LanguageID,
                   LanguageName = x.LanguageName,
                   LanguageCode = x.LanguageCode,
                   DisplayOrder = x.DisplayOrder,
                   isStandard = x.isStandard,
                   isLocal = x.isLocal,
                   isInter = x.isInter,
                   ResourceName = x.ResourceName,
               });

                language = query1.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                this.context.Dispose();
            }
            return language;
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}

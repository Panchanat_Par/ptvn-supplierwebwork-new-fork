﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.DataAccess.LogSDFavoriteRepository
{
    public class LogSDFavoriteRepository : BaseRepository, ILogSDFavoriteRepository
    {
        #region Insert
        public virtual void Insert(int id, int updateBy, string logAction)
        {
            try
            {
                var tempModel = context.Tbl_SDFavorite.Where(m => m.Id == id).FirstOrDefault();
                if (tempModel != null)
                {
                    var model = new Tbl_LogSDFavorite()
                    {
                        Id = tempModel.Id,
                        SysUserID = tempModel.SysUserID,
                        EID = tempModel.EID,
                        UserID = tempModel.UserID,
                        UpdateBy = updateBy,
                        UpdateDate = DateTime.UtcNow,
                        LogAction = logAction
                    };
                    context.Tbl_LogSDFavorite.Add(model);
                    context.SaveChanges();
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion
    }
}

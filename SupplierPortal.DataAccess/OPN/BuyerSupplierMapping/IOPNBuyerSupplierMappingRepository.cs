﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.DataAccess.OPN.BuyerSupplierMapping
{
    public interface IOPNBuyerSupplierMappingRepository
    {
        Tbl_OPNBuyerSupplierMapping GetOPNBuyerSupplierMappingByRegID(int regID);

        List<Tbl_OPNBuyerSupplierMapping> GetOPNBuyerSupplierMappingByIsJobSuccess(bool isJobSuccess);

        bool UpdateOPNBuyerSupplierMapping(Tbl_OPNBuyerSupplierMapping supplierMapping);

        List<Tbl_OPNBuyerSupplierMapping> GetOPNBuyerSupplierMappingBySupplierId(int supplierId);

        #region INSERT
        int InsertOPNBuyerSupplierMapping(Tbl_OPNBuyerSupplierMapping tbl_OPNBuyerSupplierMapping);
        #endregion
    }
}

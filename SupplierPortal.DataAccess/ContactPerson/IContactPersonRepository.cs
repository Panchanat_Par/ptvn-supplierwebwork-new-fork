﻿using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Models.Dto;
using SupplierPortal.Models.Dto.DataContract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.DataAccess.ContactPerson
{
    public partial interface IContactPersonRepository
    {
        #region INSERT
        int Insert(Tbl_ContactPerson contactPerson);
        int InsertLogContactPerson(Tbl_LogContactPerson tbl_LogContactPerson);
        #endregion

        #region UPDATE
        Boolean Update(Tbl_ContactPerson contactPerson);
        #endregion

        #region DELETE

        #endregion

        #region GET
        Tbl_ContactPerson GetContactPersonByContactId(int contactId);

        Tuple<List<Tbl_ContactPersonDto>, int, int> GetUserContactPersonsOfOrganization(int supplierId, DatatableRequest request);

        List<ContactWithPagination_Sel_Result> GetContactWithPagination(int pageIndex, int pageSize, string search, string sorting);

        bool CheckEmailContactPersionDuplicate(string email);
        #endregion





    }
}

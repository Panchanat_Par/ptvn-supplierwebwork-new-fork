﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.DataAccess.BillingCondition
{
    public partial interface IUserBillingConditionRepository
    {
        Tbl_UserBillingCondition GetUserBillingCondition(string username);

        bool UpdateUserBillingCondition(Tbl_UserBillingCondition users);
    }
}

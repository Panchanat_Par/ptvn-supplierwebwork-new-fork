﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.DataAccess.RolePrivilege
{
    public class RolePrivilegeRepository : BaseRepository, IRolePrivilegeRepository
    {
        public List<Tbl_BuyerRolePrivilege> GetBuyerRolePrivileges(int sysUserId)
        {
            try
            {
                List<Tbl_BuyerRolePrivilege> result = (from brp in context.Tbl_BuyerRolePrivilege
                                                       join bur in context.Tbl_BuyerUserRole on brp.RoleID equals bur.RoleID
                                                       where bur.SysUserID == sysUserId
                                                       group brp.PrivilegeCode by brp.PrivilegeCode into obj
                                                       select new { PrivilegeCode = obj.Key }
                                                       ).ToList().Select(obj => new Tbl_BuyerRolePrivilege { PrivilegeCode = obj.PrivilegeCode }).ToList();

                return result;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public List<Tbl_BuyerRolePrivilege> GetBuyerRolePrivilgeByEPUserId(string sessionUserId,int epUserId, int eid)
        {
            try
            {
                //List<Tbl_BuyerRolePrivilege> result = (from brp in context.Tbl_BuyerRolePrivilege
                //                                       join bur in context.Tbl_BuyerUserRole on brp.RoleID equals bur.RoleID
                //                                       join bu in context.Tbl_BuyerUser on bur.SysUserID equals bu.SysUserID
                //                                       where bu.EPSysUserID == epUserId
                //                                       group brp.PrivilegeCode by brp.PrivilegeCode into obj
                //                                       select new { PrivilegeCode = obj.Key }
                //                                       ).ToList().Select(obj => new Tbl_BuyerRolePrivilege { PrivilegeCode = obj.PrivilegeCode }).ToList();

                List<EP_SelectRolePrivilege_Result> storeResult = context.EP_SelectRolePrivilege_Result(sessionUserId, epUserId, eid).ToList();
                List<Tbl_BuyerRolePrivilege> result = null;
                if (storeResult.Any())
                {
                    result = (from sr in storeResult
                              group sr.PrivilegeCode by sr.PrivilegeCode into obj
                              select new { PrivilegeCode = obj.Key }
                              ).ToList().Select(obj => new Tbl_BuyerRolePrivilege { PrivilegeCode = obj.PrivilegeCode }).ToList();
                    //result = storeResult.Select(obj => new Tbl_BuyerRolePrivilege { PrivilegeCode = obj.PrivilegeCode }).ToList();
                }



                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

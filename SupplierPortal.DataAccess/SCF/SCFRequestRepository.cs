﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.DataAccess.SCF
{
    public class SCFRequestRepository : BaseRepository, ISCFRequestRepository
    {
        #region GET
        public int GetTopIdSCFRequest()
        {
            int result = 0;
            try
            {
                result = this.context.Tbl_SCFRequest.Select(s => s.Id).Max();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }
        #endregion


        #region GET
        public List<Tbl_SCFRequest> GetSCFRequestByOrgID(string orgID)
        {
            List<Tbl_SCFRequest> result = null;
            try
            {
                result = this.context.Tbl_SCFRequest.Where(i => i.OrgId == orgID).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }
        #endregion

        #region INSERT
        public int InsertSCFRequest(Tbl_SCFRequest sCFRequest)
        {
            try
            {
                this.context.Tbl_SCFRequest.Add(sCFRequest);
                this.context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return sCFRequest.Id;
        }
        #endregion

        #region UPDATE
        public bool UpdateSCFRequest(Tbl_SCFRequest sCFRequest)
        {
            bool result = false;
            try
            {
                var temp = this.context.Tbl_SCFRequest.Find(sCFRequest.Id);
                temp = sCFRequest;
                this.context.SaveChanges();

                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }
        #endregion

        #region DELETE
        public bool RemoveSCFRequest(Tbl_SCFRequest sCFRequest)
        {
            bool result = false;
            try
            {
                var temp = this.context.Tbl_SCFRequest.Find(sCFRequest.Id);
                context.Tbl_SCFRequest.Remove(temp);
                context.SaveChanges();
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }
        #endregion 

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}

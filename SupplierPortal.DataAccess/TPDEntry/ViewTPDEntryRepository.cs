﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.DataAccess.TPDEntry
{
    public partial class ViewTPDEntryRepository : BaseRepository, IViewTPDEntryRepository
    {
        public ViewTPDEntry GetTPDEntryBytpOrgId(string tpOrgId)
        {
            return context.ViewTPDEntries.Where(f => f.TPOrgId == tpOrgId).FirstOrDefault();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.SupportModel.AccountPortal;
using System.Net.Mail;
using System.Net.Mime;


namespace SupplierPortal.Services.AccountManage
{
    public static class EmailService
    {

        public static void SendMail(MailModel _objmail)
        {

            if (_objmail != null)
            {
                try
                {
                    MailMessage msg = new MailMessage();
                    msg.From = new MailAddress(_objmail.From);
                    foreach (var address in _objmail.To.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        msg.To.Add(address);
                    }
                    //msg.To.Add(new MailAddress(_objmail.To));
                    msg.Subject = _objmail.Subject;
                    msg.IsBodyHtml = true;
                    msg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(_objmail.Body, null, MediaTypeNames.Text.Plain));
                    msg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(_objmail.Body, null, MediaTypeNames.Text.Html));

                    SmtpClient smtpClient = new SmtpClient(_objmail.Host);
                    smtpClient.UseDefaultCredentials = false;
                    //System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(_objmail.MailAccount, _objmail.MailPassword);
                    //smtpClient.Credentials = credentials;
                    //smtpClient.EnableSsl = true;
                    smtpClient.Send(msg);

                }
                catch (Exception ex)
                {
                    throw ex;
                }      
              
            }

            //return false;
        }

        public static void SendToMultipleAddressMail(MailModel _objmail)
        {

            if (_objmail != null)
            {
                try
                {
                    MailMessage msg = new MailMessage();
                    msg.From = new MailAddress(_objmail.From);
                    foreach (var address in _objmail.To.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        msg.To.Add(address);
                    }
                    msg.Subject = _objmail.Subject;
                    msg.IsBodyHtml = true;
                    msg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(_objmail.Body, null, MediaTypeNames.Text.Plain));
                    msg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(_objmail.Body, null, MediaTypeNames.Text.Html));

                    SmtpClient smtpClient = new SmtpClient(_objmail.Host);
                    smtpClient.UseDefaultCredentials = false;
                    //System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(_objmail.MailAccount, _objmail.MailPassword);
                    //smtpClient.Credentials = credentials;
                    //smtpClient.EnableSsl = true;
                    smtpClient.Send(msg);

                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }

            //return false;
        }

        public static void SendMailToAdmin(MailModel _objmail)
        {

            if (_objmail != null)
            {
                try
                {
                    MailMessage msg = new MailMessage();
                    msg.From = new MailAddress(_objmail.From);
                    //msg.To.Add(new MailAddress(_objmail.To));
                    foreach (var address in _objmail.To.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        msg.To.Add(address);
                    }
                    if (!string.IsNullOrEmpty(_objmail.CcEmailAddress))
                    {
                        foreach (var CCaddress in _objmail.CcEmailAddress.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            msg.CC.Add(CCaddress);
                        }
                        //msg.CC.Add(new MailAddress(_objmail.CcEmailAddress));
                    }
                    if (!string.IsNullOrEmpty(_objmail.BccEmailAddress))
                    {
                        foreach (var Bccaddress in _objmail.BccEmailAddress.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            msg.Bcc.Add(Bccaddress);
                        }
                        //msg.Bcc.Add(new MailAddress(_objmail.BccEmailAddress));
                    }
                    msg.Subject = _objmail.Subject;
                    msg.IsBodyHtml = true;
                    msg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(_objmail.Body, null, MediaTypeNames.Text.Plain));
                    msg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(_objmail.Body, null, MediaTypeNames.Text.Html));

                    SmtpClient smtpClient = new SmtpClient(_objmail.Host);
                    smtpClient.UseDefaultCredentials = false;
                    //System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(_objmail.MailAccount, _objmail.MailPassword);
                    //smtpClient.Credentials = credentials;
                    //smtpClient.EnableSsl = true;
                    smtpClient.Send(msg);

                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }

            //return false;
        }
    }
}

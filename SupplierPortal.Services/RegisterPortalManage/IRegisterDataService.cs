﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;
using SupplierPortal.Data.Models.SupportModel.Register;

namespace SupplierPortal.Services.RegisterPortalManage
{
    public partial interface IRegisterDataService
    {
        bool CheckDuplicateOrganization(string infosconcat);

        bool CheckDuplicateRegInfo(string infosconcat,string oldRegID);

        string GetSupplierIDOrganizationByConcatRegis(string infosconcat);

        string GetSupplierIDOrganizationByTaxIDAndBranchNo(string taxID, string branchNo);

        //bool CheckOrgContactPersonDuplicate(string FirstName_Contact, string LastName_Contact, int SupplierID, string ServiceOPChecked, out string emailAdminBSP);

        bool CheckFullnameContactDuplicateOP(string FirstName_Contact, string LastName_Contact, int SupplierID,string invitationCode, out string emailReturn, out string stringResourceReturn, out string duplicateType);

        bool CheckFullnameContactDuplicateNonOP(string FirstName_Contact, string LastName_Contact, int SupplierID, out string stringResourceReturn, out string usernameDuplicate, out string duplicateType);

        //bool SendingEmailBSPAdminRequestContact(string email, string languageId, string emailCC = "");

        bool SendingEmailBSPAdminRequestContact(SendEmailDuplicateContactModels model, string languageId, string emailCC = "");

        bool SendingEmailRegisterSubmit(int regID, string languageId);

        bool SendingEmailRegisterSubmitPIS(int regID, string languageId);

        bool SendingEmailAdoptionNotifyExistingUser(int adoptionReqID,string languageId, string emailCC = "");

        bool SendingEmailNewRequestUser(int userReqId,string languageId, string emailCC = "");

        bool SendingEmailNewRequestUserPIS(int userReqId, string languageId);

        RegisterModels GetRegisterRegInfoDataByTicketCode(string ticketCode);
    }
}

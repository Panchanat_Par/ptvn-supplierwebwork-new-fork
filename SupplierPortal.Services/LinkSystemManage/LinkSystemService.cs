﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.Repository.UserSession;
using SupplierPortal.Data.Models.Repository.UserSystemMapping;
using SupplierPortal.Data.Models.Repository.User;
using SupplierPortal.Data.Models.Repository.Language;
using SupplierPortal.Data.Models.Repository.MenuPortal;
using SupplierPortal.Data.Models.Repository.LogPortalInterface;
using SupplierPortal.Data.Models.SupportModel.LinkSystem;
using SupplierPortal.Services.AccountManage;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Framework.Localization;
using SupplierPortal.Data.Models.SupportModel;
using Newtonsoft.Json;

namespace SupplierPortal.Services.LinkSystemManage
{
    public partial class LinkSystemService : ILinkSystemService
    {
        IUserSessionRepository _userSessionRepository;
        IUserSystemMappingRepository _userSystemMappingRepository;
        IUserRepository _userRepository;
        ILanguageRepository _languageRepository;
        IMenuPortalRepository _menuPortalRepository;
        ILogPortalInterfaceRepository _logPortalInterfaceRepository;

        public LinkSystemService()
        {
            _userSessionRepository = new UserSessionRepository();
            _userSystemMappingRepository = new UserSystemMappingRepository();
            _userRepository = new UserRepository();
            _languageRepository = new LanguageRepository();
            _menuPortalRepository = new MenuPortalRepository();
            _logPortalInterfaceRepository = new LogPortalInterfaceRepository();
        }

        public LinkSystemService(
            UserSessionRepository userSessionRepository,
            UserSystemMappingRepository userSystemMappingRepository,
            UserRepository userRepository,
            LanguageRepository languageRepository,
            MenuPortalRepository menuPortalRepository,
            LogPortalInterfaceRepository logPortalInterfaceRepository
            )
        {
            _userSessionRepository = userSessionRepository;
            _userSystemMappingRepository = userSystemMappingRepository;
            _userRepository = userRepository;
            _languageRepository = languageRepository;
            _menuPortalRepository = menuPortalRepository;
            _logPortalInterfaceRepository = logPortalInterfaceRepository;
        }

        public virtual bool CheckUserGuid(string guid)
        {
            bool chk = false;

            try
            {
                var userSession = _userSessionRepository.GetUserSession(guid);

                if (userSession != null)
                {
                    chk = true;
                }
                else
                {
                    string languageId = "";
                    string messageReturn = "";
                    if (string.IsNullOrEmpty(languageId))
                    {
                        languageId = CultureHelper.GetImplementedCulture(languageId); // This is safe
                    }
                    throw new NotImplementedException(messageReturn.GetStringResource("_ApiMsg.VerifyUserSession.UserGuidNotFound", languageId));
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }


            return chk;
        }

        public virtual IEnumerable<ResultVerifyUserSessionModel> GetVerifyUserSession(string guid, int systemId, int systemGrpId, int requireAllUserMapping, int pageID, string appType = "")
        {
            try
            {

                var guidUser = _userSessionRepository.GetUserSession(guid);

                var user = _userRepository.FindByUsername(guidUser.Username);

                int isLocal = _languageRepository.GetLocalLanguage().LanguageID;

                int isInter = _languageRepository.GetInterLanguage().LanguageID;

                string userShow = UserDataService.GetUserShowByUsernameLanguageByUserProfile(user.Username);

                string userShowLocal = UserDataService.GetUserShowByUsernameAndLanguageId(user.Username, isLocal);

                string userShowEng = UserDataService.GetUserShowByUsernameAndLanguageId(user.Username, isInter);

                string organizationName = UserDataService.GetOrganizByUsernameLanguageByUserProfile(user.Username);

                IEnumerable<Tbl_UserSystemMapping> userSystemMapping;

                if (requireAllUserMapping == 1)
                {
                    userSystemMapping = _userSystemMappingRepository.GetUserSystemMappingByGroupNonCheckMerg(guidUser.Username, systemGrpId);
                }
                else
                {
                    userSystemMapping = _userSystemMappingRepository.GetUserSystemMappingNonCheckMerg(guidUser.Username, systemId);
                }

                var resultMenu = _menuPortalRepository.GetMenuTop();


                string languageId = _languageRepository.GetLanguageIdByCode(user.LanguageCode);

                if (!string.IsNullOrEmpty(appType) && appType.Trim().ToUpper() == "EQ")
                {
                    languageId = _languageRepository.GetInterLanguage().LanguageID.ToString();
                }

                string baseURLWeb = System.Configuration.ConfigurationManager.AppSettings.Get("baseURLWeb");

                List<Menu> menu = new List<Menu>();
                string menuCaption = "";

                string callbackUrl = "";
                foreach (var menus in resultMenu)
                {
                    callbackUrl = baseURLWeb + "LinkSystem/ReturnHome?UID={0}&Menu={1}";
                    callbackUrl = string.Format(callbackUrl, guid, menus.MenuID);
                    //callbackUrl = HttpUtility.HtmlEncode(@callbackUrl);

                    menuCaption = menuCaption.GetStringResource(menus.ResourceName, languageId);


                    menu.Add(new Menu { MenuCaption = menuCaption, MenuLink = callbackUrl, Icon = menus.Icon });
                }

                if (string.IsNullOrEmpty(languageId))
                {
                    languageId = CultureHelper.GetImplementedCulture(languageId); // This is safe
                }


                List<SysUserID> sysUserID = new List<SysUserID>();
                bool isPrimary = true;
                foreach (var userSystemMappings in userSystemMapping)
                {
                    if (userSystemMappings.MergeFromUserID == 0)
                    {
                        isPrimary = true;
                    }
                    else
                    {
                        isPrimary = false;
                    }
                    sysUserID.Add(new SysUserID
                    {
                        value = userSystemMappings.SysUserID,
                        SystemID = userSystemMappings.SystemID.ToString(),
                        IsPrimary = isPrimary
                    });
                }


                List<ModifiedSysUserID> modifiedSysUserID = new List<ModifiedSysUserID>();

                if (pageID == 10)
                {
                    IEnumerable<Tbl_UserSystemMapping> userModifySystemMapping;

                    string sessionData = guidUser.SessionData;

                    var jsonSessionData = new JsonSessionDataModel();

                    if (!string.IsNullOrEmpty(sessionData))
                    {
                        jsonSessionData = JsonConvert.DeserializeObject<JsonSessionDataModel>(sessionData);

                        int modifiedUserID = 0;

                        if (!string.IsNullOrEmpty(jsonSessionData.Data.ModifiedUserID))
                        {
                            modifiedUserID = Convert.ToInt32(jsonSessionData.Data.ModifiedUserID);
                        }

                        var userModify = _userRepository.FindUserByUserID(modifiedUserID);

                        if (userModify != null)
                        {
                            if (requireAllUserMapping == 1)
                            {
                                userModifySystemMapping = _userSystemMappingRepository.GetUserSystemMappingByGroup(userModify.Username, systemGrpId);
                            }
                            else
                            {
                                userModifySystemMapping = _userSystemMappingRepository.GetUserSystemMapping(userModify.Username, systemId);
                            }


                            foreach (var userModifySystemMappings in userModifySystemMapping)
                            {
                                modifiedSysUserID.Add(new ModifiedSysUserID
                                {
                                    value = userModifySystemMappings.SysUserID,
                                    SystemID = userModifySystemMappings.SystemID.ToString()
                                }
                                );
                            }
                        }
                    }


                }



                var result = new List<ResultVerifyUserSessionModel>();
                result.Add(new ResultVerifyUserSessionModel()
                {
                    UserGUID = guid,
                    TimeZone = user.TimeZone,
                    LanguageCode = user.LanguageCode,
                    PortalLoginID = user.UserID.ToString(),
                    PortalUsername = user.Username,
                    PortalUser = userShow,
                    NameLocal = userShowLocal,
                    NameEng = userShowEng,
                    Organization = organizationName,
                    OrgID = user.OrgID,
                    SupplierID = user.SupplierID.ToString(),
                    SysUserID = sysUserID,
                    ModifiedSysUserID = modifiedSysUserID,
                    Menu = menu
                });


                //InsertLog(guidUser.Username, guid, systemId);

                return result;


            }
            catch (Exception ex)
            {

                throw ex;

            }
        }

        public virtual void InsertLog(string username, string guid, int systemId)
        {
            try
            {
                DateTime utcNow = DateTime.UtcNow;

                Tbl_LogPortalInterface _objLog = new Tbl_LogPortalInterface
                {
                    Username = username,
                    UserGUID = guid,
                    FromSystemID = systemId,
                    Action = "Verrify Session",
                    LogDate = utcNow,

                };
                _logPortalInterfaceRepository.Insert(_objLog);
            }
            catch (Exception ex)
            {
                //throw ex;
                string languageId = "";
                string messageReturn = "";
                if (string.IsNullOrEmpty(languageId))
                {
                    languageId = CultureHelper.GetImplementedCulture(languageId); // This is safe
                }
                throw new NotImplementedException(messageReturn.GetStringResource("_ApiMsg.VerifyUserSession.InsertLogError", languageId));
            }
        }
    }
}

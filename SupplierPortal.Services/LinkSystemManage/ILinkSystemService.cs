﻿using SupplierPortal.Data.Models.SupportModel.LinkSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Services.LinkSystemManage
{
    public partial interface  ILinkSystemService
    {
        bool CheckUserGuid(string guid);

        IEnumerable<ResultVerifyUserSessionModel> GetVerifyUserSession(string guid, int systemId, int systemGrpId, int requireAllUserMapping, int pageID, string appType = "");

        void InsertLog(string username, string guid, int systemId);
    }
}

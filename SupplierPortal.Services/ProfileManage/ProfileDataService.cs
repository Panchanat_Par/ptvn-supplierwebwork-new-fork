﻿using SupplierPortal.Data.Models.SupportModel.Profile;
using SupplierPortal.Data.Models.Repository.User;
using SupplierPortal.Data.Models.Repository.ContactPerson;
using SupplierPortal.Data.Models.Repository.LogContactPerson;
using SupplierPortal.Data.Models.Repository.UserRole;
using SupplierPortal.Data.Models.Repository.ACL_SystemRole;
using SupplierPortal.Data.Models.Repository.Organization;
using SupplierPortal.Data.Models.Repository.Address;
using SupplierPortal.Data.Models.Repository.Country;
using SupplierPortal.Data.Models.Repository.TimeZone;
using SupplierPortal.Data.Models.Repository.UserSystemMapping;
using SupplierPortal.Data.Models.Repository.APILogs;
using SupplierPortal.Data.Models.Repository.UserReportMapping;
using SupplierPortal.Data.Models.Repository.SystemRole;
using SupplierPortal.Data.Models.Repository.SupplierPortalReports;
using SupplierPortal.Data.Models.Repository.Language;
using SupplierPortal.Data.Models.Repository.LogUser;
using SupplierPortal.Data.Models.Repository.OrgContactAddressMapping;
using SupplierPortal.Data.Models.Repository.UserSession;
using SupplierPortal.Data.Models.Repository.UserServiceMapping;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Framework.MethodHelper;
using SupplierPortal.Framework.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Framework.AccountManage;
using SupplierPortal.Services.AccountManage;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;
using System.Web;
using SupplierPortal.Data.Helper;
using SupplierPortal.Data.Models.SupportModel;
using SupplierPortal.Data.Models.Repository.SystemConfigureAPI;
using SupplierPortal.Data.Models.Repository.Notification;
using SupplierPortal.DataAccess.OPN.BuyerSupplierMapping;
using SupplierPortal.DataAccess.OPN.Buyer;

namespace SupplierPortal.Services.ProfileManage
{
    public partial class ProfileDataService : IProfileDataService
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        IUserRepository _userRepository;
        IContactPersonRepository _contactPersonRepository;
        ILogContactPersonRepository _logContactPersonRepository;
        IUserRoleRepository _userRoleRepository;
        IACL_SystemRoleRepository _acl_SystemRoleRepository;
        IOrganizationRepository _organizationRepository;
        IAddressRepository _addressRepository;
        ICountryRepository _countryRepository;
        ITimeZoneRepository  _timeZoneRepository;
        IUserSystemMappingRepository _userSystemMappingRepository;
        IAPILogsRepository _aPILogsRepository;
        IUserReportMappingRepository _userReportMappingRepository;
        ISystemRepository _systemRepository;
        IPortalReportsRepository _portalReportsRepository;
        ILanguageRepository _languageRepository;
        ILogUserRepository _logUserRepository;
        IOrgContactAddressMappingRepository _orgContactAddressMappingRepository;
        IUserSessionRepository _userSessionRepository;
        IUserServiceMappingRepository _userServiceMappingRepository;
        ISystemConfigureAPIRepository _systemConfigureAPIRepository;
        INotificationRepository _notificationRepository;
        IOPNBuyerSupplierMappingRepository _opnBuyerSupplierMappingRepository;
        IOPNBuyerRepository _opnBuyerRepository;

        public ProfileDataService()
        {
            _userRepository = new UserRepository();
            _contactPersonRepository = new ContactPersonRepository();
            _logContactPersonRepository = new LogContactPersonRepository();
            _userRoleRepository = new UserRoleRepository();
            _acl_SystemRoleRepository = new ACL_SystemRoleRepository();
            _organizationRepository = new OrganizationRepository();
            _addressRepository = new AddressRepository();
            _countryRepository = new CountryRepository();
            _timeZoneRepository = new TimeZoneRepository();
            _userSystemMappingRepository = new UserSystemMappingRepository();
            _aPILogsRepository = new APILogsRepository();
            _userReportMappingRepository = new UserReportMappingRepository();
            _systemRepository = new SystemRepository();
            _portalReportsRepository = new PortalReportsRepository();
            _languageRepository = new LanguageRepository();
            _logUserRepository = new LogUserRepository();
            _orgContactAddressMappingRepository = new OrgContactAddressMappingRepository();
            _userSessionRepository = new UserSessionRepository();
            _userServiceMappingRepository = new UserServiceMappingRepository();
            _systemConfigureAPIRepository = new SystemConfigureAPIRepository();
            _notificationRepository = new NotificationRepository();
            _opnBuyerSupplierMappingRepository = new OPNBuyerSupplierMappingRepository();
            _opnBuyerRepository = new OPNBuyerRepository();
        }

        public ProfileDataService(
            UserRepository userRepository,
            ContactPersonRepository contactPersonRepository,
            LogContactPersonRepository logContactPersonRepository,
            UserRoleRepository userRoleRepository,
            ACL_SystemRoleRepository acl_SystemRoleRepository,
            OrganizationRepository organizationRepository,
            AddressRepository addressRepository,
            CountryRepository countryRepository,
            TimeZoneRepository timeZoneRepository,
            UserSystemMappingRepository userSystemMappingRepository,
            APILogsRepository aPILogsRepository,
            UserReportMappingRepository userReportMappingRepository,
            SystemRepository systemRepository,
            PortalReportsRepository portalReportsRepository,
            LanguageRepository languageRepository,
            LogUserRepository logUserRepository,
            OrgContactAddressMappingRepository orgContactAddressMappingRepository,
            UserSessionRepository userSessionRepository,
            UserServiceMappingRepository userServiceMappingRepository,
            SystemConfigureAPIRepository systemConfigureAPIRepository,
            NotificationRepository notificationRepository,
            OPNBuyerSupplierMappingRepository opnBuyerSupplierMappingRepository,
            OPNBuyerRepository opnBuyerRepository

            )
        {
            _userRepository = userRepository;
            _contactPersonRepository = contactPersonRepository;
            _logContactPersonRepository = logContactPersonRepository;
            _userRoleRepository = userRoleRepository;
            _acl_SystemRoleRepository = acl_SystemRoleRepository;
            _organizationRepository = organizationRepository;
            _addressRepository = addressRepository;
            _countryRepository = countryRepository;
            _timeZoneRepository = timeZoneRepository;
            _userSystemMappingRepository = userSystemMappingRepository;
            _aPILogsRepository = aPILogsRepository;
            _userReportMappingRepository = userReportMappingRepository;
            _systemRepository = systemRepository;
            _portalReportsRepository = portalReportsRepository;
            _languageRepository = languageRepository;
            _logUserRepository = logUserRepository;
            _orgContactAddressMappingRepository = orgContactAddressMappingRepository;
            _userSessionRepository = userSessionRepository;
            _userServiceMappingRepository = userServiceMappingRepository;
            _systemConfigureAPIRepository = systemConfigureAPIRepository;
            _notificationRepository = notificationRepository;
            _opnBuyerSupplierMappingRepository = opnBuyerSupplierMappingRepository;
            _opnBuyerRepository = opnBuyerRepository;
        }

        public virtual string UpdateGeneralData(GeneralModels model)
        {
            string systemUnSuccessCall = "";

            int isInter = LanguageHelper.GetLanguageIsInter();

            try
            {
                var user = _userRepository.FindByUsername(model.UserLoginID);

                if (user!=null)
                {
                    int contactID = user.ContactID??0;
                    var contactPerson = _contactPersonRepository.GetContactPersonByContectID(contactID);

                    #region Modify User Data
                    user.TimeZone = model.TimeZoneIDString;
                    user.Locale = model.LocaleName;
                    //user.Currency = model.CurrencyCode;
                    //user.LanguageCode = model.LanguageCode;
                    //user.IsPublic = model.IsPublic? 1 : 0;
                    #endregion

                    #region Modify ContactPerson Data

                    if(model.JobTitleID != -1)
                    {
                        model.OtherJobTitle = "";
                    }
                    contactPerson.Email = model.Email;
                    contactPerson.TitleID = model.TitleID;
                    contactPerson.FirstName_Inter = model.FirstName_Inter;
                    contactPerson.LastName_Inter = model.LastName_Inter;
                    contactPerson.FirstName_Local = model.FirstName_Local;
                    contactPerson.LastName_Local = model.LastName_Local;                    
                    contactPerson.JobTitleID = model.JobTitleID;
                    contactPerson.OtherJobTitle = model.OtherJobTitle ?? "";
                    contactPerson.Department = model.Department ?? "";
                    contactPerson.PhoneNo = model.PhoneNo ?? "";
                    contactPerson.PhoneExt = model.PhoneExt ?? "";
                    contactPerson.MobileNo = model.MobileNo ?? "";
                    contactPerson.FaxNo = model.FaxNo??"";
                    contactPerson.FaxExt = model.FaxExt ?? "";
                    #endregion

                    #region Modify Address

                    var address = _addressRepository.GetAddressByAddressID(model.AddressID??0);

                    if (address!= null)
                    {
                       
                            //address.HouseNo_Inter = model.Address_HouseNo_Inter;
                            //address.VillageNo_Inter = model.Address_VillageNo_Inter;
                            //address.Lane_Inter = model.Address_Lane_Inter;
                            //address.Road_Inter = model.Address_Road_Inter;
                            //address.SubDistrict_Inter = model.Address_SubDistrict_Inter;
                            //address.City_Inter = model.Address_City_Inter;
                            //address.State_Inter = model.Address_State_Inter;

                            address.HouseNo_Local = model.Address_HouseNo_Local??"";
                            address.VillageNo_Local = model.Address_VillageNo_Local ?? "";
                            address.Lane_Local = model.Address_Lane_Local ?? "";
                            address.Road_Local = model.Address_Road_Local ?? "";
                            address.SubDistrict_Local = model.Address_SubDistrict_Local ?? "";
                            address.City_Local = model.Address_City_Local ?? "";
                            address.State_Local = model.Address_State_Local ?? "";

                            address.CountryCode = model.Address_CountryCode ?? "";
                            address.PostalCode = model.Address_PostalCode ?? "";
                        

                        _addressRepository.UpdateAddress(address, model.UserID ?? 0);
                        logger.Debug("(UpdateAddress)UserID : " + model.UserID);
                    }
                    #endregion

                    #region Update Data
                    _userRepository.Update(user);
                    logger.Debug("(UpdateUser)UserID : " + model.UserID);
                    //_userRepository.Dispose();
                    _contactPersonRepository.Update(contactPerson);
                    logger.Debug("(UpdateContactPerson)UserID : " + model.UserID);
                    //_contactPersonRepository.Dispose();
                    #endregion

                    #region Insert Log
                    _logContactPersonRepository.Insert(contactID, "Modify", model.UserUpdate);
                    logger.Debug("(InsertlogContactPerson)UserID : " + model.UserID);
                    //_logContactPersonRepository.Dispose();

                    _logUserRepository.Insert(user.UserID, "Modify", model.UserUpdate);
                    logger.Debug("(InsertlogUser)UserID : " + model.UserID);
                    //_logUserRepository.Dispose();
                    #endregion

                    #region Call API Data

                    string guid = "mockGUID";

                    if (System.Web.HttpContext.Current.Session["GUID"] != null)
                    {
                        guid = System.Web.HttpContext.Current.Session["GUID"].ToString();
                    }

                    string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();

                    var listSystem = _systemConfigureAPIRepository.GetSystemListByUsernameForUpdateAPI(user.Username);

                    foreach (var sysMapping in listSystem)
                    {
                        string reqID = GeneratorGuid.GetRandomGuid();

                        string Jsonstr = "";

                        string _APIKey = "";

                        string _APIName = "";

                        var apiUpdateModels = GetDataCallUpdateUserAPI(sysMapping, reqID, sysMapping.Username);
                        Jsonstr = JsonConvert.SerializeObject(apiUpdateModels);
                        _APIKey = apiUpdateModels.Request.APIKey;
                        _APIName = apiUpdateModels.Request.APIName;

                        HttpClient client = new HttpClient();
                        client.BaseAddress = new Uri(sysMapping.API_URL);
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                        HttpContent content = new StringContent(Jsonstr);
                        content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                        #region Insert API Log Before
                        
                        var _APILogsBefore = new Tbl_APILogs()
                        {
                            ReqID = reqID,
                            SystemID = sysMapping.SystemID,
                            Events = "Request_Before",
                            Events_Time = DateTime.UtcNow,
                            IPAddress_Client = visitorsIPAddr,
                            APIKey = _APIKey,
                            APIName = _APIName,
                            Return_Code = "",
                            Return_Status = "",
                            Return_Message = "",
                            Data = Jsonstr,
                            //Data = responseBody,
                            UserGUID = guid,
                        };

                        _aPILogsRepository.Insert(_APILogsBefore);
                        logger.Debug("(InsertaPILogs)UserID : " + model.UserID);
                        #endregion

                        #region Call API
                        //HttpResponseMessage response = client.PostAsync(sysMapping.API_URL, content).Result;

                        //var responseBody = response.Content.ReadAsStringAsync().Result;

                        //if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        //{
                        //    try
                        //    {
                        //        if (!string.IsNullOrEmpty(responseBody))
                        //        {
                        //            JObject _response = JObject.Parse(responseBody);

                        //            string return_Code = _response["Response"]["Result"]["Code"].ToString();
                        //            string return_Status = _response["Response"]["Result"]["SuccessStatus"].ToString();
                        //            string return_Message = _response["Response"]["Result"]["Message"].ToString();


                        //            #region Insert API Log

                        //            var _APILogs = new Tbl_APILogs()
                        //            {
                        //                ReqID = reqID,
                        //                SystemID = sysMapping.SystemID,
                        //                Events = "Request",
                        //                Events_Time = DateTime.UtcNow,
                        //                IPAddress_Client = visitorsIPAddr,
                        //                APIKey = _APIKey,
                        //                APIName = _APIName,
                        //                Return_Code = return_Code,
                        //                Return_Status = return_Status,
                        //                Return_Message = return_Message,
                        //                Data = Jsonstr,
                        //                //Data = responseBody,
                        //                UserGUID = guid,
                        //            };

                        //            _aPILogsRepository.Insert(_APILogs);
                        //            #endregion

                        //            if (!Convert.ToBoolean(return_Status))
                        //            {
                        //                systemUnSuccessCall += sysMapping.SystemName + ",";
                        //            }
                        //        }
                        //        else
                        //        {
                        //            #region Insert API Log

                        //            var _APILogs = new Tbl_APILogs()
                        //            {
                        //                ReqID = reqID,
                        //                SystemID = sysMapping.SystemID,
                        //                Events = "Request",
                        //                Events_Time = DateTime.UtcNow,
                        //                IPAddress_Client = visitorsIPAddr,
                        //                APIKey = _APIKey,
                        //                APIName = _APIName,
                        //                Return_Code = "",
                        //                Return_Status = "",
                        //                Return_Message = "ResponseBody Empty",
                        //                Data = Jsonstr,
                        //                //Data = responseBody,
                        //                UserGUID = guid,
                        //            };

                        //            _aPILogsRepository.Insert(_APILogs);
                        //            #endregion

                        //            systemUnSuccessCall += sysMapping.SystemName + ",";
                        //        }

                        //    }
                        //    catch (Exception ex)
                        //    {
                        //        #region Insert API Log
                        //        string guidCall = "mockGUID";

                        //        if (System.Web.HttpContext.Current.Session["GUID"] != null)
                        //        {
                        //            guidCall = System.Web.HttpContext.Current.Session["GUID"].ToString();
                        //        }

                        //        string visitorsIPAddrCall = GetClientIPAddressHelper.GetClientIPAddres();


                        //        var _APILogsCheck = new Tbl_APILogs()
                        //        {
                        //            ReqID = reqID,
                        //            SystemID = sysMapping.SystemID,
                        //            Events = "Request_Check",
                        //            Events_Time = DateTime.UtcNow,
                        //            IPAddress_Client = visitorsIPAddrCall,
                        //            APIKey = _APIKey,
                        //            APIName = _APIName,
                        //            Return_Code = "",
                        //            Return_Status = response.StatusCode.ToString(),
                        //            Return_Message = responseBody,
                        //            Data = Jsonstr,
                        //            //Data = responseBody,
                        //            UserGUID = guidCall,
                        //        };

                        //        _aPILogsRepository.Insert(_APILogsCheck);


                        //        #endregion

                        //        throw;
                        //    }

                        //}
                        //else
                        //{

                        //    #region Insert API Log

                        //    var _APILogs = new Tbl_APILogs()
                        //    {
                        //        ReqID = reqID,
                        //        Events = "Request",
                        //        Events_Time = DateTime.UtcNow,
                        //        IPAddress_Client = visitorsIPAddr,
                        //        APIKey = _APIKey,
                        //        APIName = _APIName,
                        //        Return_Code = "",
                        //        Return_Status = "",
                        //        Return_Message = "",
                        //        Data = Jsonstr,
                        //        //Data = responseBody,
                        //        UserGUID = guid,
                        //    };

                        //    _aPILogsRepository.Insert(_APILogs);

                        //    systemUnSuccessCall += sysMapping.SystemName + ",";

                        //    #endregion
                        //}
                        #endregion

                        #region  API Post Task
                        string responseBody = "";
                        var continuationTask = client.PostAsync(sysMapping.API_URL, content).ContinueWith(
                                                (postTask) =>
                                                {
                                                    bool isCompleted = postTask.IsCompleted;
                                                    bool isFaulted = postTask.IsFaulted;
                                                    if(isCompleted && !isFaulted)
                                                    {
                                                        var response = postTask.Result;
                                                        if(response.StatusCode == System.Net.HttpStatusCode.OK)
                                                        {
                                                            try
                                                            {
                                                                if (!string.IsNullOrEmpty(response.Content.ReadAsStringAsync().Result))
                                                                {
                                                                    responseBody = response.Content.ReadAsStringAsync().Result;
                                                                    JObject _response = JObject.Parse(responseBody);

                                                                    string return_Code = _response["Response"]["Result"]["Code"].ToString();
                                                                    string return_Status = _response["Response"]["Result"]["SuccessStatus"].ToString();
                                                                    string return_Message = _response["Response"]["Result"]["Message"].ToString();


                                                                    #region Insert API Log

                                                                    var _APILogs = new Tbl_APILogs()
                                                                    {
                                                                        ReqID = reqID,
                                                                        SystemID = sysMapping.SystemID,
                                                                        Events = "Request",
                                                                        Events_Time = DateTime.UtcNow,
                                                                        IPAddress_Client = visitorsIPAddr,
                                                                        APIKey = _APIKey,
                                                                        APIName = _APIName,
                                                                        Return_Code = return_Code,
                                                                        Return_Status = return_Status,
                                                                        Return_Message = return_Message,
                                                                        Data = Jsonstr,
                                                                        //Data = responseBody,
                                                                        UserGUID = guid,
                                                                    };

                                                                    _aPILogsRepository.Insert(_APILogs);
                                                                    #endregion

                                                                    if (!Convert.ToBoolean(return_Status))
                                                                    {
                                                                        systemUnSuccessCall += sysMapping.SystemName + ",";
                                                                    }
                                                                }
                                                                //else
                                                                //{
                                                                //    #region Insert API Log

                                                                //    var _APILogs = new Tbl_APILogs()
                                                                //    {
                                                                //        ReqID = reqID,
                                                                //        SystemID = sysMapping.SystemID,
                                                                //        Events = "Request",
                                                                //        Events_Time = DateTime.UtcNow,
                                                                //        IPAddress_Client = visitorsIPAddr,
                                                                //        APIKey = _APIKey,
                                                                //        APIName = _APIName,
                                                                //        Return_Code = "",
                                                                //        Return_Status = "",
                                                                //        Return_Message = "ResponseBody Empty UpdateGeneralData",
                                                                //        Data = Jsonstr,
                                                                //        //Data = responseBody,
                                                                //        UserGUID = guid,
                                                                //    };

                                                                //    _aPILogsRepository.Insert(_APILogs);
                                                                //    #endregion

                                                                //    systemUnSuccessCall += sysMapping.SystemName + ",";
                                                                //}

                                                            }
                                                            catch (Exception ex)
                                                            {
                                                                #region Insert API Log
                                                                string guidCall = "mockGUID";

                                                                if (System.Web.HttpContext.Current.Session["GUID"] != null)
                                                                {
                                                                    guidCall = System.Web.HttpContext.Current.Session["GUID"].ToString();
                                                                }

                                                                string visitorsIPAddrCall = GetClientIPAddressHelper.GetClientIPAddres();


                                                                var _APILogsCheck = new Tbl_APILogs()
                                                                {
                                                                    ReqID = reqID,
                                                                    SystemID = sysMapping.SystemID,
                                                                    Events = "Request_Check",
                                                                    Events_Time = DateTime.UtcNow,
                                                                    IPAddress_Client = visitorsIPAddrCall,
                                                                    APIKey = _APIKey,
                                                                    APIName = _APIName,
                                                                    Return_Code = "",
                                                                    Return_Status = response.StatusCode.ToString(),
                                                                    Return_Message = responseBody,
                                                                    Data = Jsonstr,
                                                                    //Data = responseBody,
                                                                    UserGUID = guidCall,
                                                                };

                                                                _aPILogsRepository.Insert(_APILogsCheck);


                                                                #endregion

                                                                throw;
                                                            }
                                                        }
                                                        else
                                                        {

                                                            #region Insert API Log

                                                            var _APILogs = new Tbl_APILogs()
                                                            {
                                                                ReqID = reqID,
                                                                SystemID = sysMapping.SystemID,
                                                                Events = "Request",
                                                                Events_Time = DateTime.UtcNow,
                                                                IPAddress_Client = visitorsIPAddr,
                                                                APIKey = _APIKey,
                                                                APIName = _APIName,
                                                                Return_Code = "",
                                                                Return_Status = response.StatusCode.ToString(),
                                                                Return_Message = "",
                                                                Data = Jsonstr,
                                                                //Data = responseBody,
                                                                UserGUID = guid,
                                                            };

                                                            _aPILogsRepository.Insert(_APILogs);

                                                            systemUnSuccessCall += sysMapping.SystemName + ",";

                                                            #endregion
                                                        }
                                                    }

                                                });
                        continuationTask.Wait(20000);
                        //continuationTask.Wait(
                        #endregion

                    }

                    #endregion
                }
            


            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }

            return systemUnSuccessCall;
        }

        public virtual async Task<string> UpdateGeneralData2(GeneralModels model)
        {
            string systemUnSuccessCall = "";

            int isInter = LanguageHelper.GetLanguageIsInter();

            try
            {
                var user = _userRepository.FindByUsername(model.UserLoginID);

                if (user != null)
                {
                    int contactID = user.ContactID ?? 0;
                    var contactPerson = _contactPersonRepository.GetContactPersonByContectID(contactID);

                    #region Modify User Data
                    user.TimeZone = model.TimeZoneIDString;
                    user.Locale = model.LocaleName;
                    //user.Currency = model.CurrencyCode;
                    //user.LanguageCode = model.LanguageCode;
                    //user.IsPublic = model.IsPublic? 1 : 0;
                    #endregion

                    #region Modify ContactPerson Data
                    contactPerson.Email = model.Email;
                    contactPerson.TitleID = model.TitleID;
                    contactPerson.FirstName_Inter = model.FirstName_Inter;
                    contactPerson.LastName_Inter = model.LastName_Inter;
                    contactPerson.FirstName_Local = model.FirstName_Local;
                    contactPerson.LastName_Local = model.LastName_Local;

                    contactPerson.JobTitleID = model.JobTitleID;
                    contactPerson.OtherJobTitle = model.OtherJobTitle;
                    contactPerson.Department = model.Department;
                    contactPerson.PhoneNo = model.PhoneNo;
                    contactPerson.PhoneExt = model.PhoneExt;
                    contactPerson.MobileNo = model.MobileNo;
                    contactPerson.FaxNo = model.FaxNo;
                    contactPerson.FaxExt = model.FaxExt;
                    #endregion

                    #region Modify Address

                    var address = _addressRepository.GetAddressByAddressID(model.AddressID ?? 0);

                    if (address != null)
                    {

                        //address.HouseNo_Inter = model.Address_HouseNo_Inter;
                        //address.VillageNo_Inter = model.Address_VillageNo_Inter;
                        //address.Lane_Inter = model.Address_Lane_Inter;
                        //address.Road_Inter = model.Address_Road_Inter;
                        //address.SubDistrict_Inter = model.Address_SubDistrict_Inter;
                        //address.City_Inter = model.Address_City_Inter;
                        //address.State_Inter = model.Address_State_Inter;

                        address.HouseNo_Local = model.Address_HouseNo_Local;
                        address.VillageNo_Local = model.Address_VillageNo_Local;
                        address.Lane_Local = model.Address_Lane_Local;
                        address.Road_Local = model.Address_Road_Local;
                        address.SubDistrict_Local = model.Address_SubDistrict_Local;
                        address.City_Local = model.Address_City_Local;
                        address.State_Local = model.Address_State_Local;

                        address.CountryCode = model.Address_CountryCode;
                        address.PostalCode = model.Address_PostalCode;


                        _addressRepository.UpdateAddress(address, model.UserID ?? 0);
                    }
                    #endregion

                    #region Update Data
                    _userRepository.Update(user);
                    //_userRepository.Dispose();
                    _contactPersonRepository.Update(contactPerson);
                    //_contactPersonRepository.Dispose();
                    #endregion

                    #region Insert Log
                    _logContactPersonRepository.Insert(contactID, "Modify", model.UserUpdate);
                    //_logContactPersonRepository.Dispose();

                    _logUserRepository.Insert(user.UserID, "Modify", model.UserUpdate);
                    //_logUserRepository.Dispose();
                    #endregion

                    #region Call API Data

                    //var systemMappingList = _systemRepository.GetSystemMappingByUsername(user.Username);

                    //string orgID = _userRepository.FindOrgIDByUsername(user.Username);

                    //var listSystem = _systemRepository.GetSystemListByUsernameForUpdate(user.Username);


                    string guid = "mockGUID";

                    if (System.Web.HttpContext.Current.Session["GUID"] != null)
                    {
                        guid = System.Web.HttpContext.Current.Session["GUID"].ToString();
                    }

                    string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();

                    var listSystem = _systemConfigureAPIRepository.GetSystemListByUsernameForUpdateAPI(user.Username);

                    foreach (var sysMapping in listSystem)
                    {
                        string reqID = GeneratorGuid.GetRandomGuid();

                        string Jsonstr = "";

                        string _APIKey = "";

                        string _APIName = "";

                        var apiUpdateModels = GetDataCallUpdateUserAPI(sysMapping, reqID, sysMapping.Username);
                        Jsonstr = JsonConvert.SerializeObject(apiUpdateModels);
                        _APIKey = apiUpdateModels.Request.APIKey;
                        _APIName = apiUpdateModels.Request.APIName;

                        HttpClient client = new HttpClient();
                        client.BaseAddress = new Uri(sysMapping.API_URL);
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                        HttpContent content = new StringContent(Jsonstr);
                        content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                        #region Insert API Log Before

                        var _APILogsBefore = new Tbl_APILogs()
                        {
                            ReqID = reqID,
                            SystemID = sysMapping.SystemID,
                            Events = "Request_Before",
                            Events_Time = DateTime.UtcNow,
                            IPAddress_Client = visitorsIPAddr,
                            APIKey = _APIKey,
                            APIName = _APIName,
                            Return_Code = "",
                            Return_Status = "",
                            Return_Message = "",
                            Data = Jsonstr,
                            //Data = responseBody,
                            UserGUID = guid,
                        };

                        _aPILogsRepository.Insert(_APILogsBefore);
                        #endregion

                        #region Call API
                        HttpResponseMessage response = await client.PostAsync(sysMapping.API_URL, content);

                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            var responseBody = response.Content.ReadAsStringAsync().Result;
                            try
                            {
                                if (!string.IsNullOrEmpty(responseBody))
                                {
                                    JObject _response = JObject.Parse(responseBody);

                                    string return_Code = _response["Response"]["Result"]["Code"].ToString();
                                    string return_Status = _response["Response"]["Result"]["SuccessStatus"].ToString();
                                    string return_Message = _response["Response"]["Result"]["Message"].ToString();


                                    #region Insert API Log

                                    var _APILogs = new Tbl_APILogs()
                                    {
                                        ReqID = reqID,
                                        SystemID = sysMapping.SystemID,
                                        Events = "Request",
                                        Events_Time = DateTime.UtcNow,
                                        IPAddress_Client = visitorsIPAddr,
                                        APIKey = _APIKey,
                                        APIName = _APIName,
                                        Return_Code = return_Code,
                                        Return_Status = return_Status,
                                        Return_Message = return_Message,
                                        Data = Jsonstr,
                                        //Data = responseBody,
                                        UserGUID = guid,
                                    };

                                    _aPILogsRepository.Insert(_APILogs);
                                    #endregion

                                    if (!Convert.ToBoolean(return_Status))
                                    {
                                        systemUnSuccessCall += sysMapping.SystemName + ",";
                                    }
                                }
                                else
                                {
                                    #region Insert API Log

                                    var _APILogs = new Tbl_APILogs()
                                    {
                                        ReqID = reqID,
                                        SystemID = sysMapping.SystemID,
                                        Events = "Request",
                                        Events_Time = DateTime.UtcNow,
                                        IPAddress_Client = visitorsIPAddr,
                                        APIKey = _APIKey,
                                        APIName = _APIName,
                                        Return_Code = "",
                                        Return_Status = "",
                                        Return_Message = "ResponseBody Empty",
                                        Data = Jsonstr,
                                        //Data = responseBody,
                                        UserGUID = guid,
                                    };

                                    _aPILogsRepository.Insert(_APILogs);
                                    #endregion

                                    systemUnSuccessCall += sysMapping.SystemName + ",";
                                }

                            }
                            catch (Exception ex)
                            {
                                #region Insert API Log
                                string guidCall = "mockGUID";

                                if (System.Web.HttpContext.Current.Session["GUID"] != null)
                                {
                                    guidCall = System.Web.HttpContext.Current.Session["GUID"].ToString();
                                }

                                string visitorsIPAddrCall = GetClientIPAddressHelper.GetClientIPAddres();


                                var _APILogsCheck = new Tbl_APILogs()
                                {
                                    ReqID = reqID,
                                    SystemID = sysMapping.SystemID,
                                    Events = "Request_Check",
                                    Events_Time = DateTime.UtcNow,
                                    IPAddress_Client = visitorsIPAddrCall,
                                    APIKey = _APIKey,
                                    APIName = _APIName,
                                    Return_Code = "",
                                    Return_Status = response.StatusCode.ToString(),
                                    Return_Message = responseBody,
                                    Data = Jsonstr,
                                    //Data = responseBody,
                                    UserGUID = guidCall,
                                };

                                _aPILogsRepository.Insert(_APILogsCheck);


                                #endregion

                                throw;
                            }

                        }
                        else
                        {

                            #region Insert API Log

                            var _APILogs = new Tbl_APILogs()
                            {
                                ReqID = reqID,
                                Events = "Request",
                                Events_Time = DateTime.UtcNow,
                                IPAddress_Client = visitorsIPAddr,
                                APIKey = _APIKey,
                                APIName = _APIName,
                                Return_Code = "",
                                Return_Status = "",
                                Return_Message = "",
                                Data = Jsonstr,
                                //Data = responseBody,
                                UserGUID = guid,
                            };

                            _aPILogsRepository.Insert(_APILogs);

                            systemUnSuccessCall += sysMapping.SystemName + ",";

                            #endregion
                        }

                        #endregion

                        #region  API Post Task
                        //string responseBody = "";
                        //var continuationTask = client.PostAsync(sysMapping.API_URL, content).ContinueWith(
                        //                        (postTask) =>
                        //                        {
                        //                            bool isCompleted = postTask.IsCompleted;
                        //                            bool isFaulted = postTask.IsFaulted;
                        //                            if (isCompleted && !isFaulted)
                        //                            {
                        //                                var response = postTask.Result;
                        //                                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        //                                {
                        //                                    try
                        //                                    {
                        //                                        if (!string.IsNullOrEmpty(response.Content.ReadAsStringAsync().Result))
                        //                                        {
                        //                                            responseBody = response.Content.ReadAsStringAsync().Result;
                        //                                            JObject _response = JObject.Parse(responseBody);

                        //                                            string return_Code = _response["Response"]["Result"]["Code"].ToString();
                        //                                            string return_Status = _response["Response"]["Result"]["SuccessStatus"].ToString();
                        //                                            string return_Message = _response["Response"]["Result"]["Message"].ToString();


                        //                                            #region Insert API Log

                        //                                            var _APILogs = new Tbl_APILogs()
                        //                                            {
                        //                                                ReqID = reqID,
                        //                                                SystemID = sysMapping.SystemID,
                        //                                                Events = "Request",
                        //                                                Events_Time = DateTime.UtcNow,
                        //                                                IPAddress_Client = visitorsIPAddr,
                        //                                                APIKey = _APIKey,
                        //                                                APIName = _APIName,
                        //                                                Return_Code = return_Code,
                        //                                                Return_Status = return_Status,
                        //                                                Return_Message = return_Message,
                        //                                                Data = Jsonstr,
                        //                                                //Data = responseBody,
                        //                                                UserGUID = guid,
                        //                                            };

                        //                                            _aPILogsRepository.Insert(_APILogs);
                        //                                            #endregion

                        //                                            if (!Convert.ToBoolean(return_Status))
                        //                                            {
                        //                                                systemUnSuccessCall += sysMapping.SystemName + ",";
                        //                                            }
                        //                                        }
                        //                                        //else
                        //                                        //{
                        //                                        //    #region Insert API Log

                        //                                        //    var _APILogs = new Tbl_APILogs()
                        //                                        //    {
                        //                                        //        ReqID = reqID,
                        //                                        //        SystemID = sysMapping.SystemID,
                        //                                        //        Events = "Request",
                        //                                        //        Events_Time = DateTime.UtcNow,
                        //                                        //        IPAddress_Client = visitorsIPAddr,
                        //                                        //        APIKey = _APIKey,
                        //                                        //        APIName = _APIName,
                        //                                        //        Return_Code = "",
                        //                                        //        Return_Status = "",
                        //                                        //        Return_Message = "ResponseBody Empty UpdateGeneralData",
                        //                                        //        Data = Jsonstr,
                        //                                        //        //Data = responseBody,
                        //                                        //        UserGUID = guid,
                        //                                        //    };

                        //                                        //    _aPILogsRepository.Insert(_APILogs);
                        //                                        //    #endregion

                        //                                        //    systemUnSuccessCall += sysMapping.SystemName + ",";
                        //                                        //}

                        //                                    }
                        //                                    catch (Exception ex)
                        //                                    {
                        //                                        #region Insert API Log
                        //                                        string guidCall = "mockGUID";

                        //                                        if (System.Web.HttpContext.Current.Session["GUID"] != null)
                        //                                        {
                        //                                            guidCall = System.Web.HttpContext.Current.Session["GUID"].ToString();
                        //                                        }

                        //                                        string visitorsIPAddrCall = GetClientIPAddressHelper.GetClientIPAddres();


                        //                                        var _APILogsCheck = new Tbl_APILogs()
                        //                                        {
                        //                                            ReqID = reqID,
                        //                                            SystemID = sysMapping.SystemID,
                        //                                            Events = "Request_Check",
                        //                                            Events_Time = DateTime.UtcNow,
                        //                                            IPAddress_Client = visitorsIPAddrCall,
                        //                                            APIKey = _APIKey,
                        //                                            APIName = _APIName,
                        //                                            Return_Code = "",
                        //                                            Return_Status = response.StatusCode.ToString(),
                        //                                            Return_Message = responseBody,
                        //                                            Data = Jsonstr,
                        //                                            //Data = responseBody,
                        //                                            UserGUID = guidCall,
                        //                                        };

                        //                                        _aPILogsRepository.Insert(_APILogsCheck);


                        //                                        #endregion

                        //                                        throw;
                        //                                    }
                        //                                }
                        //                                else
                        //                                {

                        //                                    #region Insert API Log

                        //                                    var _APILogs = new Tbl_APILogs()
                        //                                    {
                        //                                        ReqID = reqID,
                        //                                        Events = "Request",
                        //                                        Events_Time = DateTime.UtcNow,
                        //                                        IPAddress_Client = visitorsIPAddr,
                        //                                        APIKey = _APIKey,
                        //                                        APIName = _APIName,
                        //                                        Return_Code = "",
                        //                                        Return_Status = "",
                        //                                        Return_Message = "",
                        //                                        Data = Jsonstr,
                        //                                        //Data = responseBody,
                        //                                        UserGUID = guid,
                        //                                    };

                        //                                    _aPILogsRepository.Insert(_APILogs);

                        //                                    systemUnSuccessCall += sysMapping.SystemName + ",";

                        //                                    #endregion
                        //                                }
                        //                            }

                        //                        });
                        //continuationTask.Wait(20000);
                        #endregion

                    }

                    #endregion
                }



            }
            catch (Exception e)
            {

                throw;
            }

            return systemUnSuccessCall;
        }

        public virtual ViewDataProfileModels GetProfileData(string username)
        {
            ViewDataProfileModels model = new ViewDataProfileModels();

            //int isInter = LanguageHelper.GetLanguageIsInter();

            var user = _userRepository.FindByUsername(username);

            if (user!=null)
            {
                int contactID = user.ContactID??0;

                var person = _contactPersonRepository.GetContactPersonByContectID(contactID);

                GeneralModels generalDataModel = new GeneralModels
                {
                    UserID = user.UserID,
                    UserLoginID = user.Username,
                    Email = person.Email,
                    InitialEmail = person.Email,
                    TitleID = person.TitleID,
                    TitleID_Inter = person.TitleID,
                    FirstName_Local = person.FirstName_Local,
                    LastName_Local = person.LastName_Local,
                    FirstName_Inter = person.FirstName_Inter,
                    LastName_Inter = person.LastName_Inter,
                    JobTitleID = person.JobTitleID,
                    OtherJobTitle = person.OtherJobTitle,
                    Department = person.Department,
                    PhoneNo = person.PhoneNo,
                    PhoneExt = person.PhoneExt,
                    MobileNo = person.MobileNo,
                    FaxNo = person.FaxNo,
                    FaxExt = person.FaxExt,
                    TimeZoneIDString = user.TimeZone,
                    LocaleName = user.Locale,
                    CurrencyCode = user.Currency,
                    LanguageCode = user.LanguageCode,
                    IsPublic = Convert.ToBoolean(user.IsPublic??0)
                };

                var addressMapping =  _orgContactAddressMappingRepository.GetOrgContactAddrMappingByContactID(contactID);

                if (addressMapping != null)
                {
                    var address = _addressRepository.GetAddressByAddressID(addressMapping.AddressID??0);
                    string address_CountryName = "";
                    if (address != null)
                    {
                        generalDataModel.AddressID = address.AddressID;

                        generalDataModel.Address_HouseNo_Local = address.HouseNo_Local;
                        generalDataModel.Address_VillageNo_Local = address.VillageNo_Local;
                        generalDataModel.Address_Lane_Local = address.Lane_Local;
                        generalDataModel.Address_Road_Local = address.Road_Local;
                        generalDataModel.Address_SubDistrict_Local = address.SubDistrict_Local;
                        generalDataModel.Address_City_Local = address.City_Local;
                        generalDataModel.Address_State_Local = address.State_Local;

                        //generalDataModel.Address_HouseNo_Inter = address.HouseNo_Inter;
                        //generalDataModel.Address_VillageNo_Inter = address.VillageNo_Inter;
                        //generalDataModel.Address_Lane_Inter = address.Lane_Inter;
                        //generalDataModel.Address_Road_Inter = address.Road_Inter;
                        //generalDataModel.Address_SubDistrict_Inter = address.SubDistrict_Inter;
                        //generalDataModel.Address_City_Inter = address.City_Inter;
                        //generalDataModel.Address_State_Inter = address.State_Inter;

                        generalDataModel.Address_PostalCode = address.PostalCode;
                        generalDataModel.Address_CountryCode = address.CountryCode;

                        address_CountryName = _countryRepository.GetCountryNameByCountryCode(address.CountryCode);

                        generalDataModel.Address_CountryName = address_CountryName;
                    }
                }

                List<Tbl_OPNBuyerSupplierMapping> modelResults = _opnBuyerSupplierMappingRepository.GetOPNBuyerSupplierMappingBySupplierId(user.SupplierID.Value);
                List<Tbl_OPNBuyerSupplierMappingViewModels> opnBuyerSupplierMappingList = new List<Tbl_OPNBuyerSupplierMappingViewModels>();

                model.OPNBuyerSupplierMappings = new List<Tbl_OPNBuyerSupplierMappingViewModels>();
                if (modelResults != null)
                {
                    foreach(Tbl_OPNBuyerSupplierMapping item in modelResults)
                    {
                        Tbl_OPNBuyer buyerModel = _opnBuyerRepository.getOPNBuyer(item.BuyerID.Value);
                        Tbl_OPNBuyerViewModels opnBuyerViewModel = new Tbl_OPNBuyerViewModels();
                        if (buyerModel != null)
                        {
                            opnBuyerViewModel.ID = buyerModel.ID;
                            opnBuyerViewModel.BuyerOrgID = buyerModel.BuyerOrgID;
                            opnBuyerViewModel.BuyerName = buyerModel.BuyerName;
                            opnBuyerViewModel.BuyerID = buyerModel.BuyerID;
                            opnBuyerViewModel.InvitationCode = buyerModel.InvitationCode;
                            opnBuyerViewModel.EffectiveDate = buyerModel.EffectiveDate;
                            opnBuyerViewModel.ExpireDate = buyerModel.ExpireDate;
                            opnBuyerViewModel.CreateDate = buyerModel.CreateDate;
                        }
                        
                        Tbl_OPNBuyerSupplierMappingViewModels tmpModel = new Tbl_OPNBuyerSupplierMappingViewModels
                        {
                            ID = item.ID,
                            BuyerID = item.BuyerID,
                            SupplierID  = item.SupplierID,
                            RegID = item.RegID,
                            BuyerOrgID  = item.BuyerOrgID,
                            PISApproved = item.PISApproved,
                            IsJobSuccess = item.IsJobSuccess,
                            JobErrorMsg = item.JobErrorMsg,
                            InvitationCode = item.InvitationCode,
                            CreatedDate = item.CreatedDate,
                            UpdatedDate = item.UpdatedDate,
                            OPNBuyer = opnBuyerViewModel

                        };

                        model.OPNBuyerSupplierMappings.Add(tmpModel);
                    }
                }
                

                model.GeneralData = generalDataModel;

            }
           
            return model;
        }

        public virtual API_UpdateUserModels GetDataCallUpdateUserAPI(SystemRoleListModels modelsData, string reqID, string userInsert, string action = "UpdateUser")
        {
            var user = _userRepository.FindAllByUsername(modelsData.Username);

            int isInter = LanguageHelper.GetLanguageIsInter();

            var updateUserModels = new API_UpdateUserModels();

            if (user != null)
            {
                var requestData = new RequestUpdate();               

                requestData.ReqID = reqID;
                requestData.APIName = modelsData.API_Name;
                requestData.APIKey = modelsData.API_Key;


                requestData.Data = new DataUpdate();

                requestData.Data.Action = action;

                var listPortalLoginID = new List<PortalLoginID>();

                var portalLoginID = new PortalLoginID();

                portalLoginID.value = user.UserID.ToString();

                listPortalLoginID.Add(portalLoginID);

                requestData.Data.PortalLoginID = listPortalLoginID;

                var listLoginID = new List<LoginID>();

                var userLoginMappingTbl = _userSystemMappingRepository.GetUserSystemMappingMergeAll(user.Username, modelsData.SystemID);               

                if (userLoginMappingTbl.Count() > 0)
                {
                    bool isPrimary = true;
                    foreach (var valueSys in userLoginMappingTbl)
                    {

                        if (valueSys.MergeFromUserID == 0)
                        {
                            isPrimary = true;
                        }else
                        {
                            isPrimary = false;
                        }
                        var loginID = new LoginID();

                        loginID.value = valueSys.SysUserID;
                        loginID.IsPrimary = isPrimary;

                        listLoginID.Add(loginID);
                    }
                }

                requestData.Data.LoginID = listLoginID;

                int sysRoleID = 0;
                if (modelsData.SysRoleID == "PrimaryUser")
                {
                    sysRoleID = _userSystemMappingRepository.GetSysRoleIDPrimaryUserByUserIDAndSystemID(user.UserID, modelsData.SystemID);
                }else
                {
                    if (!string.IsNullOrEmpty(modelsData.SysRoleID))
                    {
                        sysRoleID = Convert.ToInt32(modelsData.SysRoleID);
                    }
                }
                // "PrimaryUser" ฟิกไว้เพื่อไป Get ค่า SysRoleID ของ User ที่เป็น Primary กรณีที่ต้อง call update ใน user ที่มาจากการ merge OP ซึ่งอาจมาหลาย SysRoleID
                SupplierInfo supplierInfo = new SupplierInfo
                {
                    SupplierShortName = _userRepository.FindOrgIDByUsername(modelsData.Username)??"",
                    BSPUserGroupID = _acl_SystemRoleRepository.GetSysRoleValueBySysRoleID(sysRoleID)??""

                };

                requestData.Data.SupplierInfo = supplierInfo;

                


                var orgTbl = _organizationRepository.GetOrganizationByOrgID(user.OrgID);

                OrgInfo orgInfo = new OrgInfo
                {
                    PursiteID = orgTbl.PursiteID.ToString()??"",
                    OrgID = orgTbl.OrgID??"",
                    OrgName = (isInter == 1 ? orgTbl.CompanyName_Inter : orgTbl.CompanyName_Local) ?? "",
                    PhoneNo = orgTbl.PhoneNo ?? "",
                    PhoneExt = orgTbl.PhoneExt ?? "",
                    MobileNo = orgTbl.MobileNo ?? "",
                    FaxNo = orgTbl.FaxNo ?? "",
                    FaxExt = orgTbl.FaxExt ?? ""

                };

                requestData.Data.OrgInfo = orgInfo;

                var timezone = _timeZoneRepository.GetTimeZoneByTimeZoneIDString(user.TimeZone);

                int minuteOffset = 0;

                if (timezone != null)
                {
                    minuteOffset = timezone.MinuteOffset ?? 0;
                }

                UserInfoUpdate userInfoUpdate = new UserInfoUpdate
                {

                    Locale = user.Locale ?? "",
                    Timezone = minuteOffset,
                    CurrencyCode = user.Currency ?? "",
                    LanguageCode = user.LanguageCode ?? "",
                    //IsDisabled = Convert.ToBoolean(modelsData.IsChecked),
                    IsDisabled =  modelsData.IsChecked == 1 ? false : true,
                    IsDeleted = Convert.ToBoolean(user.IsDeleted ?? 0)
                };

                requestData.Data.UserInfo = userInfoUpdate;

                int contactID = user.ContactID ?? 0;

                var contactPerson = _contactPersonRepository.GetContactPersonByContectID(contactID);

               

                UserContact userContact = new UserContact
                {
                    FirstName = (isInter==1?contactPerson.FirstName_Inter:contactPerson.FirstName_Local)??"",
                    LastName = (isInter == 1 ? contactPerson.LastName_Inter : contactPerson.LastName_Local) ?? "",
                    Department = contactPerson.Department??"",
                    PhoneNo = contactPerson.PhoneNo ?? "",
                    PhoneExt = contactPerson.PhoneExt ?? "",
                    MobileNo = contactPerson.MobileNo ?? "",
                    FaxNo = contactPerson.FaxNo ?? "",
                    Email = contactPerson.Email ?? ""
                };

                requestData.Data.UserContact = userContact;

                var addressTbl = _addressRepository.GetAddressByUsername(user.Username);
                string countryName = "";
                if (addressTbl != null)
                {
                    string languageId = "";
                    var countryAddress = _countryRepository.GetCountryByCountryCode(addressTbl.CountryCode);
                    
                    if (countryAddress != null)
                    {
                        if(isInter == 1)
                        {
                            var _tempLanguage = _languageRepository.GetInterLanguage();
                            languageId = _tempLanguage.LanguageID.ToString();
                        }
                        else
                        {
                            var _tempLanguage = _languageRepository.GetLocalLanguage();
                            languageId = _tempLanguage.LanguageID.ToString();
                        }
                        countryName = Framework.Localization.GetLocalizedProperty.GetLocalizedValue(languageId, "Tbl_Country", "CountryName", countryAddress.CountryID.ToString());

                    }
                }


                Address address = new Address
                {
                    HouseNo = (isInter == 1 ? addressTbl.HouseNo_Inter : addressTbl.HouseNo_Local) ?? "",
                    VillageNo = (isInter == 1 ? addressTbl.VillageNo_Inter : addressTbl.VillageNo_Local) ?? "",
                    Lane = (isInter == 1 ? addressTbl.Lane_Inter : addressTbl.Lane_Local) ?? "",
                    Road = (isInter == 1 ? addressTbl.Road_Inter : addressTbl.Road_Local) ?? "",
                    SubDistrict = (isInter == 1 ? addressTbl.SubDistrict_Inter : addressTbl.SubDistrict_Local) ?? "",
                    City = (isInter == 1 ? addressTbl.City_Inter : addressTbl.City_Local) ?? "",
                    State = (isInter == 1 ? addressTbl.State_Inter : addressTbl.State_Local) ?? "",
                    PostalCode = addressTbl.PostalCode ?? "",
                    CountryCode = addressTbl.CountryCode ?? "",
                    CountryName = countryName ?? ""
                };

                requestData.Data.Address = address;

                var userMappingTbl = _userSystemMappingRepository.GetUserSystemMappingNonCheckMerg(userInsert, modelsData.SystemID);


                var listSysUserID = new List<SysUserID>();

                if (userMappingTbl.Count() > 0)
                {

                    foreach (var valueSys in userMappingTbl)
                    {
                        var sysUserID = new SysUserID();

                        sysUserID.value = valueSys.SysUserID;

                        listSysUserID.Add(sysUserID);
                    }
                }

                AddBy editBy = new AddBy
                {
                    SysUserID = listSysUserID
                };


                requestData.Data.EditBy = editBy;

                updateUserModels.Request = requestData;

            }


            return updateUserModels;
        }

        public virtual API_InsertUserModels GetDataCallInsertUserAPI(SystemRoleListModels modelsData, string reqID,string userInsert)
        {
            var user = _userRepository.FindAllByUsername(modelsData.Username);

            var insertUserModels = new API_InsertUserModels();

            if (user!=null)
            {
                var requestData = new Request();

                requestData.ReqID = reqID;
                requestData.APIKey = modelsData.API_Key;
                requestData.APIName = modelsData.API_Name;

                requestData.Data = new DataInsert();

                var listPortalLoginID = new List<PortalLoginID>();

                var portalLoginID = new PortalLoginID();

                portalLoginID.value = user.UserID.ToString();

                listPortalLoginID.Add(portalLoginID);

                requestData.Data.PortalLoginID = listPortalLoginID;

                int sysRoleID = 0;
                if (!string.IsNullOrEmpty(modelsData.SysRoleID))
                {
                    sysRoleID = Convert.ToInt32(modelsData.SysRoleID);
                }
                SupplierInfo supplierInfo = new SupplierInfo
                {
                    SupplierShortName = _userRepository.FindOrgIDByUsername(modelsData.Username)??"",
                    BSPUserGroupID = _acl_SystemRoleRepository.GetSysRoleValueBySysRoleID(sysRoleID)??""

                };               

                requestData.Data.SupplierInfo = supplierInfo;

                HttpCookie cultureCookie = HttpContext.Current.Request.Cookies["_culture"];

                string languageID = "";

                if (cultureCookie != null)
                {
                    languageID = cultureCookie.Value.ToString();

                }
                if (string.IsNullOrEmpty(languageID))
                {
                    languageID = SupplierPortal.Framework.Localization.CultureHelper.GetImplementedCulture(languageID); // This is safe
                }
                int languageId = Convert.ToInt32(languageID);

                var _tempLanguage = _languageRepository.GetLanguageById(languageId);
                int isInter = 0;
                if (_tempLanguage != null)
                {
                    isInter = _tempLanguage.isInter ?? 0;
                }

                var orgTbl = _organizationRepository.GetOrganizationByOrgID(user.OrgID);

                OrgInfo orgInfo = new OrgInfo
                {
                    PursiteID = orgTbl.PursiteID.ToString()??"",
                    OrgID = orgTbl.OrgID??"",
                    OrgName = (isInter == 1 ? orgTbl.CompanyName_Inter : orgTbl.CompanyName_Local) ?? "",
                    PhoneNo = orgTbl.PhoneNo??"",
                    PhoneExt = orgTbl.PhoneExt??"",
                    MobileNo = orgTbl.MobileNo??"",
                    FaxNo = orgTbl.FaxNo??"",
                    FaxExt = orgTbl.FaxExt??""

                };

                requestData.Data.OrgInfo = orgInfo;

                var timezone = _timeZoneRepository.GetTimeZoneByTimeZoneIDString(user.TimeZone);

                int minuteOffset = 0;

                if (timezone !=null)
                {
                    minuteOffset = timezone.MinuteOffset ?? 0;
                }

                

                UserInfo userInfo = new UserInfo
                {
                    LoginID = user.Username??"",
                    Locale = user.Locale ?? "",
                    Timezone = minuteOffset,
                    CurrencyCode = user.Currency ?? "",
                    LanguageCode = user.LanguageCode ?? "",
                    //IsDisabled = Convert.ToBoolean(modelsData.IsChecked),
                    IsDisabled = modelsData.IsChecked==1?false:true,
                    IsDeleted = Convert.ToBoolean(user.IsDeleted??0)
                };

                requestData.Data.UserInfo = userInfo;

                int contactID = user.ContactID ?? 0;

                var contactPerson = _contactPersonRepository.GetContactPersonByContectID(contactID);

                UserContact userContact = new UserContact 
                {
                    FirstName = (isInter==1?contactPerson.FirstName_Inter:contactPerson.FirstName_Local)??"",
                    LastName = (isInter==1?contactPerson.LastName_Inter:contactPerson.LastName_Local)??"",
                    Department = contactPerson.Department??"",
                    PhoneNo = contactPerson.PhoneNo??"",
                    PhoneExt = contactPerson.PhoneExt??"",
                    MobileNo = contactPerson.MobileNo??"",
                    FaxNo = contactPerson.FaxNo??"",
                    Email = contactPerson.Email ?? ""
                };

                requestData.Data.UserContact = userContact;


                var addressTbl = _addressRepository.GetAddressByUsername(user.Username);
                string countryName = "";
                if (addressTbl != null)
                {
                    countryName = _countryRepository.GetCountryNameByCountryCode(addressTbl.CountryCode);
                }
                

                Address address = new Address
                {
                    HouseNo = (isInter == 1 ? addressTbl.HouseNo_Inter : addressTbl.HouseNo_Local)??"",
                    VillageNo = (isInter == 1 ? addressTbl.VillageNo_Inter : addressTbl.VillageNo_Local) ?? "",
                    Lane = (isInter == 1 ? addressTbl.Lane_Inter : addressTbl.Lane_Local) ?? "",
                    Road = (isInter == 1 ? addressTbl.Road_Inter : addressTbl.Road_Local) ?? "",
                    SubDistrict = (isInter == 1 ? addressTbl.SubDistrict_Inter : addressTbl.SubDistrict_Local) ?? "",
                    City = (isInter == 1 ? addressTbl.City_Inter : addressTbl.City_Local) ?? "",
                    State = (isInter == 1 ? addressTbl.State_Inter : addressTbl.State_Local) ?? "",
                    PostalCode = addressTbl.PostalCode ?? "",
                    CountryCode = addressTbl.CountryCode ?? "",
                    CountryName = countryName ?? ""
                };
                requestData.Data.Address = address;

                var userMappingTbl = _userSystemMappingRepository.GetUserSystemMapping(userInsert, modelsData.SystemID);


                var listSysUserID = new List<SysUserID>();

                if (userMappingTbl.Count()>0)
                {

                    foreach (var valueSys in userMappingTbl)
                    {
                        var sysUserID = new SysUserID();

                        sysUserID.value = valueSys.SysUserID;

                        listSysUserID.Add(sysUserID);
                    }
                }

                AddBy addBy = new AddBy
                {
                    SysUserID = listSysUserID
                };


                requestData.Data.AddBy = addBy;


                insertUserModels.Request = requestData;
            }
            
            

            return insertUserModels;
        }

        public virtual string CallAPIInsertOrUpdateBSPUserPreference(BSP_UserPreferenceModels model, string languageId)
        {
            string systemUnSuccessCall = "";
            string messageReturn = "";
            string errorMessage = "";
            try
            {
                var user = _userRepository.FindUserByUserID(model.UserID);

                if(user != null)
                {
                     string reqID = GeneratorGuid.GetRandomGuid();

                     string jsonStr = "";

                     string _APIKey = "";

                     string _APIName = "";

                     string _APIProc = "";

                    string guid = "mockGUID";

                    if (System.Web.HttpContext.Current.Session["GUID"] != null)
                    {
                        guid = System.Web.HttpContext.Current.Session["GUID"].ToString();
                    }

                    string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();

                    if (model.SystemRole.API_Name == "UpdateUser")
                     {
                         var apiUpdateModels = GetDataCallUpdateUserAPI(model.SystemRole, reqID, model.UsernameCurrent);
                         jsonStr = JsonConvert.SerializeObject(apiUpdateModels);
                         _APIKey = apiUpdateModels.Request.APIKey;
                         _APIName = apiUpdateModels.Request.APIName;
                         _APIProc = "Update";
                     }
                     else
                     {
                         var apiInsertModels = GetDataCallInsertUserAPI(model.SystemRole, reqID, model.UsernameCurrent);
                         jsonStr = JsonConvert.SerializeObject(apiInsertModels);
                         _APIKey = apiInsertModels.Request.APIKey;
                         _APIName = apiInsertModels.Request.APIName;
                         _APIProc = "Insert";
                     }

                     HttpClient client = new HttpClient();
                     client.BaseAddress = new Uri(model.SystemRole.API_URL);
                     client.DefaultRequestHeaders.Accept.Clear();
                     client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                     HttpContent content = new StringContent(jsonStr);
                     content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                    #region Insert API Log Before

                    var _APILogsBefore = new Tbl_APILogs()
                    {
                        ReqID = reqID,
                        SystemID = model.SystemRole.SystemID,
                        Events = "Request_Before",
                        Events_Time = DateTime.UtcNow,
                        IPAddress_Client = visitorsIPAddr,
                        APIKey = _APIKey,
                        APIName = _APIName,
                        Return_Code = "",
                        Return_Status = "",
                        Return_Message = "",
                        Data = jsonStr,
                        //Data = responseBody,
                        UserGUID = guid,
                    };

                    _aPILogsRepository.Insert(_APILogsBefore);
                    #endregion

                    #region Call API
                    HttpResponseMessage response = client.PostAsync(model.SystemRole.API_URL, content).Result;

                    var responseBody = response.Content.ReadAsStringAsync().Result;

                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        if(!string.IsNullOrEmpty(responseBody))
                        {
                        JObject _response = JObject.Parse(responseBody);

                        string return_Code = _response["Response"]["Result"]["Code"].ToString();
                        string return_Status = _response["Response"]["Result"]["SuccessStatus"].ToString();
                        string return_Message = _response["Response"]["Result"]["Message"].ToString();

                        #region Insert API Log

                        var _APILogs = new Tbl_APILogs()
                        {
                            ReqID = reqID,
                            SystemID = model.SystemRole.SystemID,
                            Events = "Request_Before",
                            Events_Time = DateTime.UtcNow,
                            IPAddress_Client = visitorsIPAddr,
                            APIKey = _APIKey,
                            APIName = _APIName,
                            Return_Code = return_Code,
                            Return_Status = return_Status,
                            Return_Message = return_Message,
                            Data = jsonStr,
                            //Data = responseBody,
                            UserGUID = guid,
                        };

                        _aPILogsRepository.Insert(_APILogs);
                        #endregion

                        if (!Convert.ToBoolean(return_Status))
                        {
                            if (_APIProc == "Update")
                            {
                                errorMessage = messageReturn.GetStringResource("_Profile.ErrorMsg.UpdateSysUserFail", languageId);
                                systemUnSuccessCall = errorMessage.Replace("<%SystemName%>", model.SystemRole.SystemName);
                            }
                            else
                            {
                                errorMessage = messageReturn.GetStringResource("_Profile.ErrorMsg.CreateSysUserFail", languageId);
                                systemUnSuccessCall = errorMessage.Replace("<%SystemName%>", model.SystemRole.SystemName);
                            }
                        }

                        #region Mapping User

                        //string testReply = @"{'Response':{'ReqID':'568d3fd5ed6f47b5bbeb2351b2dd3859','Result':{'Code':'200','SuccessStatus':'true','Message':'Success'},'Data':[{'SysUserID':[{'Value':'S00031-User1'}]}]}}";

                        if (Convert.ToBoolean(return_Status))
                        //if (true)
                        {
                            API_ReplyDataModels replyModels = JsonConvert.DeserializeObject<API_ReplyDataModels>(responseBody);


                            if (replyModels.Response.data.Count() > 0)
                            {


                                foreach (IList<SysUserIDResponse> dataValue in replyModels.Response.data.Select(m => m.SysUserID))
                                {

                                    foreach (var sysUserResponse in dataValue)
                                    {
                                        if (_APIProc == "Insert")
                                        {
                                            int userID = _userRepository.GetUserIDByUsername(model.SystemRole.Username);

                                            var _InsertUserSystemMapping = new Tbl_UserSystemMapping()
                                            {
                                                UserID = userID,
                                                SysUserID = sysUserResponse.value,
                                                SysPassword = "",
                                                SystemID = model.SystemRole.SystemID,
                                                SysRoleID = Convert.ToInt32(model.SystemRole.SysRoleID),
                                                isEnableService = Convert.ToInt32(model.SystemRole.IsChecked),
                                                MergeFromUserID = 0
                                            };
                                            _userSystemMappingRepository.Insert(_InsertUserSystemMapping);

                                            #region Update Tbl_UserServiceMapping
                                            // Map Service OP
                                            if (!_userServiceMappingRepository.CheckExistsUserServiceMapping(userID, 1))
                                            {
                                                var userServiceMappingModel = new Tbl_UserServiceMapping()
                                                {
                                                    UserID = userID,
                                                    ServiceID = 1,
                                                    isActive = 1
                                                };
                                                int updateByUserID = 0;
                                                var usernameCurrent = _userRepository.FindByUsername(model.UsernameCurrent);
                                                if (usernameCurrent != null)
                                                {
                                                    updateByUserID = usernameCurrent.UserID;
                                                }
                                                _userServiceMappingRepository.Insert(userServiceMappingModel, updateByUserID);
                                            }
                                            #endregion
                                        }
                                        //else
                                        //{
                                        //    int userID = _userRepository.GetUserIDByUsername(model.SystemRole.Username);
                                        //    var _UpdateUserSystemMapping = new Tbl_UserSystemMapping()
                                        //    {
                                        //        UserID = userID,
                                        //        SysUserID = sysUserResponse.value,
                                        //        SysPassword = "",
                                        //        SystemID = model.SystemRole.SystemID,
                                        //        SysRoleID = Convert.ToInt32(model.SystemRole.SysRoleID),
                                        //        isEnableService = Convert.ToInt32(model.SystemRole.IsChecked),
                                        //        MergeFromUserID = 0
                                        //    };
                                        //    _userSystemMappingRepository.Update(_UpdateUserSystemMapping);

                                        //}
                                    }
                                    
                                }

                                    if (_APIProc == "Update")
                                    {
                                        var _updateUserSystemMapping = _userSystemMappingRepository.GetUserSystemMapping(model.SystemRole.Username, 1).FirstOrDefault(); // 1= system BSP
                                        if (_updateUserSystemMapping != null)
                                        {
                                            _updateUserSystemMapping.SysRoleID = Convert.ToInt32(model.SystemRole.SysRoleID);
                                            _updateUserSystemMapping.isEnableService = Convert.ToInt32(model.SystemRole.IsChecked);
                                            _userSystemMappingRepository.Update(_updateUserSystemMapping);
                                        }
                                    }

                                }else
                                {
                                    if (_APIProc == "Update")
                                    {
                                        errorMessage = messageReturn.GetStringResource("_Profile.ErrorMsg.UpdateSysUserFail", languageId);
                                        systemUnSuccessCall = errorMessage.Replace("<%SystemName%>", model.SystemRole.SystemName);
                                    }
                                    else
                                    {
                                        errorMessage = messageReturn.GetStringResource("_Profile.ErrorMsg.CreateSysUserFail", languageId);
                                        systemUnSuccessCall = errorMessage.Replace("<%SystemName%>", model.SystemRole.SystemName);
                                    }

                                }

                        }
                        #endregion
                        }


                    }
                    else
                    {

                        #region Insert API Log

                        var _APILogs = new Tbl_APILogs()
                        {
                            ReqID = reqID,
                            SystemID = model.SystemRole.SystemID,
                            Events = "Request",
                            Events_Time = DateTime.UtcNow,
                            IPAddress_Client = visitorsIPAddr,
                            APIKey = _APIKey,
                            APIName = _APIName,
                            Return_Code = "",
                            Return_Status = "",
                            Return_Message = "",
                            Data = jsonStr,
                            //Data = responseBody,
                            UserGUID = guid,
                        };

                        _aPILogsRepository.Insert(_APILogs);


                        if (_APIProc == "Update")
                        {
                            errorMessage = messageReturn.GetStringResource("_Profile.ErrorMsg.UpdateSysUserFail", languageId);
                            systemUnSuccessCall = errorMessage.Replace("<%SystemName%>", model.SystemRole.SystemName);
                        }
                        else
                        {
                            errorMessage = messageReturn.GetStringResource("_Profile.ErrorMsg.CreateSysUserFail", languageId);
                            systemUnSuccessCall = errorMessage.Replace("<%SystemName%>", model.SystemRole.SystemName);
                        }
                        //systemUnSuccessCall += model.SystemRole.SystemName + ",";

                        #endregion
                    }
                    #endregion

                    #region  API Post Task

                    //string responseBody = "";

                    //var continuationTask = client.PostAsync(model.SystemRole.API_URL, content).ContinueWith(
                    //    (postTask) =>
                    //    {
                    //        bool isCompleted = postTask.IsCompleted;
                    //        bool isFaulted = postTask.IsFaulted;
                    //        if (isCompleted && !isFaulted)
                    //        {
                    //            var response = postTask.Result;
                    //            if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    //            {
                    //                if (!string.IsNullOrEmpty(response.Content.ReadAsStringAsync().Result))
                    //                {
                    //                    responseBody = response.Content.ReadAsStringAsync().Result;

                    //                    JObject _response = JObject.Parse(responseBody);

                    //                    string return_Code = _response["Response"]["Result"]["Code"].ToString();
                    //                    string return_Status = _response["Response"]["Result"]["SuccessStatus"].ToString();
                    //                    string return_Message = _response["Response"]["Result"]["Message"].ToString();

                    //                    #region Insert API Log

                    //                    var _APILogs = new Tbl_APILogs()
                    //                    {
                    //                        ReqID = reqID,
                    //                        SystemID = model.SystemRole.SystemID,
                    //                        Events = "Request_Before",
                    //                        Events_Time = DateTime.UtcNow,
                    //                        IPAddress_Client = visitorsIPAddr,
                    //                        APIKey = _APIKey,
                    //                        APIName = _APIName,
                    //                        Return_Code = return_Code,
                    //                        Return_Status = return_Status,
                    //                        Return_Message = return_Message,
                    //                        Data = jsonStr,
                    //                        //Data = responseBody,
                    //                        UserGUID = guid,
                    //                    };

                    //                    _aPILogsRepository.Insert(_APILogs);
                    //                    #endregion

                    //                    if (!Convert.ToBoolean(return_Status))
                    //                    {
                    //                        if (_APIProc == "Update")
                    //                        {
                    //                            errorMessage = messageReturn.GetStringResource("_Profile.ErrorMsg.UpdateSysUserFail", languageId);
                    //                            systemUnSuccessCall = errorMessage.Replace("<%SystemName%>", model.SystemRole.SystemName);
                    //                        }
                    //                        else
                    //                        {
                    //                            errorMessage = messageReturn.GetStringResource("_Profile.ErrorMsg.CreateSysUserFail", languageId);
                    //                            systemUnSuccessCall = errorMessage.Replace("<%SystemName%>", model.SystemRole.SystemName);
                    //                        }
                    //                    }

                    //                    #region Mapping User

                    //                    //string testReply = @"{'Response':{'ReqID':'568d3fd5ed6f47b5bbeb2351b2dd3859','Result':{'Code':'200','SuccessStatus':'true','Message':'Success'},'Data':[{'SysUserID':[{'Value':'S00031-User1'}]}]}}";

                    //                    if (Convert.ToBoolean(return_Status))
                    //                    //if (true)
                    //                    {
                    //                        API_ReplyDataModels replyModels = JsonConvert.DeserializeObject<API_ReplyDataModels>(responseBody);


                    //                        if (replyModels.Response.data.Count() > 0)
                    //                        {


                    //                            foreach (IList<SysUserIDResponse> dataValue in replyModels.Response.data.Select(m => m.SysUserID))
                    //                            {
                    //                                //foreach (IList<SysUserIDResponse> dataResponse in dataValue)
                    //                                //{
                    //                                foreach (var sysUserResponse in dataValue)
                    //                                {
                    //                                    if (_APIProc == "Insert")
                    //                                    {
                    //                                        int userID = _userRepository.GetUserIDByUsername(model.SystemRole.Username);

                    //                                        var _InsertUserSystemMapping = new Tbl_UserSystemMapping()
                    //                                        {
                    //                                            UserID = userID,
                    //                                            SysUserID = sysUserResponse.value,
                    //                                            SysPassword = "",
                    //                                            SystemID = model.SystemRole.SystemID,
                    //                                            SysRoleID = Convert.ToInt32(model.SystemRole.SysRoleID),
                    //                                            isEnableService = Convert.ToInt32(model.SystemRole.IsChecked),
                    //                                            MergeFromUserID = 0
                    //                                        };
                    //                                        _userSystemMappingRepository.Insert(_InsertUserSystemMapping);

                    //                                        #region Update Tbl_UserServiceMapping
                    //                                        // Map Service OP
                    //                                        if (!_userServiceMappingRepository.CheckExistsUserServiceMapping(userID, 1))
                    //                                        {
                    //                                            var userServiceMappingModel = new Tbl_UserServiceMapping()
                    //                                            {
                    //                                                UserID = userID,
                    //                                                ServiceID = 1,
                    //                                                isActive = 1
                    //                                            };
                    //                                            int updateByUserID = 0;
                    //                                            var usernameCurrent = _userRepository.FindByUsername(model.UsernameCurrent);
                    //                                            if (usernameCurrent != null)
                    //                                            {
                    //                                                updateByUserID = usernameCurrent.UserID;
                    //                                            }
                    //                                            _userServiceMappingRepository.Insert(userServiceMappingModel, updateByUserID);
                    //                                        }
                    //                                        #endregion
                    //                                    }
                    //                                    else
                    //                                    {
                    //                                        int userID = _userRepository.GetUserIDByUsername(model.SystemRole.Username);
                    //                                        var _UpdateUserSystemMapping = new Tbl_UserSystemMapping()
                    //                                        {
                    //                                            UserID = userID,
                    //                                            SysUserID = sysUserResponse.value,
                    //                                            SysPassword = "",
                    //                                            SystemID = model.SystemRole.SystemID,
                    //                                            SysRoleID = Convert.ToInt32(model.SystemRole.SysRoleID),
                    //                                            isEnableService = Convert.ToInt32(model.SystemRole.IsChecked),
                    //                                            MergeFromUserID = 0
                    //                                        };
                    //                                        _userSystemMappingRepository.Update(_UpdateUserSystemMapping);

                    //                                    }
                    //                                }
                    //                                // }
                    //                            }
                    //                        }

                    //                    }
                    //                    #endregion
                    //                }


                    //            }
                    //            else
                    //            {

                    //                #region Insert API Log

                    //                var _APILogs = new Tbl_APILogs()
                    //                {
                    //                    ReqID = reqID,
                    //                    SystemID = model.SystemRole.SystemID,
                    //                    Events = "Request",
                    //                    Events_Time = DateTime.UtcNow,
                    //                    IPAddress_Client = visitorsIPAddr,
                    //                    APIKey = _APIKey,
                    //                    APIName = _APIName,
                    //                    Return_Code = "",
                    //                    Return_Status = "",
                    //                    Return_Message = "",
                    //                    Data = jsonStr,
                    //                    //Data = responseBody,
                    //                    UserGUID = guid,
                    //                };

                    //                _aPILogsRepository.Insert(_APILogs);


                    //                if (_APIProc == "Update")
                    //                {
                    //                    errorMessage = messageReturn.GetStringResource("_Profile.ErrorMsg.UpdateSysUserFail", languageId);
                    //                    systemUnSuccessCall = errorMessage.Replace("<%SystemName%>", model.SystemRole.SystemName);
                    //                }
                    //                else
                    //                {
                    //                    errorMessage = messageReturn.GetStringResource("_Profile.ErrorMsg.CreateSysUserFail", languageId);
                    //                    systemUnSuccessCall = errorMessage.Replace("<%SystemName%>", model.SystemRole.SystemName);
                    //                }
                    //                //systemUnSuccessCall += model.SystemRole.SystemName + ",";

                    //                #endregion
                    //            }
                    //        }
                    //    });

                    //continuationTask.Wait(10000);
                    #endregion



                    #region Mapping Reports

                    var reports = _portalReportsRepository.List();

                     foreach (var reportsList in reports)
                     {
                         if (reportsList.SystemID == 1)
                         {
                             var userMapping = _userSystemMappingRepository.GetUserSystemMappingByUsernameAndSystemID(user.Username, reportsList.SystemID ?? 0);
                             if (userMapping != null)
                             {
                                 var userReportMapping = _userReportMappingRepository.GerUserReportMappingByUserIDAndReportID(userMapping.UserID, reportsList.ReportID);
                                 if (userReportMapping != null)
                                 {
                                     var newUserReportMapping = new Tbl_UserReportMapping();
                                     newUserReportMapping.UserID = userReportMapping.UserID;
                                     newUserReportMapping.ReportID = userReportMapping.ReportID;
                                     newUserReportMapping.isEnable = userMapping.isEnableService;
                                     _userReportMappingRepository.Update(newUserReportMapping);
                                 }else
                                 {
                                     int userID = _userRepository.GetUserIDByUsername(user.Username);
                                     var newUserReportMapping = new Tbl_UserReportMapping();
                                     newUserReportMapping.UserID = userID;
                                     newUserReportMapping.ReportID = reportsList.ReportID;
                                     newUserReportMapping.isEnable = userMapping.isEnableService;
                                     _userReportMappingRepository.Insert(newUserReportMapping);
                                 }
                             }
                         }else
                         {
                             if (reportsList.ReportID == 1)
                             {
                                 if (model.SystemRole.SysRoleID == "2" || model.SystemRole.SysRoleID == "1")
                                 {
                                     int isEnable = 1;

                                     int userID = _userRepository.GetUserIDByUsername(user.Username);
                                     var userReportMapping = _userReportMappingRepository.GerUserReportMappingByUserIDAndReportID(userID, reportsList.ReportID);
                                     if (userReportMapping != null)
                                     {
                                         var newUserReportMapping = new Tbl_UserReportMapping();
                                         newUserReportMapping.UserID = userReportMapping.UserID;
                                         newUserReportMapping.ReportID = reportsList.ReportID;
                                         newUserReportMapping.isEnable = isEnable;
                                         _userReportMappingRepository.Update(newUserReportMapping);
                                     }
                                     else
                                     {
                                         var newUserReportMapping = new Tbl_UserReportMapping();
                                         newUserReportMapping.UserID = userID;
                                         newUserReportMapping.ReportID = reportsList.ReportID;
                                         newUserReportMapping.isEnable = isEnable;
                                         _userReportMappingRepository.Insert(newUserReportMapping);
                                     }
                                 }else
                                 {
                                     int userID = _userRepository.GetUserIDByUsername(user.Username);
                                     var userReportMapping = _userReportMappingRepository.GerUserReportMappingByUserIDAndReportID(userID, reportsList.ReportID);
                                     if (userReportMapping != null)
                                     {
                                         _userReportMappingRepository.Delete(userReportMapping);
                                     }
                                 }

                             }
                         }
                     }
                     #endregion Mapping Reports
                }

                
               
            }
            catch (Exception ex)
            {
                throw;
            }

            return systemUnSuccessCall;
        }

        public virtual async Task<string> CallAPIInsertOrUpdateBSPUserPreference2(BSP_UserPreferenceModels model, string languageId)
        {
            string systemUnSuccessCall = "";
            string messageReturn = "";
            string errorMessage = "";
            try
            {
                var user = _userRepository.FindUserByUserID(model.UserID);

                if (user != null)
                {
                    string reqID = GeneratorGuid.GetRandomGuid();

                    string jsonStr = "";

                    string _APIKey = "";

                    string _APIName = "";

                    string _APIProc = "";

                    string guid = "mockGUID";

                    if (System.Web.HttpContext.Current.Session["GUID"] != null)
                    {
                        guid = System.Web.HttpContext.Current.Session["GUID"].ToString();
                    }

                    string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();

                    if (model.SystemRole.API_Name == "UpdateUser")
                    {
                        var apiUpdateModels = GetDataCallUpdateUserAPI(model.SystemRole, reqID, model.UsernameCurrent);
                        jsonStr = JsonConvert.SerializeObject(apiUpdateModels);
                        _APIKey = apiUpdateModels.Request.APIKey;
                        _APIName = apiUpdateModels.Request.APIName;
                        _APIProc = "Update";
                    }
                    else
                    {
                        var apiInsertModels = GetDataCallInsertUserAPI(model.SystemRole, reqID, model.UsernameCurrent);
                        jsonStr = JsonConvert.SerializeObject(apiInsertModels);
                        _APIKey = apiInsertModels.Request.APIKey;
                        _APIName = apiInsertModels.Request.APIName;
                        _APIProc = "Insert";
                    }

                    HttpClient client = new HttpClient();
                    client.BaseAddress = new Uri(model.SystemRole.API_URL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpContent content = new StringContent(jsonStr);
                    content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                    #region Insert API Log Before

                    var _APILogsBefore = new Tbl_APILogs()
                    {
                        ReqID = reqID,
                        SystemID = model.SystemRole.SystemID,
                        Events = "Request_Before",
                        Events_Time = DateTime.UtcNow,
                        IPAddress_Client = visitorsIPAddr,
                        APIKey = _APIKey,
                        APIName = _APIName,
                        Return_Code = "",
                        Return_Status = "",
                        Return_Message = "",
                        Data = jsonStr,
                        //Data = responseBody,
                        UserGUID = guid,
                    };

                    _aPILogsRepository.Insert(_APILogsBefore);
                    #endregion

                    #region Call API
                    HttpResponseMessage response = await client.PostAsync(model.SystemRole.API_URL, content);

                    

                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var responseBody = response.Content.ReadAsStringAsync().Result;

                        try
                        {
                        JObject _response = JObject.Parse(responseBody);

                        string return_Code = _response["Response"]["Result"]["Code"].ToString();
                        string return_Status = _response["Response"]["Result"]["SuccessStatus"].ToString();
                        string return_Message = _response["Response"]["Result"]["Message"].ToString();

                        #region Insert API Log

                        var _APILogs = new Tbl_APILogs()
                        {
                            ReqID = reqID,
                            SystemID = model.SystemRole.SystemID,
                            Events = "Request_Before",
                            Events_Time = DateTime.UtcNow,
                            IPAddress_Client = visitorsIPAddr,
                            APIKey = _APIKey,
                            APIName = _APIName,
                            Return_Code = return_Code,
                            Return_Status = return_Status,
                            Return_Message = return_Message,
                            Data = jsonStr,
                            //Data = responseBody,
                            UserGUID = guid,
                        };

                        _aPILogsRepository.Insert(_APILogs);
                        #endregion

                        if (!Convert.ToBoolean(return_Status))
                        {
                            if (_APIProc == "Update")
                            {
                                errorMessage = messageReturn.GetStringResource("_Profile.ErrorMsg.UpdateSysUserFail", languageId);
                                systemUnSuccessCall = errorMessage.Replace("<%SystemName%>", model.SystemRole.SystemName);
                            }
                            else
                            {
                                errorMessage = messageReturn.GetStringResource("_Profile.ErrorMsg.CreateSysUserFail", languageId);
                                systemUnSuccessCall = errorMessage.Replace("<%SystemName%>", model.SystemRole.SystemName);
                            }
                        }

                        #region Mapping User

                        //string testReply = @"{'Response':{'ReqID':'568d3fd5ed6f47b5bbeb2351b2dd3859','Result':{'Code':'200','SuccessStatus':'true','Message':'Success'},'Data':[{'SysUserID':[{'Value':'S00031-User1'}]}]}}";

                        if (Convert.ToBoolean(return_Status))
                        //if (true)
                        {
                            API_ReplyDataModels replyModels = JsonConvert.DeserializeObject<API_ReplyDataModels>(responseBody);


                            if (replyModels.Response.data.Count() > 0)
                            {


                                foreach (IList<SysUserIDResponse> dataValue in replyModels.Response.data.Select(m => m.SysUserID))
                                {
                                    //foreach (IList<SysUserIDResponse> dataResponse in dataValue)
                                    //{
                                    foreach (var sysUserResponse in dataValue)
                                    {
                                        if (_APIProc == "Insert")
                                        {
                                            int userID = _userRepository.GetUserIDByUsername(model.SystemRole.Username);

                                            var _InsertUserSystemMapping = new Tbl_UserSystemMapping()
                                            {
                                                UserID = userID,
                                                SysUserID = sysUserResponse.value,
                                                SysPassword = "",
                                                SystemID = model.SystemRole.SystemID,
                                                SysRoleID = Convert.ToInt32(model.SystemRole.SysRoleID),
                                                isEnableService = Convert.ToInt32(model.SystemRole.IsChecked),
                                                MergeFromUserID = 0
                                            };
                                            _userSystemMappingRepository.Insert(_InsertUserSystemMapping);

                                            #region Update Tbl_UserServiceMapping
                                            // Map Service OP
                                            if (!_userServiceMappingRepository.CheckExistsUserServiceMapping(userID, 1))
                                            {
                                                var userServiceMappingModel = new Tbl_UserServiceMapping()
                                                {
                                                    UserID = userID,
                                                    ServiceID = 1,
                                                    isActive = 1
                                                };
                                                int updateByUserID = 0;
                                                var usernameCurrent = _userRepository.FindByUsername(model.UsernameCurrent);
                                                if (usernameCurrent != null)
                                                {
                                                    updateByUserID = usernameCurrent.UserID;
                                                }
                                                _userServiceMappingRepository.Insert(userServiceMappingModel, updateByUserID);
                                            }
                                            #endregion
                                        }
                                        else
                                        {
                                            int userID = _userRepository.GetUserIDByUsername(model.SystemRole.Username);
                                            var _UpdateUserSystemMapping = new Tbl_UserSystemMapping()
                                            {
                                                UserID = userID,
                                                SysUserID = sysUserResponse.value,
                                                SysPassword = "",
                                                SystemID = model.SystemRole.SystemID,
                                                SysRoleID = Convert.ToInt32(model.SystemRole.SysRoleID),
                                                isEnableService = Convert.ToInt32(model.SystemRole.IsChecked),
                                                MergeFromUserID = 0
                                            };
                                            _userSystemMappingRepository.Update(_UpdateUserSystemMapping);

                                        }
                                    }
                                    // }
                                }
                            }

                        }
                        #endregion

                        }catch (Exception ex)
                        {
                            #region Insert API Log
                            string guidCall = "mockGUID";

                            if (System.Web.HttpContext.Current.Session["GUID"] != null)
                            {
                                guidCall = System.Web.HttpContext.Current.Session["GUID"].ToString();
                            }

                            string visitorsIPAddrCall = GetClientIPAddressHelper.GetClientIPAddres();


                            var _APILogsCheck = new Tbl_APILogs()
                            {
                                ReqID = reqID,
                                SystemID = model.SystemRole.SystemID,
                                Events = "Request_Check",
                                Events_Time = DateTime.UtcNow,
                                IPAddress_Client = visitorsIPAddrCall,
                                APIKey = _APIKey,
                                APIName = _APIName,
                                Return_Code = "",
                                Return_Status = response.StatusCode.ToString(),
                                Return_Message = responseBody,
                                Data = jsonStr,
                                //Data = responseBody,
                                UserGUID = guidCall,
                            };

                            _aPILogsRepository.Insert(_APILogsCheck);


                            #endregion

                            throw;
                        }


                    }
                    else
                    {

                        #region Insert API Log

                        var _APILogs = new Tbl_APILogs()
                        {
                            ReqID = reqID,
                            SystemID = model.SystemRole.SystemID,
                            Events = "Request",
                            Events_Time = DateTime.UtcNow,
                            IPAddress_Client = visitorsIPAddr,
                            APIKey = _APIKey,
                            APIName = _APIName,
                            Return_Code = "",
                            Return_Status = "",
                            Return_Message = "",
                            Data = jsonStr,
                            //Data = responseBody,
                            UserGUID = guid,
                        };

                        _aPILogsRepository.Insert(_APILogs);


                        if (_APIProc == "Update")
                        {
                            errorMessage = messageReturn.GetStringResource("_Profile.ErrorMsg.UpdateSysUserFail", languageId);
                            systemUnSuccessCall = errorMessage.Replace("<%SystemName%>", model.SystemRole.SystemName);
                        }
                        else
                        {
                            errorMessage = messageReturn.GetStringResource("_Profile.ErrorMsg.CreateSysUserFail", languageId);
                            systemUnSuccessCall = errorMessage.Replace("<%SystemName%>", model.SystemRole.SystemName);
                        }
                        //systemUnSuccessCall += model.SystemRole.SystemName + ",";

                        #endregion
                    }
                    #endregion

                    #region  API Post Task

                    //string responseBody = "";

                    //var continuationTask = client.PostAsync(model.SystemRole.API_URL, content).ContinueWith(
                    //    (postTask) =>
                    //    {
                    //        bool isCompleted = postTask.IsCompleted;
                    //        bool isFaulted = postTask.IsFaulted;
                    //        if (isCompleted && !isFaulted)
                    //        {
                    //            var response = postTask.Result;
                    //            if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    //            {
                    //                if (!string.IsNullOrEmpty(response.Content.ReadAsStringAsync().Result))
                    //                {
                    //                    responseBody = response.Content.ReadAsStringAsync().Result;

                    //                    JObject _response = JObject.Parse(responseBody);

                    //                    string return_Code = _response["Response"]["Result"]["Code"].ToString();
                    //                    string return_Status = _response["Response"]["Result"]["SuccessStatus"].ToString();
                    //                    string return_Message = _response["Response"]["Result"]["Message"].ToString();

                    //                    #region Insert API Log

                    //                    var _APILogs = new Tbl_APILogs()
                    //                    {
                    //                        ReqID = reqID,
                    //                        SystemID = model.SystemRole.SystemID,
                    //                        Events = "Request_Before",
                    //                        Events_Time = DateTime.UtcNow,
                    //                        IPAddress_Client = visitorsIPAddr,
                    //                        APIKey = _APIKey,
                    //                        APIName = _APIName,
                    //                        Return_Code = return_Code,
                    //                        Return_Status = return_Status,
                    //                        Return_Message = return_Message,
                    //                        Data = jsonStr,
                    //                        //Data = responseBody,
                    //                        UserGUID = guid,
                    //                    };

                    //                    _aPILogsRepository.Insert(_APILogs);
                    //                    #endregion

                    //                    if (!Convert.ToBoolean(return_Status))
                    //                    {
                    //                        if (_APIProc == "Update")
                    //                        {
                    //                            errorMessage = messageReturn.GetStringResource("_Profile.ErrorMsg.UpdateSysUserFail", languageId);
                    //                            systemUnSuccessCall = errorMessage.Replace("<%SystemName%>", model.SystemRole.SystemName);
                    //                        }
                    //                        else
                    //                        {
                    //                            errorMessage = messageReturn.GetStringResource("_Profile.ErrorMsg.CreateSysUserFail", languageId);
                    //                            systemUnSuccessCall = errorMessage.Replace("<%SystemName%>", model.SystemRole.SystemName);
                    //                        }
                    //                    }

                    //                    #region Mapping User

                    //                    //string testReply = @"{'Response':{'ReqID':'568d3fd5ed6f47b5bbeb2351b2dd3859','Result':{'Code':'200','SuccessStatus':'true','Message':'Success'},'Data':[{'SysUserID':[{'Value':'S00031-User1'}]}]}}";

                    //                    if (Convert.ToBoolean(return_Status))
                    //                    //if (true)
                    //                    {
                    //                        API_ReplyDataModels replyModels = JsonConvert.DeserializeObject<API_ReplyDataModels>(responseBody);


                    //                        if (replyModels.Response.data.Count() > 0)
                    //                        {


                    //                            foreach (IList<SysUserIDResponse> dataValue in replyModels.Response.data.Select(m => m.SysUserID))
                    //                            {
                    //                                //foreach (IList<SysUserIDResponse> dataResponse in dataValue)
                    //                                //{
                    //                                foreach (var sysUserResponse in dataValue)
                    //                                {
                    //                                    if (_APIProc == "Insert")
                    //                                    {
                    //                                        int userID = _userRepository.GetUserIDByUsername(model.SystemRole.Username);

                    //                                        var _InsertUserSystemMapping = new Tbl_UserSystemMapping()
                    //                                        {
                    //                                            UserID = userID,
                    //                                            SysUserID = sysUserResponse.value,
                    //                                            SysPassword = "",
                    //                                            SystemID = model.SystemRole.SystemID,
                    //                                            SysRoleID = Convert.ToInt32(model.SystemRole.SysRoleID),
                    //                                            isEnableService = Convert.ToInt32(model.SystemRole.IsChecked),
                    //                                            MergeFromUserID = 0
                    //                                        };
                    //                                        _userSystemMappingRepository.Insert(_InsertUserSystemMapping);

                    //                                        #region Update Tbl_UserServiceMapping
                    //                                        // Map Service OP
                    //                                        if (!_userServiceMappingRepository.CheckExistsUserServiceMapping(userID, 1))
                    //                                        {
                    //                                            var userServiceMappingModel = new Tbl_UserServiceMapping()
                    //                                            {
                    //                                                UserID = userID,
                    //                                                ServiceID = 1,
                    //                                                isActive = 1
                    //                                            };
                    //                                            int updateByUserID = 0;
                    //                                            var usernameCurrent = _userRepository.FindByUsername(model.UsernameCurrent);
                    //                                            if (usernameCurrent != null)
                    //                                            {
                    //                                                updateByUserID = usernameCurrent.UserID;
                    //                                            }
                    //                                            _userServiceMappingRepository.Insert(userServiceMappingModel, updateByUserID);
                    //                                        }
                    //                                        #endregion
                    //                                    }
                    //                                    else
                    //                                    {
                    //                                        int userID = _userRepository.GetUserIDByUsername(model.SystemRole.Username);
                    //                                        var _UpdateUserSystemMapping = new Tbl_UserSystemMapping()
                    //                                        {
                    //                                            UserID = userID,
                    //                                            SysUserID = sysUserResponse.value,
                    //                                            SysPassword = "",
                    //                                            SystemID = model.SystemRole.SystemID,
                    //                                            SysRoleID = Convert.ToInt32(model.SystemRole.SysRoleID),
                    //                                            isEnableService = Convert.ToInt32(model.SystemRole.IsChecked),
                    //                                            MergeFromUserID = 0
                    //                                        };
                    //                                        _userSystemMappingRepository.Update(_UpdateUserSystemMapping);

                    //                                    }
                    //                                }
                    //                                // }
                    //                            }
                    //                        }

                    //                    }
                    //                    #endregion
                    //                }


                    //            }
                    //            else
                    //            {

                    //                #region Insert API Log

                    //                var _APILogs = new Tbl_APILogs()
                    //                {
                    //                    ReqID = reqID,
                    //                    SystemID = model.SystemRole.SystemID,
                    //                    Events = "Request",
                    //                    Events_Time = DateTime.UtcNow,
                    //                    IPAddress_Client = visitorsIPAddr,
                    //                    APIKey = _APIKey,
                    //                    APIName = _APIName,
                    //                    Return_Code = "",
                    //                    Return_Status = "",
                    //                    Return_Message = "",
                    //                    Data = jsonStr,
                    //                    //Data = responseBody,
                    //                    UserGUID = guid,
                    //                };

                    //                _aPILogsRepository.Insert(_APILogs);


                    //                if (_APIProc == "Update")
                    //                {
                    //                    errorMessage = messageReturn.GetStringResource("_Profile.ErrorMsg.UpdateSysUserFail", languageId);
                    //                    systemUnSuccessCall = errorMessage.Replace("<%SystemName%>", model.SystemRole.SystemName);
                    //                }
                    //                else
                    //                {
                    //                    errorMessage = messageReturn.GetStringResource("_Profile.ErrorMsg.CreateSysUserFail", languageId);
                    //                    systemUnSuccessCall = errorMessage.Replace("<%SystemName%>", model.SystemRole.SystemName);
                    //                }
                    //                //systemUnSuccessCall += model.SystemRole.SystemName + ",";

                    //                #endregion
                    //            }
                    //        }
                    //    });

                    //continuationTask.Wait(20000);
                    #endregion

                    #region Mapping Reports

                    var reports = _portalReportsRepository.List();

                    foreach (var reportsList in reports)
                    {
                        if (reportsList.SystemID == 1)
                        {
                            var userMapping = _userSystemMappingRepository.GetUserSystemMappingByUsernameAndSystemID(user.Username, reportsList.SystemID ?? 0);
                            if (userMapping != null)
                            {
                                var userReportMapping = _userReportMappingRepository.GerUserReportMappingByUserIDAndReportID(userMapping.UserID, reportsList.ReportID);
                                if (userReportMapping != null)
                                {
                                    var newUserReportMapping = new Tbl_UserReportMapping();
                                    newUserReportMapping.UserID = userReportMapping.UserID;
                                    newUserReportMapping.ReportID = userReportMapping.ReportID;
                                    newUserReportMapping.isEnable = userMapping.isEnableService;
                                    _userReportMappingRepository.Update(newUserReportMapping);
                                }
                                else
                                {
                                    int userID = _userRepository.GetUserIDByUsername(user.Username);
                                    var newUserReportMapping = new Tbl_UserReportMapping();
                                    newUserReportMapping.UserID = userID;
                                    newUserReportMapping.ReportID = reportsList.ReportID;
                                    newUserReportMapping.isEnable = userMapping.isEnableService;
                                    _userReportMappingRepository.Insert(newUserReportMapping);
                                }
                            }
                        }
                        else
                        {
                            if (reportsList.ReportID == 1)
                            {
                                if (model.SystemRole.SysRoleID == "2" || model.SystemRole.SysRoleID == "1")
                                {
                                    int isEnable = 1;

                                    int userID = _userRepository.GetUserIDByUsername(user.Username);
                                    var userReportMapping = _userReportMappingRepository.GerUserReportMappingByUserIDAndReportID(userID, reportsList.ReportID);
                                    if (userReportMapping != null)
                                    {
                                        var newUserReportMapping = new Tbl_UserReportMapping();
                                        newUserReportMapping.UserID = userReportMapping.UserID;
                                        newUserReportMapping.ReportID = reportsList.ReportID;
                                        newUserReportMapping.isEnable = isEnable;
                                        _userReportMappingRepository.Update(newUserReportMapping);
                                    }
                                    else
                                    {
                                        var newUserReportMapping = new Tbl_UserReportMapping();
                                        newUserReportMapping.UserID = userID;
                                        newUserReportMapping.ReportID = reportsList.ReportID;
                                        newUserReportMapping.isEnable = isEnable;
                                        _userReportMappingRepository.Insert(newUserReportMapping);
                                    }
                                }
                                else
                                {
                                    int userID = _userRepository.GetUserIDByUsername(user.Username);
                                    var userReportMapping = _userReportMappingRepository.GerUserReportMappingByUserIDAndReportID(userID, reportsList.ReportID);
                                    if (userReportMapping != null)
                                    {
                                        _userReportMappingRepository.Delete(userReportMapping);
                                    }
                                }

                            }
                        }
                    }
                    #endregion Mapping Reports
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return systemUnSuccessCall;
        }

        public virtual void UpdateSessionData(BSP_UserPreferenceModels model)
        {
            try
            {
                string guid = "mockGUID";
                string usernameCurrent = "";

                string jsonStr = "";

                if (System.Web.HttpContext.Current.Session["GUID"] != null)
                {
                    guid = System.Web.HttpContext.Current.Session["GUID"].ToString();
                }

                if (System.Web.HttpContext.Current.Session["username"] != null)
                {
                    usernameCurrent = System.Web.HttpContext.Current.Session["username"].ToString();
                }

                var userSesstion = _userSessionRepository.GetUserSession(guid);

                var userEdit = _userRepository.FindUserByUserID(model.UserID);

                var userCurrent = _userRepository.FindByUsername(usernameCurrent);

                if (userSesstion != null && userEdit != null && userCurrent != null)
                {
                    var jsonSessionDataModel = new JsonSessionDataModel();

                    var dataRersult = new DataResult();

                    dataRersult.SessionName = "BSP_UserPreference";
                    dataRersult.UserGUID = guid;
                    dataRersult.PortalLoginID = userCurrent.UserID.ToString();
                    dataRersult.ModifiedUserID = model.UserID.ToString();
                    dataRersult.UrlLink = model.UrlLink;

                    var actionMethod = new ActionMethod();

                    actionMethod.MethodForm = "GET";

                    var listParameterData = new List<ParameterData>();

                    actionMethod.ParameterData = listParameterData;

                    dataRersult.ActionMethod = actionMethod;

                    dataRersult.UpdateDate = DateTime.UtcNow.ToString();

                    jsonSessionDataModel.Data = dataRersult;

                    jsonStr = JsonConvert.SerializeObject(jsonSessionDataModel);

                    var userSession = _userSessionRepository.GetUserSession(guid);

                    if (userSession != null)
                    {
                        userSession.SessionData = jsonStr;

                        _userSessionRepository.Update(userSession);
                    }


                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual bool CheckSupplierMergUserInCurrentUser(string currentUsername, string mergUsername)
        {
            bool chk = false;
            try
            {
                var userCurrent = _userRepository.FindByUsername(currentUsername);

                var userMerg = _userRepository.FindByUsername(mergUsername);

                if (userCurrent.OrgID.Trim() == userMerg.OrgID.Trim())
                {
                    chk = true;
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return chk;
        }

        public virtual bool CheckUserMappingBSPInGroup(string currentUsername, string mergUsername)
        {
            bool chk = false;
            try
            {
                var userCurrentMapping = _userSystemMappingRepository.GetUserSystemMapping(currentUsername, 1);

                var userMergMapping = _userSystemMappingRepository.GetUserSystemMapping(mergUsername, 1);


                if (userCurrentMapping.Count() > 0 && userMergMapping.Count()>0)
                {
                    chk = true;
                }
                
            }
            catch (Exception ex)
            {
                throw;
            }

            return chk;
        }

        public virtual void CallAPIAddNotification(BSP_UserPreferenceModels model)
        {
            try
            {
                var userSystemMapping = _userSystemMappingRepository.GetUserSystemMappingEnableService(model.UserID, 1); // SystemID 1 = BSP OP

                if (userSystemMapping.Count() > 0)
                {
                    string notificationKey = model.UserID.ToString();

                    var notification = _notificationRepository.GetNotificationByNotificationKeyAndTopicID(notificationKey, 3).FirstOrDefault(); // TopicID 3 = New OP User

                    var apiModel = _systemConfigureAPIRepository.GetAPIBySystemIDAndAPI_Name(0, "AddNotification"); // SystemID 0 = SWW
                    
                    if(apiModel != null)
                    {

                        if(notification != null)
                        {
                            string reqID = GeneratorGuid.GetRandomGuid();

                            string jsonStr = "";

                            string _APIKey = apiModel.API_Key;

                            string _APIName = apiModel.API_Name;

                            API_AddNotificationModels addNotificationModel = JsonConvert.DeserializeObject<API_AddNotificationModels>(notification.RawData);

                            addNotificationModel.ReqID = reqID;
                            addNotificationModel.StopTime = DateTime.UtcNow;

                            jsonStr = JsonConvert.SerializeObject(addNotificationModel);

                            HttpClient client = new HttpClient();
                            client.BaseAddress = new Uri(apiModel.API_URL);
                            client.DefaultRequestHeaders.Accept.Clear();
                            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                            HttpContent content = new StringContent(jsonStr);
                            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                            HttpResponseMessage response = client.PostAsync(apiModel.API_URL, content).Result;

                            var responseBody = response.Content.ReadAsStringAsync().Result;

                            if (response.StatusCode == System.Net.HttpStatusCode.OK)
                            {
                                JObject _response = JObject.Parse(responseBody);

                                string return_Code = _response["result"].SelectToken("Code").ToString();
                                string return_Status = _response["result"].SelectToken("SuccessStatus").ToString();
                                string return_Message = _response["result"].SelectToken("Message").ToString();

                                #region Insert API Log
                                string guid = "mockGUID";

                                if (System.Web.HttpContext.Current.Session["GUID"] != null)
                                {
                                    guid = System.Web.HttpContext.Current.Session["GUID"].ToString();
                                }

                                string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();


                                var _APILogs = new Tbl_APILogs()
                                {
                                    ReqID = reqID,
                                    SystemID = model.SystemRole.SystemID,
                                    Events = "Request",
                                    Events_Time = DateTime.UtcNow,
                                    IPAddress_Client = visitorsIPAddr,
                                    APIKey = _APIKey,
                                    APIName = _APIName,
                                    Return_Code = return_Code,
                                    Return_Status = return_Status,
                                    Return_Message = return_Message,
                                    Data = jsonStr,
                                    //Data = responseBody,
                                    UserGUID = guid,
                                };

                                _aPILogsRepository.Insert(_APILogs);
                                #endregion
                            }
                            else
                            {

                                #region Insert API Log
                                string guid = "mockGUID";

                                if (System.Web.HttpContext.Current.Session["GUID"] != null)
                                {
                                    guid = System.Web.HttpContext.Current.Session["GUID"].ToString();
                                }

                                string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();


                                var _APILogs = new Tbl_APILogs()
                                {
                                    ReqID = reqID,
                                    SystemID = model.SystemRole.SystemID,
                                    Events = "Request",
                                    Events_Time = DateTime.UtcNow,
                                    IPAddress_Client = visitorsIPAddr,
                                    APIKey = _APIKey,
                                    APIName = _APIName,
                                    Return_Code = "",
                                    Return_Status = "",
                                    Return_Message = "",
                                    Data = jsonStr,
                                    //Data = responseBody,
                                    UserGUID = guid,
                                };

                                _aPILogsRepository.Insert(_APILogs);
                                #endregion
                            }
                        }
                        
                    }
                    
                }

            }
            catch (Exception ex)
            {
                throw;
            }
            
        }

    }
}


﻿using SupplierPortal.Data.Models.SupportModel.AutoMerge;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.Repository.AutoMergeUser;
using SupplierPortal.Data.Models.Repository.User;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Services.ProfileManage;
using SupplierPortal.Data.Models.SupportModel.Profile;
using SupplierPortal.Data.Models.Repository.UserSystemMapping;
using SupplierPortal.Data.Models.Repository.UserServiceMapping;
using SupplierPortal.Data.Models.Repository.LogUser;
using SupplierPortal.Data.Models.Repository.ContactPerson;
using SupplierPortal.Data.Models.Repository.LogContactPerson;
using SupplierPortal.Data.Models.Repository.Organization;
using SupplierPortal.Data.Models.Repository.OrgContactPerson;
using SupplierPortal.Data.Models.Repository.LogOrgContactPerson;
using SupplierPortal.Data.Models.Repository.SystemRole;
using SupplierPortal.Data.Models.Repository.APILogs;
using SupplierPortal.Data.Models.Repository.UserReportMapping;
using System.Transactions;
using SupplierPortal.Services.AccountManage;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;
using SupplierPortal.Framework.MethodHelper;
using Newtonsoft.Json.Linq;
using SupplierPortal.Data.Models.Repository.SystemConfigureAPI;

namespace SupplierPortal.Services.AutoMergeManage
{
    public partial class AutoMergeTransactions : IAutoMergeTransactions
    {
        IAutoMergeUserRepository _autoMergeUserRepository;
        IUserRepository _userRepository;
        IProfileDataService _profileDataService;
        IUserSystemMappingRepository _userSystemMappingRepository;
        IUserServiceMappingRepository _userServiceMappingRepository;
        ILogUserRepository _logUserRepository;
        IContactPersonRepository _contactPersonRepository;
        ILogContactPersonRepository _logContactPersonRepository;
        IOrganizationRepository _organizationRepository;
        IOrgContactPerson _orgContactPersonRepository;
        ILogOrgContactPersonRepository _logOrgContactPersonRepository;
        ISystemRepository _systemRepository;
        IAPILogsRepository _aPILogsRepository;
        IUserReportMappingRepository _userReportMappingRepository;
        ISystemConfigureAPIRepository _systemConfigureAPIRepository;



        public AutoMergeTransactions()
        {
            _autoMergeUserRepository = new AutoMergeUserRepository();
            _userRepository = new UserRepository();
            _profileDataService = new ProfileDataService();
            _userSystemMappingRepository = new UserSystemMappingRepository();
            _userServiceMappingRepository = new UserServiceMappingRepository();
            _logUserRepository = new LogUserRepository();
            _contactPersonRepository = new ContactPersonRepository();
            _logContactPersonRepository = new LogContactPersonRepository();
            _organizationRepository = new OrganizationRepository();
            _orgContactPersonRepository = new OrgContactPerson();
            _logOrgContactPersonRepository = new LogOrgContactPersonRepository();
            _systemRepository = new SystemRepository();
            _aPILogsRepository = new APILogsRepository();
            _userReportMappingRepository = new UserReportMappingRepository();
            _systemConfigureAPIRepository = new SystemConfigureAPIRepository();
        }

        public AutoMergeTransactions(
            AutoMergeUserRepository autoMergeUserRepository,
            UserRepository userRepository,
            ProfileDataService profileDataService,
            UserSystemMappingRepository userSystemMappingRepository,
            UserServiceMappingRepository userServiceMappingRepository,
            LogUserRepository logUserRepository,
            ContactPersonRepository contactPersonRepository,
            LogContactPersonRepository logContactPersonRepository,
            OrganizationRepository organizationRepository,
            OrgContactPerson orgContactPersonRepository,
            LogOrgContactPersonRepository logOrgContactPersonRepository,
            SystemRepository systemRepository,
            APILogsRepository aPILogsRepository,
            UserReportMappingRepository userReportMappingRepository,
            SystemConfigureAPIRepository systemConfigureAPIRepository
            )
        {
             _autoMergeUserRepository = autoMergeUserRepository;
             _userRepository = userRepository;
             _profileDataService = profileDataService;
             _userSystemMappingRepository = userSystemMappingRepository;
             _userServiceMappingRepository = userServiceMappingRepository;
             _logUserRepository = logUserRepository;
             _contactPersonRepository = contactPersonRepository;
             _logContactPersonRepository = logContactPersonRepository;
             _organizationRepository = organizationRepository;
             _orgContactPersonRepository = orgContactPersonRepository;
             _logOrgContactPersonRepository = logOrgContactPersonRepository;
             _systemRepository = systemRepository;
             _aPILogsRepository = aPILogsRepository;
             _userReportMappingRepository = userReportMappingRepository;
            _systemConfigureAPIRepository = systemConfigureAPIRepository;

        }

        public virtual IEnumerable<MergeUserGroupReadyModel> GetMergUserReady(int limit)
        {
            var mergeUserGroupReadyList = new List<MergeUserGroupReadyModel>();

            var mergeReady = _autoMergeUserRepository.GetAutoMergeUserUnSuccess();

            var mergeGroup = mergeReady.GroupBy(d => new { d.MergeID })
                .Select(
                s => new MergeUserGroupModel
                {
                    MergeID = s.Key.MergeID
                }
                ).Take(limit);

            foreach (var item in mergeGroup)
            {
                var model = new MergeUserGroupReadyModel();
                model.MergeID = item.MergeID;

                var mergUserReady = mergeReady.Where(m => m.MergeID == item.MergeID).ToList();
                bool isMerge;
                var mergList = CheckMergValidate(mergUserReady, out isMerge);
                model.MergUserReady = mergList;
                model.isMerge = isMerge;
                mergeUserGroupReadyList.Add(model);
            }

            return mergeUserGroupReadyList;
        }

        public virtual void MergeUser(MergeUserGroupReadyModel model)
        {

            bool chk = true;
            var listAPILogs = new List<Tbl_APILogs>();
            try
            {
                string usernameUpdateBy = "GraingerAdmin";
                var currentUsername = model.MergUserReady.Where(m => m.isPrimary == 1).FirstOrDefault();
                string masterUsername = currentUsername.UserName;

                var mergUsername = model.MergUserReady.Where(m => m.isPrimary != 1 && m.isReady ==1).ToList();

                var listsysMappingSuccess = new List<SystemRoleListModels>();
                using (TransactionScope ts = new TransactionScope())
                {
                    var masterUser = _userRepository.FindByUsername(masterUsername);

                    foreach (var userlist in mergUsername)
                    {

                        #region Update Tbl_UserSystemMapping
                        var mergUser = _userRepository.FindByUsername(userlist.UserName);
                        var mergUserMapping = _userSystemMappingRepository.GetUserSystemMappingByUserID(mergUser.UserID);

                        foreach (var list in mergUserMapping)
                        {
                            var userMapping = list;
                            _userSystemMappingRepository.UpdateByMergAccount(userMapping, masterUser.UserID);
                        }
                        #endregion
                                               
                        #region Update Tbl_UserServiceMapping

                        var serviceMappingMasterUser = _userServiceMappingRepository.GetUserServiceMappingByUserID(masterUser.UserID);

                        var serviceMappingMergUser = _userServiceMappingRepository.GetUserServiceMappingByUserID(mergUser.UserID);

                        var query = from a in serviceMappingMergUser
                                    where !(serviceMappingMasterUser.Any(m => m.ServiceID == a.ServiceID))
                                    select a;

                        var listServiceMapping = query.ToList();

                        foreach (var listService in listServiceMapping)
                        {
                            if (!_userServiceMappingRepository.CheckExistsUserServiceMapping(masterUser.UserID, listService.ServiceID))
                            {
                                var userServiceMappingModel = new Tbl_UserServiceMapping()
                                {
                                    UserID = masterUser.UserID,
                                    ServiceID = listService.ServiceID,
                                    isActive = listService.isActive
                                };

                                _userServiceMappingRepository.Insert(userServiceMappingModel, 1);
                            }
                        }

                        #endregion

                        #region Update Tbl_User
                        mergUser.MergeToUserID = masterUser.UserID;
                        mergUser.IsDeleted = 1;
                        _userRepository.Update(mergUser);
                        _logUserRepository.Insert(mergUser.UserID, "Modify", usernameUpdateBy);
                        #endregion

                        #region Update Tbl_ContactPerson
                        var contactPerson = _contactPersonRepository.GetContactPersonByContectID(mergUser.ContactID ?? 0);
                        if (contactPerson != null)
                        {
                            contactPerson.isDeleted = 1;
                            _contactPersonRepository.Update(contactPerson);
                            _logContactPersonRepository.Insert(contactPerson.ContactID, "Modify", usernameUpdateBy);
                        }
                        #endregion

                        #region Update Tbl_OrgContactPerson
                        int supplierID = _organizationRepository.GetSupplierIDByUsername(mergUser.Username);
                        var orgContactPerson = _orgContactPersonRepository.GetOrgContactPersonBySupplierIDAndContactID(supplierID, mergUser.ContactID ?? 0);
                        if (orgContactPerson != null)
                        {
                            orgContactPerson.NewContactID = masterUser.ContactID;
                            _orgContactPersonRepository.Update(orgContactPerson);
                            _logOrgContactPersonRepository.Insert(orgContactPerson.SupplierID, orgContactPerson.ContactID, "Modify", usernameUpdateBy);
                        }
                        #endregion

                        #region Mapping Reports

                        var reportMappingMasterUser = _userReportMappingRepository.GetUserReportMappingByUserID(masterUser.UserID);

                        var reportMappingMergUser = _userReportMappingRepository.GetUserReportMappingByUserID(mergUser.UserID);

                        var queryReport = from a in reportMappingMergUser
                                          where !(reportMappingMasterUser.Any(m => m.ReportID == a.ReportID))
                                          select a;

                        var listReportMapping = queryReport.ToList();

                        foreach (var listReport in listReportMapping)
                        {
                            if (!_userReportMappingRepository.CheckExistsUserReportMapping(masterUser.UserID, listReport.ReportID))
                            {
                                var userReportMappingModel = new Tbl_UserReportMapping()
                                {
                                    UserID = masterUser.UserID,
                                    ReportID = listReport.ReportID,
                                    isEnable = listReport.isEnable
                                };

                                _userReportMappingRepository.Insert(userReportMappingModel);
                            }
                        }

                        #endregion

                    }

                    #region Update System Role OP
                    var userMappingOP = _userSystemMappingRepository.GetUserSystemMappingOrderSysRoleIDASC(masterUser.UserID, 1);
                    int i = 1;
                    foreach (var list in userMappingOP)
                    {
                        if (i == 1)
                        {
                            if (list.MergeFromUserID != 0)
                            {
                                list.MergeFromUserID = 0;
                                _userSystemMappingRepository.Update(list);
                            }

                        }
                        else
                        {
                            if (list.MergeFromUserID == 0)
                            {
                                list.MergeFromUserID = list.UserID;
                                _userSystemMappingRepository.Update(list);
                            }
                        }

                        i++;
                    }
                    #endregion

                    #region Call API Data

                    //var listMapping = _systemRepository.GetSystemListByUsernameForUpdate(masterUser.Username);

                    var listMapping = _systemConfigureAPIRepository.GetSystemListByUsernameForUpdateAPI(masterUser.Username);

                    foreach (var sysMapping in listMapping)
                    {
                        string reqID = GeneratorGuid.GetRandomGuid();

                        string Jsonstr = "";

                        string _APIKey = "";

                        string _APIName = "";

                        var apiUpdateModels = _profileDataService.GetDataCallUpdateUserAPI(sysMapping, reqID, usernameUpdateBy, "MergeUser");
                        Jsonstr = JsonConvert.SerializeObject(apiUpdateModels);
                        _APIKey = apiUpdateModels.Request.APIKey;
                        _APIName = apiUpdateModels.Request.APIName;

                        HttpClient client = new HttpClient();
                        client.BaseAddress = new Uri(sysMapping.API_URL);
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                        HttpContent content = new StringContent(Jsonstr);
                        content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                        bool isFaulted = client.PostAsync(sysMapping.API_URL, content).IsFaulted;

                        if (isFaulted)
                        {
                            #region Insert API Log
                            string guid = "mockGUID";

                            if (System.Web.HttpContext.Current.Session["GUID"] != null)
                            {
                                guid = System.Web.HttpContext.Current.Session["GUID"].ToString();
                            }

                            string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();


                            var _APILogs = new Tbl_APILogs()
                            {
                                ReqID = reqID,
                                SystemID = sysMapping.SystemID,
                                Events = "Request",
                                Events_Time = DateTime.UtcNow,
                                IPAddress_Client = visitorsIPAddr,
                                APIKey = _APIKey,
                                APIName = _APIName,
                                Return_Code = "",
                                Return_Status = "",
                                Return_Message = "NO RESPONSE",
                                Data = Jsonstr,
                                UserGUID = guid,
                            };

                            listAPILogs.Add(_APILogs);
                            #endregion

                            chk = false;
                            break;
                        }

                        HttpResponseMessage response = client.PostAsync(sysMapping.API_URL, content).Result;

                        var responseBody = response.Content.ReadAsStringAsync().Result;

                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            try
                            {
                                JObject _response = JObject.Parse(responseBody);

                                string return_Code = _response["Response"]["Result"]["Code"].ToString();
                                string return_Status = _response["Response"]["Result"]["SuccessStatus"].ToString();
                                string return_Message = _response["Response"]["Result"]["Message"].ToString();

                                #region Insert API Log
                                string guid = "mockGUID";

                                if (System.Web.HttpContext.Current.Session["GUID"] != null)
                                {
                                    guid = System.Web.HttpContext.Current.Session["GUID"].ToString();
                                }

                                string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();

                                var _APILogs = new Tbl_APILogs()
                                {
                                    ReqID = reqID,
                                    SystemID = sysMapping.SystemID,
                                    Events = "Request",
                                    Events_Time = DateTime.UtcNow,
                                    IPAddress_Client = visitorsIPAddr,
                                    APIKey = _APIKey,
                                    APIName = _APIName,
                                    Return_Code = return_Code,
                                    Return_Status = return_Status,
                                    Return_Message = return_Message,
                                    Data = Jsonstr,
                                    UserGUID = guid,
                                };

                                listAPILogs.Add(_APILogs);

                                #endregion

                                if (!Convert.ToBoolean(return_Status))
                                {
                                    chk = false;
                                    break;
                                }
                                else
                                {
                                    listsysMappingSuccess.Add(sysMapping);
                                }
                            }
                            catch (Exception ex)
                            {
                                #region Insert API Log
                                string guidCall = "mockGUID";

                                if (System.Web.HttpContext.Current.Session["GUID"] != null)
                                {
                                    guidCall = System.Web.HttpContext.Current.Session["GUID"].ToString();
                                }

                                string visitorsIPAddrCall = GetClientIPAddressHelper.GetClientIPAddres();


                                var _APILogsCheck = new Tbl_APILogs()
                                {
                                    ReqID = reqID,
                                    SystemID = sysMapping.SystemID,
                                    Events = "Request_Check",
                                    Events_Time = DateTime.UtcNow,
                                    IPAddress_Client = visitorsIPAddrCall,
                                    APIKey = _APIKey,
                                    APIName = _APIName,
                                    Return_Code = "",
                                    Return_Status = response.StatusCode.ToString(),
                                    Return_Message = responseBody,
                                    Data = Jsonstr,
                                    //Data = responseBody,
                                    UserGUID = guidCall,
                                };

                                listAPILogs.Add(_APILogsCheck);


                                #endregion

                                throw;
                            }
                        }
                        else
                        {

                            #region Insert API Log
                            string guid = "mockGUID";

                            if (System.Web.HttpContext.Current.Session["GUID"] != null)
                            {
                                guid = System.Web.HttpContext.Current.Session["GUID"].ToString();
                            }

                            string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();

                            var _APILogs = new Tbl_APILogs()
                            {
                                ReqID = reqID,
                                SystemID = sysMapping.SystemID,
                                Events = "Request",
                                Events_Time = DateTime.UtcNow,
                                IPAddress_Client = visitorsIPAddr,
                                APIKey = _APIKey,
                                APIName = _APIName,
                                Return_Code = "",
                                Return_Status = "",
                                Return_Message = "",
                                Data = Jsonstr,
                                UserGUID = guid,
                            };

                            listAPILogs.Add(_APILogs);
                            #endregion
                            chk = false;
                            break;
                        }
                    }

                    #endregion

                    if (chk)
                    {
                        #region Update AutoMergeUser isSuccess

                        var autoMergeModel = model.MergUserReady.ToList();

                        foreach (var listMerge in autoMergeModel)
                        {
                            var autoMergeUser = _autoMergeUserRepository.GetAutoMergeUserByMergeIDAndMergeFromUserName(listMerge.MergeID, listMerge.UserName);
                            if (autoMergeUser != null)
                            {
                                if (listMerge.isReady == 1)
                                {
                                    autoMergeUser.isSuccess = 1;
                                }
                                else
                                {
                                    autoMergeUser.Remark = listMerge.Remark;
                                }
                                autoMergeUser.UpdateDate = DateTime.UtcNow;
                                _autoMergeUserRepository.Update(autoMergeUser);
                            }
                        }
                        #endregion

                        ts.Complete();
                        
                    }
                }

                foreach (var listLog in listAPILogs)
                {
                    _aPILogsRepository.Insert(listLog);
                }

                if (!chk)
                {
                    #region Call API Rollback
                    foreach (var userlist in mergUsername)
                    {
                        foreach (var sysMapping in listsysMappingSuccess)
                        {
                            //var listMapping = _systemRepository.GetSystemListByUsernameAndSystemIDForUpdate(userlist.UserName, sysMapping.SystemID);

                            var listMapping = _systemConfigureAPIRepository.GetSystemListByUsernameAndSystemIDForUpdateAPI(userlist.UserName, sysMapping.SystemID);

                            foreach (var list in listMapping)
                            {
                                string reqID = GeneratorGuid.GetRandomGuid();

                                string Jsonstr = "";

                                string _APIKey = "";

                                string _APIName = "";

                                var apiUpdateModels = _profileDataService.GetDataCallUpdateUserAPI(list, reqID, usernameUpdateBy);
                                Jsonstr = JsonConvert.SerializeObject(apiUpdateModels);
                                _APIKey = apiUpdateModels.Request.APIKey;
                                _APIName = apiUpdateModels.Request.APIName;

                                HttpClient client = new HttpClient();
                                client.BaseAddress = new Uri(sysMapping.API_URL);
                                client.DefaultRequestHeaders.Accept.Clear();
                                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                                HttpContent content = new StringContent(Jsonstr);
                                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                                HttpResponseMessage response = client.PostAsync(sysMapping.API_URL, content).Result;

                                var responseBody = response.Content.ReadAsStringAsync().Result;
                                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                                {
                                    JObject _response = JObject.Parse(responseBody);

                                    string return_Code = _response["Response"]["Result"]["Code"].ToString();
                                    string return_Status = _response["Response"]["Result"]["SuccessStatus"].ToString();
                                    string return_Message = _response["Response"]["Result"]["Message"].ToString();

                                    #region Insert API Log
                                    string guid = "mockGUID";

                                    if (System.Web.HttpContext.Current.Session["GUID"] != null)
                                    {
                                        guid = System.Web.HttpContext.Current.Session["GUID"].ToString();
                                    }

                                    string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();


                                    var _APILogRollback = new Tbl_APILogs()
                                    {
                                        ReqID = reqID,
                                        SystemID = sysMapping.SystemID,
                                        Events = "Request",
                                        Events_Time = DateTime.UtcNow,
                                        IPAddress_Client = visitorsIPAddr,
                                        APIKey = _APIKey,
                                        APIName = _APIName,
                                        Return_Code = return_Code,
                                        Return_Status = return_Status,
                                        Return_Message = return_Message,
                                        Data = Jsonstr,
                                        //Data = responseBody,
                                        UserGUID = guid,
                                    };

                                    _aPILogsRepository.Insert(_APILogRollback);
                                    #endregion
                                }
                                else
                                {
                                    #region Insert API Log
                                    string guid = "mockGUID";

                                    if (System.Web.HttpContext.Current.Session["GUID"] != null)
                                    {
                                        guid = System.Web.HttpContext.Current.Session["GUID"].ToString();
                                    }

                                    string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();


                                    var _APILogRollback = new Tbl_APILogs()
                                    {
                                        ReqID = reqID,
                                        SystemID = sysMapping.SystemID,
                                        Events = "Request",
                                        Events_Time = DateTime.UtcNow,
                                        IPAddress_Client = visitorsIPAddr,
                                        APIKey = _APIKey,
                                        APIName = _APIName,
                                        Return_Code = "",
                                        Return_Status = "",
                                        Return_Message = "",
                                        Data = Jsonstr,
                                        //Data = responseBody,
                                        UserGUID = guid,
                                    };

                                    _aPILogsRepository.Insert(_APILogRollback);

                                    #endregion
                                }
                            }
                        }
                    }
                    #endregion
                }

            }
            catch (Exception ex)
            {
                foreach (var listLog in listAPILogs)
                {
                    _aPILogsRepository.Insert(listLog);
                }
                throw;
            }
        }

        public virtual void UpdateMergeUserNonReady(MergeUserGroupReadyModel model)
        {
            var autoMergeModel = model.MergUserReady.ToList();

            foreach (var listMerge in autoMergeModel)
            {
                var autoMergeUser = _autoMergeUserRepository.GetAutoMergeUserByMergeIDAndMergeFromUserName(listMerge.MergeID, listMerge.UserName);
                if (autoMergeUser != null)
                {

                    autoMergeUser.isSuccess = -1;
                    autoMergeUser.Remark = listMerge.Remark;
                    autoMergeUser.UpdateDate = DateTime.UtcNow;
                    _autoMergeUserRepository.Update(autoMergeUser);
                }
            }

        }

        public virtual void UpdateUser(string username,int id)
        {

            try
            {
                //var listSystem = _systemRepository.GetSystemListByUsernameForUpdate(username);

                var listSystem = _systemConfigureAPIRepository.GetSystemListByUsernameForUpdateAPI(username);

                foreach (var sysMapping in listSystem)
                {
                    string reqID = GeneratorGuid.GetRandomGuid();

                    string Jsonstr = "";

                    string _APIKey = "";

                    string _APIName = "";

                    var apiUpdateModels = _profileDataService.GetDataCallUpdateUserAPI(sysMapping, reqID, sysMapping.Username);
                    Jsonstr = JsonConvert.SerializeObject(apiUpdateModels);
                    _APIKey = apiUpdateModels.Request.APIKey;
                    _APIName = apiUpdateModels.Request.APIName;

                    HttpClient client = new HttpClient();
                    client.BaseAddress = new Uri(sysMapping.API_URL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpContent content = new StringContent(Jsonstr);
                    content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                    HttpResponseMessage response = client.PostAsync(sysMapping.API_URL, content).Result;

                    var responseBody = response.Content.ReadAsStringAsync().Result;
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        JObject _response = JObject.Parse(responseBody);

                        string return_Code = _response["Response"]["Result"]["Code"].ToString();
                        string return_Status = _response["Response"]["Result"]["SuccessStatus"].ToString();
                        string return_Message = _response["Response"]["Result"]["Message"].ToString();


                        #region Insert API Log
                        string guid = "mockGUID";

                        if (System.Web.HttpContext.Current.Session["GUID"] != null)
                        {
                            guid = System.Web.HttpContext.Current.Session["GUID"].ToString();
                        }

                        string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();


                        var _APILogs = new Tbl_APILogs()
                        {
                            ReqID = reqID,
                            SystemID = sysMapping.SystemID,
                            Events = "Request",
                            Events_Time = DateTime.UtcNow,
                            IPAddress_Client = visitorsIPAddr,
                            APIKey = _APIKey,
                            APIName = _APIName,
                            Return_Code = return_Code,
                            Return_Status = return_Status,
                            Return_Message = return_Message,
                            Data = Jsonstr,
                            //Data = responseBody,
                            UserGUID = guid,
                        };

                        _aPILogsRepository.Insert(_APILogs);
                        #endregion

                        if (Convert.ToBoolean(return_Status))
                        {
                            #region Update AutoUpdateUser isSuccess
                            var autoUpdateUser = _autoMergeUserRepository.GetAutoUpdateUserByID(id);
                            if (autoUpdateUser != null)
                            {
                                autoUpdateUser.isSuccess = 1;
                                autoUpdateUser.UpdateDate = DateTime.UtcNow;
                                _autoMergeUserRepository.UpdateAutoUpdateUser(autoUpdateUser);
                            }
                            #endregion
                        }
                    }
                    else
                    {

                        #region Insert API Log
                        string guid = "mockGUID";

                        if (System.Web.HttpContext.Current.Session["GUID"] != null)
                        {
                            guid = System.Web.HttpContext.Current.Session["GUID"].ToString();
                        }

                        string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();


                        var _APILogs = new Tbl_APILogs()
                        {
                            ReqID = reqID,
                            Events = "Request",
                            Events_Time = DateTime.UtcNow,
                            IPAddress_Client = visitorsIPAddr,
                            APIKey = _APIKey,
                            APIName = _APIName,
                            Return_Code = "",
                            Return_Status = "",
                            Return_Message = "",
                            Data = Jsonstr,
                            //Data = responseBody,
                            UserGUID = guid,
                        };

                        _aPILogsRepository.Insert(_APILogs);

                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }



        #region--------------------------Function Helper---------------------------

        private List<MergUserReadyModel> CheckMergValidate(List<Tbl_AutoMergeUser> autoMergeUser,out bool isMerge)
        {
            isMerge = false;
            var model = new List<MergUserReadyModel>();

            var currentUsername = autoMergeUser.Where(m=>m.isPrimary == 1).Select(m=>m.MergeFromUserName).FirstOrDefault();
            var userCurrent = _userRepository.FindAllByUsername(currentUsername);
            string orgGroupCurrent = "";
            if (userCurrent != null)
            {
                orgGroupCurrent = userCurrent.OrgID;
            }
            
            string remark = "";
            string mergeToUsername = "";
            bool isReady = true;
            foreach (var item in autoMergeUser)
            {
                isReady = true;
                remark = "";
                mergeToUsername= "";
                var mergUserReady = new MergUserReadyModel();
                mergUserReady.MergeID = item.MergeID;
                mergUserReady.UserName = item.MergeFromUserName;
                mergUserReady.isPrimary = item.isPrimary;

                #region Check Merg In Org And Non Merg
 
                var userAuthen = _userRepository.FindAllByUsername(item.MergeFromUserName);

                if (userAuthen != null)
                {
                    if (userAuthen.OrgID.Trim() == orgGroupCurrent.Trim())
                    {
                        if ((userAuthen.MergeToUserID != 0) && (userAuthen.MergeToUserID != null))
                        {
                            mergeToUsername = _userRepository.GetUsernameByUserID(userAuthen.MergeToUserID??0);

                            remark += "ถูกรวมบัญชีไป UserID " + userAuthen.MergeToUserID + " (" + mergeToUsername + ") ";
                            isReady = false;
                        }

                    }else
                    {
                        remark += "ไม่ได้อยู่ภายใต้ของบริษัทเดียวกัน ";
                        isReady = false;
                    }
                }else
                {
                    remark += "ไม่มี Username นี้ ";
                    isReady = false;
                }
                #endregion

                //#region Check MappingBSPInGroup

                //foreach (var list in autoMergeUser)
                //{
                //    if (item.MergeFromUserName != list.MergeFromUserName)
                //    {
                //        if (_profileDataService.CheckUserMappingBSPInGroup(list.MergeFromUserName.Trim(), item.MergeFromUserName.Trim()))
                //        {
                //            remark += "BSP Together ";
                //            isReady = false;
                //            break;
                //        }
                //    }
                //}

                //#endregion

                mergUserReady.isReady = isReady?1:0;
                mergUserReady.Remark = remark;

                model.Add(mergUserReady);
            }

            #region Check Is Merge Ready
            var current = model.Where(m => m.isPrimary == 1 && m.isReady == 1).Count();
            var merge = model.Where(m => m.isPrimary != 1 && m.isReady == 1).Count();
            if (current > 0 && merge > 0)
            {
                isMerge = true;
            }else
            {
                isMerge = false;
            }
            #endregion

            return model;
        }

        #endregion

        
    }
}

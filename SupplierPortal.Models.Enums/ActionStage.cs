﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Models.Enums
{
    public enum ActionStage
    {
        [Description("insert")]
        INSERT_STAGE = 1,
        [Description("update")]
        UPDATE_STAGE = 2,
    }
}

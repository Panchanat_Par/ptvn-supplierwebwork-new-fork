﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Models.Enums
{
    public enum SearchOperatorTypeEnum
    {
        [SWW(Code = " Like ", TypeCode = "Like", Description = "Like Operator Condition")]
        Like,

        [SWW(Code = " =  ", TypeCode = "Equal", Description = "Equal Operator Condition")]
        Equal,

        [SWW(Code = " <> ", TypeCode = "NotEqual", Description = "Not Equal Operator Condition")]
        NotEqual,

        [SWW(Code = " >= ", TypeCode = "GreaterEqual", Description = "Greater and Equal Operator Condition")]
        GreaterEqual,

        [SWW(Code = " <= ", TypeCode = "LessEqual", Description = "Less and Equal Operator Condition (if date time concat 23:59:59)")]
        LessEqual,

        [SWW(Code = " <= ", TypeCode = "LessOrEqualWithOwnTime", Description = "Less than or Equal Operator Condition with own time")]
        LessOrEqualWithOwnTime,

        [SWW(Code = " IN ", TypeCode = "In", Description = "In Condition")]
        In,

        [SWW(Code = "NOT IN ", TypeCode = "NotIn", Description = "Not In Condition")]
        NotIn,

        [SWW(Code = " IS ", TypeCode = "Is", Description = "Is null or not null")]
        Is,

        [SWW(Code = " > ", TypeCode = "GreaterThan", Description = "Greater Than Operator Condition")]
        GreaterThan,

        [SWW(Code = " < ", TypeCode = "LessThan", Description = "Less Than Operator Condition")]
        LessThan
    }
}

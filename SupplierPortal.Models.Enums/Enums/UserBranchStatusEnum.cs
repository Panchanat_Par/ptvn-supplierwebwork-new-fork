﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Models.Enums
{
    public enum UserBranchStatusEnum
    {
        [SWW(Code = "Approved")]
        APPROVED,

        [SWW(Code = "Rejected")]
        REJECTED
    }
}

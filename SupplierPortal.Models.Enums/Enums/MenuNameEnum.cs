﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Models.Enums.Enums
{
    public enum MenuNameEnum
    {
        [SWW(Code = "AVL", TypeCode = "AVL")]
        AVL,

        [SWW(Code = "Auction", TypeCode = "Auction")]
        Auction,

        [SWW(Code = "ProcurementBoard", TypeCode = "Procurement Board")]
        ProcurementBoard
    }
}

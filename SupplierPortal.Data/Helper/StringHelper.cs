﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Helper
{
    public static class StringHelper
    {
        public static string GetMakeVerifyString(string str)
        {
            string pattern = @"[ |.|\-|,|(|)]";
            Regex rgx = new Regex(pattern);
            string result = rgx.Replace(str, "");
            return result;
        }

        public static string GetMakeCommaToASCIIString(string str)
        {
            string result = str.Replace(",", "{comma}#44");
            return result;
        }

        public static string GetMakeASCIIStringToComma(string str)
        {
            string result = str.Replace("{comma}#44", ",");
            return result;
        }
    }
}

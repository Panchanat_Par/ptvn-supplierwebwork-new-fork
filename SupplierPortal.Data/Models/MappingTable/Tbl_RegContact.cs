//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SupplierPortal.Data.Models.MappingTable
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tbl_RegContact
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Tbl_RegContact()
        {
            this.Tbl_RegContactAddressMapping = new HashSet<Tbl_RegContactAddressMapping>();
            this.Tbl_RegInfo = new HashSet<Tbl_RegInfo>();
        }
    
        public int ContactID { get; set; }
        public Nullable<int> TitleID { get; set; }
        public string FirstName_Local { get; set; }
        public string FirstName_Inter { get; set; }
        public string LastName_Local { get; set; }
        public string LastName_Inter { get; set; }
        public Nullable<int> JobTitleID { get; set; }
        public string OtherJobTitle { get; set; }
        public string Department { get; set; }
        public string PhoneCountryCode { get; set; }
        public string PhoneNo { get; set; }
        public string PhoneExt { get; set; }
        public string FaxNo { get; set; }
        public string FaxExt { get; set; }
        public string MobileCountryCode { get; set; }
        public string MobileNo { get; set; }
        public string Email { get; set; }
    
        public virtual Tbl_JobTitle Tbl_JobTitle { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tbl_RegContactAddressMapping> Tbl_RegContactAddressMapping { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tbl_RegInfo> Tbl_RegInfo { get; set; }
    }
}

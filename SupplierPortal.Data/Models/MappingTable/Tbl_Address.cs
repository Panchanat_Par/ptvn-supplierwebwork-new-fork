//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SupplierPortal.Data.Models.MappingTable
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tbl_Address
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Tbl_Address()
        {
            this.Tbl_OrgAddress = new HashSet<Tbl_OrgAddress>();
        }
    
        public int AddressID { get; set; }
        public string HouseNo_Local { get; set; }
        public string HouseNo_Inter { get; set; }
        public string VillageNo_Local { get; set; }
        public string VillageNo_Inter { get; set; }
        public string Lane_Local { get; set; }
        public string Lane_Inter { get; set; }
        public string Road_Local { get; set; }
        public string Road_Inter { get; set; }
        public string SubDistrict_Local { get; set; }
        public string SubDistrict_Inter { get; set; }
        public string City_Local { get; set; }
        public string City_Inter { get; set; }
        public string State_Local { get; set; }
        public string State_Inter { get; set; }
        public string CountryCode { get; set; }
        public string PostalCode { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tbl_OrgAddress> Tbl_OrgAddress { get; set; }
    }
}

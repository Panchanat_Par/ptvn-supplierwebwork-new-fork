﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.Profile
{
    public partial class API_AddNotificationModels
    {
        public string ReqID { get; set; }
        public string APIKey { get; set; }
        public int TopicID { get; set; }
        public int SystemID { get; set; }
        public IList<SysUserIDResponse> SysUserID { get; set; }
        public dynamic DataKey { get; set; }
        public NotificationDataResponse NotificationData { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime StopTime { get; set; }
        public dynamic URLParameters { get; set; }
    }

    //public class SysUserIDResponse
    //{
    //    public string value { get; set; }
    //}

    public class NotificationDataResponse
    {
        public dynamic Standard { get; set; }
        public dynamic EN { get; set; }
        public dynamic TH { get; set; }
    }
}

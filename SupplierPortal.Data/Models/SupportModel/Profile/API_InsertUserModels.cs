﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.Profile
{
    public partial class API_InsertUserModels
    {

        public Request Request { get; set; }
    }

    

    public class Request
    {
        public string ReqID { get; set; }
        public string APIKey { get; set; }
        public string APIName { get; set; }
        public DataInsert Data { get; set; }

    }

    public class DataInsert
    {
        public List<PortalLoginID> PortalLoginID { get; set; }
        public SupplierInfo SupplierInfo { get; set; }
        public OrgInfo OrgInfo { get; set; }
        public UserInfo UserInfo { get; set; }
        public UserContact UserContact { get; set; }
        public Address Address { get; set; }
        public AddBy AddBy { get; set; }
    }

    public class SupplierInfo
    {
        public string SupplierShortName { get; set; }
        public string BSPUserGroupID { get; set; }
    }

    public class OrgInfo
    {
        public string PursiteID { get; set; }
        public string OrgID { get; set; }
        public string OrgName { get; set; }
        public string PhoneNo { get; set; }
        public string PhoneExt { get; set; }
        public string MobileNo { get; set; }
        public string FaxNo { get; set; }
        public string FaxExt { get; set; }
    }

    public class UserInfo
    {
        public string LoginID { get; set; }
        public string Locale { get; set; }
        public int Timezone { get; set; }
        public string CurrencyCode { get; set; }
        public string LanguageCode { get; set; }
        public bool IsDisabled { get; set; }
        public bool IsDeleted { get; set; }
    }

    public class UserContact
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Department { get; set; }
        public string PhoneNo { get; set; }
        public string PhoneExt { get; set; }
        public string MobileNo { get; set; }
        public string FaxNo { get; set; }
        public string Email { get; set; }
    }

    public class Address
    {
        public string HouseNo { get; set; }
        public string VillageNo { get; set; }
        public string Lane { get; set; }
        public string Road { get; set; }
        public string SubDistrict { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
    }

    public class AddBy
    {
        public List<SysUserID> SysUserID { get; set; }
    }

    public class SysUserID
    {
        public string value { get; set; }
    }

    public class PortalLoginID
    {
        public string value { get; set; }
    }
}

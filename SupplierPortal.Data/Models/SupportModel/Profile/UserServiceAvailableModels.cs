﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.Profile
{
    public partial class UserServiceAvailableModels
    {
        public Nullable<int> ServiceID { get; set; }
        public string ServiceName { get; set; }
        public Nullable<int> IsActive { get; set; }
        public Nullable<int> IsEnableService { get; set; }
        public Nullable<int> SeqNo { get; set; }
    }
}

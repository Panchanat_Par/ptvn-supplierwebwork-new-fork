﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.Profile
{
    public partial class API_ReplyDataModels
    {

        public Response Response { get; set; }

    }

    public class Response
    {
        public string ReqID { get; set; }
        public Result Result { get; set; }
        public IEnumerable<DataResponse> data { get; set; }
    }

    public class Result
    {
        public string Code { get; set; }
        public string SuccessStatus { get; set; }
        public string Message { get; set; }
    }

    public class DataResponse
    {
        public IList<SysUserIDResponse> SysUserID { get; set; }
    }

    public class SysUserIDResponse
    {
        public string value { get; set; }
    }
}

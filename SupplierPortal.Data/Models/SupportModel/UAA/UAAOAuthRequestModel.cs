﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.UAA
{
    public class UAAOAuthRequestModel
    {
        public string username;
        public string firstName;
        public string lastName;
        public string firstNameLocal;
        public string lastNameLocal;
        public string email;
        public string telephone;
        public string roleName;
        public string password;
        public string grant_type;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.MenuPortal
{
    public partial class API_NotificationResponseModel
    {
        public Response Response { get; set; }

    }

    public class Response
    {
        public Result Result { get; set; }
        public List<DataResponse> Data { get; set; }
        public string ReqID { get; set; }
    }

    public class Result
    {
        public string Message { get; set; }
        public string SuccessStatus { get; set; }
        public string Code { get; set; }
    }

    public class DataResponse
    {
        public List<Notification> Notification { get; set; }
    }

    public class Notification
    {
        public string NotificationID { get; set; }
        public string NotificationValue { get; set; }
    }
    
}

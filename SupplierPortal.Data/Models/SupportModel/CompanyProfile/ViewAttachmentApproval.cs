﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.CompanyProfile
{
    public partial class ViewAttachmentApproval
    {
        public int ItemId { get; set; }
        public int TrackingStatusId { get; set; }
        public string TrackingStatusName { get; set; }
        public DateTime TrackingStatusDate { get; set; }
        public string Remark;
    }
}

﻿using SupplierPortal.Data.CustomValidate;
using SupplierPortal.Data.Models.CustomValidate;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SupplierPortal.Data.Models.SupportModel.CompanyProfile
{

    public class CompanyAddress : TrackingStatus
    {
        public Nullable<int> SupplierID { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblHouseNo")]
        [RequiredErrorMessageBase("_ComPro.Address.ValidateMsg.RequireHouseNo")]
        [StringLengthErrorMessage(1000, "_ComPro.ValidateMsg.AddressLength", 0)]
        public string CompanyAddress_HouseNo_Local { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblHouseNo")]
        [RequiredErrorMessageBase("_ComPro.Address.ValidateMsg.RequireHouseNo")]
        [StringLengthErrorMessage(1000, "_ComPro.ValidateMsg.AddressLength", 0)]
        public string CompanyAddress_HouseNo_Inter { get; set; }


        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblVillageNo")]
        //[RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireVillageNo")]
        public string CompanyAddress_VillageNo_Local { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblVillageNo")]
        //[RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireVillageNo")]
        public string CompanyAddress_VillageNo_Inter { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblLane")]
        //[RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireLane")]
        public string CompanyAddress_Lane_Local { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblLane")]
        //[RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireLane")]
        public string CompanyAddress_Lane_Inter { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblStreet")]
        //[RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireRoad")]
        public string CompanyAddress_Road_Local { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblStreet")]
        //[RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireRoad")]
        public string CompanyAddress_Road_Inter { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblSubDistrict")]
        [RequiredErrorMessageBase("_ComPro.Address.ValidateMsg.RequireSubDistrict")]
        public string CompanyAddress_SubDistrict_Local { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblSubDistrict")]
        [RequiredErrorMessageBase("_ComPro.Address.ValidateMsg.RequireSubDistrict")]
        public string CompanyAddress_SubDistrict_Inter { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblCity")]
        [RequiredErrorMessageBase("_ComPro.Address.ValidateMsg.RequireCity")]
        public string CompanyAddress_City_Local { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblCity")]
        [RequiredErrorMessageBase("_ComPro.Address.ValidateMsg.RequireCity")]
        public string CompanyAddress_City_Inter { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblState")]
        [RequiredErrorMessageBase("_ComPro.Address.ValidateMsg.RequireState")]
        public string CompanyAddress_State_Local { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblState")]
        [RequiredErrorMessageBase("_ComPro.Address.ValidateMsg.RequireState")]
        public string CompanyAddress_State_Inter { get; set; }


        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblPostalCode")]
        [RequiredErrorMessageBase("_ComPro.Address.ValidateMsg.RequirePostalCode")]
        //[PostalCodeValidate("_Regis.Msg_PostalCodeFormat", "deliveraddress")]
        [Remote("PostalCodeValidateComp", "ContactInformation", AdditionalFields = "CompanyAddress_CountryCode_VI")]
        public string CompanyAddress_PostalCode { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.LblCompanyCountry")]
        //[RequiredDeliverErrorMessage("_Regis.ValidateMsg.RequireCountry")]
        public string CompanyAddress_CountryCode { get; set; }

        public string CompanyAddress_CountryName { get; set; }


        //public IEnumerable<Tbl_Language> LanguageList { get; set; }

        //public DynamicLanguageModels DynamicLanguageList { get; set; }

        //=========================================================================

        public Nullable<int> CompanyAddressID { get; set; }
        ////[RequiredDeliverErrorMessage("_Regis.Msg_RequireHouseNo")]
        //public IList<LocalizedPropModel> CompanyHouseNoLocalizedList { get; set; }
        ////[RequiredDeliverErrorMessage("_Regis.Msg_RequireVillageNo")]
        //public IList<LocalizedPropModel> CompanyVillageNoLocalizedList { get; set; }
        ////[RequiredDeliverErrorMessage("_Regis.Msg_RequireLane")]
        //public IList<LocalizedPropModel> CompanyLaneLocalizedList { get; set; }
        ////[RequiredDeliverErrorMessage("_Regis.Msg_RequireRoad")]
        //public IList<LocalizedPropModel> CompanyRoadLocalizedList { get; set; }
        ////[RequiredDeliverErrorMessage("_Regis.Msg_RequireSubDistrict")]
        //public IList<LocalizedPropModel> CompanySubDistrictLocalizedList { get; set; }
        ////[RequiredDeliverErrorMessage("_Regis.Msg_RequireCity")]
        //public IList<LocalizedPropModel> CompanyCityLocalizedList { get; set; }
        ////[RequiredDeliverErrorMessage("_Regis.Msg_RequireState")]
        //public IList<LocalizedPropModel> CompanyStateLocalizedList { get; set; }

        //===========================================================================


        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblHouseNo")]
        [RequiredErrorMessageBase("_ComPro.Address.ValidateMsg.RequireHouseNo")]
        [StringLengthErrorMessage(1000, "_ComPro.ValidateMsg.AddressLength", 0)]
        public string DeliverAddress_HouseNo_Local { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblHouseNo")]
        [RequiredErrorMessageBase("_ComPro.Address.ValidateMsg.RequireHouseNo")]
        [StringLengthErrorMessage(1000, "_ComPro.ValidateMsg.AddressLength", 0)]
        public string DeliverAddress_HouseNo_Inter { get; set; }


        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblVillageNo")]
        //[RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireVillageNo")]
        public string DeliverAddress_VillageNo_Local { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblVillageNo")]
        //[RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireVillageNo")]
        public string DeliverAddress_VillageNo_Inter { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblLane")]
        //[RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireLane")]
        public string DeliverAddress_Lane_Local { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblLane")]
        //[RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireLane")]
        public string DeliverAddress_Lane_Inter { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblStreet")]
        //[RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireRoad")]
        public string DeliverAddress_Road_Local { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblStreet")]
        //[RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireRoad")]
        public string DeliverAddress_Road_Inter { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblSubDistrict")]
        [RequiredErrorMessageBase("_ComPro.Address.ValidateMsg.RequireSubDistrict")]
        public string DeliverAddress_SubDistrict_Local { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblSubDistrict")]
        [RequiredErrorMessageBase("_ComPro.Address.ValidateMsg.RequireSubDistrict")]
        public string DeliverAddress_SubDistrict_Inter { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblCity")]
        [RequiredErrorMessageBase("_ComPro.Address.ValidateMsg.RequireCity")]
        public string DeliverAddress_City_Local { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblCity")]
        [RequiredErrorMessageBase("_ComPro.Address.ValidateMsg.RequireCity")]
        public string DeliverAddress_City_Inter { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblState")]
        [RequiredErrorMessageBase("_ComPro.Address.ValidateMsg.RequireState")]
        public string DeliverAddress_State_Local { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblState")]
        [RequiredErrorMessageBase("_ComPro.Address.ValidateMsg.RequireState")]
        public string DeliverAddress_State_Inter { get; set; }


        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblPostalCode")]
        [RequiredErrorMessageBase("_ComPro.Address.ValidateMsg.RequirePostalCode")]
        //[PostalCodeValidate("_Regis.Msg_PostalCodeFormat", "deliveraddress")]
        [Remote("PostalCodeValidateDelv", "ContactInformation", AdditionalFields = "DeliverAddress_CountryCode_VI")]
        public string DeliverAddress_PostalCode { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.LblCompanyCountry")]
        //[RequiredDeliverErrorMessage("_Regis.ValidateMsg.RequireCountry")]
        public string DeliverAddress_CountryCode { get; set; }

        public string DeliverAddress_CountryName { get; set; }

        //=========================================================================

        public Nullable<int> DeliverAddressID { get; set; }
        ////[RequiredDeliverErrorMessage("_Regis.Msg_RequireHouseNo")]
        //public IList<LocalizedPropModel> DeliverHouseNoLocalizedList { get; set; }
        ////[RequiredDeliverErrorMessage("_Regis.Msg_RequireVillageNo")]
        //public IList<LocalizedPropModel> DeliverVillageNoLocalizedList { get; set; }
        ////[RequiredDeliverErrorMessage("_Regis.Msg_RequireLane")]
        //public IList<LocalizedPropModel> DeliverLaneLocalizedList { get; set; }
        ////[RequiredDeliverErrorMessage("_Regis.Msg_RequireRoad")]
        //public IList<LocalizedPropModel> DeliverRoadLocalizedList { get; set; }
        ////[RequiredDeliverErrorMessage("_Regis.Msg_RequireSubDistrict")]
        //public IList<LocalizedPropModel> DeliverSubDistrictLocalizedList { get; set; }
        ////[RequiredDeliverErrorMessage("_Regis.Msg_RequireCity")]
        //public IList<LocalizedPropModel> DeliverCityLocalizedList { get; set; }
        ////[RequiredDeliverErrorMessage("_Regis.Msg_RequireState")]
        //public IList<LocalizedPropModel> DeliverStateLocalizedList { get; set; }

        public string ComCountryCode { get; set; }
        public string ComPostalCode { get; set; }

        public string DeliCountryCode { get; set; }
        public string DeliPostalCode { get; set; }

        public string CompAddrCountry { get; set; }
        public string DelvAddrCountry { get; set; }

        public int AddressTypeID { get; set; }
        public Nullable<int> SeqNo { get; set; }
        //===========================================================================

        public bool CheckB { get; set; }

        //=====================
     
        public string CountryName { get; set; }
        public IEnumerable<SelectListItem> CountryList { get; set; }
        public Tbl_Country Country { get; set; }


        public Dictionary<string, TrackingCompProfile> trackingFields { get; set; }
        public Dictionary<string, TrackingCompProfile> trackingFieldsDel { get; set; }
        public Nullable<int> TrackingStatusID { get; set; }
        public Nullable<int> TrackingReqID { get; set; }
        public string JsonStr { get; set; }
        public string JsonStrDel { get; set; }
        public bool CheckModifyDelvAddr { get; set; }


        public Tbl_TrackingOrgAddress trackingOrgAddress { get; set; }

        public Tbl_TrackingOrgAddress trackingAddress { get; set; }
        public Tbl_TrackingOrgAddress trackingDelivery { get; set; }

        public bool chkDataChange { get; set; }
    }
    //public class LocalizedPropModel
    //{
    //    public Nullable<int> EntityID { get; set; }
    //    public Nullable<int> LanguageID { get; set; }
    //    public string LocaleKeyGroup { get; set; }
    //    public string LocaleKey { get; set; }
    //    //[RequiredSubmitErrorMessage("_Regis.Msg_RequireCompanyName")]
    //    public string LocaleValue { get; set; }
    //    public string Flag { get; set; }
    //}

}


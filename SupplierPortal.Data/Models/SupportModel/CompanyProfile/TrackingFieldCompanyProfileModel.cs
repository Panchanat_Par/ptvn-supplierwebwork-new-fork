﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.CompanyProfile
{
    public partial class TrackingFieldCompanyProfileModel
    {
        public int TrackingReqID { get; set; }
        public string TrackingKey { get; set; }
        public int FieldID { get; set; }
        public string ResourceName { get; set; }
        public int isForeignKey { get; set; }
        public int OtherFieldID { get; set; }
        public string ForeignKeyGroup { get; set; }
        public string ForeignKey { get; set; }
        public string OldKeyValue { get; set; }
        public string NewKeyValue { get; set; }
        public int TrackingStatusID { get; set; }
        public string TrackingStatusName { get; set; }
    }
}

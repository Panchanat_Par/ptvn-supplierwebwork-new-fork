﻿using SupplierPortal.Data.CustomValidate;
using SupplierPortal.Data.Models.CustomValidate;
using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SupplierPortal.Data.Models.SupportModel.CompanyProfile
{
    public partial class CompanyGeneralModel  : TrackingStatus
    {
        public string OrgID { get; set; }
        public int SupplierID { get; set; }
        [ResourceDisplayName("_ComPro.CompanyInfo.LblCompanyType")]
        public Nullable<int> CompanyTypeID { get; set; }
        [ResourceDisplayName("_ComPro.CompanyInfo.LblBisEntityType")]
        public Nullable<int> BusinessEntityID { get; set; }
        //[ResourceDisplayName("_ComPro.LblOtherBusinessEntity")]
        public string OtherBusinessEntity { get; set; }
        [ResourceDisplayName("_ComPro.CompanyInfo.LblCompanyName")]
        [RequiredErrorMessageBase("_ComPro.ValidateMsg.RequireCompanyName")]
        public string CompanyName_Local { get; set; }
        [RequiredErrorMessageBase("_ComPro.ValidateMsg.RequireCompanyName")]
        public string CompanyName_Inter { get; set; }
        [ResourceDisplayName("_ComPro.CompanyInfo.LblCompanyCountry")]
        public string CountryCode { get; set; }
        //[ResourceDisplayName("_ComPro.CompanyInfo.LblCompanyCountry")]
        public string CountryName { get; set; }
        [ResourceDisplayName("_ComPro.CompanyInfo.LblTaxNo.Corp")]
        //[Remote("ValidateTaxID", "GeneralInformation", AdditionalFields = "CountryCode_VI,CompanyTypeID")]
        public string TaxID { get; set; }
        [ResourceDisplayName("_ComPro.CompanyInfo.LblDUNSNumber")]
        [Remote("ValidateDUNSNumber", "GeneralInformation")]
        public string DUNSNumber { get; set; }
        public string BranchNo { get; set; }
        [ResourceDisplayName("_ComPro.CompanyInfo.LblTaxNo.BranchName")]
        public string BranchName_Local { get; set; }
        [ResourceDisplayName("_ComPro.CompanyInfo.LblTaxNo.BranchName")]
        public string BranchName_Inter { get; set; }


        public Dictionary<string, string> OldKeyValue { get; set; }
        public Dictionary<string, string> NewKeyValue { get; set; }

        public Dictionary<string, TrackingCompProfile> trackingFields { get; set; }
        public Nullable<int> TrackingStatusID { get; set; }
        public Nullable<int> TrackingReqID { get; set; }
        public Nullable<int> TrackingItemID { get; set; }
        public string TrackingType { get; set; }
        public string JsonStr { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.CompanyProfile
{
    public partial class ContactPersonTableModel
    {
        public int ContactID { get; set; }
        public string TitleContactName { get; set; }
        public string Fullname { get; set; }
        public Nullable<int> TitleID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Nullable<int> JobTitleID { get; set; }
        public string JobTitleName { get; set; }
        public string OtherJobTitle { get; set; }
        public string Department { get; set; }
        public string PhoneNo { get; set; }
        public string PhoneExt { get; set; }
        public string MobileNo { get; set; }
        public string FaxNo { get; set; }
        public string FaxExt { get; set; }
        public string Email { get; set; }
    }
}

﻿using SupplierPortal.Data.Helper;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.ContactPerson;
using SupplierPortal.Data.Models.Repository.Organization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.CompanyProfile
{
    public class TrackingStatus
    {
        private SupplierPortalEntities context;
        public TrackingStatus()
        {
            context = new SupplierPortalEntities();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TClass">Mapping Class Name ex. Tbl_Organization</typeparam>
        /// <returns></returns>
        public Dictionary<string, TrackingCompProfile> GetTrackingStatus<TClass>(int EntityID)
            where TClass : class
        {
            Dictionary<string, TrackingCompProfile> trackingFields = new Dictionary<string, TrackingCompProfile>();

            #region Loop for set Tracking

            int isInter = LanguageHelper.GetLanguageIsInter();

            var type = typeof(TClass);
            var tabelName = type.Name;

            foreach (var attr in type.GetProperties())
            {
                var result = new TrackingCompProfile { TrackingStatusID = 1, EditTracking = false };
                //Tbl_TrackingCompProfile
                //var tracking = context.Tbl_TrackingCompProfile
                //                      .FirstOrDefault(m => m.EntityID == EntityID
                //                          && m.TrackingKeyGroup == tabelName
                //                          && m.TrackingKey == attr.Name
                //                          && m.TrackingStatusID == 2);

                var tracking = (from a in context.Tbl_TrackingCompProfile
                               join b in context.Tbl_TrackingRequest on a.TrackingReqID equals b.TrackingReqID where b.isDeleted != 1
                               where a.EntityID == EntityID
                                  && a.TrackingKeyGroup == tabelName
                                  && a.TrackingKey == attr.Name
                                  && (a.TrackingStatusID == 2 || a.TrackingStatusID == 5)
                                  
                               select a).FirstOrDefault();
                //Tbl_TrackingOrgAddress
                //var trackingOrgAddress = (from a in context.Tbl_TrackingOrgAddress
                //                         join b in context.Tbl_TrackingRequest on a.TrackingReqID equals
                //                         b.TrackingReqID
                //                         where b.SupplierID == supID && a.TrackingStatusID == 2
                //                         select a).FirstOrDefault();

                //context.Tbl_TrackingOrgAddress.FirstOrDefault(m => m.SupplierID == supID && m.TrackingStatusID == 2);
                if (tracking != null && tracking.TrackingKeyGroup == "Tbl_Organization")
                {
                    var trackReq = context.Tbl_TrackingRequest.FirstOrDefault(m => m.TrackingReqID == tracking.TrackingReqID) ?? null;

                    result.ReqId = tracking.TrackingReqID;
                    result.EntityID = tracking.EntityID ?? null;
                    result.TrackingKeyGroup = tracking.TrackingKeyGroup ?? "";
                    result.TrackingKey = tracking.TrackingKey ?? "";
                    if (tracking.TrackingKey == "BusinessEntityID")
                    {
                        result.OldKeyValue = tracking.OldKeyValue.Split('|').FirstOrDefault() ?? "";
                        result.NewKeyValue = tracking.NewKeyValue.Split('|').FirstOrDefault() ?? "";
                    }
                    else
                    {
                        result.OldKeyValue = tracking.OldKeyValue ?? "";
                        result.NewKeyValue = tracking.NewKeyValue ?? "";
                    }

                    result.TrackingStatusID = tracking.TrackingStatusID ?? null;
                    result.FieldID = tracking.FieldID ?? null;
                    result.TrackingItemID = tracking.TrackingItemID;
                    result.TrackingGrpID = 1;

                    //result.UpdateBy = tracking.UpdateBy;

                    if (tracking.TrackingKey == "CompanyTypeID")
                    {
                        result.NameNewKeyValue = GetLocalizedProperty.GetLocalizedValue(isInter.ToString(), "Tbl_CompanyType", "CompanyType", tracking.NewKeyValue);
                        result.NameOldKeyValue = GetLocalizedProperty.GetLocalizedValue(isInter.ToString(), "Tbl_CompanyType", "CompanyType", tracking.OldKeyValue);
                    }
                    else if (tracking.TrackingKey == "BusinessEntityID")
                    {
                        result.NameNewKeyValue = GetLocalizedProperty.GetLocalizedValue(isInter.ToString(), "Tbl_BusinessEntity", "BusinessEntity", tracking.NewKeyValue.Split('|').FirstOrDefault());
                        result.NameOldKeyValue = GetLocalizedProperty.GetLocalizedValue(isInter.ToString(), "Tbl_BusinessEntity", "BusinessEntity", tracking.OldKeyValue.Split('|').FirstOrDefault());
                    }
                    else
                    {
                        result.NameNewKeyValue = tracking.NewKeyValue ?? "";
                        result.NameOldKeyValue = tracking.OldKeyValue ?? "";
                    }
                    if (trackReq != null)
                    {

                        result.UpdateDate = trackReq.ReqDate;

                        var userModel = context.Tbl_User.Where(m => m.UserID == trackReq.ReqBy).FirstOrDefault();
                        if (userModel != null)
                        {
                            var contactPersonModel = context.Tbl_ContactPerson.Where(m => m.ContactID == userModel.ContactID).FirstOrDefault();
                            if (contactPersonModel != null)
                            {
                                result.FirstName = (isInter == 1 ? contactPersonModel.FirstName_Inter : contactPersonModel.FirstName_Local);
                                result.LastName = (isInter == 1 ? contactPersonModel.LastName_Inter : contactPersonModel.LastName_Local);
                            }
                        }
                    }


                }
                else if (tracking != null && tracking.TrackingKeyGroup == "Tbl_Address")
                {
                    var trackingOrgAddress = (from a in context.Tbl_TrackingOrgAddress
                                              where a.TrackingReqID == tracking.TrackingReqID
                                              && a.TrackingStatusID == 2
                                              select a).FirstOrDefault();

                    if (trackingOrgAddress != null && tracking.TrackingReqID == trackingOrgAddress.TrackingReqID)
                    {
                        if (trackingOrgAddress.OldAddressID == trackingOrgAddress.NewAddressID)
                        {
                            //var trackReq = context.Tbl_TrackingRequest.FirstOrDefault(m => m.TrackingReqID == tracking.TrackingReqID) ?? null;

                            result.ReqId = tracking.TrackingReqID;
                            result.EntityID = tracking.EntityID ?? null;
                            result.TrackingKeyGroup = tracking.TrackingKeyGroup ?? "";
                            result.TrackingKey = tracking.TrackingKey ?? "";
                            result.OldKeyValue = tracking.OldKeyValue ?? "";
                            result.NewKeyValue = tracking.NewKeyValue ?? "";
                            result.TrackingStatusID = tracking.TrackingStatusID ?? null;
                            result.FieldID = tracking.FieldID ?? null;
                            result.TrackingItemID = tracking.TrackingItemID;
                            result.TrackingGrpID = 3;
                            result.TrackingOrgItemID = trackingOrgAddress.TrackingItemID.ToString();
                        }
                        else
                        {
                            //var trackReq = context.Tbl_TrackingRequest.FirstOrDefault(m => m.TrackingReqID == trackingOrgAddress.TrackingReqID) ?? null;

                            result.ReqId = trackingOrgAddress.TrackingReqID;
                            //result.SupplierID = trackingOrgAddress.SupplierID;
                            result.AdressTypeID = trackingOrgAddress.AdressTypeID ?? null;
                            result.OldAddressID = trackingOrgAddress.OldAddressID ?? null;
                            result.NewAddressID = trackingOrgAddress.NewAddressID ?? null;
                            result.TrackingStatusID = trackingOrgAddress.TrackingStatusID ?? null;
                            result.TrackingItemID = tracking.TrackingItemID;
                            result.TrackingGrpID = 3;
                            result.TrackingOrgItemID = trackingOrgAddress.TrackingItemID.ToString();
                        }
                    }
                    else if (tracking.TrackingReqID == -1)
                    {

                        result.ReqId = tracking.TrackingReqID;
                        result.EntityID = tracking.EntityID ?? null;
                        result.TrackingKeyGroup = tracking.TrackingKeyGroup ?? "";
                        result.TrackingKey = tracking.TrackingKey ?? "";
                        result.OldKeyValue = tracking.OldKeyValue ?? "";
                        result.NewKeyValue = tracking.NewKeyValue ?? "";
                        result.TrackingStatusID = tracking.TrackingStatusID ?? null;
                        result.FieldID = tracking.FieldID ?? null;
                        result.TrackingItemID = tracking.TrackingItemID;
                        result.TrackingGrpID = 3;
                        result.TrackingOrgItemID = trackingOrgAddress.TrackingItemID.ToString();
                    }

                }

                //var trackings = context.Tbl_TrackingCompProfile
                //                         .FirstOrDefault(m => m.EntityID == EntityID
                //                         && m.TrackingKeyGroup == "Tbl_Organization"
                //                         && m.TrackingKey == "BusinessEntityID"
                //                         && m.TrackingStatusID == 2);
                //if (int.Parse(trackings.NewKeyValue.Split('|').FirstOrDefault()) == -1)
                //{
                //    result.TrackingKeyGroup = "Tbl_Organization";
                //    result.TrackingKey = "OtherBusinessEntity";
                //    result.TrackingStatusID = 1;
                //    if (trackings.OldKeyValue.Contains('|'))
                //    {
                //        result.OldKeyValue = trackings.OldKeyValue.Split('|').LastOrDefault() ?? "";
                //    }
                //    result.NewKeyValue = trackings.NewKeyValue.Split('|').LastOrDefault() ?? "";

                //}
                trackingFields.Add(attr.Name, result);
            }

            #endregion
            return trackingFields;
        }

    }
}

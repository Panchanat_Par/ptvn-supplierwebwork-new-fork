﻿using SupplierPortal.Data.CustomValidate;
using SupplierPortal.Data.Models.CustomValidate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SupplierPortal.Data.Models.SupportModel.CompanyProfile
{
    public partial class CompanyContactPersonModel
    {
        public string Proc { get; set; }

        public int ContactID { get; set; }
        public int SupplierID { get; set; }
        [RequiredErrorMessageBase("_ComPro.ContactPerson.ValidateMsg.RequireEmail")]
        [ResourceDisplayName("_ComPro.ContactPerson.LblEmail")]
        [Remote("IsEmailAvailableConfig", "CompanyContact", AdditionalFields = "InitialEmail")]
        public string Email { get; set; }
        public string InitialEmail { get; set; }
        //[RequiredErrorMessageBase("_ComPro.ContactPerson.ValidateMsg.RequireTitleName")]
        public Nullable<int> TitleID { get; set; }
        public Nullable<int> TitleID_Inter { get; set; }
        [RequiredErrorMessageBase("_ComPro.ContactPerson.ValidateMsg.RequireFirstName")]
        [ResourceDisplayName("_ComPro.ContactPerson.LblContactName")]
        public string FirstName_Local { get; set; }
        [RequiredErrorMessageBase("_ComPro.ContactPerson.ValidateMsg.RequireLastName")]
        public string LastName_Local { get; set; }
        [RequiredErrorMessageBase("_ComPro.ContactPerson.ValidateMsg.RequireFirstName")]
        public string FirstName_Inter { get; set; }
        [RequiredErrorMessageBase("_ComPro.ContactPerson.ValidateMsg.RequireLastName")]
        public string LastName_Inter { get; set; }
        [ResourceDisplayName("_ComPro.ContactPerson.LblJobTitle")]
        public Nullable<int> JobTitleID { get; set; }
        public string OtherJobTitle { get; set; }
        [ResourceDisplayName("_ComPro.ContactPerson.LblDepartment")]
        public string Department { get; set; }
        [ResourceDisplayName("_ComPro.ContactPerson.LblPhone")]
        [RequiredErrorMessageBase("_ComPro.ContactPerson.ValidateMsg.RequirePhone")]
        public string PhoneNo { get; set; }
        [ResourceDisplayName("_ComPro.ContactPerson.LblPhoneExt")]
        public string PhoneExt { get; set; }
        [ResourceDisplayName("_ComPro.ContactPerson.LblMobile")]
        public string MobileNo { get; set; }
        [ResourceDisplayName("_ComPro.ContactPerson.LblFax")]
        public string FaxNo { get; set; }
        [ResourceDisplayName("_ComPro.ContactPerson.LblFaxExt")]
        public string FaxExt { get; set; }

        public Nullable<int> AddressID { get; set; }

        [RequiredErrorMessageBase("_ComPro.ContactPerson.ValidateMsg.RequireHouseNo")]
        [ResourceDisplayName("_ComPro.ContactPerson.Address.LblHouseNo")]
        public string Address_HouseNo_Local { get; set; }
        [ResourceDisplayName("_ComPro.ContactPerson.Address.LblVillageNo")]
        public string Address_VillageNo_Local { get; set; }
        [ResourceDisplayName("_ComPro.ContactPerson.Address.LblLane")]
        public string Address_Lane_Local { get; set; }
        [ResourceDisplayName("_ComPro.ContactPerson.Address.LblStreet")]
        public string Address_Road_Local { get; set; }
        [ResourceDisplayName("_ComPro.ContactPerson.Address.LblSubDistrict")]
        public string Address_SubDistrict_Local { get; set; }
        [ResourceDisplayName("_ComPro.ContactPerson.Address.LblCity")]
        public string Address_City_Local { get; set; }
        [ResourceDisplayName("_ComPro.ContactPerson.Address.LblState")]
        public string Address_State_Local { get; set; }
        [RequiredErrorMessageBase("_ComPro.ContactPerson.ValidateMsg.RequirePostalCode")]
        [Remote("PostalCodeValidateCompany", "CompanyContact", AdditionalFields = "Address_CountryCode_VI")]
        [ResourceDisplayName("_ComPro.ContactPerson.Address.LblPostalCode")]
        public string Address_PostalCode { get; set; }
        [ResourceDisplayName("_ComPro.ContactPerson.Address.LblCountry")]
        public string Address_CountryCode { get; set; }
        [ResourceDisplayName("_ComPro.ContactPerson.SaleInfo.LblBuyerList.Title")]
        public string Address_BuyerList { get; set; }
        [ResourceDisplayName("_ComPro.ContactPerson.SaleInfo.LblProductList.Title")]
        public string Address_Product { get; set; }

        public string Address_AddProduct { get; set; }
        public bool isAllowAccess { get; set; }

    }
}

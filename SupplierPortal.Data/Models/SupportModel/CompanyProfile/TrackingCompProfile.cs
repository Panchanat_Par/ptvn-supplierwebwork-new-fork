﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.CompanyProfile
{
    public class TrackingCompProfile
    {
        public bool EditTracking { get; set; }
        public int ReqId { get; set; }
        public Nullable<int> EntityID { get; set; }
        public string TrackingKeyGroup { get; set; }
        public string TrackingKey { get; set; }
        public string OldKeyValue { get; set; }
        public string NewKeyValue { get; set; }
        public Nullable<int> TrackingStatusID { get; set; }
        public Nullable<int> FieldID { get; set; }
        public Nullable<int> UpdateBy { get; set; }
        public Nullable<System.DateTime> UpdateDate { get; set; }
        public int TrackingItemID { get; set; }
        public string TrackingOrgItemID { get; set; }
        public int TrackingGrpID { get; set; }
        public Nullable<int> SupplierID { get; set; }
        public Nullable<int> AdressTypeID { get; set; }
        public Nullable<int> OldAddressID { get; set; }
        public Nullable<int> NewAddressID { get; set; }


        public string NameOldKeyValue { get; set; }
        public string NameNewKeyValue { get; set; }
        public string FirstName { get; set; }
        public string  LastName { get; set; }

        
    }
}

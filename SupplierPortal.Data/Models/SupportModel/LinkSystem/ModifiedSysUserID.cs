﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.LinkSystem
{
    public class ModifiedSysUserID
    {
        public string value { get; set; }
        public string SystemID { get; set; }
    }
}

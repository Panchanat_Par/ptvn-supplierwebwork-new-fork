﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel
{
    public partial class BusinessEntityDisplayModel
    {

        public int Id { get; set; }
        public string BusinessEntity { get; set; }
        public string BusinessEntityDisplay { get; set; }
        public string Description { get; set; }
        public Nullable<int> CompanyTypeID { get; set; }
        public Nullable<int> SeqNo { get; set; }
        public string ConcatStrLocal { get; set; }
        public string ConcatStrInter { get; set; }
    }
}

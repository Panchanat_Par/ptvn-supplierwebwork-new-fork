﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.Register
{
    public class CategoryModel
    {
        public int CategoryID_Lev1 { get; set; }
        public string CategoryCode { get; set; }
        public string CategoryName_Local { get; set; }
        public string CategoryName_Inter { get; set; }
        public string CategoryDesc_Local { get; set; }
        public string CategoryDesc_Inter { get; set; }
        public Tbl_ProductCategoryLev3 CategoryLv3 { get; set; }

    }

    public class productTypeCattogory
    {
        public string ProductTypeID { get; set; }
        public int CategoryID_Lev3 { get; set; }
        public int SupplierID { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.Consent
{
    public class ConsentAcceptModel : ConsentBaseModel
    {
        public ConsentAcceptPayloadModel payload { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace SupplierPortal.Data.Models.SupportModel.AccountPortal
{
    class SendEmailModel
    {

        public string Email { get; set; }
        public string DisplayName { get; set; }
        public string Name { get; set; }
        public string ToEmailAddress { get; set; }
        public string CcEmailAddress { get; set; }
        public string BccEmailAddress { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.AutoMerge
{
    public partial class MergUserReadyModel
    {
        public int MergeID { get; set; }
        public string UserName { get; set; }
        public Nullable<int> isPrimary { get; set; }
        public Nullable<int> isReady { get; set; }
        public string Remark { get; set; }

    }
}

﻿using SupplierPortal.Data.CustomValidate;
using SupplierPortal.Data.Models.CustomValidate;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.CompanyProfile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SupplierPortal.Data.Models.SupportModel
{
    public class GeneralAddressHomeAdmin : TrackingStatus
    {

        public int Userid { get; set; }
        //general model
        public string OrgID { get; set; }
        [ResourceDisplayName("_ComPro.CompanyInfo.LblCompanyType")]
        public Nullable<int> CompanyTypeID { get; set; }
        [ResourceDisplayName("_ComPro.CompanyInfo.LblBisEntityType")]
        public Nullable<int> BusinessEntityID { get; set; }
        //[ResourceDisplayName("_ComPro.LblOtherBusinessEntity")]
        public string OtherBusinessEntity { get; set; }
        [ResourceDisplayName("_ComPro.CompanyInfo.LblCompanyName")]
        [RequiredErrorMessageBase("_ComPro.ValidateMsg.RequireCompanyName")]
        public string CompanyName_Local { get; set; }
        [RequiredErrorMessageBase("_ComPro.ValidateMsg.RequireCompanyName")]
        public string CompanyName_Inter { get; set; }
        [ResourceDisplayName("_ComPro.CompanyInfo.LblCompanyCountry")]
        public string CountryCode { get; set; }
        //[ResourceDisplayName("_ComPro.CompanyInfo.LblCompanyCountry")]
        public string CountryName { get; set; }
        [ResourceDisplayName("_ComPro.CompanyInfo.LblTaxNo.Corp")]
        [Remote("ValidateTaxID", "GeneralInformation", AdditionalFields = "CountryCode_VI,CompanyTypeID")]
        public string TaxID { get; set; }
        [ResourceDisplayName("_ComPro.CompanyInfo.LblDUNSNumber")]
        [Remote("ValidateDUNSNumber", "GeneralInformation")]
        public string DUNSNumber { get; set; }
        public string BranchNo { get; set; }
        [ResourceDisplayName("_ComPro.CompanyInfo.LblTaxNo.BranchName")]
        public string BranchName_Local { get; set; }
        [ResourceDisplayName("_ComPro.CompanyInfo.LblTaxNo.BranchName")]
        public string BranchName_Inter { get; set; }

        //address model
        public Nullable<int> SupplierID { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblHouseNo")]
        [RequiredErrorMessageBase("_ComPro.Address.ValidateMsg.RequireHouseNo")]
        [StringLengthErrorMessage(1000, "_ComPro.ValidateMsg.AddressLength", 0)]
        public string CompanyAddress_HouseNo_Local { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblHouseNo")]
        [RequiredErrorMessageBase("_ComPro.Address.ValidateMsg.RequireHouseNo")]
        [StringLengthErrorMessage(1000, "_ComPro.ValidateMsg.AddressLength", 0)]
        public string CompanyAddress_HouseNo_Inter { get; set; }


        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblVillageNo")]
        //[RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireVillageNo")]
        public string CompanyAddress_VillageNo_Local { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblVillageNo")]
        //[RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireVillageNo")]
        public string CompanyAddress_VillageNo_Inter { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblLane")]
        //[RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireLane")]
        public string CompanyAddress_Lane_Local { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblLane")]
        //[RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireLane")]
        public string CompanyAddress_Lane_Inter { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblStreet")]
        //[RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireRoad")]
        public string CompanyAddress_Road_Local { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblStreet")]
        //[RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireRoad")]
        public string CompanyAddress_Road_Inter { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblSubDistrict")]
        [RequiredErrorMessageBase("_ComPro.Address.ValidateMsg.RequireSubDistrict")]
        public string CompanyAddress_SubDistrict_Local { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblSubDistrict")]
        [RequiredErrorMessageBase("_ComPro.Address.ValidateMsg.RequireSubDistrict")]
        public string CompanyAddress_SubDistrict_Inter { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblCity")]
        [RequiredErrorMessageBase("_ComPro.Address.ValidateMsg.RequireCity")]
        public string CompanyAddress_City_Local { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblCity")]
        [RequiredErrorMessageBase("_ComPro.Address.ValidateMsg.RequireCity")]
        public string CompanyAddress_City_Inter { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblState")]
        [RequiredErrorMessageBase("_ComPro.Address.ValidateMsg.RequireState")]
        public string CompanyAddress_State_Local { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblState")]
        [RequiredErrorMessageBase("_ComPro.Address.ValidateMsg.RequireState")]
        public string CompanyAddress_State_Inter { get; set; }


        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblPostalCode")]
        [RequiredErrorMessageBase("_ComPro.Address.ValidateMsg.RequirePostalCode")]
        //[PostalCodeValidate("_Regis.Msg_PostalCodeFormat", "deliveraddress")]
        [Remote("PostalCodeValidateComp", "ContactInformation", AdditionalFields = "CompanyAddress_CountryCode_VI")]
        public string CompanyAddress_PostalCode { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.LblCompanyCountry")]
        //[RequiredDeliverErrorMessage("_Regis.ValidateMsg.RequireCountry")]
        public string CompanyAddress_CountryCode { get; set; }

        public string CompanyAddress_CountryName { get; set; }

        public Nullable<int> CompanyAddressID { get; set; }


        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblHouseNo")]
        [RequiredErrorMessageBase("_ComPro.Address.ValidateMsg.RequireHouseNo")]
        [StringLengthErrorMessage(1000, "_ComPro.ValidateMsg.AddressLength", 0)]
        public string DeliverAddress_HouseNo_Local { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblHouseNo")]
        [RequiredErrorMessageBase("_ComPro.Address.ValidateMsg.RequireHouseNo")]
        [StringLengthErrorMessage(1000, "_ComPro.ValidateMsg.AddressLength", 0)]
        public string DeliverAddress_HouseNo_Inter { get; set; }


        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblVillageNo")]
        //[RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireVillageNo")]
        public string DeliverAddress_VillageNo_Local { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblVillageNo")]
        //[RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireVillageNo")]
        public string DeliverAddress_VillageNo_Inter { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblLane")]
        //[RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireLane")]
        public string DeliverAddress_Lane_Local { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblLane")]
        //[RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireLane")]
        public string DeliverAddress_Lane_Inter { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblStreet")]
        //[RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireRoad")]
        public string DeliverAddress_Road_Local { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblStreet")]
        //[RequiredDeliverSubmitErrorMessage("_Regis.Msg_RequireRoad")]
        public string DeliverAddress_Road_Inter { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblSubDistrict")]
        [RequiredErrorMessageBase("_ComPro.Address.ValidateMsg.RequireSubDistrict")]
        public string DeliverAddress_SubDistrict_Local { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblSubDistrict")]
        [RequiredErrorMessageBase("_ComPro.Address.ValidateMsg.RequireSubDistrict")]
        public string DeliverAddress_SubDistrict_Inter { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblCity")]
        [RequiredErrorMessageBase("_ComPro.Address.ValidateMsg.RequireCity")]
        public string DeliverAddress_City_Local { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblCity")]
        [RequiredErrorMessageBase("_ComPro.Address.ValidateMsg.RequireCity")]
        public string DeliverAddress_City_Inter { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblState")]
        [RequiredErrorMessageBase("_ComPro.Address.ValidateMsg.RequireState")]
        public string DeliverAddress_State_Local { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblState")]
        [RequiredErrorMessageBase("_ComPro.Address.ValidateMsg.RequireState")]
        public string DeliverAddress_State_Inter { get; set; }


        [ResourceDisplayName("_Regis.CompanyInfo.Address.LblPostalCode")]
        [RequiredErrorMessageBase("_ComPro.Address.ValidateMsg.RequirePostalCode")]
        //[PostalCodeValidate("_Regis.Msg_PostalCodeFormat", "deliveraddress")]
        [Remote("PostalCodeValidateDelv", "ContactInformation", AdditionalFields = "DeliverAddress_CountryCode_VI")]
        public string DeliverAddress_PostalCode { get; set; }

        [ResourceDisplayName("_Regis.CompanyInfo.LblCompanyCountry")]
        //[RequiredDeliverErrorMessage("_Regis.ValidateMsg.RequireCountry")]
        public string DeliverAddress_CountryCode { get; set; }

        public string DeliverAddress_CountryName { get; set; }

        //=========================================================================

        public Nullable<int> DeliverAddressID { get; set; }

        public string ComCountryCode { get; set; }
        public string ComPostalCode { get; set; }

        public string DeliCountryCode { get; set; }
        public string DeliPostalCode { get; set; }

        public string CompAddrCountry { get; set; }
        public string DelvAddrCountry { get; set; }

        public int AddressTypeID { get; set; }
        public Nullable<int> SeqNo { get; set; }
        //===========================================================================

        public bool CheckB { get; set; }

        //=====================
        public IEnumerable<SelectListItem> CountryList { get; set; }
        public Tbl_Country Country { get; set; }

        public Dictionary<string, TrackingCompProfile> trackingOrgFields { get; set; }
        public Dictionary<string, TrackingCompProfile> trackingAddressFields { get; set; }
        public Dictionary<string, TrackingCompProfile> trackingDelFields { get; set; }
        public Nullable<int> TrackingStatusID { get; set; }
        public Nullable<int> TrackingReqID { get; set; }
        public Nullable<int> TrackingItemID { get; set; }
        public string TrackingType { get; set; }
        public string JsonStr { get; set; }
    }
}

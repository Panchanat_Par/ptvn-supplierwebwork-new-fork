﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.SystemAPI
{
    public partial class SystemAPIModel
    {
        public int SystemID { get; set; }
        public Nullable<int> SystemGrpID { get; set; }
        public string APIName { get; set; }
        public string API_Key { get; set; }
        public string API_URL { get; set; }
        
    }
}

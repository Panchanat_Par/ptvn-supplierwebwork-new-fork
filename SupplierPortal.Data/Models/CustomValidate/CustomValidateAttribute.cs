﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using SupplierPortal.Core.Extension;
using SupplierPortal.Data.Models.Repository.LocaleStringResource;
using SupplierPortal.Data.Models.Repository.Function;
using SupplierPortal.Data.Models.SupportModel.AccountPortal.Register;
using SupplierPortal.Data.Models.Repository.Organization;

namespace SupplierPortal.Data.CustomValidate
{
    public class ExcludeCharAttribute : ValidationAttribute, IClientValidatable
    {
        private readonly string _chars;
        private string _resourceValue = string.Empty;
        public string _resourcekey { get; set; }

        public ExcludeCharAttribute(string chars, string resourceKey)
            : base()
        {
            _chars = chars;
            _resourcekey = resourceKey;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null)
            {
                var valueAsString = value.ToString();
                for (int i = 0; i < _chars.Length; i++)
                {
                    if (valueAsString.Contains(_chars[i]))
                    {
                        ILocaleStringRepository _repo = new LocaleStringRepository();
                        _resourceValue = _repo.GetResource(_resourcekey);
                        _repo.Dispose();

                        var errorMessage = _resourceValue;
                        return new ValidationResult(errorMessage);
                    }
                }
            }
            return ValidationResult.Success;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            ILocaleStringRepository _repo = new LocaleStringRepository();
            _resourceValue = _repo.GetResource(_resourcekey);
            _repo.Dispose();

            var rule = new ModelClientValidationRule();
            rule.ErrorMessage = _resourceValue;
            rule.ValidationParameters.Add("chars", _chars);
            rule.ValidationType = "exclude";
            yield return rule;
        }
    }

    public class TaxIDValidate : ValidationAttribute, IClientValidatable
    {
        private string _resourceValue = string.Empty;
        public string _resourcekey { get; set; }
        public string _errorMessage { get; set; }

        public TaxIDValidate(string resourceKey)
            : base()
        {
            _resourcekey = resourceKey;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null)
            {
                var country = (RegisterPortalModel)validationContext.ObjectInstance;
                if (country.CountryCode == "TH")
                {
                    string number = (string)value;
                    var sqlQuery = "select dbo.VerifyTaxNumber('" + number + "')";
                    IFunctionRepository _repo = new FunctionRepository();
                    var result = _repo.GetResultByQuery(sqlQuery);
                    _repo.Dispose();
                    if (!result)
                    {
                        ILocaleStringRepository _repoLocal = new LocaleStringRepository();
                        _resourceValue = _repoLocal.GetResource(_resourcekey);
                        _repoLocal.Dispose();

                        var errorMessage = _resourceValue;
                        _errorMessage = errorMessage;
                        return new ValidationResult(errorMessage);
                    }
                }
            }
            return ValidationResult.Success;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            ILocaleStringRepository _repo = new LocaleStringRepository();
            _resourceValue = _repo.GetResource(_resourcekey);
            _repo.Dispose();

            var rule = new ModelClientValidationRule();
            rule.ErrorMessage = _resourceValue;
            rule.ValidationParameters.Add("errormessage", _errorMessage);
            rule.ValidationType = "taxid";
            yield return rule;
        }
    }

    public class DunsValidate : ValidationAttribute, IClientValidatable
    {
        private string _resourceValue = string.Empty;
        public string ResourceKey { get; set; }
        public string _errorMessage { get; set; }

        public DunsValidate(string resourceKey)
            : base()
        {
            ResourceKey = resourceKey;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null)
            {
                string number = (string)value;
                if (!number.IsdunsNumberValid())
                {
                    ILocaleStringRepository _repo = new LocaleStringRepository();
                    _resourceValue = _repo.GetResource(ResourceKey);
                    _repo.Dispose();
                    var errorMessage = _resourceValue;
                    _errorMessage = errorMessage;
                    return new ValidationResult(errorMessage);
                }
            }
            return ValidationResult.Success;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            ILocaleStringRepository _repo = new LocaleStringRepository();
            _resourceValue = _repo.GetResource(ResourceKey);
            _repo.Dispose();

            var rule = new ModelClientValidationRule();
            rule.ErrorMessage = _resourceValue;
            rule.ValidationParameters.Add("errormessage", _errorMessage);
            rule.ValidationType = "duns";
            yield return rule;
        }
    }

    public class RequiredDeliverErrorMessage : RequiredAttribute, IClientValidatable
    {
        private string _resourcevalue = string.Empty;
        public string _resourcekey { get; set; }
        public string _errorMessage { get; set; }

        public RequiredDeliverErrorMessage(string resourcekey)
            : base()
        {
            _resourcekey = resourcekey;
        }

        //protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        //{
        //    Boolean sameAddress = false;
        //    if (!string.IsNullOrEmpty(CheckBoxExtension.GetValue<System.String>("IsSameAddress")))
        //    {
        //        sameAddress = CheckBoxExtension.GetValue<System.Boolean>("IsSameAddress");
        //    }
        //    var model = (SupplierPortal.Data.Models.SupportModel.RegisterPortal.RegisContinuePortalModel)validationContext.ObjectInstance;
        //    model.IsSameAddress = sameAddress;
        //    if ((value == null) && (!model.IsSameAddress))
        //    {
        //        ILocaleStringRepository _repoLocal = new LocaleStringRepository();
        //        _resourcevalue = _repoLocal.GetResource(_resourcekey);
        //        _repoLocal.Dispose();
        //        var errorMessage = _resourcevalue;
        //        _errorMessage = errorMessage;
        //        return new ValidationResult(errorMessage);
        //    }
        //    return ValidationResult.Success;
        //}

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            ILocaleStringRepository _repo = null;
            _repo = new LocaleStringRepository();
            _resourcevalue = _repo.GetResource(_resourcekey);
            _repo.Dispose();

            var rule = new ModelClientValidationRule();
            rule.ErrorMessage = _resourcevalue;
            rule.ValidationParameters.Add("errormessage", _errorMessage);
            rule.ValidationType = "requireddeliver";
            yield return rule;
        }
    }
    //, IClientValidatable
    public class RequiredSubmitErrorMessage : RequiredAttribute
    {
        private string _resourcevalue = string.Empty;
        public string _resourcekey { get; set; }
        public string _errorMessage { get; set; }

        public RequiredSubmitErrorMessage(string resourcekey)
            : base()
        {
            _resourcekey = resourcekey;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var model = (SupplierPortal.Data.Models.SupportModel.RegisterPortal.RegisContinuePortalModel)validationContext.ObjectInstance;
            if ((value == null) && (model.IsBtnSubmit))
            {
                ILocaleStringRepository _repoLocal = new LocaleStringRepository();
                _resourcevalue = _repoLocal.GetResource(_resourcekey);
                _repoLocal.Dispose();
                var errorMessage = _resourcevalue;
                _errorMessage = errorMessage;
                return new ValidationResult(errorMessage);
            }
            return ValidationResult.Success;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            ILocaleStringRepository _repo = null;
            _repo = new LocaleStringRepository();
            _resourcevalue = _repo.GetResource(_resourcekey);
            _repo.Dispose();

            var rule = new ModelClientValidationRule();
            rule.ErrorMessage = _resourcevalue;
            rule.ValidationParameters.Add("errormessage", _errorMessage);
            rule.ValidationType = "requiredsubmit";
            yield return rule;
        }
    }

    public class RequiredDeliverSubmitErrorMessage : RequiredAttribute, IClientValidatable
    {
        private string _resourcevalue = string.Empty;
        public string _resourcekey { get; set; }
        public string _errorMessage { get; set; }

        public RequiredDeliverSubmitErrorMessage(string resourcekey)
            : base()
        {
            _resourcekey = resourcekey;
        }

        //protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        //{
        //    Boolean sameAddress = false;
        //    if (!string.IsNullOrEmpty(CheckBoxExtension.GetValue<System.String>("IsSameAddress")))
        //    {
        //        sameAddress = CheckBoxExtension.GetValue<System.Boolean>("IsSameAddress");
        //    }
        //    var model = (SupplierPortal.Data.Models.SupportModel.RegisterPortal.RegisContinuePortalModel)validationContext.ObjectInstance;
        //    model.IsSameAddress = sameAddress;
        //    if ((value == null) && (model.IsBtnSubmit) && (!model.IsSameAddress))
        //    {
        //        ILocaleStringRepository _repoLocal = new LocaleStringRepository();
        //        _resourcevalue = _repoLocal.GetResource(_resourcekey);
        //        _repoLocal.Dispose();
        //        var errorMessage = _resourcevalue;
        //        _errorMessage = errorMessage;
        //        return new ValidationResult(errorMessage);
        //    }
        //    return ValidationResult.Success;
        //}

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            ILocaleStringRepository _repo = null;
            _repo = new LocaleStringRepository();
            _resourcevalue = _repo.GetResource(_resourcekey);
            _repo.Dispose();

            var rule = new ModelClientValidationRule();
            rule.ErrorMessage = _resourcevalue;
            rule.ValidationParameters.Add("errormessage", _errorMessage);
            rule.ValidationType = "requireddeliversubmit";
            yield return rule;
        }
    }

    public class PostalCodeValidate : ValidationAttribute, IClientValidatable
    {
        private string _resourceValue = string.Empty;
        public string _resourcekey { get; set; }
        public string _addresstype { get; set; }
        public string _errorMessage { get; set; }

        public PostalCodeValidate(string resourceKey, string addresstype)
            : base()
        {
            _resourcekey = resourceKey;
            _addresstype = addresstype;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null)
            {
                var country = (SupplierPortal.Data.Models.SupportModel.RegisterPortal.RegisContinuePortalModel)validationContext.ObjectInstance;
                var countrycode = _addresstype == "companyaddress" ? country.RegCompanyAddress_CountryCode : country.RegDeliverAddress_CountryCode;
                if (!string.IsNullOrEmpty(countrycode))
                {
                    string number = (string)value;
                    var sqlQuery = "select dbo.VerifyPostalCode('" + number + "', '" + countrycode + "')";
                    IFunctionRepository _repo = new FunctionRepository();
                    var result = _repo.GetResultByQuery(sqlQuery);
                    _repo.Dispose();
                    if (!result)
                    {
                        ILocaleStringRepository _repoLocal = new LocaleStringRepository();
                        _resourceValue = _repoLocal.GetResource(_resourcekey);
                        _repoLocal.Dispose();

                        var errorMessage = _resourceValue;
                        _errorMessage = errorMessage;
                        return new ValidationResult(errorMessage);
                    }
                }
            }
            return ValidationResult.Success;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            ILocaleStringRepository _repo = new LocaleStringRepository();
            _resourceValue = _repo.GetResource(_resourcekey);
            _repo.Dispose();

            var rule = new ModelClientValidationRule();
            rule.ErrorMessage = _resourceValue;
            rule.ValidationParameters.Add("errormessage", _errorMessage);
            rule.ValidationType = "postalcode";
            yield return rule;
        }
    }

    public class TaxIDInfosValidate : ValidationAttribute, IClientValidatable
    {
        private string _resourceValue = string.Empty;
        public string _resourcekey { get; set; }
        public string _errorMessage { get; set; }

        public TaxIDInfosValidate(string resourceKey)
            : base()
        {
            _resourcekey = resourceKey;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null)
            {
                var country = (RegisterPortalModel)validationContext.ObjectInstance;
                string infosconcat = (string)value + country.CompanyName + country.BranchNo + country.BranchName;
                infosconcat = infosconcat.Trim();
                IOrganizationRepository _repo = new OrganizationRepository();
                if (_repo.GetOrganizationByInfosConcat(infosconcat).Count() > 0)
                {
                    ILocaleStringRepository _repoLocal = new LocaleStringRepository();
                        _resourceValue = _repoLocal.GetResource(_resourcekey);
                        _repoLocal.Dispose();

                        var errorMessage = _resourceValue;
                        _errorMessage = errorMessage;
                        return new ValidationResult(errorMessage);
                }
            }
            return ValidationResult.Success;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            ILocaleStringRepository _repo = new LocaleStringRepository();
            _resourceValue = _repo.GetResource(_resourcekey);
            _repo.Dispose();

            var rule = new ModelClientValidationRule();
            rule.ErrorMessage = _resourceValue;
            rule.ValidationParameters.Add("errormessage", _errorMessage);
            rule.ValidationType = "taxidinfos";
            yield return rule;
        }
    }

    public class TaxIDInfosEditValidate : ValidationAttribute, IClientValidatable
    {
        private string _resourceValue = string.Empty;
        public string _resourcekey { get; set; }
        public string _errorMessage { get; set; }

        public TaxIDInfosEditValidate(string resourceKey)
            : base()
        {
            _resourcekey = resourceKey;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null)
            {
                IOrganizationRepository _repo = new OrganizationRepository();
                var result = _repo.GetDataByTaxID((string)value);
                string tmpCompare = "";
                if (result.Count()>0)
                {
                    foreach (var list in result)
                    {
                        tmpCompare = list.TaxID.ToString() + list.CompanyName_Local + list.BranchNo + list.BranchName_Local;
                        tmpCompare = tmpCompare.Trim();
                    }
                }
                var country = (SupplierPortal.Data.Models.SupportModel.RegisterPortal.RegisContinuePortalModel)validationContext.ObjectInstance;
                string infosconcat = (string)value + country.CompanyName_Local + country.BranchNo + country.BranchName_Local;
                infosconcat = infosconcat.Trim();
                if ((_repo.GetOrganizationByInfosConcat(infosconcat).Count() > 0) && (infosconcat != tmpCompare))
                {
                    ILocaleStringRepository _repoLocal = new LocaleStringRepository();
                    _resourceValue = _repoLocal.GetResource(_resourcekey);
                    _repoLocal.Dispose();

                    var errorMessage = _resourceValue;
                    _errorMessage = errorMessage;
                    return new ValidationResult(errorMessage);
                }
                _repo.Dispose();
            }
            return ValidationResult.Success;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            ILocaleStringRepository _repo = new LocaleStringRepository();
            _resourceValue = _repo.GetResource(_resourcekey);
            _repo.Dispose();

            var rule = new ModelClientValidationRule();
            rule.ErrorMessage = _resourceValue;
            rule.ValidationParameters.Add("errormessage", _errorMessage);
            rule.ValidationType = "taxidinfosedit";
            yield return rule;
        }
    }
}

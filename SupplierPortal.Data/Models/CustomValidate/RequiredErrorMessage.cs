﻿using SupplierPortal.Data.Models.Repository.LocaleStringResource;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SupplierPortal.Data.CustomValidate
{
    public class RequiredErrorMessage : RequiredAttribute, IClientValidatable
    {

        private string _resourceValue = string.Empty;
        public string _resourcekey { get; set; }
        public string _errorMessage { get; set; }

        public RequiredErrorMessage(string resourceKey)
            : base()
        {
            _resourcekey = resourceKey;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value == null)
            {
                ILocaleStringRepository _repoLocal = new LocaleStringRepository();
                _resourceValue = _repoLocal.GetResource(_resourcekey);
                _repoLocal.Dispose();

                var errorMessage = _resourceValue;
                _errorMessage = errorMessage;
                return new ValidationResult(errorMessage);
            }
            return ValidationResult.Success;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            ILocaleStringRepository _repo = null;
            _repo = new LocaleStringRepository();

            _resourceValue = _repo.GetResource(_resourcekey);

            var rule = new ModelClientValidationRule();
            rule.ErrorMessage = _resourceValue;
            rule.ValidationParameters.Add("errormessage", _errorMessage);
            rule.ValidationType = "myrequired";
            yield return rule;
        }

    }
}

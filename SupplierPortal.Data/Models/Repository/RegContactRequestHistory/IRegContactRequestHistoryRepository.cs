﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.RegContactRequestHistory
{
    public partial interface IRegContactRequestHistoryRepository
    {
        void Insert(Tbl_RegContactRequestHistory model);

        void Dispose();
    }
}

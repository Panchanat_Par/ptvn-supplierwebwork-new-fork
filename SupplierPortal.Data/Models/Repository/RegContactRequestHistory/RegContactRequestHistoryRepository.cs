﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.RegContactRequestHistory
{
    public partial class RegContactRequestHistoryRepository : IRegContactRequestHistoryRepository
    {

        private SupplierPortalEntities context;

        public RegContactRequestHistoryRepository()
        {
            context = new SupplierPortalEntities();
        }

        public RegContactRequestHistoryRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }

        public virtual void Insert(Tbl_RegContactRequestHistory model)
        {
            try
            {
                if (model != null)
                {

                    context.Tbl_RegContactRequestHistory.Add(model);

                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {

                throw;
            }
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.UserSession
{
    public partial interface IUserSessionRepository
    {

        void Insert(Tbl_UserSession userSession);

        Tbl_UserSession GetUserSession(string userGuid);

        void Update(Tbl_UserSession userSession);


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.UserSession
{
     public partial class UserSessionRepository : IUserSessionRepository
    {

        private SupplierPortalEntities context;


        public UserSessionRepository()
        {
            context = new SupplierPortalEntities();
        }

        public UserSessionRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }

        public void Insert(Tbl_UserSession userSession)
        {
            try
            {
                context.Tbl_UserSession.Add(userSession);
              
                context.SaveChanges();
            }
            catch (Exception e)
            {

                throw;
            }

        }

        public Tbl_UserSession GetUserSession(string userGuid)
        {

            return context.Tbl_UserSession.Find(userGuid);

        }

        public virtual void Update(Tbl_UserSession tbl_UserSession)
        {
            try
            {

                if (tbl_UserSession != null)
                {

                    var usertmp = context.Tbl_UserSession.Find(tbl_UserSession.UserGuid);
                    usertmp = tbl_UserSession;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}

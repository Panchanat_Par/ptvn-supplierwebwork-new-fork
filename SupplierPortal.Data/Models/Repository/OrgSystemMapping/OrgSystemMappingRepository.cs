﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.OrgSystemMapping
{
    public partial class OrgSystemMappingRepository : IOrgSystemMappingRepository
    {
        private SupplierPortalEntities context;

        public OrgSystemMappingRepository()
        {
            context = new SupplierPortalEntities();
        }

        public OrgSystemMappingRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }

        //public virtual IEnumerable<Tbl_OrgSystemMapping> GetOrgSystemMappingBySupplierIdAndSystemId(int supplierID,int systemID)
        //{
        //    var query = from a in context.Tbl_OrgSystemMapping
        //                where a.SupplierID == supplierID
        //                && a.SystemID == systemID
        //                && a.isCancelService != 1
        //                select a;

        //    var result = query.ToList();

        //    return result;

        //}

        public virtual bool CheckExistsOrgSystemMappingBySupplierIdAndSystemId(int supplierID, int systemID)
        {
            var query = from a in context.Tbl_OrgSystemMapping
                        where a.SupplierID == supplierID
                        && a.SystemID == systemID
                        && a.isCancelService != 1
                        select a;

            var result = query.ToList();

            if (result.Count() > 0)
            {
                return true;
            }else
            {
                return false;
            }
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel;

namespace SupplierPortal.Data.Models.Repository.Address
{
    public partial interface IAddressRepository
    {
        Tbl_Address GetAddressByUsername(string username);

        Tbl_Address GetAddressByUsernameOrderByDesc(string username);

        Tbl_Address GetAddressByAddressID(int addressID);

        Tbl_Address GetAddressBySupplierID(int SupplierID);

        List<AddressHomeAdmin> GetListDataAddressForHomeAdmin(int languageID);

        void UpdateAddress(Tbl_Address model, int updateBy);

        int InsertAddress(Tbl_Address model);

        int InsertAddressReturnaddressID(Tbl_Address model, int updateBy);

        void Delete(int addressID, int updateBy);

        TrackingDictionary GetTrackingStatus();

        void Dispose();
    }
}

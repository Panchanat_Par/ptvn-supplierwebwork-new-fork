﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;
using SupplierPortal.Data.Models.Repository.LocalizedProperty;

namespace SupplierPortal.Data.Models.Repository.ProductType
{
    public partial class ProductTypeRepository : IProductTypeRepository
    {
        private SupplierPortalEntities context;

        public ProductTypeRepository()
        {
            context = new SupplierPortalEntities();
        }

        public ProductTypeRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }

        public virtual IEnumerable<TempForMultiLang> GetMultiLangProductTypeByLangid(int languageID)
        {
            var query = from a in context.Tbl_ProductType
                        orderby a.SeqNo
                        select new TempForMultiLang
                        {
                            EntityID = a.ProductTypeID,
                            EntityValue = (from lp in context.Tbl_LocalizedProperty
                                           where lp.EntityID == a.ProductTypeID
                                          && lp.LocaleKeyGroup == "Tbl_ProductType"
                                          && lp.LocaleKey == "ProductType"
                                          && lp.LanguageID == languageID
                                           select lp.LocaleValue).FirstOrDefault() ?? a.ProductType
                        };

            var result = query.ToList();

            return result;
        }

        public virtual string GetMultiLangProductTypeByProductTypeid(int producttypeID, int languageID)
        {
            ILocalizedPropertyRepository _repo = new LocalizedPropertyRepository();
            var result = _repo.GetAllEntityidLocalizedPropByMany("Tbl_ProductType", "ProductType", languageID);
            _repo.Dispose();

            if (result.Count() > 0)
            {
                return (result.Where(i => i.EntityID == producttypeID).Select(i => i.LocaleValue)).FirstOrDefault();
            }

            return (context.Tbl_ProductType.Where(i => i.ProductTypeID == producttypeID).Select(i => i.ProductType)).FirstOrDefault();
        }

        public virtual IEnumerable<TempProServModel> GetMultiLangProductTypeByManyProductTypeid(int languageID, string[] prodtypeid)
        {
            int[] prodtypeids = prodtypeid.Select(int.Parse).ToArray();            

            return (from producttype in context.Tbl_ProductType
                    where prodtypeids.Contains(producttype.ProductTypeID)
                    from localizedProperty in context.Tbl_LocalizedProperty
                         .Where(mapping => mapping.EntityID == producttype.ProductTypeID
                         && mapping.LocaleKeyGroup == "Tbl_ProductType"
                         && mapping.LocaleKey == "ProductType"
                         && mapping.LanguageID == languageID).DefaultIfEmpty()
                    select new TempProServModel
                    {
                        ProdTypeID = producttype.ProductTypeID,
                        ProductType = localizedProperty.LocaleValue ?? producttype.ProductType
                    }).OrderBy(x => x.ProdTypeID).ToList<TempProServModel>();
        }

        public virtual IEnumerable<Tbl_ProductType> GetProductTypeAll()
        {

            var query = from db in context.Tbl_ProductType
                        orderby db.ProductTypeID
                        select db;

            return query.ToList();
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}

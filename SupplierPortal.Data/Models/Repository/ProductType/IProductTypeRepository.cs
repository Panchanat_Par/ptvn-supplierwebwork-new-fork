﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;

namespace SupplierPortal.Data.Models.Repository.ProductType
{
    public partial interface IProductTypeRepository
    {
        IEnumerable<TempForMultiLang> GetMultiLangProductTypeByLangid(int languageID);

        string GetMultiLangProductTypeByProductTypeid(int producttypeID, int languageID);

        IEnumerable<TempProServModel> GetMultiLangProductTypeByManyProductTypeid(int languageID, string[] prodtypeid);

        IEnumerable<Tbl_ProductType> GetProductTypeAll();


        void Dispose();
    }
}

﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogOrgContactAddressMapping
{
    public partial class LogOrgContactAddressMappingRepository : ILogOrgContactAddressMappingRepository
    {
        private SupplierPortalEntities context;


        public LogOrgContactAddressMappingRepository()
        {
            context = new SupplierPortalEntities();
        }

        public LogOrgContactAddressMappingRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }

        public virtual void Insert(int id, string logAction, int updateBy)
        {
            try
            {
                var model = context.Tbl_OrgContactAddressMapping.Find(id);

                if (model != null)
                {
                    var logModel = new Tbl_LogOrgContactAddressMapping()
                    {
                        Id = model.Id,
                        ContactID = model.ContactID,
                        AddressID = model.AddressID,
                        UpdateBy = updateBy,
                        UpdateDate = DateTime.UtcNow,
                        LogAction = logAction
                    };

                    context.Tbl_LogOrgContactAddressMapping.Add(logModel);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }

    }
}

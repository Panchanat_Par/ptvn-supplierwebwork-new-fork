﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.LogPortalInterface
{
    public partial class LogPortalInterfaceRepository : ILogPortalInterfaceRepository
    {
        private SupplierPortalEntities context;


        public LogPortalInterfaceRepository()
        {
            context = new SupplierPortalEntities();
        }

        public LogPortalInterfaceRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }



        public void Insert(Tbl_LogPortalInterface logPortalInterface)
        {
            try
            {
                context.Tbl_LogPortalInterface.Add(logPortalInterface);

                context.SaveChanges();
            }
            catch (Exception e)
            {

                throw;
            }

        }



        public void Dispose()
        {
            context.Dispose();
        }



    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.OPAdoptionNotify
{
    public partial interface IOPAdoptionNotifyRepository
    {

        void Insert(Tbl_OPAdoptionNotify model);

        int InsertReturnAdoptionReqID(Tbl_OPAdoptionNotify model);

        Tbl_OPAdoptionNotify GetOPAdoptionNotifyByAdoptionReqID(int adoptionReqID);

        void UpdateFlagIsSendMailByAdoptionReqID(int adoptionReqID);

        void Dispose();
    }
}

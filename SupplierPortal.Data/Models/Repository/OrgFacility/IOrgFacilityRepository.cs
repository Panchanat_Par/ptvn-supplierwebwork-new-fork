﻿using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.CompanyProfile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.OrgFacility
{
    
    public partial interface IOrgFacilityRepository
    {
        List<Tbl_OrgFacility> GetDataBySupplierID(int SupplierID);
        IQueryable<CompanyFacilityModel> GetDataFacAddressBySupplierID(int SupplierID);
        void InsertOrgFacility(Tbl_OrgFacility model);
        void UpdateOrgFacility(Tbl_OrgFacility model);
        //void DeleteOrgFacility(int SupplierID);
        void DeleteOrgFacility(int facilityID);
        void Dispose();
    }
}

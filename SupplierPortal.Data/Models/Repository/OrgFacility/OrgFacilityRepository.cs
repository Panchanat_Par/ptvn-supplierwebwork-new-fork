﻿using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.LogOrgFacility;
using SupplierPortal.Data.Models.Repository.LogOrgShareholder;
using SupplierPortal.Data.Models.SupportModel.CompanyProfile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.OrgFacility
{
    
    public partial class OrgFacilityRepository : IOrgFacilityRepository
    {
        private SupplierPortalEntities context;
        ILogOrgFacilityRepository _logOrgFacilityRepository;

        public OrgFacilityRepository()
        {
            context = new SupplierPortalEntities();
            _logOrgFacilityRepository = new LogOrgFacilityRepository();
        }

        public OrgFacilityRepository
            (
            SupplierPortalEntities context,
            LogOrgFacilityRepository logOrgFacilityRepository
            )
        {
            this.context = context;
            _logOrgFacilityRepository = logOrgFacilityRepository;
        }


        public virtual List<Tbl_OrgFacility> GetDataBySupplierID(int SupplierID)
        {

            return context.Tbl_OrgFacility.Where(w => w.SupplierID == SupplierID).ToList();
        }


        public virtual IQueryable<CompanyFacilityModel> GetDataFacAddressBySupplierID(int SupplierID)
        {
            int isInter = SupplierPortal.Data.Helper.LanguageHelper.GetLanguageIsInter();

            var query = from Fac in context.Tbl_OrgFacility
                        join addr in context.Tbl_Address on Fac.AddressID equals addr.AddressID into joinedFA
                        from a in joinedFA.DefaultIfEmpty()
                        join facType in context.Tbl_FacilityType on Fac.FacilityTypeID equals facType.FacilityTypeID into joinedFFT
                        from b in joinedFFT.DefaultIfEmpty()
                        where Fac.SupplierID==SupplierID
                        select new CompanyFacilityModel
                        {
                            FacilityID=Fac.FacilityID,
                            FacilityTypeID = b.FacilityTypeID,
                            FacilityType = (from lp in context.Tbl_LocalizedProperty
                                            where lp.EntityID == Fac.FacilityTypeID
                                            && lp.LocaleKeyGroup == "Tbl_FacilityType"
                                            && lp.LocaleKey == "FacilityType"
                                            && lp.LanguageID == isInter
                                            select lp.LocaleValue
                                           ).FirstOrDefault() ?? b.FacilityType,
                            FacilityName=Fac.FacilityName,
                            PhoneNo = Fac.PhoneNo,
                            PhoneExt = Fac.PhoneExt,
                            MobileNo = Fac.MobileNo,
                            FaxNo = Fac.FaxNo,
                            FaxExt = Fac.FaxExt,
                            WebSite=Fac.WebSite,

                            HouseNo_Inter = a.HouseNo_Inter,
                            VillageNo_Inter = a.VillageNo_Inter,
                            Lane_Inter =a.Lane_Inter,
                            Road_Inter = a.Road_Inter,
                            SubDistrict_Inter =a.SubDistrict_Inter,
                            City_Inter = a.City_Inter,
                            State_Inter = a.State_Inter,

                            HouseNo_Local = a.HouseNo_Local,
                            VillageNo_Local = a.VillageNo_Local,
                            Lane_Local = a.Lane_Local,
                            Road_Local = a.Road_Local,
                            SubDistrict_Local = a.SubDistrict_Local,
                            City_Local = a.City_Local,
                            State_Local = a.State_Local,

                            SeqNo=Fac.SeqNo,

                            AddressID = Fac.AddressID,
                            SupplierID=Fac.SupplierID,

                            PostalCode=a.PostalCode,
                            CountryCode=a.CountryCode
                            //HouseNo=(isInter==1?a.HouseNo_Inter:a.HouseNo_Local),
                            //VillageNo=(isInter==1?a.VillageNo_Inter:a.VillageNo_Local),
                            //Lane = (isInter == 1 ? a.Lane_Inter : a.Lane_Local),
                            //Street = (isInter == 1 ? a.Road_Inter : a.Road_Local),
                            //SubDistrict = (isInter == 1 ? a.SubDistrict_Inter : a.SubDistrict_Local),
                            //DistrictCity = (isInter == 1 ? a.City_Inter : a.City_Local),
                            //ProvinceState = (isInter == 1 ? a.State_Inter : a.State_Local),
                        };

            return query.Distinct();
            //return context.Tbl_OrgFacility.Where(w => w.SupplierID == SupplierID).ToList();
        }

        public virtual void InsertOrgFacility(Tbl_OrgFacility model)
        {
            try
            {
                //   if (tempModel != null)
                // {
                //var orglist = from m in context.Tbl_OrgFacility
                //              select m;
                //var i = orglist.OrderByDescending(m=>m.FacilityID).FirstOrDefault().FacilityID;
                //var orgAdd = new Tbl_OrgFacility();
                
                //    orgAdd.FacilityID =i+1;
                //    orgAdd.SupplierID = model.SupplierID;
                //    orgAdd.SeqNo = model.SeqNo;
                //    orgAdd.AddressID = model.AddressID;
                //    orgAdd.FacilityName = model.FacilityName;
                //    orgAdd.FacilityTypeID = model.FacilityTypeID;
                //    orgAdd.FaxExt = model.FaxExt;
                //    orgAdd.FaxNo = model.FaxNo;
                //    orgAdd.MobileNo = model.MobileNo;
                //    orgAdd.PhoneNo = model.PhoneNo;
                //    orgAdd.PhoneExt = model.PhoneExt;
                //    orgAdd.WebSite = model.WebSite;
                
                // context.Tbl_OrgFacility.Remove(tempModel);
                int FacilityID = 0;
                if (model != null)
                {
                    context.Tbl_OrgFacility.Add(model);
                    context.SaveChanges();

                    FacilityID = model.FacilityID;

                    _logOrgFacilityRepository.Insert(FacilityID, model.SupplierID, "Modify");
                }

                
                //   }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public virtual void UpdateOrgFacility(Tbl_OrgFacility model)
        {
            try
            {
                //   if (tempModel != null)
                // {
                var orgAdd = context.Tbl_OrgFacility.Where(m=>m.FacilityID== model.FacilityID).FirstOrDefault();
                //var orgAdd = context.Tbl_OrgFacility.Find(model.FacilityID,model.SupplierID);
                    orgAdd.FacilityID = model.FacilityID;
                    orgAdd.SupplierID = model.SupplierID;
                    orgAdd.SeqNo = model.SeqNo;
                    orgAdd.AddressID = model.AddressID;
                    orgAdd.FacilityName = model.FacilityName;
                    orgAdd.FacilityTypeID = model.FacilityTypeID;
                    orgAdd.FaxExt = model.FaxExt;
                    orgAdd.FaxNo = model.FaxNo;
                    orgAdd.MobileNo = model.MobileNo;
                    orgAdd.PhoneNo = model.PhoneNo;
                    orgAdd.PhoneExt = model.PhoneExt;
                    orgAdd.WebSite = model.WebSite;
         
                //context.Tbl_OrgFacility.Add(orgAdd);
                context.SaveChanges();


                _logOrgFacilityRepository.Insert(orgAdd.FacilityID, orgAdd.SupplierID, "Modify");
                //   }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        //public virtual void DeleteOrgFacility(int SupplierID)
        public virtual void DeleteOrgFacility(int facilityID)
        {
            try
            {

                var removeFacility = context.Tbl_OrgFacility.Where(m => m.FacilityID == facilityID);

                context.Tbl_OrgFacility.RemoveRange(removeFacility);
                context.SaveChanges();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}

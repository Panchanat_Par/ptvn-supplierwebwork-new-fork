﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.AccountPortal;



namespace SupplierPortal.Data.Models.Repository.NewsDisplay
{
    public partial interface InewsDisplayRepository
    {
        IQueryable<NewsDisplayModel> findAllNewData(int langID);

        Tbl_News findlangID(int NewsID,int LangID);

        IEnumerable<NewsDisplayModel> GetNewsShow(int languageId);

        NewsDisplayModel GetNewsByNewsId(int newsId, int languageId);

    }
}

﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogOrgProduct
{
    public partial class LogOrgProductRepository : ILogOrgProductRepository
    {
        private SupplierPortalEntities context;

        public LogOrgProductRepository()
        {
            context = new SupplierPortalEntities();
        }

        public LogOrgProductRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }

        public virtual void Insert(int supplierID, int productTypeID, string productCode, string logAction, int updateBy)
        {
            try
            {
                var _logOrgProduct = new Tbl_LogOrgProduct()
                {
                    SupplierID = supplierID,
                    ProductTypeID = productTypeID,
                    ProductCode = productCode,
                    UpdateBy = updateBy,
                    UpdateDate = DateTime.UtcNow,
                    LogAction = logAction
                };

                context.Tbl_LogOrgProduct.Add(_logOrgProduct);
                context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}

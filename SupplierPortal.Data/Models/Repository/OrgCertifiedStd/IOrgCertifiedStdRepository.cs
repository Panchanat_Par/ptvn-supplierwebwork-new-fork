﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.OrgCertifiedStd
{
    public partial interface IOrgCertifiedStdRepository
    {
        IEnumerable<Tbl_OrgCertifiedStd> GetOrgCertifiedStdBySupplierID(int supplierID);

        int InsertOrgCertifiedStdReturnCertId(Tbl_OrgCertifiedStd model, int updateBy);

        Tbl_OrgCertifiedStd GetOrgCertifiedStdByCertId(int certId);

        void Delete(int certId, int updateBy);

        void Update(Tbl_OrgCertifiedStd model, int updateBy);

        void UpdateNonLog(Tbl_OrgCertifiedStd model);

        void Dispose();
    }
}

﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.ACL_OrgMenuMapping
{
    public partial interface IACL_OrgMenuMappingRepository
    {

        bool CheckAllowAccessBySupplierIDAndMenuID(int supplierID,int menuID);
        bool CheckAllowAccessInProfileMenuBySupplierIDAndMenuID(int supplierID, int menuID);

        Tbl_ACL_OrgMenuMapping GetOrgMenuMappingBySupplierIDAndMenuID(int supplierID, int menuID);

        IEnumerable<Tbl_ACL_OrgMenuMapping> GetOrgMenuMappingBySupplierID(int supplierID);

        void Dispose();
    }
}

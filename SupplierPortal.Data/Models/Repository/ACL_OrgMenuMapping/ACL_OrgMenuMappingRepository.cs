﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.ACL_OrgMenuMapping
{
    public partial class ACL_OrgMenuMappingRepository : IACL_OrgMenuMappingRepository
    {

        private SupplierPortalEntities context;

        public ACL_OrgMenuMappingRepository()
        {
            context = new SupplierPortalEntities();
        }

        public ACL_OrgMenuMappingRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }

        public virtual bool CheckAllowAccessBySupplierIDAndMenuID(int supplierID,int menuID)
        {
            bool chk = false;

            var query = from a in context.Tbl_ACL_OrgMenuMapping
                        where a.SupplierID == supplierID
                        && a.MenuID == menuID
                        select a;

            var result = query.FirstOrDefault();

            if (result!=null)
            {
                if (result.isAllowAccess==1)
                {
                    chk = true;
                }
            }

            return chk;

        }

        public virtual bool CheckAllowAccessInProfileMenuBySupplierIDAndMenuID(int supplierID, int menuID)
        {
            bool chk = false;

            var query = from a in context.Tbl_ACL_OrgMenuMapping
                        where a.SupplierID == supplierID
                        && a.MenuID == menuID
                        select a;

            var result = query.FirstOrDefault();

            if (result != null)
            {
                if (result.isAllowAccess == 1)
                {
                    chk = true;
                }
            }
            else
            {
                chk = true;
            }

            return chk;

        }

        public virtual Tbl_ACL_OrgMenuMapping GetOrgMenuMappingBySupplierIDAndMenuID(int supplierID, int menuID)
        {

            var query = from a in context.Tbl_ACL_OrgMenuMapping
                        where a.SupplierID == supplierID
                        && a.MenuID == menuID
                        select a;

            var result = query.FirstOrDefault();

            return result;
        }

        public virtual IEnumerable<Tbl_ACL_OrgMenuMapping> GetOrgMenuMappingBySupplierID(int supplierID)
        {
            var query = from a in context.Tbl_ACL_OrgMenuMapping
                        where a.SupplierID == supplierID
                        select a;

            var result = query.ToList();

            return result;
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.CustomerDocNameMapping
{
    public partial interface ICustomerDocNameMappingRepository
    {

        bool CheckCustomerDocNameIsRequire(int documentNameID, int companyTypeID, string countryCode);

        IEnumerable<Tbl_CustomerDocNameMapping> GetCustomerDocNameMappingRequireByCompTypeAndCountryCode(int companyTypeID, string countryCode);

        IEnumerable<Tbl_CustomerDocNameMapping> GetDocumentIsRequire(int documentTypeID, int companyTypeID, string countryCode);

        void Dispose();
    }
}

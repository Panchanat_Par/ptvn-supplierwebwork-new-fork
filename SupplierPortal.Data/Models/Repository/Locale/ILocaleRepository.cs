﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.Locale
{
    public partial interface ILocaleRepository
    {

        IEnumerable<Tbl_Locale> LocaleList();

        Tbl_Locale GetLocaleByLocaleName(string localeName);

        void Dispose();

    }
}

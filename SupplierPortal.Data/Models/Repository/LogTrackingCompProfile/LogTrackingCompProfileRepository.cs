﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogTrackingCompProfile
{
    public partial class LogTrackingCompProfileRepository : ILogTrackingCompProfileRepository
    {
        private SupplierPortalEntities context;

        public LogTrackingCompProfileRepository()
        {
            context = new SupplierPortalEntities();
        }
        public LogTrackingCompProfileRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }
        public void Insert(int TrackingItemID, int UpdateBy, string logAction)
        {
            try
            {
                var LogTrackingCompro = context.Tbl_TrackingCompProfile.FirstOrDefault(m => m.TrackingItemID == TrackingItemID);

                if (LogTrackingCompro != null)
                {
                    var _logOrg = new Tbl_LogTrackingCompProfile()
                    {
                        TrackingReqID = LogTrackingCompro.TrackingReqID,
                        TrackingItemID = LogTrackingCompro.TrackingItemID,
                        EntityID = LogTrackingCompro.EntityID,
                        TrackingKeyGroup = LogTrackingCompro.TrackingKeyGroup,
                        TrackingKey = LogTrackingCompro.TrackingKey,
                        OldKeyValue = LogTrackingCompro.OldKeyValue,
                        NewKeyValue = LogTrackingCompro.NewKeyValue,
                        TrackingStatusID = LogTrackingCompro.TrackingStatusID,
                        FieldID = LogTrackingCompro.FieldID,
                        UpdateBy = UpdateBy,
                        UpdateDate = DateTime.UtcNow,
                        LogAction = logAction,
                        Remark = "",
                        TrackingStatusDate = LogTrackingCompro.TrackingStatusDate
                    };

                    context.Tbl_LogTrackingCompProfile.Add(_logOrg);
                    context.SaveChanges();
                }

            }
            catch
            {
                throw;
            }

        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}

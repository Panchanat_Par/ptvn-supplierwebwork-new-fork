﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.ACL_SystemRole
{
    public partial class ACL_SystemRoleRepository : IACL_SystemRoleRepository
    {
        private SupplierPortalEntities context;

        public ACL_SystemRoleRepository()
        {
            context = new SupplierPortalEntities();
        }

        public ACL_SystemRoleRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }

        public virtual string GetSysRoleValueBySysRoleID(int sysRoleID)
        {
            var result = context.Tbl_ACL_SystemRole.Where(b => b.SysRoleID == sysRoleID)
                     .FirstOrDefault();
            return result.SysRoleValue.ToString();
        }

        public virtual IEnumerable<Tbl_ACL_SystemRole> GetACL_SystemRoleBySystemID(int systemID)
        {
            var query = from a in context.Tbl_ACL_SystemRole
                        where a.SystemID == systemID
                        orderby a.SeqNo ascending
                        select a;

            var result = query.ToList();

            return result;
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}

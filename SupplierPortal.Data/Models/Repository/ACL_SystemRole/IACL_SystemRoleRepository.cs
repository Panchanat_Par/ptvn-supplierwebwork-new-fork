﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.ACL_SystemRole
{
    public partial interface IACL_SystemRoleRepository
    {

        string GetSysRoleValueBySysRoleID(int sysRoleID);

        IEnumerable<Tbl_ACL_SystemRole> GetACL_SystemRoleBySystemID(int systemID);

        void Dispose();
    }
}

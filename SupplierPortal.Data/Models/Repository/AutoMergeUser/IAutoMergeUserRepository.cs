﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.AutoMergeUser
{
    public partial interface IAutoMergeUserRepository
    {
        IEnumerable<Tbl_AutoMergeUser> GetAutoMergeUserSuccess();

        IEnumerable<Tbl_AutoMergeUser> GetAutoMergeUserUnSuccess();

        Tbl_AutoMergeUser GetAutoMergeUserByMergeIDAndMergeFromUserName(int mergeID, string mergeFromUserName);

        void Update(Tbl_AutoMergeUser model);

        IEnumerable<Tbl_AutoUpdateUser> GeAutoUpdateUserSuccess();

        IEnumerable<Tbl_AutoUpdateUser> GeAutoUpdateUserUnSuccess();

        void UpdateAutoUpdateUser(Tbl_AutoUpdateUser model);

        Tbl_AutoUpdateUser GetAutoUpdateUserByID(int id);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;
using SupplierPortal.Data.Models.Repository.LocalizedProperty;

namespace SupplierPortal.Data.Models.Repository.FacilityType
{
    public partial class FacilityTypeRepository : IFacilityTypeRepository
    {
        private SupplierPortalEntities context;

        public FacilityTypeRepository()
        {
            context = new SupplierPortalEntities();
        }

        public FacilityTypeRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }

        public virtual IEnumerable<Tbl_FacilityType> GetFacilityList()
        {

            return (from facility in context.Tbl_FacilityType
                    select facility);
        }

        public virtual Tbl_FacilityType GetFacilityByFacilityID(int FacilityID)
        {
            var query = from a in context.Tbl_FacilityType
                        where a.FacilityTypeID == FacilityID
                        select a;
            var result = query.FirstOrDefault();

            return result;
        }

        public virtual IEnumerable<Tbl_FacilityType> GetFacilityListByFacilityID(int FacilityID)
        {
            return (from list in context.Tbl_FacilityType
                    where list.FacilityTypeID == FacilityID
                    select list).ToList<Tbl_FacilityType>();
        }

        public virtual string GetFacilityTypeByFacilityID(int FacilityID)
        {
            var query = (from db in context.Tbl_FacilityType
                        where db.FacilityTypeID == FacilityID
                        select db.FacilityType).FirstOrDefault();

            return query;
        }


        public virtual IEnumerable<TempForMultiLang> GetMultiLFacilityList()
        {
            int isInter = SupplierPortal.Data.Helper.LanguageHelper.GetLanguageIsInter();
            var query = from db in context.Tbl_FacilityType
                        orderby db.FacilityTypeID
                        select new TempForMultiLang
                        {
                            EntityID = db.FacilityTypeID,
                            EntityValue = ((from ml in context.Tbl_LocalizedProperty
                                            where ml.EntityID == db.FacilityTypeID
                                            && ml.LanguageID == isInter
                                            && ml.LocaleKeyGroup == "Tbl_FacilityType"
                                            && ml.LocaleKey == "FacilityType"
                                            select ml.LocaleValue
                                           )).FirstOrDefault() ?? db.FacilityType
                        };


            var result = query.ToList();
            return result;
        }

        public virtual IEnumerable<Tbl_FacilityType> GetLocalizeFacilityList()
        {
            int isInter = SupplierPortal.Data.Helper.LanguageHelper.GetLanguageIsInter();
 
            var query1 = (from db in context.Tbl_LocalizedProperty
                          where db.LanguageID == isInter
                          && db.LocaleKeyGroup == "Tbl_FacilityType"
                          && db.LocaleKey == "FacilityType"
                          select new { FacilityTypeID = db.EntityID, FacilityType = db.LocaleValue }).ToList()
    .Select(x => new Tbl_FacilityType { FacilityTypeID = x.FacilityTypeID ?? default(int), FacilityType = x.FacilityType });

            // return (from db in context.Tbl_LocalizedProperty
            //         where db.LanguageID == languageID
            //         && db.LocaleKeyGroup == "Tbl_NameTitle"
            //         && db.LocaleKey == "TitleName"
            //         select new { TitleID = db.EntityID, TitleName = db.LocaleValue}).ToList()
            //.Select(x => new Tbl_NameTitle { TitleID = x.TitleID ?? default(int), TitleName = x.TitleName });

            if (query1.Count() > 0)
            {
                return query1;
            }

            var query2 = from db in context.Tbl_FacilityType
                         select db;



            return query2.ToList();
        }


        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}

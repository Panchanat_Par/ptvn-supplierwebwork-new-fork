﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.AccountPortal;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;

namespace SupplierPortal.Data.Models.Repository.LogRegEmailTicket
{
    public partial interface ILogRegEmailTicketRepository
    {
        bool Insert(string ticketcode, string logAction);

        void Insert(string ticketcode, string logAction,int updateBy);

        void Dispose();
    }
}

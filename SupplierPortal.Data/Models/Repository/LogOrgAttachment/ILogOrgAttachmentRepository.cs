﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogOrgAttachment
{
    public partial interface ILogOrgAttachmentRepository
    {
        void InsertLogAdd(int supplierID, int attachmentID, string logAction);

        void InsertLogAdd(int supplierID, string attachmentNameUnique, string logAction);

        void InsertLogDelete(int supplierID, int attachmentID, string logAction);

        void Insert(int supplierID, int attachmentID, string logAction, int updateBy);

        void Dispose();

    }
}

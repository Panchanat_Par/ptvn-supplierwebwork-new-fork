﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.LogOrgBusinessType;

namespace SupplierPortal.Data.Models.Repository.OrgBusinessType
{
    public partial class OrgBusinessTypeRepository : IOrgBusinessTypeRepository
    {

        private SupplierPortalEntities context;
        ILogOrgBusinessTypeRepository _logOrgBusinessTypeRepository;

        public OrgBusinessTypeRepository()
        {
            context = new SupplierPortalEntities();
            _logOrgBusinessTypeRepository = new LogOrgBusinessTypeRepository();
        }

        public OrgBusinessTypeRepository(
            SupplierPortalEntities context,
            LogOrgBusinessTypeRepository logOrgBusinessTypeRepository
            )
        {
            this.context = context;
            _logOrgBusinessTypeRepository = logOrgBusinessTypeRepository;
        }

        public virtual IEnumerable<Tbl_OrgBusinessType> GetOrgBusinessTypeBySupplierID(int supplierID)
        {
            var query = from a in context.Tbl_OrgBusinessType
                        where a.SupplierID == supplierID
                        select a;

            return query.ToList();

        }

        public virtual void Insert(Tbl_OrgBusinessType tbl_OrgBusinessType, int updateBy)
        {
            try
            {

                if (tbl_OrgBusinessType != null)
                {

                    context.Tbl_OrgBusinessType.Add(tbl_OrgBusinessType);
                    context.SaveChanges();

                    _logOrgBusinessTypeRepository.Insert(tbl_OrgBusinessType.Id,tbl_OrgBusinessType.SupplierID, tbl_OrgBusinessType.BusinessTypeID, "Add", updateBy);
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public virtual void Delete(int supplierID, int businessTypeID,int updateBy)
        {
            try
            {
                var tempModel = context.Tbl_OrgBusinessType.Where(m => m.SupplierID == supplierID && m.BusinessTypeID == businessTypeID).FirstOrDefault();

                if (tempModel != null)
                {
                    context.Tbl_OrgBusinessType.Remove(tempModel);
                    context.SaveChanges();

                    _logOrgBusinessTypeRepository.Insert(tempModel.Id,supplierID, businessTypeID, "Remove", updateBy);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual bool OrgBusinessTypeExists(int supplierID, int businessTypeID)
        {
            var count = context.Tbl_OrgBusinessType.Count(m => m.SupplierID == supplierID && m.BusinessTypeID == businessTypeID);

            if (count > 0)
            {
                return true;
            }

            return false;
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }

    }
}

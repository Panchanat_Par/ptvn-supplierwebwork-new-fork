﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Helper;

namespace SupplierPortal.Data.Models.Repository.Country
{
    public partial class CountryRepository : ICountryRepository
    {
        private SupplierPortalEntities context;

        public CountryRepository()
        {
            context = new SupplierPortalEntities();
        }

        public CountryRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }

        public virtual IEnumerable<Tbl_Country> GetCountryList()
        {
            return (from country in context.Tbl_Country where country.CountryCode != "00" select country).ToList<Tbl_Country>().OrderBy(m=>m.CountryName);
        }

        public virtual Tbl_Country GetCountryByCountryCode(string countrycode)
        {
            var query = from a in context.Tbl_Country
                        where a.CountryCode == countrycode
                        select a;
            var result = query.FirstOrDefault();

            return result;
        }

        public virtual IEnumerable<Tbl_Country> GetCountryListByCountryCode(string countrycode)
        {
            return (from list in context.Tbl_Country
                    where list.CountryCode == countrycode
                    select list).ToList<Tbl_Country>();
        }

        public virtual string GetCountryNameByCountryCode(string countrycode)
        {
            var query = (from db in context.Tbl_Country
                        where db.CountryCode == countrycode
                        select db.CountryName).FirstOrDefault();

            return query;
        }

        //Return only the results we want
        public virtual IEnumerable<Tbl_Country> GetCountry(string searchTerm, int pageSize, int pageNum)
        {

            var result1 = GetCountryQuery(searchTerm)
                .OrderBy(m => m.CountryName)
                .Skip(pageSize * (pageNum - 1))
                .Take(pageSize)
                .ToList();

            var result2 = GetCountryQuery(searchTerm).ToList();

            return GetCountryQuery(searchTerm)
                .OrderBy(m=>m.CountryName)
                .Skip(pageSize * (pageNum - 1))
                .Take(pageSize)
                .ToList();

        }

        //And the total count of records
        public virtual int GetCountryCount(string searchTerm, int pageSize, int pageNum)
        {
            return GetCountryQuery(searchTerm)
                .Count();
        }

        //Our search term
        private IQueryable<Tbl_Country> GetCountryQuery(string searchTerm)
        {
            searchTerm = searchTerm.ToLower();

          
            if(!string.IsNullOrEmpty(searchTerm.Trim()))
            {
                return context.Tbl_Country
                    .Where(a => searchTerm != "" && a.CountryName.ToLower().Contains(searchTerm)
                    );
            }

            return context.Tbl_Country;
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}

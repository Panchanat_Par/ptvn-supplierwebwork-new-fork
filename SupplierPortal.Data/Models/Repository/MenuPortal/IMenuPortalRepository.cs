﻿using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.MenuPortal;
using System.Collections.Generic;

namespace SupplierPortal.Data.Models.Repository.MenuPortal
{
    public partial interface IMenuPortalRepository
    {
        IEnumerable<Tbl_MenuPortal> GetMenuTop();

        IEnumerable<Tbl_MenuPortal> GetMenuSub();

        IEnumerable<SubMenuPortal> GetSubMenuPortal();

        Tbl_MenuPortal GetMenuById(int menuID);

        IEnumerable<Tbl_MenuPortal> GetMenu();

        Tbl_MenuPortal GetMenuByMenuName(string menuName);

        //PrivilegeMenuPortalModels GetPrivilegeMenu(string username,int menuID);

        IEnumerable<Tbl_MenuPortal> GetMenuProfile();

        IEnumerable<Tbl_MenuPortal> GetNotificationMenu();

        IEnumerable<Tbl_MenuPortal> GetNotificationMenuByParentMenuID(int ParentMenuID);

        void Dispose();
    }
}

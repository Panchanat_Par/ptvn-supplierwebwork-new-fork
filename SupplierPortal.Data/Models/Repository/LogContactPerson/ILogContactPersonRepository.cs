﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogContactPerson
{
    public partial interface ILogContactPersonRepository
    {

        void Insert(int contactID, string logAction, string updateBy);

        void Insert(int contactID, string logAction, int updateBy);

        void Dispose();
    }
}

﻿using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.Profile;
using SupplierPortal.Data.Models.SupportModel.SystemAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.SystemConfigureAPI
{
    public partial interface ISystemConfigureAPIRepository
    {
        IEnumerable<SystemAPIModel> GetSystemShowNotiBySystemGrpIDAndNotificationID(int systemGrpID, int notificationID);

        IEnumerable<SystemRoleListModels> GetSystemListByUsernameForUpdateAPI(string username);

        IEnumerable<SystemRoleListModels> GetSystemListByUsernameAndSystemIDForUpdateAPI(string username, int systemID);

        SystemRoleListModels GetSystemRoleAPIBySystemIDAndAPI_Name(int systemID,string api_Name);

        Tbl_API GetAPIBySystemIDAndAPI_Name(int systemID, string api_Name);

        IEnumerable<SystemRoleListModels> GetSystemListBySupplierIDForUpdateAPIwhereUpdateSupplier(int supplierID);

    }
}

﻿using SupplierPortal.Data.Helper;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.CompanyProfile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using SupplierPortal.Data.Models.Repository.LogTrackingRequest;

namespace SupplierPortal.Data.Models.Repository.TrackingRequest
{
    public partial class TrackingRequestRepository : ITrackingRequestRepository
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private SupplierPortalEntities context;
        ILogTrackingRequestRepository _logTrackingRequestRepository; 

        public TrackingRequestRepository()
        {
            context = new SupplierPortalEntities();
            _logTrackingRequestRepository = new LogTrackingRequestRepository();
        }

        public TrackingRequestRepository(
            SupplierPortalEntities ctx,
            LogTrackingRequestRepository  logTrackingRequestRepository
            )
        {
            this.context = ctx;
            _logTrackingRequestRepository = logTrackingRequestRepository;
        }

        public virtual int InsertReturnTrackingReqID(Tbl_TrackingRequest model, int updateBy)
        {
            try
            {
                int trackingReqID = 0;
                logger.Debug("(before Insert)TrackingReqID : " + trackingReqID);
                if (model != null)
                {
                    model.ApproveBy = 0;

                    context.Tbl_TrackingRequest.Add(model);
                    context.SaveChanges();

                    trackingReqID = model.TrackingReqID;
                    logger.Debug("(after Insert)TrackingReqID : " + trackingReqID);
                    _logTrackingRequestRepository.Insert(trackingReqID, updateBy, "Add");
                }

                return trackingReqID;
            }
            catch (Exception e)
            {

                throw;
            }

        }

        public virtual IQueryable<ViewTrackingRequestModel> GetTrackingRequestWaitingApproveBySupplierID(int supplierID)
        {
            HttpCookie cultureCookie = HttpContext.Current.Request.Cookies["_culture"];

            string languageID = "";

            if (cultureCookie != null)
            {
                languageID = cultureCookie.Value.ToString();

            }
            if (string.IsNullOrEmpty(languageID))
            {
                languageID = CultureHelper.GetImplementedCulture(languageID); // This is safe
            }
            int languageId = Convert.ToInt32(languageID);

            int isInter = LanguageHelper.GetLanguageIsInter();

            var query = from a in context.ViewTrackingRequests
                        join b in context.Tbl_NameTitle on a.TitleID equals b.TitleID into c
                        from d in c.DefaultIfEmpty()
                        join e in context.Tbl_TrackingStatus on a.TrackingStatusID equals e.TrackingStatusID into f
                        from g in f.DefaultIfEmpty()
                        where a.SupplierID == supplierID
                        && a.TrackingStatusID == 2
                        && a.isDeleted != 1
                        select new ViewTrackingRequestModel
                        {
                            TrackingReqID = a.TrackingReqID,
                            TrackingGrpID = a.TrackingGrpID??0,
                            ResourceName = a.ResourceName,
                            SupplierID = a.SupplierID??0,
                            ReqBy = a.ReqBy??0,
                            ReqDate = a.ReqDate,
                            ReqByTitleName = (from lp in context.Tbl_LocalizedProperty
                                              where lp.EntityID == d.TitleID
                                              && lp.LocaleKeyGroup == "Tbl_NameTitle"
                                              && lp.LocaleKey == "TitleName"
                                              && lp.LanguageID == languageId
                                              select lp.LocaleValue).FirstOrDefault() ?? d.TitleName,
                            ReqByFullname = (isInter == 1 ? a.FirstName_Inter : a.FirstName_Local) + "  " + (isInter == 1 ? a.LastName_Inter : a.LastName_Local),
                            TrackingStatusID = a.TrackingStatusID??0,
                            TrackingStatusName = (from lp in context.Tbl_LocalizedProperty
                                                  where lp.EntityID == g.TrackingStatusID
                                                  && lp.LocaleKeyGroup == "Tbl_TrackingStatus"
                                                  && lp.LocaleKey == "TrackingStatusName"
                                                  && lp.LanguageID == languageId
                                                  select lp.LocaleValue).FirstOrDefault() ?? g.TrackingStatusName,
                        };


            return query;

        }

        public virtual IQueryable<ViewTrackingRequestModel> GetTrackingRequestAllBySupplierID(int supplierID)
        {
            HttpCookie cultureCookie = HttpContext.Current.Request.Cookies["_culture"];

            string languageID = "";

            if (cultureCookie != null)
            {
                languageID = cultureCookie.Value.ToString();

            }
            if (string.IsNullOrEmpty(languageID))
            {
                languageID = CultureHelper.GetImplementedCulture(languageID); // This is safe
            }
            int languageId = Convert.ToInt32(languageID);

            int isInter = LanguageHelper.GetLanguageIsInter();

            var query = from a in context.ViewTrackingRequests
                        join b in context.Tbl_NameTitle on a.TitleID equals b.TitleID into c
                        from d in c.DefaultIfEmpty()
                        join e in context.Tbl_TrackingStatus on a.TrackingStatusID equals e.TrackingStatusID into f
                        from g in f.DefaultIfEmpty()
                        where a.SupplierID == supplierID
                        && a.isDeleted != 1
                        select new ViewTrackingRequestModel
                        {
                            TrackingReqID = a.TrackingReqID,
                            TrackingGrpID = a.TrackingGrpID ?? 0,
                            ResourceName = a.ResourceName,
                            TrackingGrpName = (from lp in context.Tbl_LocaleStringResource
                                              where lp.ResourceName == a.ResourceName
                                              && lp.LanguageID == languageId
                                              select lp.ResourceValue).FirstOrDefault() ?? "",
                            SupplierID = a.SupplierID ?? 0,
                            ReqBy = a.ReqBy ?? 0,
                            ReqDate = a.ReqDate,
                            ReqByTitleName = (from lp in context.Tbl_LocalizedProperty
                                              where lp.EntityID == d.TitleID
                                              && lp.LocaleKeyGroup == "Tbl_NameTitle"
                                              && lp.LocaleKey == "TitleName"
                                              && lp.LanguageID == languageId
                                              select lp.LocaleValue).FirstOrDefault() ?? d.TitleName,
                            ReqByFullname = (isInter == 1 ? a.FirstName_Inter : a.FirstName_Local) + "  " + (isInter == 1 ? a.LastName_Inter : a.LastName_Local),
                            TrackingStatusID = a.TrackingStatusID ?? 0,
                            TrackingStatusName = (from lp in context.Tbl_LocalizedProperty
                                                  where lp.EntityID == g.TrackingStatusID
                                                  && lp.LocaleKeyGroup == "Tbl_TrackingStatus"
                                                  && lp.LocaleKey == "TrackingStatusName"
                                                  && lp.LanguageID == languageId
                                                  select lp.LocaleValue).FirstOrDefault() ?? g.TrackingStatusName,
                        };


            return query;

        }

        public virtual Tbl_TrackingRequest GetTrackingRequestByTrackingReqID(int trackingReqID)
        {
            var model = context.Tbl_TrackingRequest.Where(m => m.TrackingReqID == trackingReqID && m.isDeleted != 1).FirstOrDefault();

            return model;
        }

        public virtual Tbl_TrackingGroup GetTrackingGroupByTrackingGrpID(int trackingGrpID)
        {
            var model = context.Tbl_TrackingGroup.Where(m => m.TrackingGrpID == trackingGrpID).FirstOrDefault();

            return model;
        }

        public virtual string GetTrackingStatusNameWaitingApproveByTrackingStatusID(int trackingStatusID)
        {
            HttpCookie cultureCookie = HttpContext.Current.Request.Cookies["_culture"];

            string languageID = "";

            if (cultureCookie != null)
            {
                languageID = cultureCookie.Value.ToString();

            }
            if (string.IsNullOrEmpty(languageID))
            {
                languageID = CultureHelper.GetImplementedCulture(languageID); // This is safe
            }
            int languageId = Convert.ToInt32(languageID);

            var query = from a in context.Tbl_TrackingStatus
                        where a.TrackingStatusID == trackingStatusID
                        select new 
                        {
                            TrackingStatusName = (from lp in context.Tbl_LocalizedProperty
                                                  where lp.EntityID == a.TrackingStatusID
                                                  && lp.LocaleKeyGroup == "Tbl_TrackingStatus"
                                                  && lp.LocaleKey == "TrackingStatusName"
                                                  && lp.LanguageID == languageId
                                                  select lp.LocaleValue).FirstOrDefault() ?? a.TrackingStatusName,
                        };

            var result = query.Select(m=>m.TrackingStatusName).FirstOrDefault();

            return result;
        }

        public virtual void Update(Tbl_TrackingRequest model, int updateBy)
        {
            try
            {
                if (model != null)
                {
                    var tempModel = context.Tbl_TrackingRequest.Where(m => m.TrackingReqID == model.TrackingReqID).FirstOrDefault();
                    if (tempModel != null)
                    {
                        tempModel = model;
                        context.SaveChanges();

                        _logTrackingRequestRepository.Insert(tempModel.TrackingReqID, updateBy, "Modify");

                    }
                }

            }
            catch (Exception e)
            {
                throw;
            }
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}

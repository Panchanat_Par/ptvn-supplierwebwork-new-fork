﻿using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.NotificationTopic
{
    public partial class NotificationTopicRepository : INotificationTopicRepository
    {
        private SupplierPortalEntities context;

        public NotificationTopicRepository()
        {
            context = new SupplierPortalEntities();
        }

        public NotificationTopicRepository(
           SupplierPortalEntities context
           )
        {
            this.context = context;
        }

        public virtual Tbl_NotificationTopic GetDataByTopicID(int TopicID)
        {
            var query = from a in context.Tbl_NotificationTopic
                        where a.TopicID == TopicID
                        select a;
            return query.FirstOrDefault();
        }

        public List<NotificationTopicType> GetComboboxTopicList(int supID, int languageID)
        {
            var emptyRow = new List<NotificationTopicType>();
            emptyRow.Add(new NotificationTopicType
            {
                LocaleKeyGroup = "Tbl_NotificationTopic",
                LocaleKey = "TopicName",
                EntityID = 0,
                LocaleValue = languageID == 1 ? "SelectAll" : languageID == 2 ? "เลือกทั้งหมด" : "SelectAll"
            });

            var query = (from a in context.Tbl_NotificationTopic
                         join b in context.Tbl_NotificationTopicOrgMapping on a.TopicID equals b.TopicID
                         where b.isActive == 1
                         && b.SupplierID == supID
                         select new NotificationTopicType()
                         {
                             EntityID = a.TopicID,
                             //LocaleKey = "TopicName",
                             //LocaleKeyGroup = "Tbl_NotificationTopic",
                             LocaleValue = (from ml in context.Tbl_LocalizedProperty
                                            where ml.EntityID == a.TopicID
                                         && ml.LocaleKeyGroup == "Tbl_NotificationTopic"
                                         && ml.LocaleKey == "TopicName"
                                         && ml.LanguageID == languageID
                                            select ml.LocaleValue).FirstOrDefault() ?? a.TopicName
                         }).ToList();

            //context.Tbl_NotificationTopic.ToList();
            return emptyRow.Union(query).ToList();
        }

        public List<NotificationModel> GetListStrJoinNotiTopic(int LanguageID)
        {
            var query = (from a in context.Tbl_NotificationTopic
                         join
                             b in context.Tbl_LocaleStringResource
                             on a.TopicStringResource_Header equals b.ResourceName
                         where b.LanguageID == LanguageID
                         select new NotificationModel 
                         {
                             TopicID = a.TopicID,
                             ResourceValue = b.ResourceValue,
                             PageID = a.PageID ?? 0,
                             TopicURL = a.TopicURL,
                             TopicURLParameters = a.TopicURLParameters,
                             GroupID = a.GroupID,
                             HeaderLength = a.HeaderLength
                         }).ToList();
            return query;
        }

        public void Dispose()
        {
            context.Dispose();
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.LogOrganization;
using SupplierPortal.Data.Helper;
using SupplierPortal.Data.Models.SupportModel.CompanyProfile;

namespace SupplierPortal.Data.Models.Repository.Organization
{
    public partial class OrganizationRepository : IOrganizationRepository
    {
        private SupplierPortalEntities context;
        ILogOrganizationRepository _logOrganizationRepository;

        public OrganizationRepository()
        {
            context = new SupplierPortalEntities();
            _logOrganizationRepository = new LogOrganizationRepository();
        }

        public OrganizationRepository
            (
            SupplierPortalEntities context,
            LogOrganizationRepository logOrganizationRepository
            )
        {
            this.context = context;
            _logOrganizationRepository = logOrganizationRepository;
        }

        public virtual IEnumerable<Tbl_Organization> GetOrganizationAll()
        {
            return (from list in context.Tbl_Organization select list).ToList<Tbl_Organization>();
        }

        public virtual IEnumerable<Tbl_Organization> GetDataByTaxID(string taxid)
        {
            return (from list in context.Tbl_Organization
                    where list.TaxID == taxid
                    && list.isDeleted != 1
                    select list).ToList<Tbl_Organization>();
        }
        public virtual Tbl_Organization GetDataBySupplierID(int supID)
        {
            return context.Tbl_Organization.Find(supID);
        }

        public virtual IEnumerable<Tbl_Organization> GetOrganizationByInfosConcat(string infosconcat)
        {

            var query1 = from list in context.Tbl_Organization
                         where (list.TaxID.ToString() + list.CompanyName_Local + list.BranchNo + list.BranchName_Local).ToUpper() == infosconcat
                         && list.isDeleted != 1
                         select list;

            var result1 = query1.ToList();

            if (result1.Count() > 0)
            {
                return result1;
            }

            var query2 = from list in context.Tbl_Organization
                         where (list.TaxID.ToString() + list.CompanyName_Inter + list.BranchNo + list.BranchName_Inter).ToUpper() == infosconcat
                         && list.isDeleted != 1
                         select list;

            var result2 = query2.ToList();

            return result2;
        }
		
		public virtual Tbl_Organization GetOrganizationByOrgID(string orgID)
        {
            return context.Tbl_Organization.Where(m => m.OrgID == orgID && m.isDeleted != 1).FirstOrDefault();
        }

        public virtual int GetSupplierIDByUsername(string username)
        {
            var query = from a in context.Tbl_Organization
                        join b in context.Tbl_User on a.SupplierID equals b.SupplierID into c
                        from d in c.DefaultIfEmpty()
                        where d.Username == username
                        && a.isDeleted != 1
                        select a;

            var result = query.FirstOrDefault();

            if (result==null)
            {
                return 0;
            }

            return result.SupplierID;

        }

        public virtual bool CheckDuplicateOrganizationVerifyString(string infosconcat)
        {
            bool chk = false;
            var query1 = from list in context.Tbl_Organization
                         select list;

            var result1 = query1.ToList();

            var resultChk1 = from a in result1
                             where (StringHelper.GetMakeVerifyString(a.TaxID.ToString() + a.CompanyName_Local + a.BranchNo).ToUpper()) == infosconcat
                             && a.isDeleted != 1
                             select a;

            if (resultChk1.Count() > 0)
            {
                return true;
            }

            var query2 = from list in context.Tbl_Organization
                         select list;

            var result2 = query2.ToList();

            var resultChk2 = from a in result2
                             where (StringHelper.GetMakeVerifyString(a.TaxID.ToString() + a.CompanyName_Inter + a.BranchNo).ToUpper()) == infosconcat
                             && a.isDeleted != 1
                             select a;

            if (resultChk2.Count() > 0)
            {
                return true;
            }

            return chk;
        }

        public virtual bool CheckDuplicateOrganization(string infosconcat)
        {
            bool chk = false;

            var query1 = from list in context.Tbl_Organization
                         where (list.TaxID.ToString() + list.CompanyName_Local + list.BranchNo).ToUpper() == infosconcat
                         && list.isDeleted != 1
                         select list;

            var result1 = query1.ToList();

            if (result1.Count() > 0)
            {
                return true;
            }

            var query2 = from list in context.Tbl_Organization
                         where (StringHelper.GetMakeVerifyString(list.TaxID.ToString() + list.CompanyName_Inter + list.BranchNo).ToUpper()) == infosconcat
                         && list.isDeleted != 1
                         select list;

            var result2 = query2.ToList();

            if (result2.Count() > 0)
            {
                return true;
            }

            return chk;
        }

        public virtual Tbl_Organization GetOrganizationByInfosConcatRegis(string infosconcat)
        {

            var query = from list in context.Tbl_Organization
                        where list.isDeleted != 1
                         select list;

            var resultList1 = query.ToList();

            var resultChk1 = from a in resultList1
                             where (StringHelper.GetMakeVerifyString(a.TaxID.ToString() + a.CompanyName_Local + a.BranchNo).ToUpper()) == infosconcat
                             select a;

            var result1 = resultChk1.FirstOrDefault();
            if (result1 != null)
            {
                return result1;
            }


            var resultList2 = query.ToList();

            var resultChk2 = from a in resultList1
                             where (StringHelper.GetMakeVerifyString(a.TaxID.ToString() + a.CompanyName_Inter + a.BranchNo).ToUpper()) == infosconcat
                             select a;

            var result2 = resultChk2.FirstOrDefault();
         
            return result2;
            
        }

        public virtual void Update(Tbl_Organization model, int UpdateBy)
        {
            try
            {
                if (model != null)
                {
                    model.LastUpdate = DateTime.UtcNow;

                    var modelTemp = context.Tbl_Organization.Find(model.SupplierID);
                    modelTemp = model;
                    context.SaveChanges();

                    _logOrganizationRepository.Insert(modelTemp.SupplierID, UpdateBy, "Modify");
                    //_logOrganizationRepository.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public virtual bool CheckDuplicateOrganizationByTaxIDAndBranchNo(string taxID, string branchNo)
        {
            bool chk = false;

            var query = from a in context.Tbl_Organization
                        where a.TaxID.Trim() == taxID.Trim()
                        && a.BranchNo.Trim() == branchNo.Trim()
                        && a.isDeleted != 1
                        select a;

            var result = query.ToList();
            if (result.Count() > 0)
            {
                return true;
            }

            return chk;
        }

        public virtual Tbl_Organization GetOrganizationByTaxIDAndBranchNo(string taxID, string branchNo)
        {
            var query = from a in context.Tbl_Organization
                        where a.TaxID.Trim() == taxID.Trim()
                        && a.BranchNo.Trim() == branchNo.Trim()
                        && a.isDeleted != 1
                        select a;

            var result = query.FirstOrDefault();

            return result;
        }
        public virtual TrackingDictionary GetTrackingStatus() 
        {
            //pass class for getTrackingStatus
            var result = context.GetTrackingStatus<Tbl_Organization>();

            return result;

        }

        public virtual void UpdateLastUpdate(int supplierID, int UpdateBy)
        {
            try
            {
                var model = context.Tbl_Organization.Find(supplierID);
                if (model != null)
                {
                    model.LastUpdate = DateTime.UtcNow;
                    context.SaveChanges();

                    _logOrganizationRepository.Insert(model.SupplierID, UpdateBy, "Modify");
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual CompanyGeneralModel GetDataBySupplierIDAndlanguageID(int supID, int languageID)
        {
            var query = (from a in context.Tbl_Organization.Where(w => w.SupplierID == supID)
                         join be in context.Tbl_BusinessEntity on a.BusinessEntityID equals be.Id into join_be
                         from jbe in join_be.DefaultIfEmpty()
                         join lp in context.Tbl_LocalizedProperty.Where(w => w.LocaleKeyGroup == "Tbl_BusinessEntity" && w.LocaleKey == "BusinessEntityDisplay" && w.LanguageID == languageID) on jbe.Id equals lp.EntityID into join_lp
                         from jlp in join_lp.DefaultIfEmpty()
                         select new CompanyGeneralModel()
                         {
                             BranchNo = a.BranchNo,
                             CompanyName_Inter = languageID == 1 ? a.CompanyName_Inter + " " + jlp.LocaleValue : jbe.BusinessEntityDisplay + " " + a.CompanyName_Local,
                             CompanyTypeID = a.CompanyTypeID,
                             TaxID = a.TaxID,
                             BranchName_Inter = a.BranchName_Inter,
                             BranchName_Local = a.BranchName_Local,
                             BusinessEntityID = a.BusinessEntityID,
                             CompanyName_Local = a.CompanyName_Local,
                             CountryCode = a.CountryCode,
                             DUNSNumber = a.DUNSNumber,
                             OtherBusinessEntity = a.OtherBusinessEntity
                         });

            return query.FirstOrDefault();
        }
        

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}

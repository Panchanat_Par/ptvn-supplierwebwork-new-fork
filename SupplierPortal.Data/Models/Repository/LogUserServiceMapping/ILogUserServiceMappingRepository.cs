﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.LogUserServiceMapping
{
    public partial interface ILogUserServiceMappingRepository
    {
        void Insert(Tbl_LogUserServiceMapping model);

        void Dispose();
    }
}

﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogUserServiceMapping
{
    public partial class LogUserServiceMappingRepository : ILogUserServiceMappingRepository
    {

        SupplierPortalEntities context;

        public LogUserServiceMappingRepository()
        {
            context = new SupplierPortalEntities();
        }

        public LogUserServiceMappingRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }

        public virtual void Insert(Tbl_LogUserServiceMapping model)
        {
            try
            {
                if (model != null)
                {
                    context.Tbl_LogUserServiceMapping.Add(model);
                    context.SaveChanges();
                }
            }
            catch
            {
                throw;
            }

        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}

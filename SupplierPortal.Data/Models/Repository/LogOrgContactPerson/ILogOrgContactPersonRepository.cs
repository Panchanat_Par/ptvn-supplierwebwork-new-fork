﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogOrgContactPerson
{
    public partial interface ILogOrgContactPersonRepository
    {
        void Insert(int supplierID,int contactID, string logAction, string updateBy);

        void Insert(int supplierID, int contactID, string logAction, int updateBy);

        void Dispose();
    }
}

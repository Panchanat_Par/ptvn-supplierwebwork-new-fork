﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.DocumentNoFormat
{
    public partial class DocumentNoFormatRepository : IDocumentNoFormatRepository
    {
        private SupplierPortalEntities context;

        public DocumentNoFormatRepository()
        {
            context = new SupplierPortalEntities();
        }

        public DocumentNoFormatRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }

        public virtual IEnumerable<Tbl_DocumentNoFormat> GetDocumentNoFormatListBy(string docName)
        {
            return (from list in context.Tbl_DocumentNoFormat
                    where list.DocumentName == docName
                          && list.ExpireDate >= DateTime.Now
                    select list).ToList<Tbl_DocumentNoFormat>();
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}

﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.TimeZone
{
    public partial interface ITimeZoneRepository
    {
        IEnumerable<Tbl_TimeZone> TimeZoneList();

        Tbl_TimeZone GetTimeZoneByTimeZoneIDString(string timeZoneIDString);

        void Dispose();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using System.Collections;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;

namespace SupplierPortal.Data.Models.Repository.Language
{
    public partial interface ILanguageRepository
    {

        IEnumerable<Tbl_Language> List();

        IEnumerable<Tbl_Language> ListOrderByAsc();

        IEnumerable<Tbl_Language> ListOrderByDisplayOrder();

        Tbl_Language First();

        IEnumerable<string> GetAllLanguageCode();

        IEnumerable<string> GetAllLanguageId();

        Tbl_Language GetLanguageById(int languageId);

        string GetLanguageIdByCode(string languageCode);

        string GetLanguageCodeById(int languageId);

        Tbl_Language GetLocalLanguage();

        Tbl_Language GetInterLanguage();

        DynamicLanguageModels GetDynamicLanguage();

        void Dispose();


    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogTrackingRequest
{
    public partial interface ILogTrackingRequestRepository
    {
        void Insert(int trackingReqID, int updateBy, string logAction);

        void Dispose();
    }
}

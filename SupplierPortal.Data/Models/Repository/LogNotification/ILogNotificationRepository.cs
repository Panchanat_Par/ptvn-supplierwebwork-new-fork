﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogNotification
{
    public partial interface ILogNotificationRepository
    {
        void Insert(int notificationID, string logAction, int updateBy);

        void Dispose();
    }
}

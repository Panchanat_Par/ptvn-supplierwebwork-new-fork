﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.AutoComplete_City
{
    public partial interface IAutoComplete_CityRepository
    {
        IEnumerable<Tbl_AutoComplete_City> GetCityByMany(string countrycode, int languageID);

        IEnumerable<Tbl_AutoComplete_City> GetCityByState(string countrycode,int stateID, int languageID);

        IQueryable<Tbl_AutoComplete_City> GetCityAllByCountrycodeAndLanguageID(string countrycode, int languageID);

        IQueryable<Tbl_AutoComplete_City> GetCityAllByCountrycode(string countrycode);

        IQueryable<Tbl_AutoComplete_City> GetCityRange(int skip, int take, string filter);

        IEnumerable<Tbl_AutoComplete_City> GetCityList(string term);

        void Dispose();
    }
}

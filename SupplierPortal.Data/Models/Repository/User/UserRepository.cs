﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.Profile;
using System.Web;
using SupplierPortal.Data.Helper;
using SupplierPortal.Data.Models.Repository.LogUser;
using SupplierPortal.Data.CustomModels.User;

namespace SupplierPortal.Data.Models.Repository.User
{
    public partial class UserRepository : IUserRepository
    {

        private SupplierPortalEntities context;
        ILogUserRepository _logUserRepository;


        public UserRepository()
        {
            context = new SupplierPortalEntities();
            _logUserRepository = new LogUserRepository();
        }

        public UserRepository(
            SupplierPortalEntities ctx,
            LogUserRepository logUserRepository
            )
        {
            this.context = ctx;
            _logUserRepository = logUserRepository;
        }

        public virtual UserContactPersonDataModels GetUserContactInfoByUsername(string username)
        {

            int isInter = LanguageHelper.GetLanguageIsInter();

            var result = from user in context.Tbl_User
                         join org in context.Tbl_Organization on user.SupplierID equals org.SupplierID
                         join contact in context.Tbl_ContactPerson on user.ContactID equals contact.ContactID
                         join jobTitle in context.Tbl_JobTitle on contact.JobTitleID equals jobTitle.JobTitleID
                         join nameTitle in context.Tbl_NameTitle on contact.TitleID equals nameTitle.TitleID
                         where user.Username.Equals(username)
                         select new UserContactPersonDataModels
                         {
                             SupplierID = (int)user.SupplierID,
                             CompanyName = org.InvName_Inter,
                             CompanyName_Local = org.InvName_Local,
                             Username = user.Username,
                             ContactID = contact.ContactID,
                             NameTitle = nameTitle.TitleName,
                             FirstName = contact.FirstName_Inter,
                             LastName = contact.LastName_Inter,
                             FirstName_Local = contact.FirstName_Local,
                             LastName_Local = contact.FirstName_Inter,
                             JobTitle = jobTitle.JobTitleName,
                             OtherJobTitle = contact.OtherJobTitle,
                             Department = contact.Department,
                             PhoneNo = contact.PhoneNo,
                             PhoneExt = contact.PhoneExt,
                             MobileNo = contact.MobileNo,
                             FaxNo = contact.FaxNo,
                             FaxExt = contact.FaxExt,
                             Email = contact.Email
                         };

            return result.FirstOrDefault();
        }

        public virtual UserContactResponse GetUserInfoFromProcurementBoard(string username)
        {

            int isInter = LanguageHelper.GetLanguageIsInter();

            var result = from user in context.Tbl_User
                         join org in context.Tbl_Organization on user.SupplierID equals org.SupplierID
                         join contact in context.Tbl_ContactPerson on user.ContactID equals contact.ContactID
                         join jobTitle in context.Tbl_JobTitle on contact.JobTitleID equals jobTitle.JobTitleID
                         join nameTitle in context.Tbl_NameTitle on contact.TitleID equals nameTitle.TitleID
                         where user.Username.Equals(username)
                         select new UserContactResponse
                         {
                             companyID = (int)user.SupplierID,
                             companyName = org.InvName_Inter,
                             companyNameLocal = org.InvName_Local,
                             companyTaxID = org.TaxID,
                             companyCountryCode = org.CountryCode,
                             userID = user.UserID,
                             firstname = contact.FirstName_Inter,
                             lastname = contact.LastName_Inter,
                             firstnameLocal = contact.FirstName_Local,
                             lastnameLocal = contact.FirstName_Inter,
                             email = contact.Email
                         };

            return result.FirstOrDefault();
        }

        public virtual Tbl_User FindAllByUsername(string username)
        {
            var query = from pc in context.Tbl_User
                        where pc.Username == username
                        select pc;

            return query.FirstOrDefault();
        }

        public virtual Tbl_User FindByUsernameIsActive(string username)
        {
            var query = from a in context.Tbl_User
                        join b in context.Tbl_Organization on a.SupplierID equals b.SupplierID
                        where a.Username == username
                        && a.IsDeleted == 0
                        && b.isDeleted == 0
                        select a;

            return query.FirstOrDefault();

        }

        public virtual Tbl_User FindUserByUserID(int userID)
        {
            var query = from pc in context.Tbl_User
                        where pc.UserID == userID
                        select pc;

            return query.FirstOrDefault();
        }

        public virtual Tbl_User FindByUsername(string username)
        {

            var query = from pc in context.Tbl_User
                        where pc.Username == username
                        && pc.IsDeleted == 0
                        select pc;

            return query.FirstOrDefault();
        }
        public virtual int GetUserIDByUsername(string username)
        {
            int userID = 0;
            var query = from pc in context.Tbl_User
                        where pc.Username == username
                        && pc.IsDeleted == 0
                        select pc;

            var result = query.FirstOrDefault();

            if (result != null)
            {
                userID = result.UserID;
            }

            return userID;
        }
        public virtual string FindOrganizNameByUsername(string username)
        {
            HttpCookie cultureCookie = HttpContext.Current.Request.Cookies["_culture"];

            string languageID = "";

            if (cultureCookie != null)
            {
                languageID = cultureCookie.Value.ToString();

            }
            if (string.IsNullOrEmpty(languageID))
            {
                languageID = CultureHelper.GetImplementedCulture(languageID); // This is safe
            }
            int languageId = Convert.ToInt32(languageID);

            var _tempLanguage = context.Tbl_Language.Find(languageId);
            int isInter = 0;
            if (_tempLanguage != null)
            {
                isInter = _tempLanguage.isInter ?? 0;
            }



            var query = from pc in context.Tbl_User
                        join c in context.Tbl_Organization on pc.OrgID equals c.OrgID
                        where pc.Username == username
                        && c.isDeleted != 1
                        select (isInter == 1 ? c.CompanyName_Inter : c.CompanyName_Local);


            return query.FirstOrDefault();
        }

        public virtual string FindOrgIDByUsername(string username)
        {
            var query = from pc in context.Tbl_User
                        where pc.Username == username
                        select pc.OrgID;


            return query.FirstOrDefault();
        }

        public virtual void Update(Tbl_User tbl_User)
        {
            try
            {

                if (tbl_User != null)
                {

                    var usertmp = context.Tbl_User.Find(tbl_User.UserID);
                    usertmp = tbl_User;
                    context.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public virtual void Insert(Tbl_User tbl_User)
        {
            try
            {
                if (tbl_User != null)
                {

                    context.Tbl_User.Add(tbl_User);
                    context.SaveChanges();

                    string userUpdate = "";

                    #region  Insert Log
                    if (System.Web.HttpContext.Current.Session["username"] != null)
                    {
                        userUpdate = System.Web.HttpContext.Current.Session["username"].ToString();
                    }
                    int userID = tbl_User.UserID;
                    _logUserRepository.Insert(userID, "Add", userUpdate);
                    #endregion

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public virtual void DeleteUser(string username)
        {
            try
            {
                var usertmp = context.Tbl_User.Where(m => m.Username == username).FirstOrDefault();
                usertmp.IsDeleted = 1;
                context.SaveChanges();

            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public virtual void DisableUser(string username)
        {
            try
            {
                var usertmp = context.Tbl_User.Where(m => m.Username == username).FirstOrDefault();
                usertmp.IsDisabled = 1;
                context.SaveChanges();

            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public virtual void EnableUser(string username)
        {
            try
            {
                var usertmp = context.Tbl_User.Where(m => m.Username == username).FirstOrDefault();
                usertmp.IsDisabled = 0;
                context.SaveChanges();

            }
            catch (Exception ex)
            {
                throw;
            }

        }

        //ASN
        public virtual void UpdateIsAcceptermId(int UserId, int Id)
        {
            try
            {

                if (Id != null)
                {

                    var usertmp = context.Tbl_User.Find(UserId);
                    usertmp.isAcceptTermOfUse = Id;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public IQueryable<UserManageModels> GetUserAll()
        {
            int isInter = LanguageHelper.GetLanguageIsInter();

            IQueryable<UserManageModels> query = from db in context.Tbl_User
                                                 join ps in context.Tbl_ContactPerson
                                                 on db.ContactID equals ps.ContactID into joined
                                                 from j in joined.DefaultIfEmpty()
                                                 where db.IsDeleted == 0
                                                 select new UserManageModels
                                                 {
                                                     Username = db.Username,
                                                     Email = j.Email,
                                                     FirstName = (isInter == 1 ? j.FirstName_Inter : j.FirstName_Local),
                                                     LastName = (isInter == 1 ? j.LastName_Inter : j.LastName_Local)
                                                 };
            return query;
        }

        public IQueryable<UserManageModels> GetUserAllByOrgID(string orgId)
        {
            int isInter = LanguageHelper.GetLanguageIsInter();

            IQueryable<UserManageModels> query = from db in context.Tbl_User
                                                 join ps in context.Tbl_ContactPerson
                                                 on db.ContactID equals ps.ContactID into joined
                                                 from j in joined.DefaultIfEmpty()
                                                 join ur in context.Tbl_UserRole
                                                 on new { db.UserID } equals new { ur.UserID } into urole
                                                 from r in urole.DefaultIfEmpty()
                                                 join acl in context.Tbl_ACL_Role
                                                 on new { r.RoleID } equals new { acl.RoleID } into g
                                                 from c in g.DefaultIfEmpty()
                                                 where db.IsDeleted == 0 && db.OrgID == orgId
                                                 && c.RoleID == r.RoleID
                                                 select new UserManageModels
                                                 {
                                                     Username = db.Username,
                                                     Email = j.Email,
                                                     FirstName = (isInter == 1 ? j.FirstName_Inter : j.FirstName_Local),
                                                     LastName = (isInter == 1 ? j.LastName_Inter : j.LastName_Local),
                                                     RoleName = c.RoleName,
                                                     IsDisabled = db.IsDisabled ?? 0
                                                 };
            return query;
        }

        public IQueryable<UserInformationModels> GetUserInformationByOrgID(string orgId)
        {
            HttpCookie cultureCookie = HttpContext.Current.Request.Cookies["_culture"];

            string languageID = "";

            if (cultureCookie != null)
            {
                languageID = cultureCookie.Value.ToString();

            }
            if (string.IsNullOrEmpty(languageID))
            {
                languageID = CultureHelper.GetImplementedCulture(languageID); // This is safe
            }
            int languageId = Convert.ToInt32(languageID);

            int isInter = LanguageHelper.GetLanguageIsInter();

            IQueryable<UserInformationModels> query = from a in context.Tbl_User
                                                      join b in context.Tbl_ContactPerson on a.ContactID equals b.ContactID into c
                                                      from d in c.DefaultIfEmpty()
                                                      join e in context.Tbl_UserSystemMapping on a.UserID equals e.UserID into f
                                                      from g in f.Where(m => m.SystemID == 1 && m.MergeFromUserID == 0).DefaultIfEmpty()
                                                      join h in context.Tbl_ACL_SystemRole on g.SysRoleID equals h.SysRoleID into i
                                                      from j in i.DefaultIfEmpty()
                                                      join k in context.Tbl_NameTitle on d.TitleID equals k.TitleID into l
                                                      from m in l.DefaultIfEmpty()
                                                      join n in context.Tbl_JobTitle on d.JobTitleID equals n.JobTitleID into o
                                                      from p in o.DefaultIfEmpty()
                                                      where a.OrgID == orgId
                                                      && a.IsDeleted == 0
                                                      && a.UserID != 1
                                                      select new UserInformationModels
                                                      {
                                                          UserID = a.UserID,
                                                          Username = a.Username,
                                                          TitleContactName = (from lp in context.Tbl_LocalizedProperty
                                                                              where lp.EntityID == m.TitleID
                                                                              && lp.LocaleKeyGroup == "Tbl_NameTitle"
                                                                              && lp.LocaleKey == "TitleName"
                                                                              && lp.LanguageID == languageId
                                                                              select lp.LocaleValue).FirstOrDefault() ?? m.TitleName,
                                                          Fullname = (isInter == 1 ? d.FirstName_Inter : d.FirstName_Local) + "  " + (isInter == 1 ? d.LastName_Inter : d.LastName_Local),
                                                          JobTitleID = p.JobTitleID,
                                                          JobTitleName = (from lp2 in context.Tbl_LocalizedProperty
                                                                          where lp2.EntityID == p.JobTitleID
                                                                          && lp2.LocaleKeyGroup == "Tbl_JobTitle"
                                                                          && lp2.LocaleKey == "JobTitleName"
                                                                          && lp2.LanguageID == languageId
                                                                          select lp2.LocaleValue).FirstOrDefault() ?? p.JobTitleName,
                                                          OtherJobTitle = d.OtherJobTitle ?? "",
                                                          Tel = d.PhoneNo ?? "",
                                                          Mobile = d.MobileNo ?? "",
                                                          Email = d.Email ?? "",
                                                          OPChecked = g.isEnableService ?? 0,
                                                          OPRoleID = g.SysRoleID ?? 0,
                                                          OPRoleName = j.SysRoleName ?? ""
                                                      };

            return query;

        }

        public virtual bool UsernameExists(string username)
        {
            var count = context.Tbl_User.Count(m => m.Username == username);

            if (count > 0)
            {
                return true;
            }

            return false;
        }

        public virtual void UpdateIsActivated(int userID)
        {
            try
            {
                var usertmp = context.Tbl_User.Find(userID);
                if (usertmp != null)
                {
                    usertmp.IsActivated = 1;
                    context.SaveChanges();

                    _logUserRepository.Insert(usertmp.UserID, "Modify", usertmp.Username);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual string GetUsernameByUserID(int userID)
        {
            string username = "";

            var result = context.Tbl_User.Where(m => m.UserID == userID).FirstOrDefault();

            if (result != null)
            {
                username = result.Username;
            }

            return username;
        }

        public void Dispose()
        {
            context.Dispose();
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.LogUser
{
    public partial class LogUserRepository : ILogUserRepository
    {

        private SupplierPortalEntities context;


        public LogUserRepository()
        {
            context = new SupplierPortalEntities();
        }

        public LogUserRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }

        public virtual void Insert(int userID, string logAction, string updateByUsrename)
        {

            try
            {
                var userTemp = context.Tbl_User.FirstOrDefault(m => m.UserID == userID);

                var _user = context.Tbl_User.FirstOrDefault(m => m.Username == updateByUsrename);

                int userIDUpdateBy = 0;
                if (_user != null)
                {
                    userIDUpdateBy = _user.UserID;
                }

                if (userTemp != null)
                {
                    var _logUser = new Tbl_LogUser
                    {
                        UserID = userTemp.UserID,
                        OrgID = userTemp.OrgID,
                        SupplierID = userTemp.SupplierID,
                        ContactID = userTemp.ContactID,
                        Password = userTemp.Password,
                        Salt = userTemp.Salt,
                        IsActivated = userTemp.IsActivated,
                        TimeZone = userTemp.TimeZone,
                        Locale = userTemp.Locale,
                        LanguageCode = userTemp.LanguageCode,
                        Currency = userTemp.Currency,
                        DaysToExpire = userTemp.DaysToExpire,
                        PwdRecoveryToken = userTemp.PwdRecoveryToken,
                        MigrateFlag = userTemp.MigrateFlag,
                        IsDisabled = userTemp.IsDisabled,
                        IsPublic = userTemp.IsPublic,
                        IsDeleted = userTemp.IsDeleted,
                        UpdateBy = userIDUpdateBy,
                        UpdateDate = DateTime.UtcNow,
                        LogAction = logAction,
                        Username = userTemp.Username,
                        MergeToUserID = userTemp.MergeToUserID,
                        isAcceptTermOfUse = userTemp.isAcceptTermOfUse,
                        SendMailFlag = userTemp.SendMailFlag

                    };

                    context.Tbl_LogUser.Add(_logUser);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }


        public virtual void Dispose()
        {
            context.Dispose();
        }

    }
}

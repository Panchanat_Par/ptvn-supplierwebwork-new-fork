﻿using SupplierPortal.Data.Helper;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.Address;
using SupplierPortal.Data.Models.Repository.LogTrackingOrgAddress;
using SupplierPortal.Data.Models.Repository.LogTrackingRequest;
using SupplierPortal.Data.Models.Repository.OrgAddress;
using SupplierPortal.Data.Models.Repository.TrackingComprofile;
using SupplierPortal.Data.Models.SupportModel.CompanyProfile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace SupplierPortal.Data.Models.Repository.TrackingOrgAddress
{
    public partial class TrackingOrgAddressRepository : ITrackingOrgAddressRepository
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private SupplierPortalEntities context;
        IAddressRepository _addressRepository;
        IOrgAddressRepository _orgAddressRepository;
        ITrackingComprofileRepository _trackingComProfileRepository;
        ILogTrackingOrgAddressRepository _logTrackingOrgAddressRepository;
        ILogTrackingRequestRepository _logTrackingRequestRepository;
        public TrackingOrgAddressRepository()
        {
            context = new SupplierPortalEntities();
            _addressRepository = new AddressRepository();
            _orgAddressRepository = new OrgAddressRepository();
            _trackingComProfileRepository = new TrackingComprofileRepository();
            _logTrackingOrgAddressRepository = new LogTrackingOrgAddressRepository();
            _logTrackingRequestRepository = new LogTrackingRequestRepository();
        }
        public TrackingOrgAddressRepository
           (
             SupplierPortalEntities context,
             AddressRepository addressRepository,
             OrgAddressRepository orgAddressRepository,
             TrackingComprofileRepository trackingComProfileRepository,
             LogTrackingOrgAddressRepository logTrackingOrgAddressRepository,
             ILogTrackingRequestRepository logTrackingRequestRepository
           )
        {
            this.context = context;
            _addressRepository = addressRepository;
            _orgAddressRepository = orgAddressRepository;
            _trackingComProfileRepository = trackingComProfileRepository;
            _logTrackingOrgAddressRepository = logTrackingOrgAddressRepository;
            _logTrackingRequestRepository = logTrackingRequestRepository;
        }    

        public virtual IEnumerable<Tbl_TrackingOrgAddress> GetDataBySupplierID(int SupID)
        {
            //return (from list in context.Tbl_TrackingOrgAddress
            //        where list.SupplierID == SupID
            //        select list).ToList<Tbl_TrackingOrgAddress>();

            return (from a in context.Tbl_TrackingOrgAddress
                    join b in context.Tbl_TrackingRequest on a.TrackingReqID equals
                    b.TrackingReqID
                    where b.SupplierID == SupID && a.TrackingStatusID == 2
                    select a).ToList();
        }
        public virtual Tbl_TrackingOrgAddress GetLastDataBySupplierID(int SupID)
        {
            var query = (from a in context.Tbl_TrackingOrgAddress
                         join b in context.Tbl_TrackingRequest on a.TrackingReqID equals
                         b.TrackingReqID
                         where b.SupplierID == SupID
                         && a.TrackingStatusID == 2
                         && a.AdressTypeID == 2
                         select a).OrderByDescending(x => x.TrackingReqID);

            return query.FirstOrDefault();
        }
        public virtual Tbl_TrackingOrgAddress GetLastDataBySupplierIDForCheckDataDeliver(int SupID)
        {
            var query = (from a in context.Tbl_TrackingOrgAddress
                         join b in context.Tbl_TrackingRequest on a.TrackingReqID equals
                         b.TrackingReqID
                         where b.SupplierID == SupID
                         && a.TrackingStatusID != 2
                         && a.AdressTypeID == 2
                         select a).OrderByDescending(x => x.TrackingReqID);

            return query.FirstOrDefault();
        }
        public virtual Tbl_TrackingOrgAddress GetDataforCheckUpdateBySupplierID(int SupID)
        {
            var query = (from a in context.Tbl_TrackingOrgAddress
                         join b in context.Tbl_TrackingRequest on a.TrackingReqID equals
                         b.TrackingReqID
                         where b.SupplierID == SupID
                         && a.OldAddressID != a.NewAddressID
                         && a.TrackingStatusID == 2
                         select a).OrderByDescending(x => x.TrackingReqID);

            return query.FirstOrDefault();
        }
        public void InsertTrackingOrg(string Type, int SupID, int user, int addressID, int deliverID,int trackingReqID)
        {
            try
            {
                var dataOrgAdd = _orgAddressRepository.GetAddresType(SupID, 1);
                var dataOrgDel = _orgAddressRepository.GetAddresType(SupID, 2);

                //var dataAdd = _orgAddressRepository.GetAddress(SupID, dataOrgAdd.AddressID, dataOrgAdd.AddressTypeID);
                //var dataDel = _orgAddressRepository.GetAddress(SupID, dataOrgDel.AddressID, dataOrgDel.AddressTypeID);


                if (Type == "insert")
                {
                    var _TrackingOrgAddress = new Tbl_TrackingOrgAddress()
                    {
                        TrackingReqID = trackingReqID,
                        AdressTypeID = dataOrgDel.AddressTypeID,
                        OldAddressID = addressID,
                        NewAddressID = deliverID,
                        TrackingStatusID = 2,
                        Remark = "",
                        TrackingStatusDate = DateTime.UtcNow
                    };
                    context.Tbl_TrackingOrgAddress.Add(_TrackingOrgAddress);
                    context.SaveChanges();

                    _logTrackingOrgAddressRepository.Insert(_TrackingOrgAddress.TrackingItemID, user, "Add");
                }
                else if (Type == "update")
                {
                    var _TrackingOrgAddress = new Tbl_TrackingOrgAddress()
                    {
                        TrackingReqID = trackingReqID,
                        AdressTypeID = dataOrgAdd.AddressTypeID,
                        OldAddressID = addressID,
                        NewAddressID = addressID,
                        TrackingStatusID = 2,
                        Remark = "",
                        TrackingStatusDate = DateTime.UtcNow
                    };
                    context.Tbl_TrackingOrgAddress.Add(_TrackingOrgAddress);
                    context.SaveChanges();

                    _logTrackingOrgAddressRepository.Insert(_TrackingOrgAddress.TrackingItemID, user, "Add");
                }
                else
                {
                    var _TrackingOrgAddress = new Tbl_TrackingOrgAddress()
                    {
                        TrackingReqID = trackingReqID,
                        AdressTypeID = dataOrgDel.AddressTypeID,
                        OldAddressID = addressID,
                        NewAddressID = addressID,
                        TrackingStatusID = 2,
                        Remark = "",
                        TrackingStatusDate = DateTime.UtcNow
                    };
                    context.Tbl_TrackingOrgAddress.Add(_TrackingOrgAddress);
                    context.SaveChanges();

                    _logTrackingOrgAddressRepository.Insert(_TrackingOrgAddress.TrackingItemID, user, "Add");
                }

            }
            catch (Exception)
            {

                throw;
            }
        }

        public void UpdateTrackingOrg(string Type, int SupID, int user, int addressID, int deliverID,int trackingReqID)
        {
            try
            {
                var dataOrgAdd = _orgAddressRepository.GetAddresType(SupID, 1);
                var dataOrgDel = _orgAddressRepository.GetAddresType(SupID, 2);

                //var dataAdd = _orgAddressRepository.GetAddress(SupID, dataOrgAdd.AddressID, dataOrgAdd.AddressTypeID);
                //var dataDel = _orgAddressRepository.GetAddress(SupID, dataOrgDel.AddressID, dataOrgDel.AddressTypeID);

               
                var modelTrackingOrgAddress = (from a in context.Tbl_TrackingOrgAddress
                                               join b in context.Tbl_TrackingRequest on a.TrackingReqID equals
                                               b.TrackingReqID
                                               where b.SupplierID == SupID
                                               && a.OldAddressID != a.NewAddressID
                                               && a.TrackingStatusID == 2
                                               select a).OrderByDescending(x => x.TrackingReqID);

                var datatracking = modelTrackingOrgAddress.FirstOrDefault();

                var TrackingOrgAddress = (from a in context.Tbl_TrackingOrgAddress
                                          join b in context.Tbl_TrackingRequest on a.TrackingReqID equals
                                          b.TrackingReqID
                                          where b.SupplierID == SupID
                                          && a.OldAddressID == a.NewAddressID
                                          && a.TrackingStatusID == 2
                                          && a.AdressTypeID == 2
                                          select a).OrderByDescending(x => x.TrackingReqID);

                var tracking = TrackingOrgAddress.FirstOrDefault();

                if (Type == "insert" && datatracking != null)
                {
                    if (datatracking.TrackingStatusID != 2)
                    {
                        context.Tbl_TrackingOrgAddress.Remove(datatracking);
                        var _TrackingOrgAddress = new Tbl_TrackingOrgAddress()
                        {
                            AdressTypeID = 2,
                            TrackingReqID = trackingReqID,
                            OldAddressID = addressID,
                            NewAddressID = deliverID,
                            TrackingStatusID = 2,
                            Remark = "",
                            TrackingStatusDate = DateTime.UtcNow
                        };
                        context.Tbl_TrackingOrgAddress.Add(_TrackingOrgAddress);
                        context.SaveChanges();

                        _logTrackingOrgAddressRepository.Insert(_TrackingOrgAddress.TrackingItemID, user, "Modify");
                    }

                }
                else if (Type == "update" && datatracking != null)
                {
                    context.Tbl_TrackingOrgAddress.Remove(datatracking);
                    var _TrackingOrgAddress = new Tbl_TrackingOrgAddress()
                    {
                        AdressTypeID = 2,
                        TrackingReqID = trackingReqID,
                        OldAddressID = addressID,
                        NewAddressID = addressID,
                        TrackingStatusID = 2,
                        Remark = "",
                        TrackingStatusDate = DateTime.UtcNow
                    };
                    context.Tbl_TrackingOrgAddress.Add(_TrackingOrgAddress);
                    context.SaveChanges();

                    _logTrackingOrgAddressRepository.Insert(_TrackingOrgAddress.TrackingItemID, user, "Modify");

                }
                else if (Type == "Remove" && tracking != null)
                {
                    
                    foreach (var item in TrackingOrgAddress)
                    {
                        context.Tbl_TrackingOrgAddress.Remove(item);
                    }
                    
                    var dataTrackingCom = (from a in context.Tbl_TrackingCompProfile
                                           where a.EntityID == deliverID
                                           select a).ToList();
                    foreach (var data in dataTrackingCom)
                    {
                        context.Tbl_TrackingCompProfile.Remove(data);
                    }
                    context.SaveChanges();
                }
                else
                {
                    context.Tbl_TrackingOrgAddress.Remove(datatracking);
                    var _TrackingOrgAddress = new Tbl_TrackingOrgAddress()
                    {
                        TrackingReqID = trackingReqID,
                        AdressTypeID = dataOrgDel.AddressTypeID,
                        OldAddressID = addressID,
                        NewAddressID = addressID,
                        TrackingStatusID = 2,
                        Remark = "",
                        TrackingStatusDate = DateTime.UtcNow
                    };
                    context.Tbl_TrackingOrgAddress.Add(_TrackingOrgAddress);
                    context.SaveChanges();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public virtual int InsertReq(int UpdateBy, int supID)
        {
            try
            {
                int trackingReqID = 0;
                var _TrackingReq = new Tbl_TrackingRequest()
                {
                    SupplierID = supID,
                    ReqBy = UpdateBy,
                    ReqDate = DateTime.UtcNow,
                    ApproveBy = 0,
                    TrackingGrpID = 3,
                    TrackingStatusID = 2,
                    isDeleted = 0,
                    Remark = "",
                    TrackingStatusDate = DateTime.UtcNow

                };
                context.Tbl_TrackingRequest.Add(_TrackingReq);
                context.SaveChanges();

                trackingReqID = _TrackingReq.TrackingReqID;

                _logTrackingRequestRepository.Insert(_TrackingReq.TrackingReqID, supID, "Add");

                return trackingReqID;
            }
            catch (Exception)
            {

                throw;
            }

        }

        public virtual int InsertReqReturnTrackingReqID(int UpdateBy, int supID)
        {
            try
            {
                int trackingReqID = 0;
                logger.Debug("(before Insert)TrackingReqID : " + trackingReqID);
                var _TrackingReq = new Tbl_TrackingRequest()
                {
                    SupplierID = supID,
                    ReqBy = UpdateBy,
                    ReqDate = DateTime.UtcNow,
                    ApproveBy = 0,
                    TrackingGrpID = 3,
                    TrackingStatusID = 2,
                    isDeleted = 0,
                    Remark = "",
                    TrackingStatusDate = DateTime.UtcNow

                };
                context.Tbl_TrackingRequest.Add(_TrackingReq);
                context.SaveChanges();
               
                trackingReqID = _TrackingReq.TrackingReqID;
                logger.Debug("(After Insert)TrackingReqID : " + trackingReqID);
                _logTrackingRequestRepository.Insert(_TrackingReq.TrackingReqID, supID, "Add");

                return trackingReqID;
            }
            catch (Exception)
            {

                throw;
            }

        }

        public virtual IEnumerable<ViewTrackingContactAddressModel> GetTrackingContactAddressByTrackingReqID(int trackingReqID)
        {
            HttpCookie cultureCookie = HttpContext.Current.Request.Cookies["_culture"];

            string languageID = "";

            if (cultureCookie != null)
            {
                languageID = cultureCookie.Value.ToString();

            }
            if (string.IsNullOrEmpty(languageID))
            {
                languageID = CultureHelper.GetImplementedCulture(languageID); // This is safe
            }
            int languageId = Convert.ToInt32(languageID);

            var query = from a in context.Tbl_TrackingOrgAddress
                        join b in context.Tbl_AddressType on a.AdressTypeID equals b.AddressTypeID into c
                        from d in c.DefaultIfEmpty()
                        join e in context.Tbl_TrackingStatus on a.TrackingStatusID equals e.TrackingStatusID into f
                        from g in f.DefaultIfEmpty()
                        where a.TrackingReqID == trackingReqID
                        select new ViewTrackingContactAddressModel
                        {
                            TrackingReqID = a.TrackingReqID,
                            TrackingItemID = a.TrackingItemID,
                            AdressTypeID = a.AdressTypeID ?? 0,
                            AdressType = (from lp in context.Tbl_LocalizedProperty
                                          where lp.EntityID == a.AdressTypeID
                                          && lp.LocaleKeyGroup == "Tbl_AddressType"
                                          && lp.LocaleKey == "AddressType"
                                          && lp.LanguageID == languageId
                                          select lp.LocaleValue).FirstOrDefault() ?? d.AddressType,
                            OldAddressID = a.OldAddressID ?? 0,
                            NewAddressID = a.NewAddressID ?? 0,
                            TrackingStatusID = a.TrackingStatusID ?? 0,
                            TrackingStatusName = (from lp in context.Tbl_LocalizedProperty
                                                  where lp.EntityID == a.TrackingStatusID
                                                  && lp.LocaleKeyGroup == "Tbl_TrackingStatus"
                                                  && lp.LocaleKey == "TrackingStatusName"
                                                  && lp.LanguageID == languageId
                                                  select lp.LocaleValue).FirstOrDefault() ?? g.TrackingStatusName,

                        };

            var result = query.ToList();

            return result;

        }

        public virtual IEnumerable<ViewTrackingContactAddressModel> GetTrackingContactAddressByTrackingItemID(int trackingItemID)
        {
            HttpCookie cultureCookie = HttpContext.Current.Request.Cookies["_culture"];

            string languageID = "";

            if (cultureCookie != null)
            {
                languageID = cultureCookie.Value.ToString();

            }
            if (string.IsNullOrEmpty(languageID))
            {
                languageID = CultureHelper.GetImplementedCulture(languageID); // This is safe
            }
            int languageId = Convert.ToInt32(languageID);

            var query = from a in context.Tbl_TrackingOrgAddress
                        join b in context.Tbl_AddressType on a.AdressTypeID equals b.AddressTypeID into c
                        from d in c.DefaultIfEmpty()
                        join e in context.Tbl_TrackingStatus on a.TrackingStatusID equals e.TrackingStatusID into f
                        from g in f.DefaultIfEmpty()
                        where a.TrackingItemID == trackingItemID
                        select new ViewTrackingContactAddressModel
                        {
                            TrackingReqID = a.TrackingReqID,
                            TrackingItemID = a.TrackingItemID,
                            AdressTypeID = a.AdressTypeID ?? 0,
                            AdressType = (from lp in context.Tbl_LocalizedProperty
                                          where lp.EntityID == a.AdressTypeID
                                          && lp.LocaleKeyGroup == "Tbl_AddressType"
                                          && lp.LocaleKey == "AdressType"
                                          && lp.LanguageID == languageId
                                          select lp.LocaleValue).FirstOrDefault() ?? d.AddressType,
                            OldAddressID = a.OldAddressID ?? 0,
                            NewAddressID = a.NewAddressID ?? 0,
                            TrackingStatusID = a.TrackingStatusID ?? 0,
                            TrackingStatusName = (from lp in context.Tbl_LocalizedProperty
                                                  where lp.EntityID == a.TrackingStatusID
                                                  && lp.LocaleKeyGroup == "Tbl_TrackingStatus"
                                                  && lp.LocaleKey == "TrackingStatusName"
                                                  && lp.LanguageID == languageId
                                                  select lp.LocaleValue).FirstOrDefault() ?? g.TrackingStatusName,

                        };

            var result = query.ToList();

            return result;

        }
        public virtual IEnumerable<ViewAddressApproval> GetTrackingContactAddressByTrackingItemIDList(List<int> trackingItemID)
        {
            HttpCookie cultureCookie = HttpContext.Current.Request.Cookies["_culture"];

            string languageID = "";

            if (cultureCookie != null)
            {
                languageID = cultureCookie.Value.ToString();

            }
            if (string.IsNullOrEmpty(languageID))
            {
                languageID = CultureHelper.GetImplementedCulture(languageID); // This is safe
            }
            int languageId = Convert.ToInt32(languageID);

            var query = from a in context.Tbl_TrackingOrgAddress
                        join b in context.Tbl_AddressType on a.AdressTypeID equals b.AddressTypeID into c
                        from d in c.DefaultIfEmpty()
                        join e in context.Tbl_TrackingStatus on a.TrackingStatusID equals e.TrackingStatusID into f
                        from g in f.DefaultIfEmpty()
                        where trackingItemID.Contains(a.TrackingItemID)
                        select new ViewAddressApproval
                        {
                            ItemId = a.TrackingItemID,
                            AddressTypeId = a.AdressTypeID ?? 0,
                            AddressTypeName = (from lp in context.Tbl_LocalizedProperty
                                          where lp.EntityID == a.AdressTypeID
                                          && lp.LocaleKeyGroup == "Tbl_AddressType"
                                          && lp.LocaleKey == "AdressType"
                                          && lp.LanguageID == languageId
                                          select lp.LocaleValue).FirstOrDefault() ?? d.AddressType,
                            TrackingStatusId = a.TrackingStatusID ?? 0,
                            Remark = a.Remark,
                            TrackingStatusDate = (DateTime) a.TrackingStatusDate,
                            TrackingStatusName = (from lp in context.Tbl_LocalizedProperty
                                                  where lp.EntityID == a.TrackingStatusID
                                                  && lp.LocaleKeyGroup == "Tbl_TrackingStatus"
                                                  && lp.LocaleKey == "TrackingStatusName"
                                                  && lp.LanguageID == languageId
                                                  select lp.LocaleValue).FirstOrDefault() ?? g.TrackingStatusName,

                        };

            var result = query.ToList();

            return result;

        }

        public virtual Tbl_TrackingOrgAddress GetTrackingOrgAddressWaitingApproveByTrackingReqIDAndAddressTypeID(int trackingReqID,int addressTypeID)
        {
            var query = from a in context.Tbl_TrackingOrgAddress
                        where a.TrackingReqID == trackingReqID
                        && a.AdressTypeID == addressTypeID
                        && a.TrackingStatusID == 2
                        select a;

            return query.FirstOrDefault();
        }

        public virtual Tbl_TrackingOrgAddress GetDataCheckAddress(int SupID)
        {
            var query = (from a in context.Tbl_TrackingOrgAddress
                         join b in context.Tbl_TrackingRequest on a.TrackingReqID equals
                         b.TrackingReqID
                         where b.SupplierID == SupID
                         && a.OldAddressID == a.NewAddressID
                         && a.AdressTypeID == 1
                         && a.TrackingStatusID == 2
                         select a).OrderByDescending(x => x.TrackingReqID);

            return query.FirstOrDefault();
        }

        public virtual Tbl_TrackingOrgAddress GetDataCheckDelivery(int SupID)
        {
            var query = (from a in context.Tbl_TrackingOrgAddress
                         join b in context.Tbl_TrackingRequest on a.TrackingReqID equals
                         b.TrackingReqID
                         where b.SupplierID == SupID
                         && a.OldAddressID == a.NewAddressID
                         && a.AdressTypeID == 2
                         && a.TrackingStatusID == 2
                         select a).OrderByDescending(x => x.TrackingReqID);

            return query.FirstOrDefault();
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}

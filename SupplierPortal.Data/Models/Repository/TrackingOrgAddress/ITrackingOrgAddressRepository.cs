﻿using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.CompanyProfile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.TrackingOrgAddress
{
    public partial interface ITrackingOrgAddressRepository
    {
        IEnumerable<Tbl_TrackingOrgAddress> GetDataBySupplierID(int SupID);

        Tbl_TrackingOrgAddress GetLastDataBySupplierID(int SupID);

        Tbl_TrackingOrgAddress GetLastDataBySupplierIDForCheckDataDeliver(int SupID);

        Tbl_TrackingOrgAddress GetDataforCheckUpdateBySupplierID(int SupID);
        //type = insert or update
        void InsertTrackingOrg(string Type, int SupID, int user, int addressID ,int deliverID,int trackingReqID);

        void UpdateTrackingOrg(string Type, int SupID, int user, int addressID, int deliverID,int trackingReqID);

        int InsertReq(int UpdateBy, int supID);

        int InsertReqReturnTrackingReqID(int UpdateBy, int supID);

        IEnumerable<ViewTrackingContactAddressModel> GetTrackingContactAddressByTrackingReqID(int trackingReqID);

        IEnumerable<ViewTrackingContactAddressModel> GetTrackingContactAddressByTrackingItemID(int trackingItemID);

        IEnumerable<ViewAddressApproval> GetTrackingContactAddressByTrackingItemIDList(List<int> trackingItemID);

        Tbl_TrackingOrgAddress GetTrackingOrgAddressWaitingApproveByTrackingReqIDAndAddressTypeID(int trackingReqID, int addressTypeID);

        Tbl_TrackingOrgAddress GetDataCheckAddress(int SupID);

        Tbl_TrackingOrgAddress GetDataCheckDelivery(int SupID);

        void Dispose();
    }
}

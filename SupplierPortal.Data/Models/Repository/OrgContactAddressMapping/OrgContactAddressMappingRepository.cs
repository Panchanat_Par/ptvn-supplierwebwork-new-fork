﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.Repository.LogOrgContactAddressMapping;

namespace SupplierPortal.Data.Models.Repository.OrgContactAddressMapping
{
    public partial class OrgContactAddressMappingRepository : IOrgContactAddressMappingRepository
    {
        private SupplierPortalEntities context;
        ILogOrgContactAddressMappingRepository _logOrgContactAddressMappingRepository;

        public OrgContactAddressMappingRepository()
        {
            context = new SupplierPortalEntities();
            _logOrgContactAddressMappingRepository = new LogOrgContactAddressMappingRepository();
        }

        public OrgContactAddressMappingRepository(
            SupplierPortalEntities context,
            LogOrgContactAddressMappingRepository logOrgContactAddressMappingRepository
            )
        {
            this.context = context;
            _logOrgContactAddressMappingRepository = logOrgContactAddressMappingRepository;
        }

        public virtual Tbl_OrgContactAddressMapping GetOrgContactAddrMappingByContactID(int contactID)
        {

            var query = context.Tbl_OrgContactAddressMapping.Where(m => m.ContactID == contactID).FirstOrDefault();

            return query;
                        
        }

        public virtual void Insert(Tbl_OrgContactAddressMapping model, int updateBy)
        {
            if (model != null)
            {
                context.Tbl_OrgContactAddressMapping.Add(model);
                context.SaveChanges();

                _logOrgContactAddressMappingRepository.Insert(model.Id, "Add", updateBy);
            }

        }

        public virtual void Delete(int id, int updateBy)
        {
            var model = context.Tbl_OrgContactAddressMapping.Find(id);
            if (model != null)
            {
                context.Tbl_OrgContactAddressMapping.Remove(model);

                _logOrgContactAddressMappingRepository.Insert(model.Id, "Remove", updateBy);

                context.SaveChanges();

                
            }

        }

        public virtual void Dispose()
        {
            context.Dispose();
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.AppConfig
{
    public partial interface IAppConfigRepository
    {

        Tbl_AppConfig GetAppConfigByName(string name);

        string GetValueAppConfigByName(string name);

        void Dispose();

    }
}

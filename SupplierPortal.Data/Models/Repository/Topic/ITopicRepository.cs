﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.Topic
{
    public partial interface ITopicRepository
    {
		string GetResourceBody(string resourceKey, string languageId);
        IEnumerable<Tbl_Topic> GetTopicDataListParam(string sectionname, int languageid);
        string GetDownloadLinkAgreement(int languageID);
        string GetDownloadLinkOPAgreement(int languageID);
        void Dispose();
    }
}

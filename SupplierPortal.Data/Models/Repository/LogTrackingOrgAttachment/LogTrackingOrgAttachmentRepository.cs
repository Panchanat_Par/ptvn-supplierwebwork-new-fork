﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogTrackingOrgAttachment
{
    public partial class LogTrackingOrgAttachmentRepository : ILogTrackingOrgAttachmentRepository
    {

        SupplierPortalEntities context;

        public LogTrackingOrgAttachmentRepository()
        {
            context = new SupplierPortalEntities();
        }

        public LogTrackingOrgAttachmentRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }

        public virtual void Insert(int trackingItemID, int updateBy, string logAction)
        {
            try
            {
                var tempModel = context.Tbl_TrackingOrgAttachment.Where(m => m.TrackingItemID == trackingItemID).FirstOrDefault();
                if (tempModel != null)
                {
                    var model = new Tbl_LogTrackingOrgAttachment()
                    {
                        TrackingReqID = tempModel.TrackingReqID,
                        TrackingItemID = tempModel.TrackingItemID,
                        OldAttachmentID = tempModel.OldAttachmentID,
                        NewAttachmentID = tempModel.NewAttachmentID,
                        TrackingStatusID = tempModel.TrackingStatusID,
                        TrackingStatusDate = tempModel.TrackingStatusDate,
                        Remark = tempModel.Remark,
                        DocumentNameID = tempModel.DocumentNameID,
                        UpdateBy = updateBy,
                        UpdateDate = DateTime.UtcNow,
                        LogAction = logAction
                    };
                    context.Tbl_LogTrackingOrgAttachment.Add(model);
                    context.SaveChanges();
                }
            }
            catch
            {
                throw;
            }
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}

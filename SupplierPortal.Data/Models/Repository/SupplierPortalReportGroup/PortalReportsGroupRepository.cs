﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.SupplierPortalReportGroup
{
    public partial class PortalReportsGroupRepository : IPortalReportsGroupRepository
    {
        private SupplierPortalEntities context;


        public PortalReportsGroupRepository()
        {
            context = new SupplierPortalEntities();
        }

        public PortalReportsGroupRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }


        public virtual IEnumerable<Tbl_SupplierPortalReportGroup> List()
        {
            return context.Tbl_SupplierPortalReportGroup.ToList();
        }
    }
}

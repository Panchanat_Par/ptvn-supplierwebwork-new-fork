﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogOrgCertifiedStdAttachment
{
    public partial interface ILogOrgCertifiedStdAttachmentRepository
    {
        void Insert(int certId, int attachmentID, string logAction, int updateBy);

        void Dispose();
    }
}

﻿using SupplierPortal.Data.Helper;
using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;

namespace SupplierPortal.Data.Models.Repository.CompanyType
{
    public partial class CompanyTypeRepository : ICompanyTypeRepository
    {

        private SupplierPortalEntities context;

        public CompanyTypeRepository()
        {
            context = new SupplierPortalEntities();
        }

        public CompanyTypeRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }


        public IEnumerable<string> GetCompanyTypeByLanguageId(int languageId)
        {

            var query = from db in context.Tbl_CompanyType
                        where db.CompanyTypeID != 0
                        orderby db.CompanyTypeID
                        select new
                        {
                            CompanyType = (from lp in context.Tbl_LocalizedProperty
                                               where lp.EntityID == db.CompanyTypeID
                                               && lp.LocaleKeyGroup == "Tbl_CompanyType"
                                               && lp.LocaleKey == "CompanyType"
                                               && lp.LanguageID == languageId
                                           select lp.LocaleValue).FirstOrDefault() ?? db.CompanyType
                        };
                        

            var result = query.Select(m=>m.CompanyType).ToList();

            return result;
        }

        public IEnumerable<CompanyTypeModels> GetCompanyTypeByMultiLanguageId()
        {

            HttpCookie cultureCookie = HttpContext.Current.Request.Cookies["_culture"];

            string languageID = "";

            if (cultureCookie != null)
            {
                languageID = cultureCookie.Value.ToString();

            }
            if (string.IsNullOrEmpty(languageID))
            {
                languageID = CultureHelper.GetImplementedCulture(languageID); // This is safe
            }
            int languageId = Convert.ToInt32(languageID);

            var query = from db in context.Tbl_CompanyType
                        where db.CompanyTypeID != 0
                        orderby db.CompanyTypeID
                        select new CompanyTypeModels
                        {
                            CompanyTypeID = db.CompanyTypeID,
                            CompanyType = (from lp in context.Tbl_LocalizedProperty
                                           where lp.EntityID == db.CompanyTypeID
                                           && lp.LocaleKeyGroup == "Tbl_CompanyType"
                                           && lp.LocaleKey == "CompanyType"
                                           && lp.LanguageID == languageId
                                           select lp.LocaleValue).FirstOrDefault() ?? db.CompanyType

                        };


            var result = query.ToList();

            return result;
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }

    }
}

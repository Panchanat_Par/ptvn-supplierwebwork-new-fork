﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.Profile;

namespace SupplierPortal.Data.Models.Repository.SystemRole
{
    public partial interface ISystemRepository
    {

        IEnumerable<SystemRoles_sel_Result> GetSystemRoleSelectList(int systemID, int sysRoleID);

        //IEnumerable<SystemRoleListModels> GetSystemListByUsernameForUpdate(string username);

        //IEnumerable<SystemRoleListModels> GetSystemListByUsernameAndSystemIDForUpdate(string username, int systemID);

        void Dispose();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;
using SupplierPortal.Data.Models.Repository.LogRegEmailTicket;

namespace SupplierPortal.Data.Models.Repository.RegEmailTicket
{
    public partial class RegEmailTicketRepository : IRegEmailTicketRepository
    {
        SupplierPortalEntities context;

        public RegEmailTicketRepository()
        {
            context = new SupplierPortalEntities();
        }

        public RegEmailTicketRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }

        public virtual IEnumerable<Tbl_RegEmailTicket> GetEmailTicketDataListByTicketcode(string ticketcode)
        {
            return (from list in context.Tbl_RegEmailTicket
                    where list.TicketCode == ticketcode
                    select list).ToList<Tbl_RegEmailTicket>();
        }

        public virtual Tbl_RegEmailTicket GetLatestTicketcodeByEmailTicket(string emil)
        {
            return (context.Tbl_RegEmailTicket
                              .Where(m => m.RegisteredEmail == emil)
                              .OrderByDescending(m => m.TicketCode) 
                              .FirstOrDefault());
        }

        public virtual void Insert(Tbl_RegEmailTicket model)
        {
            try
            {
                if (model != null)
                {
                    context.Tbl_RegEmailTicket.Add(model);
                    context.SaveChanges();

                    ILogRegEmailTicketRepository _logRegEmlTicket = new LogRegEmailTicketRepository();
                    _logRegEmlTicket.Insert(model.TicketCode, "Add");
                    //_logRegEmlTicket.Dispose();
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public virtual void Update(Tbl_RegEmailTicket model, int updateBy)
        {
            var tempModel = context.Tbl_RegEmailTicket.Where(m => m.TicketCode == model.TicketCode).FirstOrDefault();

            if (tempModel != null)
            {
                tempModel = model;
                context.SaveChanges();

                ILogRegEmailTicketRepository _logRegEmlTicket = new LogRegEmailTicketRepository();
                _logRegEmlTicket.Insert(model.TicketCode, "Modify", updateBy);
            }
        }

        public virtual bool Insert(RegisterPortalModel model, string ticketcode, string languageCode)
        {
            try
            {
                var _regEmailTicket = new Tbl_RegEmailTicket()
                {
                    TicketCode = ticketcode,
                    RegisteredEmail = model.Email,
                    LanguageCode = languageCode
                };
                if (_regEmailTicket != null)
                {
                    context.Tbl_RegEmailTicket.Add(_regEmailTicket);
                    context.SaveChanges();

                    ILogRegEmailTicketRepository _logRegEmlTicket = new LogRegEmailTicketRepository();
                    _logRegEmlTicket.Insert(_regEmailTicket.TicketCode, "Add");
                    _logRegEmlTicket.Dispose();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public virtual bool Update(RegisContinuePortalModel _model, string languageCode)
        {
            try
            {
                var modeleml = context.Tbl_RegEmailTicket.FirstOrDefault(m => m.TicketCode == _model.TicketCode);
                modeleml.RegisteredEmail = _model.Email;
                modeleml.LanguageCode = languageCode;
                if (modeleml != null)
                {
                    context.Entry(modeleml).State = System.Data.Entity.EntityState.Modified;
                    context.SaveChanges();

                    ILogRegEmailTicketRepository _logRegEmlTicket = new LogRegEmailTicketRepository();
                    _logRegEmlTicket.Insert(modeleml.TicketCode, "Modify");
                    _logRegEmlTicket.Dispose();
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        public virtual bool EmailExists(string email, string initialEmail)
        {

            //if (string.IsNullOrEmpty(initialEmail))
            //{
            //    var count = context.Tbl_RegEmailTicket.Count(m => m.RegisteredEmail == email && m.RegisteredEmail != initialEmail);
            //}else
            //{
            //    var count = context.Tbl_RegEmailTicket.Count(m => m.RegisteredEmail == email && m.RegisteredEmail != initialEmail);
            //}
            var count = context.Tbl_RegEmailTicket.Count(m => m.RegisteredEmail == email && m.RegisteredEmail != initialEmail);

            if (count > 0)
            {
                return true;
            }

            return false;
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.AccountPortal;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;

namespace SupplierPortal.Data.Models.Repository.RegEmailTicket
{
    public partial interface IRegEmailTicketRepository
    {
        IEnumerable<Tbl_RegEmailTicket> GetEmailTicketDataListByTicketcode(string ticketcode);

        bool Insert(RegisterPortalModel model, string ticketcode, string languageCode);

        void Insert(Tbl_RegEmailTicket model);

        void Update(Tbl_RegEmailTicket model,int updateBy);

        Tbl_RegEmailTicket GetLatestTicketcodeByEmailTicket(string emil);

        bool Update(RegisContinuePortalModel _model, string languageCode);

        bool EmailExists(string email, string initialEmail = "");

        void Dispose();
    }
}

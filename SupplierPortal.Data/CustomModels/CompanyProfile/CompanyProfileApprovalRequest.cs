﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.CustomModels.CompanyProfile
{
    public partial class CompanyProfileApprovalRequest
    {
        public List<int> trackingAddressItemIdList { get; set; }
        public List<int> trackingAttachmentItemIdList { get; set; }
    }
}

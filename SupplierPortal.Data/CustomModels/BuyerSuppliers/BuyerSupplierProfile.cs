﻿using SupplierPortal.Data.CustomValidate;
using SupplierPortal.Data.Models.CustomValidate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SupplierPortal.Data.CustomModels.BuyerSuppliers
{
    public partial class BuyerSupplierProfile
    {
        public Nullable<int> UserID { get; set; }
        public Nullable<int> ContactID { get; set; }
        public int EID { get; set; }
        public string UserLoginID { get; set; }
        public string TaxID { get; set; }
        public string CompanyName_Local { get; set; }
        public string CompanyName_Inter { get; set; }

        [RequiredErrorMessageBase("_Profile.ValidateMsg.RequireEmail")]
        [Remote("IsEmailAvailableConfig", "BuyerPortal", AdditionalFields = "InitialEmail")]
        public string Email { get; set; }

        public string InitialEmail { get; set; }
        public Nullable<int> TitleID { get; set; }
        public Nullable<int> TitleID_Inter { get; set; }

        [RequiredErrorMessageBase("_Profile.ValidateMsg.RequireFirstName")]
        public string FirstName_Local { get; set; }

        [RequiredErrorMessageBase("_Profile.ValidateMsg.RequireLastName")]
        public string LastName_Local { get; set; }

        [RequiredErrorMessageBase("_Profile.ValidateMsg.RequireFirstName")]
        public string FirstName_Inter { get; set; }

        [RequiredErrorMessageBase("_Profile.ValidateMsg.RequireLastName")]
        public string LastName_Inter { get; set; }

        //[RequiredErrorMessageBase("_Profile.ValidateMsg.RequirePhone")]
        public string PhoneNo { get; set; }

        public string PhoneExt { get; set; }

        //[RequiredErrorMessageBase("_Profile.ValidateMsg.RequireMobileNo")]
        public string MobileNo { get; set; }

        public string MobileCountryCode { get; set; }
        public string PhoneCountryCode { get; set; }
        public Boolean IsPrimaryContact { get; set; }
        public Nullable<int> SupplierID { get; set; }
        public Nullable<int> IsDisabled { get; set; }
    }
}
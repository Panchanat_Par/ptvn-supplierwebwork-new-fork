﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.CustomModels.BuyerSuppliers
{
    public partial class BuyerUserConfig
    {
        public Tbl_BuyerUser Tbl_BuyerUser { get; set; }

        public Tbl_BuyerConfig Tbl_BuyerConfig { get; set; }
    }
}

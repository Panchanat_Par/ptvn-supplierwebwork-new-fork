﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.CustomModels.BuyerSupplier
{
    public partial class ViewBuyerSupplier
    {
        public int CurrentId { get; set; }

        public string CurrentUser { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.CustomModels.SendEmail
{
    public partial class SCFEmailMapping
    {
        public string NAME { get; set; }
        public string PHONE_NUMBER { get; set; }
        public string EMAIL { get; set; }
        public string TAX_ID { get; set; }
        public string COMPANY_NAME { get; set; }
        public string BRANCH_NUMBER { get; set; }
        public string COMPANY_ADDRESS { get; set; }
        public string REQUEST_FROM { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.CustomModels.User
{
    public partial class UserContactResponse
    {
        public int userID { get; set; }
        public string email { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string firstnameLocal { get; set; }
        public string lastnameLocal { get; set; }
        public string companyName { get; set; }
        public string companyNameLocal { get; set; }
        public int companyID { get; set; }
        public string companyTaxID { get; set; }
        public string companyCountryCode { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.CustomModels.RegisterSupplier
{
    public partial class RegisterSupplierResponse
    {
        public int RegId { get; set; }
  
        public int RegAddressId { get; set; }

        public int RegContactId { get; set; }

        public string TicketCode { get; set; }

        public string TaxId { get; set; }

        public string BranchNumber { get; set; }

        public string CompanyName { get; set; }

        public string CompanyNameLocal { get; set; }

        public string TaxCountryCode { get; set; }

        public string ErrorMsg { get; set; }
    }
}

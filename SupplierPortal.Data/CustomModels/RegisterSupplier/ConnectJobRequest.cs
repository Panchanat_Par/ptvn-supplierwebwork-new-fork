﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.CustomModels.RegisterSupplier
{
    public partial class ConnectJobRequest
    {
        public int regId { get; set; }

        public string status { get; set; }

        public string type { get; set; }
    }
}

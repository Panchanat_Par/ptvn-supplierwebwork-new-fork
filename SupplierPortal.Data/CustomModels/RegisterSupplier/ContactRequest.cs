﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.CustomModels.RegisterSupplier
{
    public partial class ContactRequest
    {
        [Required(ErrorMessage = "Register.ErrorMsg.RegID_NotNull")]
        public int RegID { get; set; }

        public string EmailAdminBSP { get; set; }

        public bool Service1 { get; set; }

        public bool Service2 { get; set; }

        public bool Service3 { get; set; }

        [Required(ErrorMessage = "Register.ErrorMsg.ContactTitleID_NotNull")]
        public int ContactTitleID { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Register.ErrorMsg.FirstName_Local_NotNullOrEmpty")]
        public string FirstName_Local { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Register.ErrorMsg.LastName_Local_NotNullOrEmpty")]
        public string LastName_Local { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Register.ErrorMsg.FirstName_Inter_NotNullOrEmpty")]
        public string FirstName_Inter { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Register.ErrorMsg.LastName_Inter_NotNullOrEmpty")]
        public string LastName_Inter { get; set; }

        [Required(ErrorMessage = "Register.ErrorMsg.ContactJobTitleID_NotNull")]
        public int ContactJobTitleID { get; set; }

        public string ContactOtherJobTitle { get; set; }

        public string ContactPhoneNoCountryCode { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Register.ErrorMsg.ContactPhoneNo_NotNullOrEmpty")]
        public string ContactPhoneNo { get; set; }

        public string ContactPhoneExt { get; set; }

        public string ContactMobileCountryCode { get; set; }

        public string ContactMobile { get; set; }

        [Required(ErrorMessage = "Register.ErrorMsg.ContactEmail_NotNullOrEmpty")]
        public string ContactEmail { get; set; }

        public string ContactInvitationCode { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Register.ErrorMsg.LanguageCode_NotNullOrEmpty")]
        public string LanguageCode { get; set; }
    }
}

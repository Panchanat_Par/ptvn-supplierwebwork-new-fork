﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Models.Dto.COC
{
    public class COCBaseResponseDto
    {
        public bool isSuccess { get; set; }
        public dynamic data { get; set; }
        public string errorMessage { get; set; }
    }
}

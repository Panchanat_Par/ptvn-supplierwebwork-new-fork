﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Models.Dto.DataContract
{
    public class GetBuyerSuppliersRequest : DatatableRequest
    {
        public int EID { get; set; }

        public int Language { get; set; }
    }
}

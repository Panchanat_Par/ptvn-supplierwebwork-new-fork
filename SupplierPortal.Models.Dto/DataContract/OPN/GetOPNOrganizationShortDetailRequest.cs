﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Models.Dto.DataContract
{
    public class GetOPNOrganizationShortDetailRequest
    {
        public string taxId;
        public string branchNo;
    }
}

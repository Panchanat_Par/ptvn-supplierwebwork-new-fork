﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Models.Dto
{
    public class Tbl_SDFavoriteDto
    {
        public int Id { get; set; }
        public string SysUserID { get; set; }
        public Nullable<int> EID { get; set; }
        public Nullable<int> UserID { get; set; }
    }
}

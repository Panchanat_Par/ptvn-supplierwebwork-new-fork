USE [Supplier_Connect]
GO
/****** Object:  StoredProcedure [dbo].[SP_MigrateDataSWWToSC]    Script Date: 11/9/2020 9:47:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Piyavut>
-- Create date: <2020-11-05>
-- Description:	<Migrate Data from Supplier webwork to Supplier Connect>
-- =============================================
ALTER PROCEDURE [dbo].[SP_MigrateRegDataSWWToSC]
	@pCreateBy		NVARCHAR(100) = '',
	@pListOfIDs		NVARCHAR(1000) = ''
AS
BEGIN

	DECLARE @tableRegInfo TABLE (
		RegId			INT,
		SupplierId		INT,
		TaxId			NVARCHAR(50),
		TaxCountryCode	NVARCHAR(10),
		BranchNumber	NVARCHAR(100)
	)

	DECLARE @tableRegID TABLE (
		RegId			INT
	)

	BEGIN --SWW Variable
		DECLARE @sww_RegId INT, @sww_ContactId INT, @sww_AddressId INT, @sww_CompanyTypeId INT, @sww_BusinessEntityId INT, @sww_RegStatusId INT, @sww_TitleId INT, @sww_JobTitleId INT

		DECLARE	@sww_TicketCode NVARCHAR(100), @sww_RegisteredEmail NVARCHAR(100), @sww_LanguageCode NVARCHAR(100), @sww_CountryCode NVARCHAR(100), @sww_TaxID NVARCHAR(100), 
				@sww_RegisteredCapital NVARCHAR(100), @sww_OtherBusinessEntity NVARCHAR(100), @sww_CompanyName_Local NVARCHAR(100), @sww_CompanyName_Inter NVARCHAR(100), 
				@sww_BranchNo NVARCHAR(100), @sww_BranchName_Local NVARCHAR(100), @sww_BranchName_Inter NVARCHAR(100), @sww_PhoneNo NVARCHAR(100), @sww_PhoneExt NVARCHAR(100), 
				@sww_MobileNo NVARCHAR(100), @sww_FaxNo NVARCHAR(100), @sww_FaxExt NVARCHAR(100), @sww_CurrencyCode NVARCHAR(100), @sww_WebSite NVARCHAR(100),
				@sww_ContactFirstName NVARCHAR(100), @sww_ContactLastName NVARCHAR(100), @sww_ContactFirstNameLocal NVARCHAR(100), @sww_ContactLastNameLocal NVARCHAR(100), 
				@sww_ContactEmail NVARCHAR(100), @sww_ContactPhone NVARCHAR(100), @sww_InvitationCode NVARCHAR(100), @sww_OtherJobTitle  NVARCHAR(100), @sww_ContactPhoneExt NVARCHAR(100),
				@sww_ContactFaxNo  NVARCHAR(100), @sww_ContactFaxExt  NVARCHAR(100), @sww_ContactMobileCountryCode NVARCHAR(100), @sww_ContactMobileNo NVARCHAR(100),
				@sww_HouseNo_Local NVARCHAR(1000), @sww_HouseNo_Inter NVARCHAR(1000), @sww_VillageNo_Local NVARCHAR(1000), @sww_VillageNo_Inter NVARCHAR(1000),
				@sww_Lane_Local NVARCHAR(1000), @sww_Lane_Inter NVARCHAR(1000), @sww_Road_Local NVARCHAR(1000), @sww_Road_Inter NVARCHAR(1000), @sww_SubDistrict_Local NVARCHAR(1000),
				@sww_SubDistrict_Inter NVARCHAR(1000), @sww_City_Local NVARCHAR(1000), @sww_City_Inter NVARCHAR(1000), @sww_State_Local NVARCHAR(1000),@sww_State_Inter NVARCHAR(1000), 
				@sww_PostalCode NVARCHAR(100), @sww_SqlText NVARCHAR(1000)

	END

	BEGIN --SC Variable
		DECLARE @sc_CountryCodeThailand NVARCHAR(100), @sc_OtherAddress NVARCHAR(1000), @sc_OtherAddressLocal NVARCHAR(1000),@sc_HouseNumber NVARCHAR(1000), @sc_HouseNumberLocal NVARCHAR(1000),
				@sc_MooNumber NVARCHAR(1000), @sc_MooNumberLocal NVARCHAR(1000), @sc_Alley NVARCHAR(1000), @sc_AlleyLocal NVARCHAR(1000),
				@sc_Street NVARCHAR(1000), @sc_StreetLocal NVARCHAR(1000), @sc_ProvinceName NVARCHAR(1000), @sc_ProvinceNameLocal NVARCHAR(1000), @sc_DistrictName NVARCHAR(1000), 
				@sc_DistrictNameLocal NVARCHAR(1000), @sc_SubDistrictName NVARCHAR(1000), @sc_SubDistrictNameLocal NVARCHAR(1000), @sc_PostalCode NVARCHAR(100)

		DECLARE @sc_CreatedBy INT = 1, @sc_UserId INT, @sc_SupplierId INT, @sc_BranchId INT, @sc_CountryId INT, @sc_BranchStatusId INT,
				@sc_SupplierStatusId_Awaiting INT, @sc_SupplierStatusId_Reject INT, @sc_BranchStatusId_Awaiting INT, @sc_BranchStatusId_Reject INT, @sc_UserBranchStatusId_Awaiting INT, 
				@sc_UserBranchStatusId_Reject INT, @sc_AddressTypeId_Supplier INT, @sc_AddressTypeId_Document INT, @sc_AddressTypeId_Billing INT, @sc_SupplierStatusId_Approve INT

		DECLARE @sc_IsHeadQuarter BIT, @sc_IsEnglishOfficial BIT, @sc_IsThailand BIT, @sc_IsManufacturer BIT, @sc_IsDistributor BIT, @sc_IsDealer BIT, @sc_IsServiceProvider BIT,
				@sc_IsAddressMasterMatch BIT

		SET @sc_CountryCodeThailand = 'th'

		SET @sc_SupplierStatusId_Awaiting  = (SELECT Id FROM SupplierStatus WHERE Name = 'Common.SupplierStatus.Awaiting')
		SET @sc_SupplierStatusId_Reject  = (SELECT Id FROM SupplierStatus WHERE Name = 'Common.SupplierStatus.Rejected')
		SET @sc_SupplierStatusId_Approve  = (SELECT Id FROM SupplierStatus WHERE Name = 'Common.SupplierStatus.Approved')

		SET @sc_BranchStatusId_Awaiting  = (SELECT Id FROM BranchStatus WHERE Name = 'Common.BranchStatus.Awaiting')
		SET @sc_BranchStatusId_Reject  = (SELECT Id FROM BranchStatus WHERE Name = 'Common.BranchStatus.Rejected')

		SET @sc_UserBranchStatusId_Awaiting  = (SELECT Id FROM UserBranchStatus WHERE Name = 'Common.UserBranchStatus.Awaiting')
		SET @sc_UserBranchStatusId_Reject  = (SELECT Id FROM UserBranchStatus WHERE Name = 'Common.UserBranchStatus.Rejected')

		SET @sc_AddressTypeId_Supplier = (SELECT Id FROM AddressType WHERE Name = 'Common.AddressType.Supplier')
		SET @sc_AddressTypeId_Document = (SELECT Id FROM AddressType WHERE Name = 'Common.AddressType.Document')
		SET @sc_AddressTypeId_Billing = (SELECT Id FROM AddressType WHERE Name = 'Common.AddressType.Billing')		
	END

	BEGIN

		IF (LEN(RTRIM(LTRIM(@pListOfIDs))) = 0)
			BEGIN
				SET @sww_SqlText = 'SELECT RegID FROM [192.168.10.23].[SupplierPortal].[dbo].[Tbl_RegInfo] WHERE RegStatusId IN (2,3) AND RegID NOT IN (SELECT SWW_RegId FROM Temp_MappingDataSWWAndSC WHERE IsMigrateSuccess = 1)'
			END
		ELSE
			BEGIN
				SET @sww_SqlText = 'SELECT RegID FROM [192.168.10.23].[SupplierPortal].[dbo].[Tbl_RegInfo] WHERE RegID IN ('+@pListOfIDs+')'
			END

		INSERT INTO @tableRegID
		EXECUTE(@sww_SqlText)
	END

	BEGIN --TRANSACTION
		
		DECLARE cInsertSupplier CURSOR
		FOR

		SELECT	RegID,
				TicketCode,
				RegisteredEmail,
				LanguageCode,
				RegStatusId,
			    UPPER(CountryCode),
			    TaxID,
			    CompanyTypeID,
			    BusinessEntityID,
			    OtherBusinessEntity,
			    CompanyName_Local,
			    CompanyName_Inter,
			    BranchNo,
			    BranchName_Local,
			    BranchName_Inter,
			    CompanyAddrID,
			    MainContactID,
			    PhoneNo,
			    PhoneExt,
			    MobileNo,
			    FaxNo,
			    FaxExt,
			    RegisteredCapital,
			    CurrencyCode,
			    WebSite
		FROM	[192.168.10.23].[SupplierPortal].[dbo].[Tbl_RegInfo] 
		WHERE	RegId in (SELECT  RegID FROM @tableRegID)
		--WHERE RegID IN (477,551,662)

		OPEN cInsertSupplier
			FETCH NEXT FROM cInsertSupplier into @sww_RegId,
			   @sww_TicketCode,
			   @sww_RegisteredEmail,
			   @sww_LanguageCode,
			   @sww_RegStatusId,
			   @sww_CountryCode,
			   @sww_TaxID,
			   @sww_CompanyTypeId,
			   @sww_BusinessEntityId,
			   @sww_OtherBusinessEntity,
			   @sww_CompanyName_Local,
			   @sww_CompanyName_Inter,
			   @sww_BranchNo,
			   @sww_BranchName_Local,
			   @sww_BranchName_Inter,
			   @sww_AddressId,
			   @sww_ContactId,
			   @sww_PhoneNo,
			   @sww_PhoneExt,
			   @sww_MobileNo,
			   @sww_FaxNo,
			   @sww_FaxExt,
			   @sww_RegisteredCapital,
			   @sww_CurrencyCode,
			   @sww_WebSite
			
			WHILE @@FETCH_STATUS = 0 
				BEGIN
					print '---START---'
					print @sww_RegId
					BEGIN -- Set data to global variable
						SET @sc_IsAddressMasterMatch = 0

						SELECT	 @sww_HouseNo_Local = HouseNo_Local,
								 @sww_HouseNo_Inter = HouseNo_Inter,
								 @sww_VillageNo_Local = VillageNo_Local,
								 @sww_VillageNo_Inter = VillageNo_Inter,
								 @sww_Lane_Local = Lane_Local,
								 @sww_Lane_Inter = Lane_Inter,
								 @sww_Road_Local = Road_Local,
								 @sww_Road_Inter = Road_Inter,
								 @sww_SubDistrict_Local = SubDistrict_Local,
								 @sww_SubDistrict_Inter = SubDistrict_Inter,
								 @sww_City_Local = City_Local,
								 @sww_City_Inter = City_Inter,
								 @sww_State_Local = State_Local,
								 @sww_State_Inter = State_Inter,
								 @sww_PostalCode = PostalCode
						FROM	 [192.168.10.23].[SupplierPortal].[dbo].[Tbl_RegAddress] 
						WHERE	 AddressID = @sww_AddressId

						IF (@sww_CountryCode = 'th')
							BEGIN

								IF EXISTS(SELECT '*' FROM AddressMaster WHERE SubDistrictLocal like N'%' + (@sww_SubDistrict_Local  COLLATE Latin1_General_CI_AS) + '%')
									BEGIN										
										SELECT	TOP 1 @sc_ProvinceNameLocal = StateNameLocal,
												@sc_ProvinceName = StateName,
												@sc_DistrictNameLocal = CityLocal,
												@sc_DistrictName = City,
												@sc_SubDistrictNameLocal = SubDistrictLocal,
												@sc_SubDistrictName = SubDistrict,
												@sc_PostalCode = PostalCode
										FROM	AddressMaster 
										WHERE	SubDistrictLocal like N'%' + (@sww_SubDistrict_Local  COLLATE Latin1_General_CI_AS) + '%'

										SET @sc_IsAddressMasterMatch = 1
									END

								IF @sc_IsAddressMasterMatch = 0 AND EXISTS(SELECT '*' FROM AddressMaster WHERE SubDistrict like N'%' + (@sww_SubDistrict_Local  COLLATE Latin1_General_CI_AS) + '%')
									BEGIN
										SELECT	TOP 1 @sc_ProvinceNameLocal = StateNameLocal,
												@sc_ProvinceName = StateName,
												@sc_DistrictNameLocal = CityLocal,
												@sc_DistrictName = City,
												@sc_SubDistrictNameLocal = SubDistrictLocal,
												@sc_SubDistrictName = SubDistrict,
												@sc_PostalCode = PostalCode
										FROM	AddressMaster 
										WHERE	SubDistrict like N'%' + @sww_SubDistrict_Local + '%'

										SET @sc_IsAddressMasterMatch = 1
									END

								-- IF @sc_PostalCode <> @sww_PostalCode
									-- BEGIN
										
										-- SET @sc_PostalCode = @sww_PostalCode
									-- END
							END
						ELSE
							BEGIN
								SET @sc_IsAddressMasterMatch = 1

								SET	@sc_SubDistrictNameLocal = @sww_SubDistrict_Local
								SET	@sc_SubDistrictName = @sww_SubDistrict_Inter
								SET	@sc_DistrictNameLocal = @sww_City_Local
								SET	@sc_DistrictName = @sww_City_Inter
								SET	@sc_ProvinceNameLocal = @sww_State_Local
								SET	@sc_ProvinceName = @sww_State_Inter
								SET	@sc_PostalCode = @sww_PostalCode
							END
					 
						SELECT	@sww_TitleId = TitleID,
								@sww_ContactFirstNameLocal = FirstName_Local,
								@sww_ContactFirstName = FirstName_Inter,
								@sww_ContactLastNameLocal = LastName_Local,
								@sww_ContactLastName = LastName_Inter,
								@sww_JobTitleId = JobTitleID,
								@sww_OtherJobTitle = OtherJobTitle,
								@sww_ContactPhone = PhoneNo,
								@sww_ContactPhoneExt = PhoneExt,
								@sww_ContactFaxNo = FaxNo,
								@sww_ContactFaxExt = FaxExt,
								@sww_ContactMobileCountryCode = MobileCountryCode,
								@sww_ContactMobileNo = MobileNo,
								@sww_ContactEmail = Email  
						FROM	[192.168.10.23].[SupplierPortal].[dbo].[Tbl_RegContact] 
						WHERE	ContactID = @sww_ContactId 

						SELECT	@sc_CountryId = Id,
								@sc_IsEnglishOfficial = IsEnglishOfficial 
						FROM	Country 
						WHERE	CountryCode = @sww_CountryCode

						SET @sww_InvitationCode = (SELECT InvitationCode FROM [192.168.10.23].[SupplierPortal].[dbo].[Tbl_RegServiceTypeMapping] WHERE RegID = @sww_RegId AND ServiceTypeID = 2)
						SET @sc_IsHeadQuarter = CASE WHEN CAST(@sww_BranchNo AS INT) = 0 THEN 1 ELSE 0 END
						SET @sc_BranchStatusId = CASE WHEN @sww_RegStatusId = 2 THEN @sc_BranchStatusId_Awaiting ELSE @sc_BranchStatusId_Reject END
						SET @sc_IsThailand = CASE WHEN UPPER(@sww_CountryCode) = UPPER(@sc_CountryCodeThailand) THEN 1 ELSE 0 END

						SET @sc_IsManufacturer = CASE WHEN 
											(
												SELECT		SUM(RegID) 
												FROM		[192.168.10.23].[SupplierPortal].[dbo].[Tbl_RegBusinessType] 
												WHERE		RegID = @sww_RegId AND BusinessTypeID = 1
											) > 0 THEN 1 ELSE 0 END

						SET @sc_IsDistributor = CASE WHEN 
											(
												SELECT		SUM(RegID) 
												FROM		[192.168.10.23].[SupplierPortal].[dbo].[Tbl_RegBusinessType] 
												WHERE		RegID = @sww_RegId AND BusinessTypeID = 2
											) > 0 THEN 1 ELSE 0 END

						SET @sc_IsDealer = CASE WHEN 
											(
												SELECT		SUM(RegID) 
												FROM		[192.168.10.23].[SupplierPortal].[dbo].[Tbl_RegBusinessType] 
												WHERE		RegID = @sww_RegId AND BusinessTypeID = 3
											) > 0 THEN 1 ELSE 0 END		
																				
						SET @sc_IsServiceProvider = CASE WHEN 
											(
												SELECT		SUM(RegID) 
												FROM		[192.168.10.23].[SupplierPortal].[dbo].[Tbl_RegBusinessType] 
												WHERE		RegID = @sww_RegId AND BusinessTypeID = 4
											) > 0 THEN 1 ELSE 0 END						
					END

			IF (@sc_IsAddressMasterMatch = 1)
				BEGIN 

					BEGIN -- Insert User
						
						IF (LEN(RTRIM(LTRIM(@pCreateBy))) = 0)
							BEGIN
								SET @sc_UserId = (SELECT TOP 1 Id FROM Users WHERE Username = @sww_RegisteredEmail)
							END
						ELSE
							BEGIN
								SET @sc_UserId = (SELECT Id FROM Users WHERE Username = @pCreateBy)
							END
							
						IF @sc_UserId IS NULL
							BEGIN
								INSERT INTO Users 
									(
										Username,
										Password,
										FirstName,
										LastName,
										FirstNameLocal,
										LastNameLocal,
										DefaultLanguageCode,
										Email,
										Telephone,
										CountryId,
										StatusId,
										IsAcceptTermOfService,
										IsCheckConsent,
										TicketCode,	
										IsTicketCodeTerminate	
									)
								SELECT	@sww_ContactEmail,--Username,
										N'{bcrypt}$2a$10$9VwOtR5bggHNZMTbENlByOuncWf1Ejs.sg/fpmHQRvKklEMmBel2q',--Password,
										@sww_ContactFirstName,--FirstName,
										@sww_ContactLastName,--LastName,
										@sww_ContactFirstNameLocal,--FirstNameLocal,
										@sww_ContactLastNameLocal,--LastNameLocal,
										@sww_LanguageCode,--DefaultLanguageCode,
										@sww_ContactEmail,--Email,
										@sww_PhoneNo,--Telephone,
										(SELECT Id FROM Country WHERE CountryCode = @sww_CountryCode),--CountryId,
										1,--StatusId,
										1,--isAcceptTermOfService,
										1,--isCheckConsent,
										@sww_TicketCode,--TicketCode
										0--isTicketCodeTerminate

								SET @sc_UserId = SCOPE_IDENTITY()
							END
						
						SET @sc_CreatedBy = @sc_UserId
					END 

					BEGIN -- Insert Supplier

						IF NOT EXISTS (SELECT '*' FROM Supplier WHERE TaxId = @sww_TaxID AND TaxCountryCode = @sww_CountryCode AND SupplierStatusId = @sc_SupplierStatusId_Approve)	
							BEGIN
								INSERT INTO Supplier
									(
										  SupplierName,
										  SupplierNameLocal,
										  FirstName,
										  LastName,
										  FirstNameLocal,
										  LastNameLocal,
										  TaxId,
										  IsVatRegistration,
										  IsRDVerify,
										  TaxCountryCode,
										  BusinessEntityId,
										  OtherBusinessEntityName,
										  IdentityTypeId,
										  SupplierStatusId,
										  CreatedBy,
										  CreatedAt,
										  UpdatedBy,
										  UpdatedAt
									)
								SELECT
										  @sww_CompanyName_Inter,--SupplierName,
										  @sww_CompanyName_Local,--SupplierNameLocal,
										  @sww_CompanyName_Inter,--FirstName,
										  @sww_CompanyName_Inter,--LastName,
										  @sww_CompanyName_Local,--FirstNameLocal,
										  @sww_CompanyName_Local,--LastNameLocal,
										  @sww_TaxID,--TaxId,
										  0,--IsVatRegistration,
										  0,--IsRDVerify,
										  @sww_CountryCode,--TaxCountryCode,
										  @sww_BusinessEntityID,--BusinessEntityId,
										  @sww_OtherBusinessEntity,--OtherBusinessEntityName,
										  @sww_CompanyTypeID,--IdentityTypeId,
										  CASE WHEN @sww_RegStatusId = 2 THEN @sc_SupplierStatusId_Awaiting ELSE @sc_SupplierStatusId_Reject END,--SupplierStatusId,
										  @sc_CreatedBy,--CreatedBy,
										  GETDATE(),--CreatedAt,
										  @sc_CreatedBy,--UpdatedBy,
										  GETDATE()--UpdatedAt

								SET @sc_SupplierId = SCOPE_IDENTITY()
							END
						ELSE
							BEGIN
								SET @sc_SupplierId = (SELECT Id FROM Supplier WHERE TaxId = @sww_TaxID AND TaxCountryCode = @sww_CountryCode AND SupplierStatusId = @sc_SupplierStatusId_Approve)
							END
					END 

					BEGIN -- Insert Branch

						INSERT INTO Branch
							(
								 SupplierId,
								 BranchNumber,
								 BranchName,
								 BranchNameLocal,
								 BranchEmail,
								 BranchWebSite,
								 BranchStatusId,
								 RegisteredCapital,
								 CurrencyCode,
								 YearEstablished,
								 IsUseOP,
								 InvitationCode,
								 IsHeadQuarter,
								 BillingContactName,
								 BillingContactNameLocal,
								 BillingContactEmail,
								 BillingContactPhone,
								 BillingContactCountryCode,
								 CreatedBy,
								 CreatedAt,
								 UpdatedBy,
								 UpdatedAt
							)
							SELECT
								 @sc_SupplierId,--SupplierId,
								 CASE WHEN @sww_BranchNo = '' THEN N'00000' ELSE @sww_BranchNo END,--BranchNumber,
								 CASE WHEN @sc_IsHeadQuarter = 1 THEN N'Headquarter' ELSE N'Branch' + CAST(CAST(@sww_BranchNo AS INT) AS NVARCHAR(10)) END,--BranchName,
								 CASE WHEN @sc_IsHeadQuarter = 1 AND @sc_IsEnglishOfficial = 0 THEN N'HQ' ELSE N'Branch' + CAST(CAST(@sww_BranchNo AS INT) AS NVARCHAR(10)) END,--BranchNameLocal,
								 @sww_RegisteredEmail,--BranchEmail,
								 @sww_WebSite,--BranchWebSite,
								 @sc_BranchStatusId,--BranchStatusId,
								 ISNULL(@sww_RegisteredCapital, 0),--RegisteredCapital,
								 CASE WHEN @sww_CurrencyCode IS NULL THEN 
									 CASE WHEN @sww_CountryCode = 'TH' THEN 'THB'
										  WHEN @sww_CountryCode = 'VN' THEN 'VND'
									ELSE 'USD' END
								  ELSE @sww_CurrencyCode END,--CurrencyCode,
								 N'',--YearEstablished,
								 CASE WHEN @sww_InvitationCode IS NULL THEN 0 ELSE 1 END,--IsUseOP,
								 ISNULL(@sww_InvitationCode, ''),--InvitationCode,
								 @sc_IsHeadQuarter,--IsHeadQuarter,
								 @sww_ContactFirstName + ' ' + @sww_ContactLastName,--BillingContactName,
								 @sww_ContactFirstNameLocal + ' ' + @sww_ContactLastNameLocal,--BillingContactNameLocal,
								 @sww_ContactEmail,--BillingContactEmail,
								 @sww_ContactPhone,--BillingContactPhone,
								 @sww_CountryCode,--BillingContactCountryCode,
								 @sc_CreatedBy,--CreatedBy,
								 GETDATE(),--CreatedAt,
								 @sc_CreatedBy,--UpdatedBy,
								 GETDATE()--UpdatedAt

							SET @sc_BranchId = SCOPE_IDENTITY()
					END

					BEGIN -- Insert UserBranch
							INSERT INTO UserBranch
								(
									 UserId,
									 BranchId,
									 RoleId,
									 ActivateStatusId,
									 UserBranchStatusId,
									 JobTitleId,
									 OtherJobTitleName,
									 ContactEmail,
									 ContactPhoneNumber,
									 ContactPhoneNumberExt,
									 SWWRegId,
									 ContactPhoneCountryCode
								)
							SELECT	 @sc_UserId,--UserId,
									 @sc_BranchId,--BranchId,
									 NULL,--RoleId,
									 1,--ActivateStatusId,
									 @sc_BranchStatusId,--UserBranchStatusId,
									 @sww_JobTitleId,--JobTitleId,
									 @sww_OtherJobTitle,--OtherJobTitleName,
									 @sww_ContactEmail,--ContactEmail,
									 @sww_ContactPhone,--ContactPhoneNumber,
									 @sww_ContactPhoneExt,--ContactPhoneNumberExt,
									 @sww_RegId,--SWWRegId,
									 @sww_CountryCode--PhoneCountryCode

					END

					BEGIN -- Insert BranchProductCommodity

							INSERT INTO BranchProductCommodity
								(
									BranchId,
									ProductCommodityId,
									IsManufacturer,
									IsDistributor,
									IsDealer,
									IsServiceProvider,
									ProductCategoryLv3Id
								)
							SELECT	@sc_BranchId,--BranchId,
									NULL,--ProductCommodityId,
									@sc_IsManufacturer,--IsManufacturer,
									@sc_IsDistributor,--IsDistributor,
									@sc_IsDealer,--IsDealer,
									@sc_IsServiceProvider,--IsServiceProvider,
									CategoryID_Lev3--ProductCategoryLv3Id
							FROM	[192.168.10.23].[SupplierPortal].[dbo].[Tbl_RegProductCategory] 
							WHERE	RegID = @sww_RegId
					END 

					BEGIN -- Insert BranchProductKeyword
							INSERT INTO BranchProductKeyword
								(
									BranchId,
									ProductKeyword
								)
							SELECT	@sc_BranchId,--BranchId,
									Keyword--ProductKeyword
							FROM	[192.168.10.23].[SupplierPortal].[dbo].[Tbl_RegProductKeyword]
							WHERE	RegID = @sww_RegId
					END

					BEGIN -- Insert BranchTelephone

						INSERT INTO BranchTelephone
							(
							  BranchId,
							  TelephoneNumber,
							  TelephoneNumberExt,
							  CountryCode
							)
						SELECT	@sc_BranchId,--BranchId,
								@sww_PhoneNo,--TelephoneNumber,
								@sww_PhoneExt,--TelephoneNumberExt,
								@sww_CountryCode--CountryCode
						FROM	[192.168.10.23].[SupplierPortal].[dbo].[Tbl_RegProductKeyword]
						WHERE	RegID = @sww_RegId
					END 

					BEGIN -- Insert RegistrationFile
						select ''
					END 

					BEGIN -- Insert Address

							SET	@sc_OtherAddressLocal = CASE WHEN @sc_IsThailand = 1 THEN ''
									WHEN @sc_IsEnglishOfficial = 1 THEN ''
									ELSE @sww_HouseNo_Local + ' ' + @sww_VillageNo_Local + ' ' + @sww_Lane_Local END

							SET	@sc_OtherAddress = CASE WHEN @sc_IsThailand = 1 THEN '' 
									ELSE @sww_HouseNo_Local + ' ' + @sww_VillageNo_Local + ' ' + @sww_Lane_Local END

							SET	@sc_HouseNumber = CASE WHEN @sc_IsThailand = 0 THEN '' 
									ELSE @sww_HouseNo_Local END

							SET	@sc_HouseNumberLocal = CASE WHEN @sc_IsThailand = 0 THEN '' 
									ELSE @sww_HouseNo_Local END

							SET	@sc_MooNumber = CASE WHEN @sc_IsThailand = 0 THEN '' 
									ELSE @sww_VillageNo_Local END

							SET	@sc_MooNumberLocal = CASE WHEN @sc_IsThailand = 0 THEN '' 
									ELSE @sww_VillageNo_Local END

							SET	@sc_Alley = CASE WHEN @sc_IsThailand = 0 THEN '' 
									ELSE @sww_Lane_Local END

							SET	@sc_AlleyLocal = CASE WHEN @sc_IsThailand = 0 THEN '' 
									ELSE @sww_Lane_Local END

							INSERT INTO Address
								(
									BranchId,
									AddressTypeId,
									OtherAddress,
									OtherAddressLocal,
									BuildingName,
									BuildingNameLocal,
									Room,
									RoomLocal,
									Floor,
									FloorLocal,
									VillageName,
									VillageNameLocal,
									HouseNumber,
									HouseNumberLocal,
									MooNumber,
									MooNumberLocal,
									Alley,
									AlleyLocal,
									Street,
									StreetLocal,
									CountryId,
									ProvinceName,
									ProvinceNameLocal,
									DistrictName,
									DistrictNameLocal,
									SubDistrictName,
									SubDistrictNameLocal,
									PostalCode,
									CreatedBy,
									CreatedAt,
									UpdatedBy,
									UpdatedAt
								)
							SELECT	@sc_BranchId,--BranchId,
									@sc_AddressTypeId_Supplier,--AddressTypeId,
									@sc_OtherAddress,--OtherAddress,
									@sc_OtherAddressLocal,--OtherAddressLocal,
									'',--BuildingName,
									'',--BuildingNameLocal,
									'',--Room,
									'',--RoomLocal,
									'',--Floor,
									'',--FloorLocal,
									'',--VillageName,
									'',--VillageNameLocal,
									@sc_HouseNumber,--HouseNumber,
									@sc_HouseNumberLocal,--HouseNumberLocal,
									@sc_MooNumber,--MooNumber,
									@sc_MooNumberLocal,--MooNumberLocal,
									@sc_Alley,--Alley,
									@sc_AlleyLocal,--AlleyLocal,
									@sww_Road_Local,--Street,
									@sww_Road_Local,--StreetLocal,
									@sc_CountryId,--CountryId,
									@sc_ProvinceName,--ProvinceName,
									@sc_ProvinceNameLocal,--ProvinceNameLocal,
									@sc_DistrictName,--DistrictName,
									@sc_DistrictNameLocal,--DistrictNameLocal,
									@sc_SubDistrictName,--SubDistrictName,
									@sc_SubDistrictNameLocal,--SubDistrictNameLocal,
									@sc_PostalCode,--PostalCode,
									@sc_CreatedBy,--CreatedBy,
									GETDATE(),--CreatedAt,
									@sc_CreatedBy,--UpdatedBy,
									GETDATE()--UpdatedAt

							INSERT INTO Address
								(
									BranchId,
									AddressTypeId,
									OtherAddress,
									OtherAddressLocal,
									BuildingName,
									BuildingNameLocal,
									Room,
									RoomLocal,
									Floor,
									FloorLocal,
									VillageName,
									VillageNameLocal,
									HouseNumber,
									HouseNumberLocal,
									MooNumber,
									MooNumberLocal,
									Alley,
									AlleyLocal,
									Street,
									StreetLocal,
									CountryId,
									ProvinceName,
									ProvinceNameLocal,
									DistrictName,
									DistrictNameLocal,
									SubDistrictName,
									SubDistrictNameLocal,
									PostalCode,
									CreatedBy,
									CreatedAt,
									UpdatedBy,
									UpdatedAt
								)
							SELECT	@sc_BranchId,--BranchId,
									@sc_AddressTypeId_Document,--AddressTypeId,
									'',--OtherAddress,
									'',--OtherAddressLocal,
									'',--BuildingName,
									'',--BuildingNameLocal,
									'',--Room,
									'',--RoomLocal,
									'',--Floor,
									'',--FloorLocal,
									'',--VillageName,
									'',--VillageNameLocal,
									'',--HouseNumber,
									'',--HouseNumberLocal,
									'',--MooNumber,
									'',--MooNumberLocal,
									'',--Alley,
									'',--AlleyLocal,
									'',--Street,
									'',--StreetLocal,
									@sc_CountryId,--CountryId,
									@sc_ProvinceName,--ProvinceName,
									@sc_ProvinceNameLocal,--ProvinceNameLocal,
									@sc_DistrictName,--DistrictName,
									@sc_DistrictNameLocal,--DistrictNameLocal,
									@sc_SubDistrictName,--SubDistrictName,
									@sc_SubDistrictNameLocal,--SubDistrictNameLocal,
									@sc_PostalCode,--PostalCode,
									@sc_CreatedBy,--CreatedBy,
									GETDATE(),--CreatedAt,
									@sc_CreatedBy,--UpdatedBy,
									GETDATE()--UpdatedAt

							INSERT INTO Address
								(
									BranchId,
									AddressTypeId,
									OtherAddress,
									OtherAddressLocal,
									BuildingName,
									BuildingNameLocal,
									Room,
									RoomLocal,
									Floor,
									FloorLocal,
									VillageName,
									VillageNameLocal,
									HouseNumber,
									HouseNumberLocal,
									MooNumber,
									MooNumberLocal,
									Alley,
									AlleyLocal,
									Street,
									StreetLocal,
									CountryId,
									ProvinceName,
									ProvinceNameLocal,
									DistrictName,
									DistrictNameLocal,
									SubDistrictName,
									SubDistrictNameLocal,
									PostalCode,
									CreatedBy,
									CreatedAt,
									UpdatedBy,
									UpdatedAt
								)
							SELECT	@sc_BranchId,--BranchId,
									@sc_AddressTypeId_Billing,--AddressTypeId,
									'',--OtherAddress,
									'',--OtherAddressLocal,
									'',--BuildingName,
									'',--BuildingNameLocal,
									'',--Room,
									'',--RoomLocal,
									'',--Floor,
									'',--FloorLocal,
									'',--VillageName,
									'',--VillageNameLocal,
									'',--HouseNumber,
									'',--HouseNumberLocal,
									'',--MooNumber,
									'',--MooNumberLocal,
									'',--Alley,
									'',--AlleyLocal,
									'',--Street,
									'',--StreetLocal,
									@sc_CountryId,--CountryId,
									@sc_ProvinceName,--ProvinceName,
									@sc_ProvinceNameLocal,--ProvinceNameLocal,
									@sc_DistrictName,--DistrictName,
									@sc_DistrictNameLocal,--DistrictNameLocal,
									@sc_SubDistrictName,--SubDistrictName,
									@sc_SubDistrictNameLocal,--SubDistrictNameLocal,
									@sc_PostalCode,--PostalCode,
									@sc_CreatedBy,--CreatedBy,
									GETDATE(),--CreatedAt,
									@sc_CreatedBy,--UpdatedBy,
									GETDATE()--UpdatedAt

					END

					BEGIN	-- Insert Data Migration
						INSERT INTO Temp_MappingDataSWWAndSC
							(
								SWW_RegId,
								SC_BranchId,
								SC_UserId,
								IsMigrateSuccess,
								IsAddressNotMatch
							)
						SELECT	@sww_RegId,
								@sc_BranchId,
								@sc_UserId,
								1,
								0

						INSERT INTO [192.168.10.23].[SupplierPortal].[dbo].[Temp_MappingDataSWWAndSC]
							(
								SWW_RegId,
								SC_BranchId,
								SC_UserId,
								IsMigrateSuccess,
								IsAddressNotMatch
							)
						SELECT	@sww_RegId,
								@sc_BranchId,
								@sc_UserId,
								1,
								0

					END
				END
			ELSE
				-- Address Not Match
				BEGIN
						INSERT INTO Temp_MappingDataSWWAndSC
							(
								SWW_RegId,
								SC_BranchId,
								SC_UserId,
								IsMigrateSuccess,
								IsAddressNotMatch
							)
						SELECT	@sww_RegId,
								@sc_BranchId,
								@sc_UserId,
								0,
								1
					
						INSERT INTO [192.168.10.23].[SupplierPortal].[dbo].[Temp_MappingDataSWWAndSC]
							(
								SWW_RegId,
								SC_BranchId,
								SC_UserId,
								IsMigrateSuccess,
								IsAddressNotMatch
							)
						SELECT	@sww_RegId,
								@sc_BranchId,
								@sc_UserId,
								0,
								1
				END
					--IF @@ERROR <> 0 
					--	BEGIN
					--		INSERT INTO @TempMessage(Number, Step) 
					--		VALUES(@vNumber, '#INSERT TABLE Contract_Header')
					--	END
					--ELSE
					--	BEGIN
					--		INSERT INTO @TempContractHeader
					--		VALUES(@vContractHederId_Insert)
					--	END
					--END			
					--SELECT	*		
					--FROM	[192.168.10.23].[SupplierPortal].[dbo].[Tbl_RegInfo] 
					--WHERE	RegId = @sww_RegId 
			print @sww_RegId
			print '---END---'

			FETCH NEXT FROM cInsertSupplier into  @sww_RegId,
			   @sww_TicketCode,
			   @sww_RegisteredEmail,
			   @sww_LanguageCode,
			   @sww_RegStatusId,
			   @sww_CountryCode,
			   @sww_TaxID,
			   @sww_CompanyTypeId,
			   @sww_BusinessEntityId,
			   @sww_OtherBusinessEntity,
			   @sww_CompanyName_Local,
			   @sww_CompanyName_Inter,
			   @sww_BranchNo,
			   @sww_BranchName_Local,
			   @sww_BranchName_Inter,
			   @sww_AddressId,
			   @sww_ContactId,
			   @sww_PhoneNo,
			   @sww_PhoneExt,
			   @sww_MobileNo,
			   @sww_FaxNo,
			   @sww_FaxExt,
			   @sww_RegisteredCapital,
			   @sww_CurrencyCode,
			   @sww_WebSite
				END
		CLOSE cInsertSupplier
		DEALLOCATE cInsertSupplier
	END
	--COMMIT
	--ROLLBACK
	SELECT * FROM [Temp_MappingDataSWWAndSC]

END

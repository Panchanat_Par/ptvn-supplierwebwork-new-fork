﻿
If Not Exists (Select 'x' From [dbo].[Tbl_LocaleStringResource] Where LanguageID = 1 And ResourceName = '_Agreement.CodeOfConduct.HeaderTitle')
Begin
	INSERT INTO [dbo].[Tbl_LocaleStringResource] ([LanguageID], [ResourceName], [ResourceValue], [OldResourceName])
	VALUES (1,'_Agreement.CodeOfConduct.HeaderTitle', 'Supplier Code of Conduct', NULL);
End

If Not Exists (Select 'x' From [dbo].[Tbl_LocaleStringResource] Where LanguageID = 2 And ResourceName = '_Agreement.CodeOfConduct.HeaderTitle')
Begin
	INSERT INTO [dbo].[Tbl_LocaleStringResource] ([LanguageID], [ResourceName], [ResourceValue], [OldResourceName])
	VALUES (2,'_Agreement.CodeOfConduct.HeaderTitle', 'จรรยาบรรณและข้อพึงปฏิบัติ สำหรับคู่ค้า', NULL);
End

If Not Exists (Select 'x' From [dbo].[Tbl_LocaleStringResource] Where LanguageID = 1 And ResourceName = '_Agreement.CodeOfConduct.Acknowledged')
Begin
	INSERT INTO [dbo].[Tbl_LocaleStringResource] ([LanguageID], [ResourceName], [ResourceValue], [OldResourceName])
	VALUES (1,'_Agreement.CodeOfConduct.Acknowledged', 'Acknowledged', NULL);
End

If Not Exists (Select 'x' From [dbo].[Tbl_LocaleStringResource] Where LanguageID = 2 And ResourceName = '_Agreement.CodeOfConduct.Acknowledged')
Begin
	INSERT INTO [dbo].[Tbl_LocaleStringResource] ([LanguageID], [ResourceName], [ResourceValue], [OldResourceName])
	VALUES (2,'_Agreement.CodeOfConduct.Acknowledged', 'ยอมรับ', NULL);
End
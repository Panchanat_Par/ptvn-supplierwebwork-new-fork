﻿Update [dbo].[Tbl_MessageTemplate] set Body = N'


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<html>
<head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <title>Untitled Document</title>
    <link href="http://supplierwebwork.com/Content/images/EmailTemplateContent/text.css" rel="stylesheet" type="text/css" />


</head>
<body style="margin-left: 0px;margin-top: 0px;margin-right: 0px;margin-bottom: 0px;background-color: #eeeeee;background-position:center;background-repeat:repeat-y;">
    <table border="0" cellspacing="0" cellpadding="0" align="center">
        <tbody>
            <tr>
                <th bgcolor="#ffffff" valign="top" scope="col">
                    <img alt="" src="http://supplierwebwork.com/Content/images/EmailTemplateContent/h_mail2.jpg"
                         width="850">
                    <table border="0" cellspacing="0" cellpadding="0" width="100%">
                        <tbody>
                            <tr>
                                <td>
                                    <div style="width: 650px; padding-bottom: 20px; padding-left: 80px;font-family: Tahoma, Geneva, sans-serif; font-size: 10pt; font-weight: normal;" align="left">
                                        <p><font style="color: rgb(153, 0, 0); font-family: Tahoma, Geneva, sans-serif; font-size: 8pt;">* อีเมลฉบับนี้เป็นอีเมลจากระบบอัตโนมัติ กรุณาไม่ตอบกลับที่อีเมลนี้ *</font></p>
                                        <strong>เรียน <%ContactName_Local%></strong> <br /><br />

                                        ขอบคุณสำหรับการลงทะเบียนกับพันธวณิช <br />
                                        ขณะนี้ระบบได้ทำการบันทึกข้อมูลการลงทะเบียนของท่านแล้ว<br />
                                        ข้อมูลการลงทะเบียนของท่าน อยู่ระหว่างขั้นตอนการตรวจสอบความถูกต้องจากพันธวณิช<br />
                                        ในระหว่างนี้ ท่านสามารถนำหมายเลขลงทะเบียน “<strong><%TicketCode%></strong>” มาใช้เพื่อแก้ไขข้อมูลเพิ่มเติมได้ที่ <br />



                                        <a href="">www.supplierwebwork.com/</a><br /><br />


                                        ขอแสดงความนับถือ <br />
                                        <font style="color: rgb(0, 51, 102); font-family: Tahoma, Geneva, sans-serif; font-size: 11pt; font-weight: bold;">บริษัท พันธวณิช จำกัด</font>

                                        <hr />

                                        <p><font style="color: rgb(153, 0, 0); font-family: Tahoma, Geneva, sans-serif; font-size: 8pt;">* This is an automatically generated email, please do not reply to this address* </font></p>

                                        <strong>Dear <%ContactName_Inter%></strong> <br /><br />

                                        Thank you for your registration. Your information has been saved. It is now under verification process by
                                        Pantavanij. In the mean time, you can use registration number “<strong><%TicketCode%></strong>” to make any changes, if
                                        required, on our <a href="">www.supplierwebwork.com/</a>	<br /><br />



                                        Sincerely yours
                                        <br />
                                        <font style="color: rgb(0, 51, 102); font-family: Tahoma, Geneva, sans-serif; font-size: 11pt; font-weight: bold;">Pantavanij Co., Ltd.</font>

                                        <br />
                                        <p><!--<img src="images/bt01.jpg" />--><a class=</p>




                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div style="width: 780px; padding-top: 5px; padding-bottom: 5px; padding-left: 70px; background-color: rgb(140, 198, 63);font-family: Tahoma, Geneva, sans-serif; font-size: 10pt; font-weight: normal;" align="left">
                        <table border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td>
                                        <img src="http://supplierwebwork.com/Content/images/EmailTemplateContent/spacer.gif" width="10"
                                             height="1">
                                    </td>
                                    <td valign="top">
                                        <strong>Pantavanij Co., Ltd.</strong><br>
                                        31st Floor Lumpini Tower, 1168/94 Rama IV Road,<br> Tungmahamek,
                                        Sathorn, Bangkok 10120
                                    </td>
                                    <td>
                                        <img src="http://supplierwebwork.com/Content/images/EmailTemplateContent/spacer.gif" width="30"
                                             height="1">
                                    </td>
                                    <td class="dot">
                                        <img src="http://supplierwebwork.com/Content/images/EmailTemplateContent/spacer.gif"
                                             width="10" height="1">
                                    </td>
                                    <td valign="top">
                                        <strong>CUSTOMER CARE:</strong><br>Tel: 0-2034-4333
                                        Fax: 0-2679-7474 <br>Email: <a href="mailto:customercare@pantavanij.com">customercare@pantavanij.com</a><br>
                                        Opening of officials Monday - Friday 8:30 am. - 18:00 pm.
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </th>
            </tr>
            <tr></tr>
        </tbody>
    </table>
</body>
</html>' Where MessageId = 3
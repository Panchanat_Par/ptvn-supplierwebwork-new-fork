If Not Exists (Select 'x' From [dbo].[Tbl_MenuPortal] Where [MenuName] = 'm_Sourcing_AVLRequest')
Begin
	INSERT INTO [dbo].[Tbl_MenuPortal]
           ([MenuID]
           ,[MenuName]
           ,[ResourceName]
           ,[MenuLevel]
           ,[Section]
           ,[SeqNo]
           ,[SystemGrpID]
           ,[SystemPageID]
           ,[ControllerName]
           ,[ActionName]
           ,[SystemURL]
           ,[PrivilegeTypeID]
           ,[NotificationID]
           ,[Icon]
           ,[IsActive]
           ,[ParentMenuID])
     VALUES
           (48
           ,'m_Sourcing_AVLRequest'
           ,'_MenuSub.Sourcing.AVLRequest'
           ,2
           ,'Sourcing'
           ,0
           ,0
           ,0
           ,'Redirect'
           ,'RedirectAVL'
           ,''
           ,0
           ,0
           ,''
           ,1
           ,0)
End



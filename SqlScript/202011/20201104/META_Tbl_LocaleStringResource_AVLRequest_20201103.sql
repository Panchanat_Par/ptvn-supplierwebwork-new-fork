﻿If Not Exists (Select 'x' From [dbo].[Tbl_LocaleStringResource] Where LanguageID = 1 And ResourceName = '_MenuSub.Sourcing.AVLRequest')
Begin
	INSERT INTO [dbo].[Tbl_LocaleStringResource] ([LanguageID], [ResourceName], [ResourceValue], [OldResourceName])
	VALUES (1,'_MenuSub.Sourcing.AVLRequest', 'AVL Request', NULL);
End

If Not Exists (Select 'x' From [dbo].[Tbl_LocaleStringResource] Where LanguageID = 2 And ResourceName = '_MenuSub.Sourcing.AVLRequest')
Begin
	INSERT INTO [dbo].[Tbl_LocaleStringResource] ([LanguageID], [ResourceName], [ResourceValue], [OldResourceName])
	VALUES (2,'_MenuSub.Sourcing.AVLRequest', 'คำขอขึ้นทะเบียนผู้ค้า', NULL);
End
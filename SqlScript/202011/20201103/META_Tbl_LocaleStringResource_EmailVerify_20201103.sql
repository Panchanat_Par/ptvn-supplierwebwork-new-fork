﻿If Not Exists (Select 'x' From [dbo].[Tbl_LocaleStringResource] Where LanguageID = 1 And ResourceName = '_ContactPersion.EmailVerify.DuplicateEmail')
Begin
	INSERT INTO [dbo].[Tbl_LocaleStringResource] ([LanguageID], [ResourceName], [ResourceValue], [OldResourceName])
	VALUES (1,'_ContactPersion.EmailVerify.DuplicateEmail', 'This email address already exist.', NULL);
End

If Not Exists (Select 'x' From [dbo].[Tbl_LocaleStringResource] Where LanguageID = 2 And ResourceName = '_ContactPersion.EmailVerify.DuplicateEmail')
Begin
	INSERT INTO [dbo].[Tbl_LocaleStringResource] ([LanguageID], [ResourceName], [ResourceValue], [OldResourceName])
	VALUES (2,'_ContactPersion.EmailVerify.DuplicateEmail', 'มีอีเมลนี้อยู่ในระบบแล้ว', NULL);
End

If Not Exists (Select 'x' From [dbo].[Tbl_LocaleStringResource] Where LanguageID = 1 And ResourceName = '_ContactPersion.EmailVerify.InvalidEmail')
Begin
	INSERT INTO [dbo].[Tbl_LocaleStringResource] ([LanguageID], [ResourceName], [ResourceValue], [OldResourceName])
	VALUES (1,'_ContactPersion.EmailVerify.InvalidEmail', 'Invalid Email format.', NULL);
End

If Not Exists (Select 'x' From [dbo].[Tbl_LocaleStringResource] Where LanguageID = 2 And ResourceName = '_ContactPersion.EmailVerify.InvalidEmail')
Begin
	INSERT INTO [dbo].[Tbl_LocaleStringResource] ([LanguageID], [ResourceName], [ResourceValue], [OldResourceName])
	VALUES (2,'_ContactPersion.EmailVerify.InvalidEmail', 'รูปแบบอีเมลไม่ถูกต้อง', NULL);
End

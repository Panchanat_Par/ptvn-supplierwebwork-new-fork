﻿using SupplierPortal.Core.Extension;
using SupplierPortal.Data.CustomModels.AccountPortal;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.APILogs;
using SupplierPortal.Data.Models.Repository.AppConfig;
using SupplierPortal.Data.Models.Repository.Language;
using SupplierPortal.Data.Models.Repository.LogonAttempt;
using SupplierPortal.Data.Models.Repository.User;
using SupplierPortal.DataAccess.BuyerPortal;
using SupplierPortal.Framework.AccountManage;
using SupplierPortal.Framework.Localization;
using SupplierPortal.Framework.MethodHelper;
using SupplierPortal.MainFunction.Helper;
using SupplierPortal.Services.AccountManage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Configuration;

namespace SupplierPortal.BusinessLogic.BuyerPortal
{
    public partial class BuyerPortalManager : IBuyerPortalManager
    {
        protected readonly IAPILogsRepository _aPILogsRepository;
        protected readonly IUserRepository _userRepository;
        protected readonly ILogonAttemptRepository _logonAttemptRepository;
        protected readonly IAppConfigRepository _appConfigRepository;
        protected readonly IBuyerPortalRepository _buyerPortalRepository;

        public BuyerPortalManager()
        {
            _aPILogsRepository = new APILogsRepository();
            _userRepository = new UserRepository();
            _logonAttemptRepository = new LogonAttemptRepository();
            _appConfigRepository = new AppConfigRepository();
            _buyerPortalRepository = new BuyerPortalRepository();
        }

        public HttpResponseMessage LoginValidate(string publicKey, string authInfo, string encodeUserpassword, string strLanguageId = "", string browserType = "", string clientIPAddress = "")
        {
            if (string.IsNullOrEmpty(strLanguageId))
            {
                strLanguageId = CultureHelper.GetImplementedCulture(strLanguageId); // This is safe
            }

            string messageReturn = string.Empty;
            string userName = string.Empty;
            string signature = string.Empty;
            string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();

            try
            {
                // getData from Header
                string secretKey = WebConfigurationManager.AppSettings["secretKey"];
                string ultraSecret = WebConfigurationManager.AppSettings["ultraSecret"];
                string DecodeauthInfo = CryptoExtension.Base64Decode(authInfo);
                string DecodeUserpassword = CryptoExtension.Base64Decode(encodeUserpassword);
                string sigN = CryptoExtension.Signature(publicKey, secretKey);

                //split authInfo
                var splitAuthInfo = DecodeauthInfo.Split(':');
                if (splitAuthInfo.Length >= 2)
                {
                    userName = splitAuthInfo[0];
                    signature = splitAuthInfo[1];
                }

                if (sigN != signature)
                {
                    HttpResponseMessage respError = new HttpResponseMessage();
                    respError.ReasonPhrase = "Wrong Signature Partner";
                    respError.StatusCode = HttpStatusCode.NotFound;
                    var result = new List<Object>();

                    #region Insert Log
                    try
                    {
                        string jsonData = JsonHelper.CompleteJsonString(result, respError);

                        _aPILogsRepository.InsertLogByParam("",
                            "Response",
                            visitorsIPAddr,
                            "",
                            "LoginValidate",
                            ((Int32?)respError.StatusCode).ToString(),
                            respError.IsSuccessStatusCode ? "true" : "false",
                            respError.ReasonPhrase, jsonData, "");
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                    #endregion

                    #region Insert LogonAttempt
                    var logonAttemptModel = new Tbl_LogonAttempt()
                    {
                        Username = "Empty",
                        IPAddress = clientIPAddress,
                        BrowserType = browserType,
                        LoginTime = DateTime.UtcNow,
                        isLoginStatus = "Fail",
                        isActive = 1,
                        Remark = "Wrong Signature Partner",
                    };
                    _logonAttemptRepository.Insert(logonAttemptModel);
                    #endregion

                    return JsonHelper.CompleteJson(result, respError);
                }

                //split Userpassword
                var splitUserpassword = DecodeUserpassword.Split(':');

                string strUserName = string.Empty;

                string password = string.Empty;

                if (splitUserpassword.Length >= 2)
                {
                    strUserName = splitUserpassword[0];
                    password = splitUserpassword[1];
                }

                #region Declare Config Logon
                string portal_LogonAttemptAllow = _appConfigRepository.GetValueAppConfigByName("Portal_LogonAttemptAllow");
                string portal_LogonPeriodCheck = _appConfigRepository.GetValueAppConfigByName("Portal_LogonPeriodCheck");
                string portal_LogonLockTime = _appConfigRepository.GetValueAppConfigByName("Portal_LogonLockTime");

                int logonAttemptAllow = 0;
                int logonPeriodCheck = 0;
                int logonLockTime = 0;

                Int32.TryParse(portal_LogonAttemptAllow, out logonAttemptAllow);
                Int32.TryParse(portal_LogonPeriodCheck, out logonPeriodCheck);
                Int32.TryParse(portal_LogonLockTime, out logonLockTime);
                #endregion

                #region Check Locked
                bool isLocked = _logonAttemptRepository.CheckLockedLogonAttempt(strUserName, logonLockTime);
                if (isLocked)
                {
                    #region Insert LogonAttempt
                    var logonAttemptModel = new Tbl_LogonAttempt()
                    {
                        Username = strUserName,
                        IPAddress = clientIPAddress,
                        BrowserType = browserType,
                        LoginTime = DateTime.UtcNow,
                        isLoginStatus = "Locked",
                        isActive = 0,
                        Remark = "In time locked",
                    };
                    _logonAttemptRepository.Insert(logonAttemptModel);
                    #endregion

                    #region Return ResponseMessage By Locked
                    var lockedLogonAttemptModel = _logonAttemptRepository.GetLockedLogonAttempt(strUserName, logonLockTime);

                    string messageReturnLocked = messageReturn.GetStringResource("_Login.ValidateMsg.UsernameLock", strLanguageId);

                    string lockTimeSeconds = string.Empty;
                    if (lockedLogonAttemptModel != null)
                    {
                        DateTime dateNow = DateTime.UtcNow;
                        DateTime loginTime = lockedLogonAttemptModel.LoginTime ?? DateTime.UtcNow;
                        DateTime lockedTime = loginTime.AddSeconds(logonLockTime);
                        var seconds = lockedTime.Subtract(dateNow).TotalSeconds;
                        int totalSeconds = Convert.ToInt32(seconds);
                        lockTimeSeconds = totalSeconds.ToString();

                        messageReturnLocked = string.Format(messageReturnLocked, logonAttemptAllow, "<span id='countDownTime'></span>");
                    }
                    //ส่งค่า lockTimeSeconds ไปในตัวแปร isAcceptTermOfUse เพื่อนำไปทำ Countdown Timer
                    HttpResponseMessage respErrorLocked = new HttpResponseMessage();
                    respErrorLocked.ReasonPhrase = "Login Locked";
                    respErrorLocked.StatusCode = HttpStatusCode.OK;

                    var resultLocked = new List<Object>();
                    resultLocked.Add(new ResultValidated()
                    {
                        Username = strUserName,
                        SysUserID = string.Empty,
                        GUID = "Locked",
                        Result = false,
                        Message = messageReturnLocked,
                        User = string.Empty,
                        Organization = string.Empty,
                        LanguageId = strLanguageId,
                        isAcceptTermOfUse = lockTimeSeconds
                    });
                    #region Insert Log
                    try
                    {
                        string jsonData = JsonHelper.CompleteJsonString(resultLocked, respErrorLocked);

                        _aPILogsRepository.InsertLogByParam("",
                            "Response",
                            visitorsIPAddr,
                            "",
                            "LoginValidate",
                            ((Int32?)respErrorLocked.StatusCode).ToString(),
                            respErrorLocked.IsSuccessStatusCode ? "true" : "false",
                            respErrorLocked.ReasonPhrase, jsonData, "");
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                    #endregion

                    return JsonHelper.CompleteJson(resultLocked, respErrorLocked);
                    #endregion
                }

                #endregion

                var userInfo = _buyerPortalRepository.GetBuyerUserInformationByUserName(strUserName).FirstOrDefault();
                string returnMessage = string.Empty;

                if (this.CheckLogin(strUserName, password, publicKey, userInfo, out returnMessage))
                {
                    int intLanguageId = Convert.ToInt32(strLanguageId);
                    string genGUID = GeneratorGuid.GetRandomGuid();

                    string userShow = this.GetUserShowByUsername(userInfo, intLanguageId);
                    string organizationName = userInfo.Tbl_BuyerContactPerson.Department;
                    GeneratorGuid.InsertGuid(genGUID, userInfo.Login);

                    HttpResponseMessage respOK = new HttpResponseMessage();
                    respOK.ReasonPhrase = "Success";
                    respOK.StatusCode = HttpStatusCode.OK;

                    var resultList = new List<ResultValidated>();
                    resultList.Add(new ResultValidated()
                    {
                        Username = userInfo.Login,
                        SysUserID = userInfo.SysUserID.ToString(),
                        GUID = genGUID,
                        Result = true,
                        EID = userInfo.EID.ToString(),
                        Message = "Success",
                        User = userShow,
                        Organization = organizationName,
                        LanguageId = strLanguageId,
                        isAcceptTermOfUse = (userInfo.isAcceptTermOfUse ?? 0).ToString()
                    });

                    #region Insert Log
                    try
                    {
                        string jsonData = JsonHelper.CompleteJsonString(resultList, respOK);

                        _aPILogsRepository.InsertLogByParam("",
                            "Response",
                            visitorsIPAddr,
                            "",
                            "LoginValidate",
                            ((Int32?)respOK.StatusCode).ToString(),
                            respOK.IsSuccessStatusCode ? "true" : "false",
                            respOK.ReasonPhrase, jsonData, genGUID);
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                    #endregion

                    #region Insert LogonAttempt
                    var logonAttemptModel = new Tbl_LogonAttempt()
                    {
                        Username = strUserName,
                        IPAddress = clientIPAddress,
                        BrowserType = browserType,
                        LoginTime = DateTime.UtcNow,
                        isLoginStatus = "Success",
                        isActive = 1,
                        Remark = string.Empty,
                    };
                    _logonAttemptRepository.Insert(logonAttemptModel);

                    DateTime dateNow = DateTime.UtcNow;
                    DateTime datePeriodCheck = dateNow.AddSeconds(-logonPeriodCheck);

                    var logonAttemptList = _logonAttemptRepository.GetLogonAttemptByUsernameAndPeriodTime(strUserName, datePeriodCheck);

                    foreach (var item in logonAttemptList)
                    {
                        item.isActive = 0;
                        item.Remark = item.Remark + ":Reset by Success";

                        _logonAttemptRepository.Update(item);
                    }

                    #endregion

                    return JsonHelper.CompleteJson(resultList, respOK);
                }
                else
                {

                    HttpResponseMessage respNotFound = new HttpResponseMessage();
                    respNotFound.ReasonPhrase = returnMessage;
                    respNotFound.StatusCode = HttpStatusCode.NotFound;

                    var result1 = new List<Object>();

                    #region Insert Log
                    string jsonData = JsonHelper.CompleteJsonString(result1, respNotFound);

                    _aPILogsRepository.InsertLogByParam("",
                        "Response",
                        visitorsIPAddr,
                        "",
                        "LoginValidate",
                        ((Int32?)respNotFound.StatusCode).ToString(),
                        respNotFound.IsSuccessStatusCode ? "true" : "false",
                        respNotFound.ReasonPhrase, jsonData, "");

                    #endregion

                    #region Insert LogonAttempt
                    bool isLockedByFail = false;
                    CheckLogonAttemptFail(strUserName, clientIPAddress, browserType, returnMessage, out isLockedByFail);
                    if (isLockedByFail)
                    {
                        #region Return ResponseMessage By Locked
                        var lockedLogonAttemptModel = _logonAttemptRepository.GetLockedLogonAttempt(strUserName, logonLockTime);

                        string messageReturnLocked = messageReturn.GetStringResource("_Login.ValidateMsg.UsernameLock", strLanguageId);

                        string lockTimeSeconds = string.Empty;
                        if (lockedLogonAttemptModel != null)
                        {
                            DateTime dateNow = DateTime.UtcNow;
                            DateTime loginTime = lockedLogonAttemptModel.LoginTime ?? DateTime.UtcNow;
                            DateTime lockedTime = loginTime.AddSeconds(logonLockTime);
                            var seconds = lockedTime.Subtract(dateNow).TotalSeconds;
                            int totalSeconds = Convert.ToInt32(seconds);
                            lockTimeSeconds = totalSeconds.ToString();

                            messageReturnLocked = string.Format(messageReturnLocked, logonAttemptAllow, "<span id='countDownTime'></span>");
                        }
                        //ส่งค่า lockTimeSeconds ไปในตัวแปร isAcceptTermOfUse เพื่อนำไปทำ Countdown Timer
                        HttpResponseMessage respErrorLocked = new HttpResponseMessage();
                        respErrorLocked.ReasonPhrase = "Login Locked";
                        respErrorLocked.StatusCode = HttpStatusCode.OK;

                        var resultLocked = new List<Object>();
                        resultLocked.Add(new ResultValidated()
                        {
                            Username = strUserName,
                            SysUserID = string.Empty,
                            GUID = "Locked",
                            Result = false,
                            Message = messageReturnLocked,
                            User = string.Empty,
                            Organization = string.Empty,
                            LanguageId = strLanguageId,
                            isAcceptTermOfUse = lockTimeSeconds
                        });
                        #region Insert Log
                        try
                        {
                            string jsonDataLocked = JsonHelper.CompleteJsonString(resultLocked, respErrorLocked);

                            _aPILogsRepository.InsertLogByParam("",
                                "Response",
                                visitorsIPAddr,
                                "",
                                "LoginValidate",
                                ((Int32?)respErrorLocked.StatusCode).ToString(),
                                respErrorLocked.IsSuccessStatusCode ? "true" : "false",
                                respErrorLocked.ReasonPhrase, jsonDataLocked, "");
                        }
                        catch (Exception ex)
                        {
                            throw;
                        }
                        #endregion

                        return JsonHelper.CompleteJson(resultLocked, respErrorLocked);
                        #endregion
                    }
                    #endregion

                    return JsonHelper.CompleteJson(result1, respNotFound);

                }

            }
            catch (Exception ex)
            {
                HttpResponseMessage respExcep = new HttpResponseMessage();
                respExcep.ReasonPhrase = ex.Message;
                respExcep.StatusCode = HttpStatusCode.BadRequest;
                var resultExcep = new List<Object>();

                #region Insert Log
                string jsonData = JsonHelper.CompleteJsonString(resultExcep, respExcep);

                _aPILogsRepository.InsertLogByParam("",
                    "Response",
                    visitorsIPAddr,
                    "",
                    "LoginValidate",
                    ((Int32?)respExcep.StatusCode).ToString(),
                    respExcep.IsSuccessStatusCode ? "true" : "false",
                    respExcep.ReasonPhrase, jsonData, "");

                #endregion

                return JsonHelper.CompleteJson(resultExcep, respExcep);
            }
        }

        private void CheckLogonAttemptFail(string strUserName, string clientIPAddress, string browserType, string remark, out bool isLocked)
        {
            #region Declare Config Logon
            string portal_LogonAttemptAllow = _appConfigRepository.GetValueAppConfigByName("Portal_LogonAttemptAllow");
            string portal_LogonPeriodCheck = _appConfigRepository.GetValueAppConfigByName("Portal_LogonPeriodCheck");

            int logonAttemptAllow = 0;
            int logonPeriodCheck = 0;

            Int32.TryParse(portal_LogonAttemptAllow, out logonAttemptAllow);
            Int32.TryParse(portal_LogonPeriodCheck, out logonPeriodCheck);
            #endregion

            isLocked = false;

            DateTime dateNow = DateTime.UtcNow;
            DateTime datePeriodCheck = dateNow.AddSeconds(-logonPeriodCheck);

            var logonAttemptList = _logonAttemptRepository.GetLogonAttemptByUsernameAndPeriodTime(strUserName, datePeriodCheck);

            if (logonAttemptList.Count() >= logonAttemptAllow - 1)
            {
                var logonAttemptModel = new Tbl_LogonAttempt()
                {
                    Username = strUserName,
                    IPAddress = clientIPAddress,
                    BrowserType = browserType,
                    LoginTime = DateTime.UtcNow,
                    isLoginStatus = "Locked",
                    isActive = 1,
                    Remark = remark,
                };
                _logonAttemptRepository.Insert(logonAttemptModel);

                foreach (var item in logonAttemptList)
                {
                    item.isActive = 0;
                    item.Remark = item.Remark + ":Reset by locked";

                    _logonAttemptRepository.Update(item);
                }

                isLocked = true;
            }
            else
            {
                var logonAttemptModel = new Tbl_LogonAttempt()
                {
                    Username = strUserName,
                    IPAddress = clientIPAddress,
                    BrowserType = browserType,
                    LoginTime = DateTime.UtcNow,
                    isLoginStatus = "Fail",
                    isActive = 1,
                    Remark = remark,
                };
                _logonAttemptRepository.Insert(logonAttemptModel);
            }
        }

        #region Check Username Password
        public Boolean CheckLogin(string usernameLogin, string passwordLogin, string publicKey, Tbl_BuyerUser buyerUser, out string returnMessage)
        {
            bool result = false;
            returnMessage = string.Empty;
            if (buyerUser != null)
            {
                string password = CryptoExtension.Base64Decode(passwordLogin);

                //Split SecretKey
                var passwordDecode = password.Trim().Replace(publicKey, "");

                //Decode Password
                var strPassword = CryptoExtension.Base64Decode(passwordDecode);

                string fullPwdStr = strPassword + buyerUser.Salt;
                byte[] passwordHash = ClsHashManagement.ComputeHash(fullPwdStr);
                string inPassword = Convert.ToBase64String(passwordHash);

                if (inPassword == buyerUser.Password)
                {
                    result = true;
                }
                else
                {
                    returnMessage = "Wrong password in Username : " + usernameLogin;
                    result = false;
                }
            }
            else
            {
                returnMessage = "Username : " + usernameLogin + " Not Found";
                result = false;
            }
            return result;
        }
        #endregion

        #region Check Username Password
        public string GetUserShowByUsername(Tbl_BuyerUser buyerUser, int languageId)
        {
            StringBuilder builder = new StringBuilder();
            string userShow = string.Empty;
            if (buyerUser.Tbl_BuyerContactPerson != null)
            {
                if (languageId == 1)
                {
                    if (!string.IsNullOrEmpty(buyerUser.Tbl_BuyerContactPerson.FirstName_Inter))
                    {
                        builder.Append(buyerUser.Tbl_BuyerContactPerson.FirstName_Inter + " ");
                    }

                    if (!string.IsNullOrEmpty(buyerUser.Tbl_BuyerContactPerson.LastName_Inter))
                    {
                        builder.Append(buyerUser.Tbl_BuyerContactPerson.LastName_Inter);
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(buyerUser.Tbl_BuyerContactPerson.FirstName_Local))
                    {
                        builder.Append(buyerUser.Tbl_BuyerContactPerson.FirstName_Local + " ");
                    }

                    if (!string.IsNullOrEmpty(buyerUser.Tbl_BuyerContactPerson.LastName_Local))
                    {
                        builder.Append(buyerUser.Tbl_BuyerContactPerson.LastName_Local);
                    }
                }

                userShow = builder.ToString();
                return userShow;
            }

            return userShow;
        }
        #endregion
    }
}

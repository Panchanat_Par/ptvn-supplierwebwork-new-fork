﻿using SupplierPortal.BusinessLogic.ExternalService;
using SupplierPortal.BusinessLogic.ExternalService.Factory;
using SupplierPortal.Data.CustomModels.Consent;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.User;
using SupplierPortal.Data.Models.SupportModel.Consent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace SupplierPortal.BusinessLogic.TermAndCondition
{
    public partial class TermAndConditionManager : ITermAndConditionManager
    {
        private readonly IConsentServiceManager _consentServiceManager;
        private readonly IUserRepository _userRepository;
        public string termApplicationName;

        public TermAndConditionManager()
        {
            _userRepository = new UserRepository();
            _consentServiceManager = new PTVNConsentServiceManager();
            this.termApplicationName = WebConfigurationManager.AppSettings["ptvn_term_application_name"];
        }

        public async Task<ConsentRequestAcceptModel> GetTermAndCondition(string username)
        {
            ConsentRequestAcceptModel response = null;

            try
            {
                Tbl_User user = _userRepository.FindByUsername(username);
                if(user == null)
                {
                    return null;
                }

                username = username.ToUpper();
                ConsentRequestAcceptModel result = await _consentServiceManager.GetRequestAcceptWithApplicationName(username, this.termApplicationName);
                if (result != null && result.payload != null && result.payload.Count > 0)
                {
                    response = result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return response;
        }

        public async Task<ConsentAcceptModel> AcceptTermAndCondition(string username, ConsentAcceptRequest request)
        {
            ConsentAcceptModel response = null;

            try
            {
                Tbl_User user = _userRepository.FindByUsername(username);
                if (user == null)
                {
                    return null;
                }

                request.dataSubject = request.dataSubject.ToUpper();
                ConsentAcceptModel result = await _consentServiceManager.AcceptWithApplicationName(request, this.termApplicationName);
                if (result != null && result.payload != null && result.result.FirstOrDefault().message == "Succeed")
                {
                    response = result;
                    user.isAcceptTermOfUse = 1;
                    _userRepository.Update(user);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return response;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using SupplierPortal.Data.CustomModels.Organization;
using SupplierPortal.Data.CustomModels.SearchResult;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.DataAccess.BusinessType;
using SupplierPortal.DataAccess.Country;
using SupplierPortal.DataAccess.Organization;
using SupplierPortal.MainFunction.Helper;
using SupplierPortal.Models.Dto;

namespace SupplierPortal.BusinessLogic.Organization
{
    public class OrganizationManager : IOrganizationManager
    {
        public readonly IOrganizationRepository _organizationRepository;
        public readonly IBusinessTypeRepository _businessTypeRepository;
        public OrganizationManager()
        {
            _organizationRepository = new OrganizationRepository();
            _businessTypeRepository = new BusinessTypeRepository();
        }

        public OrganizationShortDetailDto GetOrganizationShortDetail(string taxid, string branchNo)
        {
            OrganizationShortDetailDto organizationDto = null;
            try
            {
                Tbl_Organization organization = _organizationRepository.GetOrganization(taxid, branchNo);
                if (organization != null)
                {
                    organizationDto = new OrganizationShortDetailDto();
                    organizationDto.taxId = organization.TaxID;
                    organizationDto.branchNo = organization.BranchNo;
                    organizationDto.companyActive = organization.isDeleted == 0 ? false : true;
                    organizationDto.registeredCapital = organization.RegisteredCapital;
                    organizationDto.currencyCode = organization.CurrencyCode;
                    organizationDto.yearEstablished = organization.YearEstablished;

                    // Set Organization Address
                    Tbl_OrgAddress orgAddress = organization.Tbl_OrgAddress.Where(i => i.AddressTypeID == 1).FirstOrDefault();

                    if (orgAddress != null)
                    {
                        if (orgAddress.Tbl_Address != null)
                        {
                            Tbl_Address address = orgAddress.Tbl_Address;
                            organizationDto.address = "ที่อยู่ " + (string.IsNullOrEmpty(address.HouseNo_Local.Trim()) ? "-" : address.HouseNo_Local.Trim())
                                                    + " หมู่ที่ " + (string.IsNullOrEmpty(address.VillageNo_Local.Trim()) ? "-" : address.VillageNo_Local.Trim())
                                                    + " ซอย " + (string.IsNullOrEmpty(address.Lane_Local.Trim()) ? "-" : address.Lane_Local.Trim())
                                                    + " ถนน " + (string.IsNullOrEmpty(address.Road_Local.Trim()) ? "-" : address.Road_Local.Trim())
                                                    + " ตำบล " + (string.IsNullOrEmpty(address.SubDistrict_Local.Trim()) ? "-" : address.SubDistrict_Local.Trim())
                                                    + " อำเภอ " + (string.IsNullOrEmpty(address.City_Local.Trim()) ? "-" : address.City_Local.Trim());
                            organizationDto.state = address.State_Local;
                            organizationDto.postcode = address.PostalCode;
                            organizationDto.country = address.CountryCode;
                        }
                    }

                    //Set BusinessType
                    List<Tbl_OrgBusinessType> orgBusinessTypes = organization.Tbl_OrgBusinessType == null ? null : organization.Tbl_OrgBusinessType.ToList();
                    if (orgBusinessTypes != null)
                    {
                        organizationDto.businessTypes = new List<BusinessTypeShortDetailDto>();

                        foreach (Tbl_OrgBusinessType item in orgBusinessTypes)
                        {
                            Tbl_BusinessType businessType = _businessTypeRepository.GetBusinessType(item.BusinessTypeID);
                            if (businessType != null)
                            {
                                organizationDto.businessTypes.Add(new BusinessTypeShortDetailDto
                                {
                                    BusinessTypeID = businessType.BusinessTypeID,
                                    businessTypeDesc = businessType.BusinessTypeDesc

                                });
                            }
                        }

                    }

                    // set Contact
                    Tbl_OrgContactPerson orgContactPerson = organization.Tbl_OrgContactPerson.Where(i => i.isPrimaryContact == 1).FirstOrDefault();
                    if (orgContactPerson != null)
                    {
                        if (orgContactPerson.Tbl_ContactPerson != null)
                        {
                            Tbl_ContactPerson contactPerson = orgContactPerson.Tbl_ContactPerson;
                            organizationDto.contactName = contactPerson.Tbl_NameTitle.TitleName + contactPerson.FirstName_Local.Trim() + " " + contactPerson.LastName_Local.Trim();
                            organizationDto.phone = contactPerson.PhoneNo + (string.IsNullOrEmpty(organization.PhoneExt.Trim()) ? "" : " " + organization.PhoneExt.Trim());
                            organizationDto.mobile = contactPerson.MobileNo;
                            organizationDto.department = contactPerson.Department;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return organizationDto;
        }

        public List<OrganizationShortDetailDto> GetOrganizationList(string taxId, string countryCode)
        {
            List<OrganizationShortDetailDto> organizationDto = new List<OrganizationShortDetailDto>();

            try
            {
                List<Tbl_Organization> organizationList = _organizationRepository.GetOrganizationByTaxIdAndCountryCode(taxId, countryCode);
                if (organizationList != null && organizationList.Count > 0)
                {
                    organizationList.ForEach(org =>
                    {
                        OrganizationShortDetailDto orgObj = new OrganizationShortDetailDto()
                        {
                            taxId = org.TaxID,
                            branchNo = org.BranchNo,
                            companyActive = org.isDeleted == 0 ? false : true,
                            registeredCapital = org.RegisteredCapital,
                            currencyCode = org.CurrencyCode,
                            yearEstablished = org.YearEstablished
                        };

                        Tbl_OrgAddress orgAddress = org.Tbl_OrgAddress.Where(i => i.AddressTypeID == 1).FirstOrDefault();
                        if (orgAddress != null)
                        {
                            if (orgAddress.Tbl_Address != null)
                            {
                                Tbl_Address address = orgAddress.Tbl_Address;
                                orgObj.address = "ที่อยู่ " + (string.IsNullOrEmpty(address.HouseNo_Local.Trim()) ? "-" : address.HouseNo_Local.Trim())
                                                        + " หมู่ที่ " + (string.IsNullOrEmpty(address.VillageNo_Local.Trim()) ? "-" : address.VillageNo_Local.Trim())
                                                        + " ซอย " + (string.IsNullOrEmpty(address.Lane_Local.Trim()) ? "-" : address.Lane_Local.Trim())
                                                        + " ถนน " + (string.IsNullOrEmpty(address.Road_Local.Trim()) ? "-" : address.Road_Local.Trim())
                                                        + " ตำบล " + (string.IsNullOrEmpty(address.SubDistrict_Local.Trim()) ? "-" : address.SubDistrict_Local.Trim())
                                                        + " อำเภอ " + (string.IsNullOrEmpty(address.City_Local.Trim()) ? "-" : address.City_Local.Trim());
                                orgObj.state = address.State_Local;
                                orgObj.postcode = address.PostalCode;
                                orgObj.country = address.CountryCode;
                            }
                        }

                        //Set BusinessType
                        List<Tbl_OrgBusinessType> orgBusinessTypes = org.Tbl_OrgBusinessType == null ? null : org.Tbl_OrgBusinessType.ToList();
                        if (orgBusinessTypes != null)
                        {
                            orgObj.businessTypes = new List<BusinessTypeShortDetailDto>();

                            foreach (Tbl_OrgBusinessType item in orgBusinessTypes)
                            {
                                Tbl_BusinessType businessType = _businessTypeRepository.GetBusinessType(item.BusinessTypeID);
                                if (businessType != null)
                                {
                                    orgObj.businessTypes.Add(new BusinessTypeShortDetailDto
                                    {
                                        BusinessTypeID = businessType.BusinessTypeID,
                                        businessTypeDesc = businessType.BusinessTypeDesc

                                    });
                                }
                            }

                        }

                        // set Contact
                        Tbl_OrgContactPerson orgContactPerson = org.Tbl_OrgContactPerson.Where(i => i.isPrimaryContact == 1).FirstOrDefault();
                        if (orgContactPerson != null)
                        {
                            if (orgContactPerson.Tbl_ContactPerson != null)
                            {
                                Tbl_ContactPerson contactPerson = orgContactPerson.Tbl_ContactPerson;
                                orgObj.contactName = contactPerson.Tbl_NameTitle.TitleName + contactPerson.FirstName_Local.Trim() + " " + contactPerson.LastName_Local.Trim();
                                orgObj.phone = contactPerson.PhoneNo + (string.IsNullOrEmpty(org.PhoneExt.Trim()) ? "" : " " + org.PhoneExt.Trim());
                                orgObj.mobile = contactPerson.MobileNo;
                                orgObj.department = contactPerson.Department;
                            }

                        }

                        organizationDto.Add(orgObj);
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return organizationDto;
        }

        public SearchResultResponse<List<SearchOrgResponse>> GetOrganizationWithPagination(SearchResultRequest request)
        {
            SearchResultResponse<List<SearchOrgResponse>> response;
            try
            {
                response = new SearchResultResponse<List<SearchOrgResponse>>();
                List<OrganizationWithPagination_Sel_Result> listOrganizeResult = new List<OrganizationWithPagination_Sel_Result>();

                string condition = SearchConditionHelper.CreateCondition(request.advanceSearch);
                string sorting = SortingHelper.CreateSorting(request.sortAscending, request.sortColumn);

                listOrganizeResult = _organizationRepository.GetOrganizationWithPagination(request.pageIndex, request.pageSize, condition, sorting);

                if (listOrganizeResult.Any())
                {
                    response.pageIndex = request.pageIndex;
                    response.pageSize = request.pageSize;
                    response.totalRecord = (int)listOrganizeResult.FirstOrDefault().TotalRecord;
                    response.totalMainRecord = listOrganizeResult.Count();
                    response.totalPage = (response.totalRecord / response.pageSize) + (response.totalRecord % response.pageSize > 0 ? 1 : 0);
                    response.dataResult = MappingSearchOrgResponse(listOrganizeResult);
                    response.totalItem = response.dataResult.Count();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return response;
        }

        public List<SearchOrgResponse> MappingSearchOrgResponse(List<OrganizationWithPagination_Sel_Result> data)
        {
            List<SearchOrgResponse> responses = new List<SearchOrgResponse>();

            foreach (OrganizationWithPagination_Sel_Result result in data)
            {
                SearchOrgResponse orgResponse = new SearchOrgResponse();
                SearchContactResponse contactResponse = new SearchContactResponse();

                var dataResult = responses.Find(f => f.SupplierID == result.SupplierID);

                contactResponse.ContactID = result.ContactID == null ? 0 : (int)result.ContactID;
                contactResponse.FirstName_Local = result.FirstName_Local;
                contactResponse.LastName_Local = result.LastName_Local;
                contactResponse.FirstName_Inter = result.FirstName_Inter;
                contactResponse.LastName_Inter = result.LastName_Inter;
                contactResponse.PhoneNo = result.PhoneNo;
                contactResponse.PhoneExt = result.PhoneExt;
                contactResponse.PhoneCountryCode = result.PhoneCountryCode;
                contactResponse.MobileNo = result.MobileNo;
                contactResponse.MobileCountryCode = result.MobileCountryCode;
                contactResponse.Email = result.Email;
                contactResponse.UserID = result.UserID == null ? 0 : (int)result.UserID;
                contactResponse.Username = result.Username;

                if (dataResult != null)
                {
                    dataResult.SearchContacts.Add(contactResponse);
                }
                else
                {
                    orgResponse.RowNumber = (int)result.RowNumber;
                    orgResponse.OrgID = result.OrgID;
                    orgResponse.SupplierID = (int)result.SupplierID;
                    orgResponse.CompanyName_Inter = result.CompanyName_Inter;
                    orgResponse.CompanyName_Local = result.CompanyName_Local;

                    orgResponse.SearchContacts = new List<SearchContactResponse>();
                    orgResponse.SearchContacts.Add(contactResponse);

                    responses.Add(orgResponse);
                }
            }
            return responses;
        }
    }
}

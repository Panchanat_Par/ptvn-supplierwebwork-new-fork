﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.BusinessLogic.PDPA
{
    public partial interface IUserPDPAManager
    {
        Users_PDPA GetUserPDPA(string username);
        int InsertUserPDPA(Users_PDPA users);
    }
}

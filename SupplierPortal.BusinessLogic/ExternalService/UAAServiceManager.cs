﻿using SupplierPortal.Core.Extension;
using SupplierPortal.Data.CustomModels.SCF;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.User;
using SupplierPortal.Data.Models.SupportModel.UAA;
using SupplierPortal.Models.Enums;
using SupplierPortal.Services.APIService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace SupplierPortal.DataAccess.User
{
    public class UAAServiceManager : APIConnectionService
    {
        private string uaaServiceAPI;
        private string uaaServiceGetTokenEndpoint;
        private string uaaGrantType;
        private string uaaUsername;
        private string uaaPassword;
        private string uaaServiceName;
        private string publicKey;

        private readonly IUserRepository _userRepository;

        public UAAServiceManager()
        {
            this.uaaServiceAPI = WebConfigurationManager.AppSettings["uaa_api"];
            this.uaaServiceGetTokenEndpoint = WebConfigurationManager.AppSettings["uaa_get_token_endpoint"];
            this.uaaGrantType = WebConfigurationManager.AppSettings["uaa_grant_type"];
            this.uaaUsername = WebConfigurationManager.AppSettings["uaa_username"];
            this.uaaPassword = WebConfigurationManager.AppSettings["uaa_password"];
            this.uaaServiceName = WebConfigurationManager.AppSettings["uaa_service_name"];
            this.publicKey = WebConfigurationManager.AppSettings["publicKey"];

            _userRepository = new UserRepository();
        }

        public async Task<UAAOAuthModel> GetAccessToken(UAAOAuthRequestModel request)
        {
            try
            {
                string serviceName = String.Empty;
                List<APIKey> headers = new List<APIKey>();
                headers.Add(new APIKey() { key = "Authorization", value = "Basic " + Convert.ToBase64String(Encoding.Default.GetBytes(this.uaaUsername + ":" + this.uaaPassword)) });
                headers.Add(new APIKey() { key = "Accept", value = "application/json" });

                if (request.grant_type.Equals(GrantTypeEnum.Password.GetAttributeTypeCode().ToString()))
                {
                    string password = this.DecodePassword(request.password);
                    if (String.IsNullOrWhiteSpace(password))
                    {
                        return new UAAOAuthModel();
                    }
                    request.password = password;
                    serviceName = this.uaaServiceName;
                }

                string baseURL = this.uaaServiceAPI + this.uaaServiceGetTokenEndpoint + serviceName;
                using (HttpResponseMessage httpResponse = await this.PostApiConnectionWithFormUrlEncoded(baseURL, "{0}", this.SetRequestGetAccessToken(request), headers))
                {
                    if (httpResponse.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        return this.streamReaderToObject<UAAOAuthModel>(httpResponse);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return new UAAOAuthModel();
        }


        private String DecodePassword(string password)
        {
            string result = string.Empty;
            try
            {
                string decodeFirst = CryptoExtension.Base64Decode(password);
                string pwd = decodeFirst.Split(':')[1];
                string decodeSecond = CryptoExtension.Base64Decode(pwd);
                string decodeThird = decodeSecond.Replace(this.publicKey, "");
                result = CryptoExtension.Base64Decode(decodeThird);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }
        private List<KeyValuePair<string, string>> SetRequestGetAccessToken(UAAOAuthRequestModel request)
        {
            return new List<KeyValuePair<string, string>>() {
                new KeyValuePair<string, string>("username", request.username),
                new KeyValuePair<string, string>("roleName", request.roleName),
                new KeyValuePair<string, string>("firstName", request.firstName),
                new KeyValuePair<string, string>("lastName", request.lastName),
                new KeyValuePair<string, string>("email", request.email),
                new KeyValuePair<string, string>("firstNameLocal", request.firstNameLocal),
                new KeyValuePair<string, string>("lastNameLocal", request.lastNameLocal),
                new KeyValuePair<string, string>("telephone", request.telephone),
                new KeyValuePair<string, string>("password", request.password),
                new KeyValuePair<string, string>("grant_type", request.grant_type)
            };
        }
    }
}

﻿using SupplierPortal.Data.CustomModels.RegisterSupplier;
using SupplierPortal.Data.CustomModels.SCF;
using SupplierPortal.Data.CustomModels.SendEmail;
using SupplierPortal.Data.Models.SupportModel.OPN;
using SupplierPortal.Models.Dto.SC;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SupplierPortal.BusinessLogic.ExternalService
{
    public interface IExternalServiceManager
    {
        Task<Boolean> OPNSupplierSerivce(SupplierOPNModel request);

        Task<List<ConnectJobResponse>> UpdateStatusSupplierConnect(List<ConnectJobRequest> request);

        Task<int> GetSupplierId(string orgId);

        Task<Boolean> CheckInvoice(string supplierMpid, string month, string buyerMpId);

        Task<Boolean> CheckGR(string supplierMpId, int supplierId, string supplierUsername, int month, bool isIncludeCurrentMonth);

        Task<VaultEncryptResponse> VaultEncryptSerivce(VaultEncryptRequest request);

        Task<VaultDecryptResponse> VaultDecryptSerivce(VaultDecryptRequest request);

        Task<bool> SCFSendEmail(SendEmailRequest request);

        Task<VerifyTicketCodeResponseDto> VerifyTicketCodeSupplierConnect(string ticketCode);

        Task<VerifyTicketCodeResponseDto> VerifyEmailSupplierConnect(string email);
    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SupplierPortal.Data.CustomModels.CompanyProfile;

namespace SupplierPortal.BusinessLogic.CompanyProfile
{
    public partial interface IImageCompanyManager
    {
        Task<List<ImageCompanyProfileModel>> GetImagePowerAndCompany(string taxId, string branchNumber);

        string GetSupplierIdByTaxId(string taxId, string branchNumber);
         
    }
}

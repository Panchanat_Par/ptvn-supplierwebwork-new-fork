﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.BusinessLogic.RegMigrateAttachmentToSC
{
    public interface IRegMigrateAttachmentToSCManager
    {
        Task<bool> MigrateAttachmentToSC();
    }
}

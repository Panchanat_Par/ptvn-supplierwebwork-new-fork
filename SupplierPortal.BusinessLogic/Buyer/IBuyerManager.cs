﻿using SupplierPortal.Data.CustomModels.Buyer;
using SupplierPortal.Data.CustomModels.BuyerSuppliers;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Models.Dto.DataContract;
using System;
using System.Collections.Generic;

namespace SupplierPortal.BusinessLogic.Buyer
{
    public interface IBuyerManager
    {
        #region Supplier USer
        #region INSERT
        int insertSupplierUser(SupplierUserModel buyerSupplier);
        #endregion

        #region GET
        bool resetPasswordOfSupplierUser(string visitorsIPAddr);
        #endregion

        #endregion

        #region BuyerSupplier

        #region INSERT
        int InsertBuyerUser(BuyerUserModel buyerUserModel);
        #endregion

        #region GET
        Tuple<List<Buyer_Suppliers_Result>, int, int> GetSuppliers(GetBuyerSuppliersRequest request);
        Tbl_Organization GetOrganization(int id);
        #endregion
        #endregion

        #region GetBuyerSupplierContact
        #region GET
        ViewBuyerSupplier GetBuyerSupplierContactDetail(int userId);
        #endregion
        #endregion

        #region UpdateSupplierContactPerson
        #region GET
        Boolean UpdateSupplierContactPerson(BuyerSuppliersModel buyerSupplier);
        #endregion
        #endregion

        #region GetListHistoryBuyerContactPersonByUserId
        #region GET
        List<BuyerHistoryContactPersonModel> GetListHistorySupplierContactPersonByUserId(int userId);
        #endregion
        #endregion

        #region GetBuyerUserWithConfigBySysUserID
        #region GET
        Tbl_BuyerConfig GetBuyerConfigByEID(int eid);
        #endregion
        #endregion

    }
}

﻿
using SupplierPortal.BusinessLogic.ExternalService;
using SupplierPortal.Data.Models.MappingTable;

using SupplierPortal.Data.Models.Repository.Address;
using SupplierPortal.Data.Models.Repository.ContactPerson;
using SupplierPortal.Data.Models.Repository.Country;
using SupplierPortal.Data.Models.Repository.Organization;
using SupplierPortal.Data.Models.Repository.OrgBusinessType;
using SupplierPortal.Data.Models.SupportModel.OPN;
using SupplierPortal.Data.Models.SupportModel.Profile;
using SupplierPortal.DataAccess.OPN.Buyer;

using SupplierPortal.DataAccess.OPN.BuyerSupplierMapping;
using SupplierPortal.DataAccess.OPN.SupplierMapping;
using SupplierPortal.DataAccess.RegApprove;
using SupplierPortal.Models.Dto;
using SupplierPortal.Models.Dto.OPN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.BusinessLogic.OPN.BuyerSupplierMapping
{
    public class OPNBuyerSupplierMappingManager : IOPNBuyerSupplierMappingManager
    {
        public readonly IOPNBuyerSupplierMappingRepository _oPNBuyerSupplierMappingRepository;
        public readonly IOPNSupplierMappingRepository _oPNSupplierMappingRepository;
        public readonly IOrganizationRepository _organizationRepository;
        public readonly IRegApproveRepository _regApproveRepository;

        public readonly IOrgBusinessTypeRepository _orgBusinessTypeRepository;
        public readonly IAddressRepository _addressRepository;
        public readonly IContactPersonRepository _contactPersonRepository;
        public readonly ICountryRepository _countryRepository;

        public readonly IExternalServiceManager _externalServiceManager;
        public readonly IOPNBuyerRepository _opnBuyer;


        public OPNBuyerSupplierMappingManager()
        {
            _oPNBuyerSupplierMappingRepository = new OPNBuyerSupplierMappingRepository();
            _oPNSupplierMappingRepository = new OPNSupplierMappingRepository();
            _organizationRepository = new OrganizationRepository();
            _regApproveRepository = new RegApproveRepository();
            _orgBusinessTypeRepository = new OrgBusinessTypeRepository();
            _addressRepository = new AddressRepository();
            _contactPersonRepository = new ContactPersonRepository();
            _externalServiceManager = new ExternalServiceManager();
            _countryRepository = new CountryRepository();
            _opnBuyer = new OPNBuyerRepository();
        }

        public async Task<bool> JobRunningUpdateBuyerSupplierMapping()
        {
            bool result = false;
            try
            {
                List<Tbl_OPNBuyerSupplierMapping> listBuyerSupplier = this._oPNBuyerSupplierMappingRepository.GetOPNBuyerSupplierMappingByIsJobSuccess(false);

                if (listBuyerSupplier.Any())
                {
                    foreach (Tbl_OPNBuyerSupplierMapping buyerSupplier in listBuyerSupplier)
                    {
                        Tbl_RegApprove approve = new Tbl_RegApprove();
                        approve = this._regApproveRepository.GetRegApproveByRegID((int)buyerSupplier.RegID);

                        if (approve != null && approve.SupplierID != null)
                        {
                            Tbl_OPNSupplierMapping supplierMapping = new Tbl_OPNSupplierMapping();
                            supplierMapping = this._oPNSupplierMappingRepository.GetOPNBuyerSupplierMappingByRegID((int)buyerSupplier.RegID);

                            if (supplierMapping == null)
                            {
                                buyerSupplier.SupplierID = null;
                                buyerSupplier.IsJobSuccess = false;
                                buyerSupplier.JobErrorMsg = "Error Tbl_OPNSupplierMapping Data not Found RegID: " + buyerSupplier.RegID;
                            }
                            else
                            {
                                supplierMapping.SupplierID = approve.SupplierID;

                                buyerSupplier.SupplierID = approve.SupplierID;
                                buyerSupplier.UpdatedDate = DateTime.Now;

                                result = await this.CallServiceOPNSupplier(supplierMapping);

                                if (result)
                                {
                                    buyerSupplier.IsJobSuccess = true;
                                    buyerSupplier.JobErrorMsg = "";
                                    result = this._oPNSupplierMappingRepository.UpdateOPNSupplierMapping(supplierMapping);
                                }
                                else
                                {
                                    buyerSupplier.SupplierID = null;
                                    buyerSupplier.IsJobSuccess = false;
                                    buyerSupplier.JobErrorMsg = "Error Call Service SupplierID: " + buyerSupplier.SupplierID + " RegID: " + buyerSupplier.RegID;
                                }
                            }

                            result = this._oPNBuyerSupplierMappingRepository.UpdateOPNBuyerSupplierMapping(buyerSupplier);
                        }
                    }
                }
                else
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public async Task<bool> CallServiceOPNSupplier(Tbl_OPNSupplierMapping supplierMapping)
        {
            bool result = false;
            try
            {
                int supplierID = (int)supplierMapping.SupplierID;
                Tbl_Organization organization = this._organizationRepository.GetDataBySupplierID(supplierID);

                List<Tbl_OrgBusinessType> listOrgBusinessType = this._orgBusinessTypeRepository.GetOrgBusinessTypeBySupplierID(supplierID).ToList();
                Tbl_Address address = this._addressRepository.GetAddressBySupplierID(supplierID);
                Tbl_Country country = this._countryRepository.GetCountryByCountryCode(organization.CountryCode);
                ContactPersonDataModels contactPerson = this._contactPersonRepository.JsonContactPersonJoinOrgContactPersonBySupplierID(supplierID, 1);

                bool isManufacturer = false;
                bool isDistributor = false;
                bool isDealer = false;
                bool isServiceProvider = false;

                if (listOrgBusinessType.Any())
                {
                    isManufacturer = listOrgBusinessType.Where(w => w.BusinessTypeID == 1).Any() ? true : false;
                    isDistributor = listOrgBusinessType.Where(w => w.BusinessTypeID == 2).Any() ? true : false; ;
                    isDealer = listOrgBusinessType.Where(w => w.BusinessTypeID == 3).Any() ? true : false; ;
                    isServiceProvider = listOrgBusinessType.Where(w => w.BusinessTypeID == 4).Any() ? true : false; ;
                }

                string companyAddress = (string.IsNullOrEmpty(address.HouseNo_Local.Trim()) ? "-" : address.HouseNo_Local.Trim())
                                                    + " หมู่ที่ " + (string.IsNullOrEmpty(address.VillageNo_Local.Trim()) ? "-" : address.VillageNo_Local.Trim())
                                                    + " ซอย " + (string.IsNullOrEmpty(address.Lane_Local.Trim()) ? "-" : address.Lane_Local.Trim())
                                                    + " ถนน " + (string.IsNullOrEmpty(address.Road_Local.Trim()) ? "-" : address.Road_Local.Trim())
                                                    + " ตำบล " + (string.IsNullOrEmpty(address.SubDistrict_Local.Trim()) ? "-" : address.SubDistrict_Local.Trim())
                                                    + " อำเภอ " + (string.IsNullOrEmpty(address.City_Local.Trim()) ? "-" : address.City_Local.Trim());

                string contactFullName = (string.IsNullOrEmpty(contactPerson.FirstName) ? "-" : contactPerson.FirstName)
                    + " " +
                    (string.IsNullOrEmpty(contactPerson.LastName) ? "-" : contactPerson.LastName);

                SupplierOPNModel oPNModel = new SupplierOPNModel();

                if (organization != null && address != null && contactPerson != null)
                {
                    oPNModel.buyerSupplierId = (int)supplierMapping.OPNSupplierID;
                    oPNModel.buyerSupplierContactId = (int)supplierMapping.ContactID;
                    oPNModel.supplierUserId = (int)supplierMapping.SupplierUserID;
                    oPNModel.taxId = organization.TaxID;
                    oPNModel.registeredCapital = (decimal)organization.RegisteredCapital;
                    oPNModel.currency = organization.CurrencyCode;
                    oPNModel.yearEstablished = organization.YearEstablished;
                    oPNModel.manufacturer = isManufacturer;
                    oPNModel.distributor = isDistributor;
                    oPNModel.dealer = isDealer;
                    oPNModel.serviceProvider = isServiceProvider;
                    oPNModel.address = companyAddress;
                    oPNModel.province = address.State_Local;
                    oPNModel.postcode = address.PostalCode;
                    oPNModel.country = country.CountryName;
                    oPNModel.contactName = contactFullName;
                    oPNModel.department = contactPerson.Department;
                    oPNModel.phone = contactPerson.PhoneNo;
                    oPNModel.mobile = contactPerson.MobileNo;
                    oPNModel.supplierDirectory = true;
                    oPNModel.deliveryDate = null;
                    //result = true;
                    result = await this._externalServiceManager.OPNSupplierSerivce(oPNModel);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public bool PISApproveBuyerSupplierMapping(int regID)
        {
            bool result = false;
            try
            {
                Tbl_OPNBuyerSupplierMapping supplierMapping = this._oPNBuyerSupplierMappingRepository.GetOPNBuyerSupplierMappingByRegID(regID);

                if (supplierMapping != null)
                {
                    supplierMapping.PISApproved = true;
                    supplierMapping.IsJobSuccess = false;
                    supplierMapping.UpdatedDate = DateTime.Now;
                    result = this._oPNBuyerSupplierMappingRepository.UpdateOPNBuyerSupplierMapping(supplierMapping);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        #region INSERT
        public int InsertOPNBuyerSupplierMapping(RegisterOPNSupplierMapping request)
        {
            try
            {
                int Id = 0;
                Tbl_OPNBuyerSupplierMapping tbl_OPNBuyerSupplierMapping = new Tbl_OPNBuyerSupplierMapping();
                tbl_OPNBuyerSupplierMapping.InvitationCode = request.buyerShortName;
                tbl_OPNBuyerSupplierMapping.RegID = request.regId;
                tbl_OPNBuyerSupplierMapping.PISApproved = false;
                tbl_OPNBuyerSupplierMapping.IsJobSuccess = false;
                tbl_OPNBuyerSupplierMapping.CreatedDate = DateTime.Now;
                tbl_OPNBuyerSupplierMapping.UpdatedDate = DateTime.Now;

                if (!string.IsNullOrEmpty(tbl_OPNBuyerSupplierMapping.InvitationCode))
                {
                    Tbl_OPNBuyer result = _opnBuyer.getOPNBuyer(tbl_OPNBuyerSupplierMapping.InvitationCode);
                    if (result != null)
                    {
                        tbl_OPNBuyerSupplierMapping.BuyerOrgID = result.BuyerOrgID;
                        tbl_OPNBuyerSupplierMapping.BuyerID = result.BuyerID;
                        Id = _oPNBuyerSupplierMappingRepository.InsertOPNBuyerSupplierMapping(tbl_OPNBuyerSupplierMapping);
                    }

                }


                return Id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int InsertOPNBuyerSupplierMapping(Tbl_OPNBuyerSupplierMappingDto request)
        {
            try
            {
                int Id = 0;
                Tbl_OPNBuyerSupplierMapping tbl_OPNBuyerSupplierMapping = new Tbl_OPNBuyerSupplierMapping();
                tbl_OPNBuyerSupplierMapping.SupplierID = request.SupplierID;
                tbl_OPNBuyerSupplierMapping.RegID = request.RegID;
                tbl_OPNBuyerSupplierMapping.PISApproved = true;
                tbl_OPNBuyerSupplierMapping.IsJobSuccess = true;
                tbl_OPNBuyerSupplierMapping.InvitationCode = request.InvitationCode;
                tbl_OPNBuyerSupplierMapping.JobErrorMsg = null;
                tbl_OPNBuyerSupplierMapping.CreatedDate = DateTime.Now;
                tbl_OPNBuyerSupplierMapping.UpdatedDate = DateTime.Now;


                if (!string.IsNullOrEmpty(tbl_OPNBuyerSupplierMapping.InvitationCode))
                {
                    Tbl_OPNBuyer result = _opnBuyer.getOPNBuyer(tbl_OPNBuyerSupplierMapping.InvitationCode);
                    if (result != null)
                    {
                        tbl_OPNBuyerSupplierMapping.BuyerID = result.BuyerID;
                        tbl_OPNBuyerSupplierMapping.BuyerOrgID = result.BuyerOrgID;

                        Id = _oPNBuyerSupplierMappingRepository.InsertOPNBuyerSupplierMapping(tbl_OPNBuyerSupplierMapping);
                    }

                }


                return Id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}

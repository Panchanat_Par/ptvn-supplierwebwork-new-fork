﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using SupplierPortal.Core.Extension;
using SupplierPortal.Data.Models.SupportModel.Register;
using SupplierPortal.Data.Models.Repository.CustomerDocName;
using SupplierPortal.Data.Models.Repository.RegEmailTicket;
using SupplierPortal.Data.Models.Repository.Language;
using SupplierPortal.Data.Models.Repository.RegAddress;
using SupplierPortal.Data.Models.Repository.RegContact;
using SupplierPortal.Data.Models.Repository.RegInfo;
using SupplierPortal.Data.Models.Repository.RegServiceTypeMapping;
using SupplierPortal.Data.Models.Repository.RegContactAddressMapping;
using SupplierPortal.Data.Models.Repository.RegBusinessType;
using SupplierPortal.Data.Models.Repository.BusinessType;
using SupplierPortal.Data.Models.Repository.RegProductKeyword;
using SupplierPortal.Data.Models.Repository.RegisProductCategory;
using SupplierPortal.Data.Models.Repository.OPNBuyer;
using SupplierPortal.Services.RegisterPortalManage;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Framework.Localization;
using SupplierPortal.BusinessLogic.OPN.BuyerSupplierMapping;
using SupplierPortal.BusinessLogic.OPN.SupplierMapping;
using SupplierPortal.Models.Dto;
using SupplierPortal.Data.CustomModels.RegisterSupplier;
using SupplierPortal.DataAccess.RegFromConnect;
using Minio;
using System.Web.Configuration;
using System.Threading.Tasks;
using System.IO;
using SupplierPortal.Data.Models.Repository.AppConfig;
using SupplierPortal.Data.Models.Repository.RegAttachment;
using SupplierPortal.Data.Models.Repository.LogRegAttachment;
using SupplierPortal.Data.Models.Repository.RegContactRequest;
using SupplierPortal.Data.Models.Repository.LogRegContactRequest;
using SupplierPortal.Data.Models.Repository.RegContactRequestHistory;
using SupplierPortal.Data.Models.Repository.RegContactRequestServiceTypeMapping;
using SupplierPortal.Data.Models.Repository.RegApprove;
using SupplierPortal.Data.Models.Repository.Organization;

namespace SupplierPortal.BusinessLogic.RegisterSupplier
{
    public class RegisterSupplierManager : IRegisterSupplierManager
    {
        private ICustomerDocNameRepository _customerDocNameRepository;
        private IRegEmailTicketRepository _regEmailTicketRepository;
        private IRegisterDataService _registerDataService;
        private ILanguageRepository _languageRepository;
        private IRegAddressRepository _regAddressRepository;
        private IRegContactRepository _regContactRepository;
        private IRegInfoRepository _regInfoRepository;
        private IRegServiceTypeMappingRepository _regServiceTypeMappingRepository;
        private IRegContactAddressMappingRepository _regContactAddressMappingRepository;
        private IRegBusinessTypeRepository _regBusinessTypeRepository;
        private IBusinessTypeRepository _businessTypeRepository;
        private IRegProductKeywordRepository _regProductKeywordRepository;
        private IRegisProductCategoryRepository _regisProductCategoryRepository;
        private IOPNBuyerRepository _opnBuyerRepository;
        private IOPNBuyerSupplierMappingManager _oPNBuyerSupplierMappingManager;
        private IOPNSupplierMappingManager _opnSupplierMappingManager;
        private IRegFromConnectRepository _regFromConnectRepository;
        private IAppConfigRepository _appConfigRepository;
        private IRegAttachmentRepository _regAttachmentRepository;
        private ILogRegAttachmentRepository _logRegAttachmentRepository;
        private IRegContactRequestRepository _regContactRequestRepository;
        private ILogRegContactRequestRepository _logRegContactRequestRepository;
        private IRegContactRequestHistoryRepository _regContactRequestHistoryRepository;
        private IRegContactRequestServiceTypeMappingRepository _regContactReqServMapping;
        private IRegApproveRepository _regApproveRepository;
        private IOrganizationRepository _organizationRepository;

        private string minio_url;
        private string minio_access_key;
        private string minio_secret_key;
        private string minio_bucket_name;
        private string minio_object_name;
        private string attachmentURL;
        private string pathTempRegis;

        public RegisterSupplierManager()
        {
            _customerDocNameRepository = new CustomerDocNameRepository();
            _regEmailTicketRepository = new RegEmailTicketRepository();
            _registerDataService = new RegisterDataService();
            _languageRepository = new LanguageRepository();
            _regAddressRepository = new RegAddressRepository();
            _regContactRepository = new RegContactRepository();
            _regInfoRepository = new RegInfoRepository();
            _regServiceTypeMappingRepository = new RegServiceTypeMappingRepository();
            _regContactAddressMappingRepository = new RegContactAddressMappingRepository();
            _regBusinessTypeRepository = new RegBusinessTypeRepository();
            _businessTypeRepository = new BusinessTypeRepository();
            _regProductKeywordRepository = new RegProductKeywordRepository();
            _regisProductCategoryRepository = new RegisProductCategoryRepository();
            _opnBuyerRepository = new OPNBuyerRepository();
            _oPNBuyerSupplierMappingManager = new OPNBuyerSupplierMappingManager();
            _opnSupplierMappingManager = new OPNSupplierMappingManager();
            _regFromConnectRepository = new RegFromConnectRepository();
            _appConfigRepository = new AppConfigRepository();
            _regAttachmentRepository = new RegAttachmentRepository();
            _logRegAttachmentRepository = new LogRegAttachmentRepository();
            _regContactRequestRepository = new RegContactRequestRepository();
            _logRegContactRequestRepository = new LogRegContactRequestRepository();
            _regContactRequestHistoryRepository = new RegContactRequestHistoryRepository();
            _regContactReqServMapping = new RegContactRequestServiceTypeMappingRepository();
            _regApproveRepository = new RegApproveRepository();
            _organizationRepository = new OrganizationRepository();

            this.minio_url = WebConfigurationManager.AppSettings["minio_url"];
            this.minio_access_key = WebConfigurationManager.AppSettings["minio_access_key"];
            this.minio_secret_key = WebConfigurationManager.AppSettings["minio_secret_key"];
            this.minio_bucket_name = WebConfigurationManager.AppSettings["minio_bucket_name"];
            this.minio_object_name = WebConfigurationManager.AppSettings["minio_object_name"];
            this.attachmentURL = WebConfigurationManager.AppSettings["attachmentURL"];
            this.pathTempRegis = WebConfigurationManager.AppSettings["pathTempRegis"];
        }

        public async Task<RegisterSupplierResponse> SupplierRegister(RegisterSupplierRequest model)
        {
            RegisterSupplierResponse result;

            try
            {
                result = new RegisterSupplierResponse();

                if (model.RegID != 0)
                {
                    Tbl_RegInfo regInfo = _regInfoRepository.GetRegInfoByTicketcode(model.TicketCode);

                    if (regInfo == null)
                    {
                        result.ErrorMsg = "_Regis.ValidateMsg.TicketCodeNotExisting";
                        return result;
                    }

                    if (regInfo.RegID != model.RegID)
                    {
                        result.ErrorMsg = "_Regis.ValidateMsg.RegIDNotMatchTicketCode";
                        return result;
                    }

                    return await UpdateSupplierRegister(model);
                }

                return await InsertSupplierRegister(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<RegisterSupplierResponse> InsertSupplierRegister(RegisterSupplierRequest model)
        {
            RegisterSupplierResponse result;
            try
            {
                result = new RegisterSupplierResponse();
                MinioClient minio = null;

                #region Validate Duplicate

                if (_regInfoRepository.CheckDuplicateRegisterFromConnect(model.TaxID, model.BranchNo, model.CountryCode))
                {
                    result.ErrorMsg = "_Regis.ValidateMsg.DuplicateRegister";
                    return result;
                }

                #endregion

                #region Check Connect Minio Server

                try
                {
                    minio = new MinioClient(this.minio_url, this.minio_access_key, this.minio_secret_key);
                    bool found = await minio.BucketExistsAsync(this.minio_bucket_name);
                    if (!found)
                    {
                        result.ErrorMsg = "_Regis.ValidateMsg.BucketNameNotExistsMinio";
                        return result;
                    }
                }
                catch (Exception ex)
                {
                    result.ErrorMsg = "_Regis.ValidateMsg.NotConnectMinioServer";
                    return result;
                }

                #endregion

                var productCategory = new List<int>();
                if (model.CategoryID_Lev3 != null)
                {
                    productCategory = JsonConvert.DeserializeObject<List<int>>(model.CategoryID_Lev3);
                }

                string languageCode = model.LanguageCode.ToLower();
                string languageId = _languageRepository.GetLanguageIdByCode(languageCode);

                #region-------------------------------------Generate TicketCode -------------------------------------

                if (String.IsNullOrWhiteSpace(model.TicketCode))
                {
                    IEnumerable<Tbl_DocumentNoFormat> tbl_DocumentNoFormat = "RegisterTicket".GetPatternByDocName();
                    string _charRandom = "", _ticketCode = "";
                    foreach (var list in tbl_DocumentNoFormat)
                    {
                        _charRandom = list.IsRandomCode == 1 ? list.Length.ToString().GetRandomChar(list.Pattern) : "";
                        _ticketCode = list.Suffix == "" ? list.Prefix + _charRandom : list.Pattern + _charRandom + list.Suffix;
                    }

                    model.TicketCode = _ticketCode;
                }
                #endregion-------------------------------------End Generate TicketCode-----------------------------------

                #region-------------------------------------Insert Main Contact -------------------------------------
                var mainContactModel = new Tbl_RegContact()
                {
                    TitleID = model.NameTitleID,
                    FirstName_Local = (model.FirstName_Local ?? "").Trim(),
                    FirstName_Inter = (model.FirstName_Inter ?? "").Trim(),
                    LastName_Local = (model.LastName_Local ?? "").Trim(),
                    LastName_Inter = (model.LastName_Inter ?? "").Trim(),
                    JobTitleID = model.JobTitleID,
                    OtherJobTitle = (model.OtherJobTitle ?? "").Trim(),
                    Department = "",
                    PhoneCountryCode = (model.PhoneNoCountryCode ?? "").Trim(),
                    PhoneNo = (model.PhoneNo ?? "").Trim(),
                    PhoneExt = (model.PhoneExt ?? "").Trim(),
                    FaxNo = "",
                    FaxExt = "",
                    MobileCountryCode = (model.MobileNoCountryCode ?? "").Trim(),
                    MobileNo = (model.MobileNo ?? "").Trim(),
                    Email = (model.Email ?? "").Trim()
                };

                model.MainContactID = _regContactRepository.InsertReturnContactID(mainContactModel);

                #endregion-------------------------------------End Main Contact--------------------------------------

                #region-------------------------------------Insert Address -------------------------------------
                var addressModel = new Tbl_RegAddress()
                {
                    HouseNo_Local = (model.RegCompanyAddress_HouseNo_Local ?? "").Trim(),
                    HouseNo_Inter = (model.RegCompanyAddress_HouseNo_Inter ?? "").Trim(),
                    VillageNo_Local = (model.RegCompanyAddress_VillageNo_Local ?? "").Trim(),
                    VillageNo_Inter = (model.RegCompanyAddress_VillageNo_Inter ?? "").Trim(),
                    Lane_Local = (model.RegCompanyAddress_Lane_Local ?? "").Trim(),
                    Lane_Inter = (model.RegCompanyAddress_Lane_Inter ?? "").Trim(),
                    Road_Local = (model.RegCompanyAddress_Road_Local ?? "").Trim(),
                    Road_Inter = (model.RegCompanyAddress_Road_Inter ?? "").Trim(),
                    SubDistrict_Local = (model.RegCompanyAddress_SubDistrict_Local ?? "").Trim(),
                    SubDistrict_Inter = (model.RegCompanyAddress_SubDistrict_Inter ?? "").Trim(),
                    City_Local = (model.RegCompanyAddress_City_Local ?? "").Trim(),
                    City_Inter = (model.RegCompanyAddress_City_Inter ?? "").Trim(),
                    State_Local = (model.RegCompanyAddress_State_Local ?? "").Trim(),
                    State_Inter = (model.RegCompanyAddress_State_Inter ?? "").Trim(),
                    CountryCode = (model.RegCompanyAddress_CountryCode ?? "").Trim(),
                    PostalCode = (model.RegCompanyAddress_PostalCode ?? "").Trim(),
                };

                model.CompanyAddrID = _regAddressRepository.InsertReturnAddressID(addressModel);
                _regContactAddressMappingRepository.Insert(model.MainContactID, model.CompanyAddrID);
                #endregion-------------------------------------End Insert Address-------------------------------

                #region-------------------------------------Insert RegInfo -------------------------------------------
                var regInfoModel = new Tbl_RegInfo()
                {
                    TicketCode = model.TicketCode.Trim(),
                    RegStatusID = 2,
                    CountryCode = (model.CountryCode ?? "").Trim(),
                    TaxID = (model.TaxID ?? "").Trim(),
                    DUNSNumber = "",
                    CompanyTypeID = model.CompanyTypeID,
                    BusinessEntityID = model.BusinessEntityID,
                    OtherBusinessEntity = (model.OtherBusinessEntity ?? "").Trim(),
                    CompanyName_Local = (model.CompanyName_Local.ToUpper() ?? "").Trim(),
                    CompanyName_Inter = (model.CompanyName_Inter.ToUpper() ?? "").Trim(),
                    BranchNo = (model.BranchNo ?? "").Trim(),
                    BranchName_Local = "",
                    BranchName_Inter = "",
                    MainBusinessCode = "00000000",
                    MainBusiness = (model.MainBusiness ?? "").Trim(),
                    CompanyAddrID = model.CompanyAddrID,
                    DeliveredAddrID = model.CompanyAddrID,
                    MainContactID = model.MainContactID,
                    PhoneNo = (model.PhoneNo ?? "").Trim(),
                    PhoneExt = (model.PhoneExt ?? "").Trim(),
                    MobileNo = (model.MobileNo ?? "").Trim(),
                    FaxNo = "",
                    FaxExt = "",
                    WebSite = "",
                    Latitude = 0,
                    Longtitude = 0,
                    LastUpdateDate = DateTime.UtcNow,
                    LastRejectDate = DateTime.UtcNow,
                    isDeleted = 0,
                    RegisteredCapital = model.RegisteredCapital,
                    CurrencyCode = (model.CurrencyCode ?? "").Trim(),
                    ApprovedBy = 0,
                    ApprovedDate = new DateTime(1900, 01, 01),
                    RegisteredEmail = (model.Email ?? "").Trim(),
                    LanguageCode = (languageCode ?? "").Trim(),
                    RequestDate = DateTime.UtcNow
                };

                model.RegID = _regInfoRepository.InsertReturnRegID(regInfoModel);

                #endregion-------------------------------------End Insert RegInfo--------------------------------------

                #region-------------------------------------Insert TicketCode -------------------------------------

                var emailTicket = new Tbl_RegEmailTicket()
                {
                    RegID = model.RegID,
                    TicketCode = (model.TicketCode ?? "").Trim(),
                    RegisteredEmail = (model.Email ?? "").Trim(),
                    LanguageCode = (languageCode ?? "").Trim()
                };

                _regEmailTicketRepository.Insert(emailTicket);

                #endregion-------------------------------------End Insert TicketCode-----------------------------------

                #region-------------------------------------Insert RegServiceType -------------------------------------------
                foreach (int item in model.ServiceTypeList)
                {
                    _regServiceTypeMappingRepository.InsertByValue(model.RegID, item, item == 2 ? (model.InvitationCode ?? "") : "");
                }
                #endregion-------------------------------------End Insert RegServiceType--------------------------------------

                #region-------------------------------------Attachment Upload-------------------------------------

                try
                {
                    string pathTemp = _appConfigRepository.GetAppConfigByName("Register_AttachmentPathTemp").Value;
                    string pathSave = _appConfigRepository.GetAppConfigByName("Register_AttachmentPath").Value;

                    if (model.RegAttachments.Any())
                    {
                        foreach (RegAttachment attachment in model.RegAttachments)
                        {
                            bool uploadResult = await MinioDownloadUploadAttachment(minio, attachment, model.RegID, pathTemp, pathSave);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                #endregion-------------------------------------Attachment Upload-------------------------------------

                #region --------------------------------Insert RegBusinessType--------------------------------

                if (model.OrgBusinessType.Any())
                {
                    foreach (var itemList in model.OrgBusinessType)
                    {

                        Tbl_RegBusinessType regBusinessType = new Tbl_RegBusinessType();

                        regBusinessType.RegID = model.RegID;
                        regBusinessType.BusinessTypeID = itemList.BusinessTypeID;

                        _regBusinessTypeRepository.InsertRegBusinessType(regBusinessType);

                    }
                }

                #endregion

                #region ---------------------------------------------Insert KeyWord---------------------------------------------

                if (model.ProductKeyword != null)
                {
                    var dataKey = model.ProductKeyword.Split(',').ToList();
                    if (model.ProductKeyword != "")
                    {
                        foreach (var Key in dataKey)
                        {
                            //_orgProductKeywordRepository.Insert(model.SupplierID, Key.ToString() , userID);
                            var data = new Tbl_RegProductKeyword();
                            data.RegID = model.RegID;
                            data.Keyword = (Key.ToString() ?? "").Trim();

                            _regProductKeywordRepository.Insert(data);
                        }
                    }
                }
                #endregion

                #region ---------------------------------------Insert Category---------------------------------------
                if (productCategory != null)
                {
                    foreach (var item in productCategory)
                    {
                        var data = new Tbl_RegProductCategory();
                        data.RegID = model.RegID;
                        data.CategoryID_Lev3 = item;
                        data.ProductTypeID = 3;

                        _regisProductCategoryRepository.Insert(data);
                    }
                }

                #endregion

                //อย่าลืมเปิดส่งเมล์เมื่อแก้เสร็จ
                _registerDataService.SendingEmailRegisterSubmit(model.RegID, languageId);
                _registerDataService.SendingEmailRegisterSubmitPIS(model.RegID, languageId);

                string messageReturn = "";
                string email = "<a href=\"mailto:" + model.Email + "\"> " + model.Email + " </a>";
                messageReturn = messageReturn.GetStringResource("_Regis.InfoMsg.RegisterSuccess", languageId);
                messageReturn = messageReturn.Replace("<%email_address%>", email);

                #region ---------------------- OPN Supplier Register -------------------------

                // verify OPN invitation code or not ?
                bool isOPNInvitationCode = _opnBuyerRepository.verifyInvitationCode(model.InvitationCode);

                // OPN Supplier Register
                if (model.OpnSupplierRegister != null && model.RegID != 0 && isOPNInvitationCode == true)
                {
                    if (model.OpnSupplierRegister.buyerSupplierId > 0 && model.RegID > 0)
                    {
                        model.OpnSupplierRegister.regId = model.RegID;
                        model.OpnSupplierRegister.email = model.Email;
                        model.OpnSupplierRegister.buyerShortName = model.InvitationCode;

                        RegisterOPNSupplierMapping registerOPNSupplierMapping = new RegisterOPNSupplierMapping()
                        {
                            regId = model.OpnSupplierRegister.regId,
                            buyerShortName = model.OpnSupplierRegister.buyerShortName,
                            email = model.OpnSupplierRegister.email,
                            buyerSupplierId = model.OpnSupplierRegister.buyerSupplierId,
                            buyerSupplierContactId = model.OpnSupplierRegister.buyerSupplierContactId,
                            supplierUserId = model.OpnSupplierRegister.supplierUserId
                        };

                        bool insertOPNBuyerSupplierMappingResult = _oPNBuyerSupplierMappingManager.InsertOPNBuyerSupplierMapping(registerOPNSupplierMapping) > 0;
                        if (insertOPNBuyerSupplierMappingResult)
                        {
                            bool insertOPNSupplierMappingResult = _opnSupplierMappingManager.InsertOPNSupplierMapping(registerOPNSupplierMapping) > 0;
                            //if (!insertOPNSupplierMappingResult)
                            //{
                            //    logger.Info("Insert OPNBuyerSupplierMapping & OPNSupplierMapping success");
                            //}
                            //else
                            //{
                            //    logger.Error("Cannot insert OPNSupplierMapping");
                            //}
                        }
                        //else
                        //{
                        //    logger.Error("Cannot insert OPNBuyerSupplierMapping");
                        //}
                    }
                }
                else if (model.OpnSupplierRegister == null && model.RegID != 0 && isOPNInvitationCode == true)
                {
                    OPNSupplierRegister opnSupplierRegister = new OPNSupplierRegister();

                    opnSupplierRegister.regId = model.RegID;
                    opnSupplierRegister.email = model.Email;
                    opnSupplierRegister.buyerShortName = model.InvitationCode;
                    model.OpnSupplierRegister = opnSupplierRegister;

                    RegisterOPNSupplierMapping registerOPNSupplierMapping = new RegisterOPNSupplierMapping()
                    {
                        regId = model.OpnSupplierRegister.regId,
                        buyerShortName = model.OpnSupplierRegister.buyerShortName,
                        email = model.OpnSupplierRegister.email,
                        buyerSupplierId = model.OpnSupplierRegister.buyerSupplierId,
                        buyerSupplierContactId = model.OpnSupplierRegister.buyerSupplierContactId,
                        supplierUserId = model.OpnSupplierRegister.supplierUserId
                    };

                    bool insertOPNBuyerSupplierMappingResult = _oPNBuyerSupplierMappingManager.InsertOPNBuyerSupplierMapping(registerOPNSupplierMapping) > 0;
                    //if (insertOPNBuyerSupplierMappingResult)
                    //{
                    //    logger.Info("Insert OPNBuyerSupplierMapping success");
                    //}
                    //else
                    //{
                    //    logger.Error("Cannot insert OPNBuyerSupplierMapping");
                    //}
                }

                Tbl_RegFromConnect regFromConnect = new Tbl_RegFromConnect()
                {

                    RegId = model.RegID,
                    RegAddressId = model.CompanyAddrID,
                    RegContactId = model.MainContactID,
                    SupplierId = 0,
                    TicketCode = model.TicketCode,
                    TaxId = model.TaxID,
                    BranchNumber = model.BranchNo,
                    CompanyName = model.CompanyName_Inter,
                    CompanyNameLocal = model.CompanyName_Local,
                    TaxCountryCode = model.CountryCode,
                    IsSupplierRegister = true,
                    IsJobCompleted = false,
                };

                int regFromSupplierId = _regFromConnectRepository.Insert(regFromConnect);

                if (regFromSupplierId > 0)
                {
                    result.RegId = model.RegID;
                    result.RegAddressId = model.CompanyAddrID;
                    result.RegContactId = model.MainContactID;
                    result.TicketCode = model.TicketCode;
                    result.TaxId = model.TaxID;
                    result.BranchNumber = model.BranchNo;
                    result.CompanyName = model.CompanyName_Inter;
                    result.CompanyNameLocal = model.CompanyName_Local;
                    result.TaxCountryCode = model.CountryCode;
                }
                else
                {
                    result.ErrorMsg = "_Regis.ValidateMsg.ErrorInsertRegFromConnect";
                }

                #endregion
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public async Task<RegisterSupplierResponse> UpdateSupplierRegister(RegisterSupplierRequest model)
        {
            RegisterSupplierResponse result;

            try
            {
                result = new RegisterSupplierResponse();
                MinioClient minio = null;

                try
                {
                    minio = new MinioClient(this.minio_url, this.minio_access_key, this.minio_secret_key);
                    bool found = await minio.BucketExistsAsync(this.minio_bucket_name);
                    if (!found)
                    {
                        result.ErrorMsg = "_Regis.ValidateMsg.BucketNameNotExistsMinio";
                        return result;
                    }
                }
                catch (Exception ex)
                {
                    result.ErrorMsg = "_Regis.ValidateMsg.NotConnectMinioServer";
                    return result;
                }

                var productCategory = new List<int>();
                if (model.CategoryID_Lev3 != null)
                {
                    productCategory = JsonConvert.DeserializeObject<List<int>>(model.CategoryID_Lev3);
                }

                string languageCode = model.LanguageCode.ToLower();
                string languageId = _languageRepository.GetLanguageIdByCode(languageCode);


                #region-------------------------------------Update RegInfo -------------------------------------

                var regInfoModel = _regInfoRepository.GetRegInfoByRegID(model.RegID).FirstOrDefault();

                model.CompanyAddrID = (int)regInfoModel.CompanyAddrID;
                model.MainContactID = (int)regInfoModel.MainContactID;

                if (regInfoModel != null)
                {
                    regInfoModel.CountryCode = (model.CountryCode ?? "").Trim();
                    regInfoModel.TaxID = (model.TaxID ?? "").Trim();
                    regInfoModel.CompanyTypeID = model.CompanyTypeID;
                    regInfoModel.BusinessEntityID = model.BusinessEntityID;
                    regInfoModel.OtherBusinessEntity = (model.OtherBusinessEntity ?? "").Trim();
                    regInfoModel.CompanyName_Local = (model.CompanyName_Local.ToUpper() ?? "").Trim();
                    regInfoModel.CompanyName_Inter = (model.CompanyName_Inter.ToUpper() ?? "").Trim();
                    regInfoModel.BranchNo = (model.BranchNo ?? "").Trim();
                    regInfoModel.MainBusiness = (model.MainBusiness ?? "").Trim();
                    regInfoModel.PhoneNo = (model.PhoneNo ?? "").Trim();
                    regInfoModel.PhoneExt = (model.PhoneExt ?? "").Trim();
                    regInfoModel.MobileNo = (model.MobileNo ?? "").Trim();
                    regInfoModel.LastUpdateDate = DateTime.UtcNow;
                    regInfoModel.ApprovedBy = 0;
                    regInfoModel.ApprovedDate = new DateTime(1900, 01, 01);
                    regInfoModel.RegisteredCapital = model.RegisteredCapital;
                    regInfoModel.CurrencyCode = (model.CurrencyCode ?? "").Trim();
                    regInfoModel.RegisteredEmail = (model.Email ?? "").Trim();
                    regInfoModel.LanguageCode = (languageCode ?? "").Trim();
                    regInfoModel.RequestDate = DateTime.UtcNow;

                    if (regInfoModel.RegStatusID == 3)
                    {
                        regInfoModel.RegStatusID = 2;
                    }

                    _regInfoRepository.UpdateRegInfo(regInfoModel);
                }

                #endregion----------------------------------End RegInfo-----------------------------------

                #region-------------------------------------Update Address -------------------------------------

                var addressModel = _regAddressRepository.GetRegAddressByaddrid(model.CompanyAddrID).FirstOrDefault();
                if (addressModel != null)
                {
                    addressModel.HouseNo_Inter = (model.RegCompanyAddress_HouseNo_Inter).Trim();
                    addressModel.HouseNo_Local = (model.RegCompanyAddress_HouseNo_Local).Trim();
                    addressModel.VillageNo_Inter = (model.RegCompanyAddress_VillageNo_Inter).Trim();
                    addressModel.VillageNo_Local = (model.RegCompanyAddress_VillageNo_Local).Trim();
                    addressModel.Lane_Inter = (model.RegCompanyAddress_Lane_Inter).Trim();
                    addressModel.Lane_Local = (model.RegCompanyAddress_Lane_Local).Trim();
                    addressModel.Road_Inter = (model.RegCompanyAddress_Road_Inter ?? "").Trim();
                    addressModel.Road_Local = (model.RegCompanyAddress_Road_Local ?? "").Trim();
                    addressModel.SubDistrict_Inter = (model.RegCompanyAddress_SubDistrict_Inter ?? "").Trim();
                    addressModel.SubDistrict_Local = (model.RegCompanyAddress_SubDistrict_Local ?? "").Trim();
                    addressModel.City_Inter = (model.RegCompanyAddress_City_Inter ?? "").Trim();
                    addressModel.City_Local = (model.RegCompanyAddress_City_Local ?? "").Trim();
                    addressModel.State_Inter = (model.RegCompanyAddress_State_Inter ?? "").Trim();
                    addressModel.State_Local = (model.RegCompanyAddress_State_Local ?? "").Trim();
                    addressModel.CountryCode = (model.RegCompanyAddress_CountryCode ?? "").Trim();
                    addressModel.PostalCode = (model.RegCompanyAddress_PostalCode ?? "").Trim();


                    _regAddressRepository.UpdateRegAddress(addressModel);
                }

                #endregion----------------------------------End Address-----------------------------------

                #region-------------------------------------Update Main Contact -------------------------------------

                var mainContactModel = _regContactRepository.GetRegContactByContactid(model.MainContactID).FirstOrDefault();
                if (mainContactModel != null)
                {
                    mainContactModel.TitleID = model.NameTitleID;
                    mainContactModel.FirstName_Inter = (model.FirstName_Inter ?? "").Trim();
                    mainContactModel.FirstName_Local = (model.FirstName_Local ?? "").Trim();
                    mainContactModel.LastName_Inter = (model.LastName_Inter ?? "").Trim();
                    mainContactModel.LastName_Local = (model.LastName_Local ?? "").Trim();
                    mainContactModel.JobTitleID = model.JobTitleID;
                    mainContactModel.OtherJobTitle = (model.OtherJobTitle ?? "").Trim();
                    mainContactModel.PhoneCountryCode = (model.PhoneNoCountryCode ?? "").Trim();
                    mainContactModel.PhoneNo = (model.PhoneNo ?? "").Trim();
                    mainContactModel.PhoneExt = (model.PhoneExt ?? "").Trim();
                    mainContactModel.MobileCountryCode = (model.MobileNoCountryCode ?? "").Trim();
                    mainContactModel.MobileNo = (model.MobileNo ?? "").Trim();
                    mainContactModel.Email = (model.Email ?? "").Trim();

                    _regContactRepository.UpdateRegContact(mainContactModel);
                }
                #endregion----------------------------------End Main Contact-----------------------------------

                #region-------------------------------------Update RegEmailTicket ------------------------------

                var regEmailTicket = _regEmailTicketRepository.GetEmailTicketDataListByTicketcode(model.TicketCode).FirstOrDefault();

                if (regEmailTicket != null)
                {
                    regEmailTicket.RegisteredEmail = (model.Email ?? "").Trim();
                    _regEmailTicketRepository.Update(regEmailTicket, 1);
                }

                #endregion----------------------------------End Update RegEmailTicket---------------------------

                #region-------------------------------------Update RegServiceType -------------------------------------------
                _regServiceTypeMappingRepository.RemoveByRegID(model.RegID);

                foreach (int item in model.ServiceTypeList)
                {
                    _regServiceTypeMappingRepository.InsertByValue(model.RegID, item, item == 2 ? (model.InvitationCode ?? "") : "");
                }

                #endregion-------------------------------------End Update RegServiceType--------------------------------------

                #region-------------------------------------Update Attachment Upload-------------------------------------

                try
                {
                    string pathTemp = _appConfigRepository.GetAppConfigByName("Register_AttachmentPathTemp").Value;
                    string pathSave = _appConfigRepository.GetAppConfigByName("Register_AttachmentPath").Value;

                    var listRegAttachment = _regAttachmentRepository.GetRegAttachmentByRegID(model.RegID);
                    if (listRegAttachment.Any())
                    {
                        foreach (var attachment in listRegAttachment)
                        {
                            string pathForSave = this.attachmentURL + pathSave + model.RegID;
                            string filePathSave = pathForSave + "\\" + attachment.AttachmentNameUnique;

                            if (System.IO.File.Exists(filePathSave))
                                System.IO.File.Delete(filePathSave);
                        }

                        _regAttachmentRepository.RemoveByRegID(model.RegID);
                    }

                    if (model.RegAttachments.Any())
                    {
                        foreach (RegAttachment attachment in model.RegAttachments)
                        {
                            bool uploadResult = await MinioDownloadUploadAttachment(minio, attachment, model.RegID, pathTemp, pathSave);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                #endregion-------------------------------------End Update Attachment Upload-------------------------------------

                #region --------------------------------Update RegBusinessType--------------------------------

                if (model.OrgBusinessType.Any())
                {
                    _regBusinessTypeRepository.RemoveRegBusinessTypeByRegID(model.RegID);

                    foreach (var itemList in model.OrgBusinessType)
                    {

                        Tbl_RegBusinessType regBusinessType = new Tbl_RegBusinessType();

                        regBusinessType.RegID = model.RegID;
                        regBusinessType.BusinessTypeID = itemList.BusinessTypeID;

                        _regBusinessTypeRepository.InsertRegBusinessType(regBusinessType);

                    }
                }

                #endregion

                #region ---------------------------------------------Update KeyWord---------------------------------------------

                if (model.ProductKeyword != null)
                {
                    _regProductKeywordRepository.RemoveByRegID(model.RegID);

                    var dataKey = model.ProductKeyword.Split(',').ToList();
                    if (model.ProductKeyword != "")
                    {
                        foreach (var Key in dataKey)
                        {
                            var data = new Tbl_RegProductKeyword();
                            data.RegID = model.RegID;
                            data.Keyword = (Key.ToString() ?? "").Trim();

                            _regProductKeywordRepository.Insert(data);
                        }
                    }
                }
                #endregion

                #region ---------------------------------------Update Category---------------------------------------
                if (productCategory != null)
                {
                    _regisProductCategoryRepository.RemoveDataOrgProductCategory(model.RegID);

                    foreach (var item in productCategory)
                    {
                        var data = new Tbl_RegProductCategory();
                        data.RegID = model.RegID;
                        data.CategoryID_Lev3 = item;
                        data.ProductTypeID = 3;

                        _regisProductCategoryRepository.Insert(data);
                    }
                }


                Tbl_RegFromConnect regFromConnect = _regFromConnectRepository.GetByRegId(model.RegID);
                regFromConnect.IsJobCompleted = false;

                bool updateResult = _regFromConnectRepository.Update(regFromConnect);

                if (updateResult)
                {
                    result.RegId = (int)regFromConnect.RegId;
                    result.RegAddressId = (int)regFromConnect.RegAddressId;
                    result.RegContactId = (int)regFromConnect.RegContactId;
                    result.TicketCode = regFromConnect.TicketCode;
                    result.TaxId = regFromConnect.TaxId;
                    result.BranchNumber = regFromConnect.BranchNumber;
                    result.CompanyName = regFromConnect.CompanyName;
                    result.CompanyNameLocal = regFromConnect.CompanyNameLocal;
                    result.TaxCountryCode = regFromConnect.TaxCountryCode;
                }
                else
                {
                    result.ErrorMsg = "_Regis.ValidateMsg.ErrorInsertRegFromConnect";
                }
                #endregion
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public ContactResponse AddContactRequest(ContactRequest contactModel)
        {
            ContactResponse result;
            try
            {
                result = new ContactResponse();

                string languageCode = contactModel.LanguageCode.ToLower();
                string languageId = _languageRepository.GetLanguageIdByCode(languageCode);

                Tbl_RegApprove regApprove = _regApproveRepository.GetRegApproveByRegID(contactModel.RegID);

                if (regApprove == null)
                {
                    result.ErrorMsg = "_Regis.ValidateMsg.RegIDNotExists";
                    return result;
                }

                Tbl_Organization organization = _organizationRepository.GetDataBySupplierID((int)regApprove.SupplierID);

                if (organization == null)
                {
                    result.ErrorMsg = "_Regis.ValidateMsg.SupplierNotFound";
                    return result;
                }

                #region-------- Insert RegContactRequest And log -----------------

                var model = new Tbl_RegContactRequest()
                {
                    SupplierID = regApprove.SupplierID,
                    UserReqStatusID = 2,
                    RequestDate = DateTime.UtcNow,
                    TitleID = contactModel.ContactTitleID,
                    FirstName_Local = contactModel.FirstName_Local,
                    FirstName_Inter = contactModel.FirstName_Inter,
                    LastName_Local = contactModel.LastName_Local,
                    LastName_Inter = contactModel.LastName_Inter,
                    JobTitleID = contactModel.ContactJobTitleID,
                    OtherJobTitle = contactModel.ContactOtherJobTitle ?? "",
                    Department = "",
                    PhoneCountryCode = contactModel.ContactPhoneNoCountryCode ?? "",
                    PhoneNo = contactModel.ContactPhoneNo,
                    PhoneExt = contactModel.ContactPhoneExt ?? "",
                    MobileCountryCode = contactModel.ContactMobileCountryCode ?? "",
                    MobileNo = contactModel.ContactMobile ?? "",
                    FaxNo = "",
                    FaxExt = "",
                    Email = contactModel.ContactEmail,
                    UserID = 0,
                    isDeleted = 0
                };

                int userReqId = _regContactRequestRepository.InsertReturnUserReqId(model);

                _logRegContactRequestRepository.Insert(userReqId, "Add", 1);

                #endregion

                #region-------- Insert RegContactRequestHistory -----------------
                var historyModel = new Tbl_RegContactRequestHistory()
                {
                    UserReqId = userReqId,
                    UserReqStatusID = 2,
                    Remark = "",
                    UpdateBy = 1,
                    UpdateDate = DateTime.UtcNow
                };

                _regContactRequestHistoryRepository.Insert(historyModel);
                #endregion

                #region-------- Insert RegContactRequestServiceTypeMapping -----------------

                if (contactModel.Service1)
                {
                    var mappingModel = new Tbl_RegContactRequestServiceTypeMapping()
                    {
                        UserReqId = userReqId,
                        ServiceTypeID = 1,
                        InvitationCode = ""
                    };

                    _regContactReqServMapping.Insert(mappingModel);
                }

                if (contactModel.Service2)
                {
                    var mappingModel = new Tbl_RegContactRequestServiceTypeMapping()
                    {
                        UserReqId = userReqId,
                        ServiceTypeID = 2,
                        InvitationCode = contactModel.ContactInvitationCode ?? ""
                    };

                    _regContactReqServMapping.Insert(mappingModel);
                }

                if (contactModel.Service3)
                {
                    var mappingModel = new Tbl_RegContactRequestServiceTypeMapping()
                    {
                        UserReqId = userReqId,
                        ServiceTypeID = 3,
                        InvitationCode = contactModel.ContactInvitationCode ?? ""
                    };

                    _regContactReqServMapping.Insert(mappingModel);
                }

                #endregion

                _registerDataService.SendingEmailNewRequestUser(userReqId, languageId, contactModel.ContactEmail);

                _registerDataService.SendingEmailNewRequestUserPIS(userReqId, languageId);

                Tbl_RegFromConnect regFromConnect = new Tbl_RegFromConnect()
                {

                    RegId = userReqId,
                    RegAddressId = 0,
                    RegContactId = 0,
                    SupplierId = organization.SupplierID,
                    TicketCode = String.Empty,
                    TaxId = String.Empty,
                    BranchNumber = String.Empty,
                    CompanyName = String.Empty,
                    CompanyNameLocal = String.Empty,
                    TaxCountryCode = String.Empty,
                    IsSupplierRegister = false,
                    IsJobCompleted = false,
                };

                int regFromSupplierId = _regFromConnectRepository.Insert(regFromConnect);

                if (regFromSupplierId > 0)
                {
                    result.ContactRegID = userReqId;
                    result.SupplierID = organization.SupplierID;
                    result.ContactEmail = contactModel.ContactEmail;
                }
                else
                {
                    result.ErrorMsg = "_Regis.ValidateMsg.ErrorInsertRegFromConnect";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public async Task<bool> TestConnectMinioServer()
        {
            bool result = false;
            try
            {
                var minio = new MinioClient(this.minio_url, this.minio_access_key, this.minio_secret_key);
                result = await minio.BucketExistsAsync(this.minio_bucket_name);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public async Task<bool> TestMinioDownloadAndUploadAttachment(RegAttachment attachment)
        {
            bool result = false;
            try
            {
                var minio = new MinioClient(this.minio_url, this.minio_access_key, this.minio_secret_key);
                string pathTemp = _appConfigRepository.GetAppConfigByName("Register_AttachmentPathTemp").Value;
                string pathSave = _appConfigRepository.GetAppConfigByName("Register_AttachmentPath").Value;

                result = await MinioDownloadUploadAttachment(minio, attachment, 0, pathTemp, pathSave);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public async Task<bool> MinioDownloadUploadAttachment(MinioClient minio, RegAttachment attachment, int regID, string pathTemp, string pathSave)
        {
            bool result = false;
            MemoryStream memory = new MemoryStream();
            FileStream fileSave = null;
            FileStream fileTemp = null;
            try
            {

                await minio.GetObjectAsync(this.minio_bucket_name, this.minio_object_name + "/" + attachment.AttachmentNameUnique,
               (stream) =>
               {
                   // Uncomment to print the file on output console
                   memory = new MemoryStream();
                   stream.CopyTo(memory);

                   string attachmentNameUniqueWithExtension = attachment.AttachmentNameUnique + "." + attachment.Extension;

                   string pathForTemp = this.attachmentURL + pathTemp + this.pathTempRegis;
                   string filePathTemp = pathForTemp + "\\" + attachmentNameUniqueWithExtension;

                   fileTemp = new FileStream(filePathTemp, FileMode.Create, FileAccess.Write);
                   memory.WriteTo(fileTemp);

                   if (System.IO.File.Exists(filePathTemp))
                   {
                       string pathForSave = this.attachmentURL + pathSave + regID;
                       string filePathSave = pathForSave + "\\" + attachmentNameUniqueWithExtension;

                       this.CreateFolderIfNeeded(pathForSave);
                       fileSave = new FileStream(filePathSave, FileMode.Create, FileAccess.Write);
                       memory.WriteTo(fileSave);

                       var models = new Tbl_RegAttachment
                       {
                           RegID = regID,
                           AttachmentName = attachment.AttachmentName,
                           AttachmentNameUnique = attachmentNameUniqueWithExtension,
                           SizeAttach = attachment.SizeAttach.ToString(),
                           DocumentNameID = attachment.DocumentNameID,
                           OtherDocument = ""
                       };

                       if (regID != 0)
                       {
                           _regAttachmentRepository.Insert(models);

                           _logRegAttachmentRepository.InsertLogAdd(regID, attachmentNameUniqueWithExtension, "Add");
                       }
                   }

                   result = true;
               });
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (fileSave != null)
                    fileSave.Close();

                if (fileTemp != null)
                    fileTemp.Close();

                if (memory != null)
                    memory.Close();
            }

            return result;
        }

        private void CreateFolderIfNeeded(string path)
        {

            if (!Directory.Exists(path))
            {
                try
                {
                    Directory.CreateDirectory(path);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }
}
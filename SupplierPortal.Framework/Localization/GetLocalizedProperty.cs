﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.Repository.LocalizedProperty;
using System.Dynamic;
using System.Web.Routing;

namespace SupplierPortal.Framework.Localization
{
    public static class GetLocalizedProperty
    {

        //public static string GetLocalizedValue(string languageID, string localeKeyGroup, string localeKey, string pkTableMain)
        //{
        //    string sqlQuery = "";
        //    string resultValue = "";

        //    try
        //    {
        //         ILocalizedPropertyRepository _repoLocalizedProperty = null;
        //         _repoLocalizedProperty = new LocalizedPropertyRepository();

        //        sqlQuery = "EXEC LocalizedProperty_find '" + languageID + "','" + localeKeyGroup + "','" + localeKey + "','" + pkTableMain + "' ";

        //        resultValue = _repoLocalizedProperty.GetLocalizedValue(sqlQuery);

        //        //_repoLocalizedProperty.Dispose();

        //    }
        //    catch (Exception e)
        //    {

        //        throw;
        //    }

        //    return resultValue;
        //}

        public static string GetLocalizedValue(string languageID, string localeKeyGroup, string localeKey, string pkTableMain)
        {
            string resultValue = "";
            string sqlQuery = "";
            string variabledynamic = "";

            variabledynamic = localeKey + "_" + languageID;

            try
            {

                ILocalizedPropertyRepository _repoLocalizedProperty = new LocalizedPropertyRepository();

                sqlQuery = "EXEC LocalizedProperty_find '" + languageID + "','" + localeKeyGroup + "','" + pkTableMain + "' ";

                var result = _repoLocalizedProperty.GetLocalizedByStore(sqlQuery);

                if (!result.ContainsKey(variabledynamic))
                {
                    resultValue = result[localeKey].ToString();
                }
                else if (!string.IsNullOrEmpty(result[variabledynamic].ToString()))
                {
                    resultValue = result[variabledynamic];
                }
                else 
                {
                    resultValue = result[localeKey].ToString();
                }

            }
            catch (Exception e)
            {

                throw;
            }

            return resultValue;
        }

        //public static ExpandoObject ToExpando(this object anonymousObject)
        //{
        //    IDictionary<string, object> anonymousDictionary = new RouteValueDictionary(anonymousObject);
        //    IDictionary<string, object> expando = new ExpandoObject();
        //    foreach (var item in anonymousDictionary)
        //        expando.Add(item);
        //    return (ExpandoObject)expando;
        //}


    }
}

﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using SupplierPortal.Data.CustomModels.Consent;
using SupplierPortal.Models.Dto.COC;
using Newtonsoft.Json;
using SupplierPortal.BusinessLogic.TermAndCondition;

namespace SupplierPortal.WebApi.Controllers
{
    [RoutePrefix("api/term")]
    public class TermAndConditionController : ApiController
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly ITermAndConditionManager _manager;

        public TermAndConditionController()
        {
            _manager = new TermAndConditionManager();
        }

        [Route("get/{username}")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetTermAndCondition(string username)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            COCBaseResponseDto result = new COCBaseResponseDto() { isSuccess = false };

            try
            {

                var results = await _manager.GetTermAndCondition(username);

                if (results != null)
                {
                    result.isSuccess = true;
                    result.data = results;
                    response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    result.errorMessage = "Not found Term and Condition or this user already acknowledge or user notfound";
                    response.StatusCode = HttpStatusCode.BadRequest;
                }

                response.Content = new StringContent(JsonConvert.SerializeObject(result), System.Text.Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error("Get Term and Condition (API) : " + ex.Message);
            }

            return response;
        }

        [Route("acknowledge/{username}")]
        [HttpPost]
        public async Task<HttpResponseMessage> AcceptTermAndCondition(string username, [FromBody] ConsentAcceptRequest request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            COCBaseResponseDto result = new COCBaseResponseDto() { isSuccess = false };

            try
            {
                var results = await _manager.AcceptTermAndCondition(username, request);
                if (results != null)
                {
                    result.isSuccess = true;
                    result.data = results;
                    response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    result.errorMessage = "Can not accept term and condition or user notfound";
                    response.StatusCode = HttpStatusCode.BadRequest;
                }

                response.Content = new StringContent(JsonConvert.SerializeObject(result), System.Text.Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error("Accept Term and Condition (API) : " + ex.Message);
            }

            return response;
        }
        //// GET: UserCOC
        //public ActionResult Index()
        //{
        //    return View();
        //}
    }
}
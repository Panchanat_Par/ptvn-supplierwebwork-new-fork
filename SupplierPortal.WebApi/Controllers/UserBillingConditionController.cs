﻿using SupplierPortal.BusinessLogic.BillingCondition;
using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace SupplierPortal.WebApi.Controllers
{
    [RoutePrefix("api/user/billing")]
    public class UserBillingConditionController : ApiController
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IUserBillingConditionManager _manager;

        public UserBillingConditionController()
        {
            _manager = new UserBillingConditionManager();
        }

        [Route("check")]
        [HttpPost]
        public HttpResponseMessage CheckAcceeptBillingCondition([FromBody] string username)
        {
            HttpResponseMessage response = null;
            try
            {
                var results = _manager.CheckAcceeptBillingCondition(username);

                response = new HttpResponseMessage();
                response.StatusCode = HttpStatusCode.OK;

                string jsonResult = new JavaScriptSerializer().Serialize(results);
                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error("CheckAcceeptBillingCondition (API) : " + ex.Message);
            }

            return response;
        }

        [Route("update")]
        [HttpPost]
        public HttpResponseMessage UpdateBillingCondition([FromBody] string userGUID)
        {
            HttpResponseMessage response = null;
            try
            {
                var results = _manager.UpdateBillingCondition(userGUID);
                response = new HttpResponseMessage();

                response.StatusCode = HttpStatusCode.OK;


                string jsonResult = new JavaScriptSerializer().Serialize(results);
                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error("UpdateBillingCondition (API) : " + ex.Message);
            }

            return response;
        }
    }
}
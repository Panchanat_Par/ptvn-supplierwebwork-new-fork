﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using SupplierPortal.Data.Models.MappingTable;
using System.Web.Configuration;
using System.Threading.Tasks;
using SupplierPortal.Data.CustomModels.Consent;
using Newtonsoft.Json;
using SupplierPortal.BusinessLogic.ExternalService;
using SupplierPortal.Models.Dto.SC;

namespace SupplierPortal.WebApi.Controllers
{
    [RoutePrefix("api/sc")]
    public class SCController : ApiController
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IExternalServiceManager _manager;
        public SCController()
        {
            _manager = new ExternalServiceManager();
        }

        [Route("verifyTicketCode")]
        [HttpGet]
        public async Task<HttpResponseMessage> VerifyTicketCode(string ticketCode)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            VerifyTicketCodeResponseDto result = new VerifyTicketCodeResponseDto();

            try
            {
                result = await _manager.VerifyTicketCodeSupplierConnect(ticketCode);
                if(!result.isError)
                {
                    response.Content = new StringContent(JsonConvert.SerializeObject(result), System.Text.Encoding.UTF8, "application/json");
                    response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            catch (Exception ex)
            {
                logger.Error("SCController (API) : " + ex.Message);
            }

            return response;
        }

        [Route("verifyEmail")]
        [HttpGet]
        public async Task<HttpResponseMessage> VerifyEmail(string email)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            VerifyTicketCodeResponseDto result = new VerifyTicketCodeResponseDto();

            try
            {
                result = await _manager.VerifyEmailSupplierConnect(email);
                if (!result.isError)
                {
                    response.Content = new StringContent(JsonConvert.SerializeObject(result), System.Text.Encoding.UTF8, "application/json");
                    response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            catch (Exception ex)
            {
                logger.Error("SCController (API) : " + ex.Message);
            }

            return response;
        }
    }
}
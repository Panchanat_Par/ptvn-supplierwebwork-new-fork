﻿using SupplierPortal.BusinessLogic.User;
using SupplierPortal.Data.CustomModels.User;
using SupplierPortal.Data.Models.SupportModel.HttpMassage;
using System;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace SupplierPortal.WebApi.Controllers
{
    [RoutePrefix("api/user")]
    public class UserController : ApiController
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IUserManager _manager;

        public UserController()
        {
            _manager = new UserManager();
        }

        [Route("updateactive")]
        [HttpPost]
        public HttpResponseMessage UpdateUserActiveInActive([FromBody] int userId)
        {
            HttpResponseMessage response = null;
            try
            {
                var results = _manager.UpdateUserActiveInActive(userId);

                response = new HttpResponseMessage();
                response.StatusCode = HttpStatusCode.OK;
                string jsonResult = new JavaScriptSerializer().Serialize(results);
                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error("UpdateUserActiveInActive (API) : " + ex.Message);
            }

            return response;
        }

        [Route("getinfo")]
        [HttpGet]
        public HttpResponseMessage GetUserInfo(string username)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                var results = _manager.GetUserInfo(username);
                if (results.ContactID == 0)
                {
                    response.StatusCode = HttpStatusCode.NotFound;
                    return response;
                }

                response.StatusCode = HttpStatusCode.OK;
                string jsonResult = new JavaScriptSerializer().Serialize(results);
                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error("GetUserInfo (API) : " + ex.Message);
                response.StatusCode = HttpStatusCode.InternalServerError;
                return response;
            }

            return response;
        }

        [Route("epb/getinfo")]
        [HttpGet]
        public HttpResponseMessage GetUserInfoFromProcurementBoard(string username)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            string jsonResult = String.Empty;
            try
            {
                var results = _manager.GetUserInfoFromProcurementBoard(username);
                if (results != null)
                {
                    response.StatusCode = HttpStatusCode.OK;

                    HttpMessageResponse messageResponse = new HttpMessageResponse();
                    messageResponse.data = results;

                    jsonResult = new JavaScriptSerializer().Serialize(messageResponse);
                    response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");

                    return response;
                }

                response.StatusCode = HttpStatusCode.BadRequest;

                HttpErrorResponse errorResponse = new HttpErrorResponse();
                errorResponse.timestamp = DateTime.Now;
                errorResponse.status = 400;
                errorResponse.error = "";
                errorResponse.message = "User not found";

                jsonResult = new JavaScriptSerializer().Serialize(errorResponse);
                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error("GetUserInfoFromProcurementBoard (API) : " + ex.Message);
                response.StatusCode = HttpStatusCode.InternalServerError;
                return response;
            }

            return response;
        }

        [Route("uaa/pwd")]
        [HttpPost]
        public HttpResponseMessage GetUserForUAAByUsername([FromBody] UserRequest request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                var results = _manager.GetUserForUAAByUsername(request);
                if (results == null)
                {
                    response.StatusCode = HttpStatusCode.NotFound;
                    return response;
                }

                response.StatusCode = HttpStatusCode.OK;
                string jsonResult = new JavaScriptSerializer().Serialize(results);
                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error("GetUserInfo (API) : " + ex.Message);
                response.StatusCode = HttpStatusCode.InternalServerError;
                return response;
            }

            return response;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SupplierPortal.WebApi.Models
{
    public class AttachmentDeleteModels
    {

        public int AttachmentID { get; set; }
        public int SupplierID { get; set; }
    }
}
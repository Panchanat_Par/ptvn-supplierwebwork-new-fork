﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace SupplierPortal.WebApi.Models.AccountPortal
{
    public class ResetPasswordModel
    {

        //public string Username { get; set; }
        [Required]
        public string Token { get; set; }
        [Required]
        public string Password { get; set; }

        //public string ConfirmPassword { get; set; }

    }
}
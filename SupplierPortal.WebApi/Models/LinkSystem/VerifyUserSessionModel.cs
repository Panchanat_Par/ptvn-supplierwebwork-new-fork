﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace SupplierPortal.WebApi.Models.LinkSystem
{
    public class VerifyUserSessionModel
    {

        [Required]
        public string UserGUID { get; set; }
        [Required]
        public int SystemID { get; set; }
        [Required]
        public int SystemGrpID { get; set; }
        [Required]
        public int RequireAllUserMapping { get; set; }
        [Required]
        public int PageID { get; set; }

        public string AppType { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using static SupplierPortal.WindowService.NotificationModel;
using static SupplierPortal.WindowService.WriteLog;

namespace SupplierPortal.WindowService
{
    public partial class SupplierPortalNotification : APIConnectionService, IDisposable
    {
        WriteLog writeLog;
        public SupplierPortalNotification()
        {
            writeLog = new WriteLog();
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {

            }
            // Free your own state (unmanaged objects).
            // Set large fields to null.
        }

        ~SupplierPortalNotification()
        {
            // Simply call Dispose(false).
            Dispose(false);
        }

        public async Task<SendEmailStatus> SendMailWelcome()
        {
            SendEmailStatus result = null;
            HttpResponseMessage httpResponse = null;
            try
            {

                string url = "api/buyer/email/invite/resetpassword";
                httpResponse = await this.GetApiConnection(url);

                if (httpResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    result = new SendEmailStatus();
                    Stream responseString = httpResponse.Content.ReadAsStreamAsync().Result;
                    StreamReader r = new StreamReader(responseString);
                    string jsonResult = r.ReadToEnd();
                    JavaScriptSerializer JsonConvert = new JavaScriptSerializer();
                    //writeLog.WriteToFile("SendMailWelcome(Success) " + DateTime.Now + " " + jsonResult);
                    result = JsonConvert.Deserialize<SendEmailStatus>(jsonResult);
                }
            }
            catch (Exception ex)
            {
                writeLog.WriteToFile("SendMailWelcome(Error) " + DateTime.Now + " " + ex.Message);
            }
            finally
            {
                if (httpResponse != null)
                    httpResponse.Dispose();
            }

            return result;
        }

        public async Task<bool> CheckStatusSendMailWelcome()
        {
            bool result = false;
            HttpResponseMessage httpResponse = null;
            SendEmailStatus status = null;
            try
            {
                writeLog.WriteToFile("CheckStatusSendMailWelcome(Start) " + DateTime.Now);
                status = new SendEmailStatus();
                status = await SendMailWelcome();

                if (status.Success)
                    result = await this.CheckStatusSendMailWelcome();
            }
            catch (Exception ex)
            {
                writeLog.WriteToFile("CheckStatusSendMailWelcome(Error) " + DateTime.Now + " " + ex.Message);
                throw;
            }
            finally
            {
                if (httpResponse != null)
                    httpResponse.Dispose();

                status = null;
                writeLog.WriteToFile("CheckStatusSendMailWelcome(Success) " + DateTime.Now);
            }

            return result;
        }
    }
}

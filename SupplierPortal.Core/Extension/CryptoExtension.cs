﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace SupplierPortal.Core.Extension
{
    public static class CryptoExtension
    {

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

        public static string Signature(string PublicKey, string SecretKey)
        {
            string publicKey = PublicKey;
            string secretKey = SecretKey;

            if (String.IsNullOrWhiteSpace(secretKey) || String.IsNullOrWhiteSpace(publicKey))
                return "";

            string signature;
            var secretBytes = Encoding.UTF8.GetBytes(secretKey);
            var valueBytes = Encoding.UTF8.GetBytes(publicKey);

            using (var hmac = new HMACSHA256(secretBytes))
            {
                var hash = hmac.ComputeHash(valueBytes);
                signature = Convert.ToBase64String(hash);
            }
            return signature;
        }
        //TRS 19-11-2014 class used for generate char to crate ticket code
        public static string GetRandomChar(this string digits, string _charspattern)
        {
            try
            {
                var _digits = Convert.ToInt32(digits);
                var random = new Random();
                var result = new string(
                    Enumerable.Repeat(_charspattern, _digits)
                              .Select(s => s[random.Next(s.Length)])
                              .ToArray());
                return result;
            }
            catch (Exception ex)
            {
                return "Error! digits is not number.";
            }
        }
        //-----------------------------------------End-----------------------------------------------
        //TRS 19-11-2014 class used for check valid DUNS number
        public static bool IsdunsNumberValid(this string dunsNumber)
        {
            if (dunsNumber.Any(c => !Char.IsDigit(c)))
            {
                return false;
            }

            int checksum = dunsNumber
               .Select((c, i) => (c - '0') << ((dunsNumber.Length - i - 1) & 1))
               .Sum(n => n > 9 ? n - 9 : n);

            return (checksum % 10) == 0 && checksum > 0;
        }
        //--------------------------------End------------------------------------


        public static string EncodeRouteData(string plainText)
        {
            string publicKey = "MTQ3R12345ILOVEYOU";
            string result = "";

            result = Base64Encode(plainText);
            result = result + publicKey;
            result = Base64Encode(result);

            return result;
        }

        public static string DecodeRouteData(string plainText)
        {
            string publicKey = "MTQ3R12345ILOVEYOU";

            plainText = Base64Decode(plainText);
            string plainTextDecode = plainText.Trim().Replace(publicKey, "");
            plainTextDecode = Base64Decode(plainTextDecode);

            return plainTextDecode;
        }

    }
}
